<tfoot>
    <tr>
        <td colspan="2">
            {!! Form::select('perform', [
                'export' => 'Export Posts',
                'set_visible' => 'Set Visible',
                'set_invisible' => 'Set Invisible',
                'toggle_visibility' => 'Toggle Visibility',
                'delete' => 'Delete Posts'
            ], 'send_text', ['class'=>'form-control pull-left perform-selector']) !!}
        </td>
        <td>
            {!! Form::submit('Submit', ['class' => 'btn btn-md pull-left btn-primary']) !!}
        </td>
    </tr>
</tfoot>