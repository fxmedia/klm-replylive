@extends('backend.layout.app')

@section('header')
    <h1>Reply.live Quizzes</h1>
    <ol class="breadcrumb">
        <li><a href="{{ URL::route('backend.index') }}">Backend</a></li>
        <li><a href="{{ URL::route('backend.quizzes.index') }}">Quizzes</a></li>
        <li>{{ $quiz->name }}</li>
    </ol>
@endsection

@section('content')
    <div class="col-xs-12">
        @include('backend.quizzes._partials.quiz-form')
    </div>
@endsection

@section('scripts')
    <script type="text/javascript" charset="UTF-8" src="{{ asset('js/backend/tables.js') }}"></script>
@endsection