@extends('backend.layout.app')

@section('header')
    <h1>Reply.live Quizzes</h1>
    <ol class="breadcrumb">
        <li><a href="{{ URL::route('backend.index') }}">Backend</a></li>
        <li>Quizzes</li>
    </ol>
@endsection

@section('content')
    <div class="col-xs-3">
        @include('backend.quizzes._partials.aside')
    </div>

    <div class="col-xs-9">
        @include('backend._partials.errors')

        @foreach($quizzes as $quiz)
            <ul class="list-group panel-default">
                <li class="list-group-item panel-heading">
                    {{ $quiz->name }}
                </li>
                <li class="list-group-item list-grou-heading" data-quiz-id="{{ $quiz->id }}">
                    @if($quiz->isActive)
                        <a class="toggle-quiz-start-stop btn btn-danger btn-xs" data-question-index="{{ $quiz->activeData['question_index'] }}" @if( $quiz->activeData['locked'] ) data-check-answer="{{ $quiz->activeData['locked'] }}" @endif><i class="fa fa-stop"></i> <span>Stop Quiz</span></a>
                    @else
                        <a class="toggle-quiz-start-stop btn btn-primary btn-xs"><i class="fa fa-play"></i> <span>Start Quiz</span></a>
                    @endif

                    <span>Groups: {{ App\Group::printGroups( $quiz->groups ) }}</span>

                    {!! Form::open(['method' => 'post', 'route' => ['backend.quizzes.remove', $quiz->id], 'class' => 'pull-right', 'onsubmit' => 'return confirm("Please confirm to remove the selected quiz?")']) !!}
                    <button class="btn btn-xs btn-danger" title="Delete"><span class="glyphicon glyphicon-trash"></span></button>
                    {!! Form::close() !!}

                    {!! Form::open(['method' => 'post', 'route' => ['backend.quizzes.reset.single', $quiz->id], 'class' => 'pull-right', 'onsubmit' => 'return confirm("Are you sure you want to reset the score?")']) !!}
                    <button class="btn btn-xs btn-info" title="Reset score"><span class="glyphicon glyphicon-retweet"></span></button>
                    {!! Form::close() !!}

                    <a href="{{ route('backend.quizzes.edit', [$quiz->id]) }}" class="btn btn-success btn-xs pull-right" title="Edit"><span class="glyphicon glyphicon-edit"></span></a>
                </li>

                @foreach($quiz->questions as $key => $question)
                    <li class="list-group-item">
                        <span class="badge pull-left">{{ $question->order }}</span>
                        {{--<span class="pull-right">--}}
                            {{--<a class="toggle-question-start-stop btn btn-primary btn-xs" data-quiz-id="{{ $quiz->id }}" data-index="{{ $key }}"><i class="fa fa-play"></i> <span>Start Question</span></a>--}}
                        {{--</span>--}}
                        &nbsp;&nbsp;&nbsp;{{ str_limit($question->title, 60) }}
                    </li>
                @endforeach
            </ul>
        @endforeach
        @if(count($quizzes) === 0)
            <h3>No Quizzes in the database.</h3>
            <p>Click on "Add Quiz" in the menu on the left to create a new quiz</p>
        @endif
    </div>

    <div class="modal fade" id="quizModal" data-backdrop="static" data-keyboard="false">
        <div class="modal-dialog">
            <div class="modal-content">
                <div class="modal-header">
                    <button type="button" class="close" aria-label="Close"><span aria-hidden="true">&times;</span></button>
                    <h4 class="modal-title">Quiz: <span class="quiz-name"></span></h4>
                </div>
                <div class="modal-body">

                    {{--<div class="question-navigation" id="questionNavigation"></div>--}}

                    <div class="question-counter">Question <span class="question-number">1</span> of <span class="question-numbers">1</span></div>

                    <div class="question"><b>Question: <span class="question-name"></span></b></div>
                    {{--<a class="quiz-start btn btn-primary">Start quiz!</a>--}}
                    <span class="question-answers"></span>
                </br/>
                    <b>Correct answer: <span class="question-answer"></span></b><br/><br/>
                    
                </br/>
                    <div class="modal-actions">
                        <span class="btn btn-primary check-answer">Check answer</span>
                        <span class="btn btn-default next" disabled="disabled">Next question</span>
                        {{--<span class="btn btn-info prev-result hide">Previous results</span>--}}
                        {{--<span class="btn btn-default page-number hide">1</span>--}}
                        {{--<span class="btn btn-info next-result hide">Next results</span>--}}
                        <span class="btn btn-success results">Toggle results</span>
                        <span class="btn btn-warning end hide">Stop the quiz</span>
                    </div>
                </div>
                <div class="modal-footer">
                    {{--<span class="btn btn-success score hide pull-right">Show score</span>--}}
                </div>
            </div>
        </div>
    </div>


@endsection

@section('scripts')
    <script>
        var api_routes = {
            other: {
                checkAnswer: '{{ route('api.quiz.check-answer') }}',
                stop: '{{ route('api.quiz.stop') }}',
                toggleResults: '{{ route('api.quiz.toggle-results')}}'
            }
        };
        @foreach($quizzes as $quiz)
            api_routes['{{ $quiz->id }}'] = {
                start: '{{ route('api.quiz.start', [$quiz->id]) }}',
                showQuestion: '{{ route('api.quiz.show-question', [$quiz->id]) }}',
                getData: '{{ route('api.quiz.get-data', [$quiz->id]) }}'
            };
        @endforeach
    </script>
    <script type="text/javascript" charset="UTF-8" src="{{ asset('js/backend/quizzes.js') }}"></script>
@endsection
