@extends('backend.layout.app')

@section('header')

    <h1>Reply.live Edit Open Question</h1>
    <ol class="breadcrumb">
        <li><a href="{{ URL::route('backend.index') }}">Backend</a></li>
        <li><a href="{{ URL::route('backend.open_questions.index') }}">Open Questions</a></li>
        <li>Editing Open Question {{ $open_question->id }}</li>
    </ol>
@endsection

@section('content')
    <div class="col-xs-12">
        @include('backend.open_questions._partials.open_question-form')
    </div>
@endsection

@section('scripts')
    <script type="text/javascript" charset="UTF-8" src="{{ asset('js/backend/forms.js')  }}"></script>
@endsection
