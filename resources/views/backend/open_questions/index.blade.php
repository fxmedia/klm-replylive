@extends('backend.layout.app')

@section('header')
    <h1>Reply.live Open Questions</h1>
    <ol class="breadcrumb">
        <li><a href="{{ URL::route('backend.index') }}">Backend</a></li>
        <li>Open Questions</li>
    </ol>
@endsection

@section('content')
    <div class="col-xs-3">
        @include('backend.open_questions._partials.aside')
    </div>

    <div class="col-xs-9">
        @include('backend._partials.errors')

        @foreach($open_questions as $open_question)
            <ul class="list-group  panel-default">
                <li class="list-group-item panel-heading">
                    {{ $open_question->question }}
                </li>
                <li class="list-group-item list-group-heading" data-open_question-id="{{ $open_question->id }}">

                    {!! Form::open(['method' => 'post', 'route' => ['backend.open_questions.remove', $open_question->id], 'class' => 'pull-right', 'onsubmit' => 'return confirm("Please confirm to remove the selected open_question?")']) !!}
                    <button class="btn btn-xs btn-danger"><span class="glyphicon glyphicon-trash"></span></button>
                    {!! Form::close() !!}

                    {!! Form::open(['method' => 'post', 'route' => ['backend.open_questions.reset.single', $open_question->id], 'class' => 'pull-right', 'onsubmit' => 'return confirm("Are you sure you want to reset the score?")']) !!}
                    <button class="btn btn-xs btn-info"><span class="glyphicon glyphicon-retweet"></span></button>
                    {!! Form::close() !!}

                    <a href="{{ route('backend.open_questions.edit', [$open_question->id]) }}" class="btn btn-success btn-xs pull-right"><span class="glyphicon glyphicon-edit"></span></a>

                    <span class="pull-right">
                        <a href="{{ route('backend.open_questions.answers', $open_question->id) }}">Total Answers:
                            <span class="answer-open_question-{{ $open_question->id }}">
                                {{ count($open_question->answers) }}
                            </span>
                        </a>
                            &nbsp;&nbsp;&nbsp;
                    </span>

                    <a class="toggle-open_question-start-stop btn @if($open_question->isActive) btn-danger @else btn-primary @endif btn-xs"><i class="fa @if($open_question->isActive) fa-stop @else fa-play @endif"></i> <span>@if($open_question->isActive) Stop Open Question @else Start Open Question @endif</span></a>
                    <span>Groups: {{ App\Group::printGroups( $open_question->groups ) }}</span>
                </li>
            </ul>
        @endforeach
    </div>
@endsection

@section('scripts')
    <script>
        var api_routes = {
            'other': {
                answerCount: '{{ URL::route('api.open_question.status') }}'
            }
        };
        @foreach($open_questions as $open_question)
            api_routes['{{ $open_question->id }}'] = {
                start: '{{ URL::route('api.open_question.start', [$open_question->id]) }}',
                stop: '{{ URL::route('api.open_question.stop') }}'
            };
        @endforeach
    </script>
    <script type="text/javascript" charset="UTF-8" src="{{ asset('js/backend/open_questions.js') }}"></script>
@endsection
