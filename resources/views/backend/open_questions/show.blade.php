@extends('backend.layout.app')

@section('header')
    <h1>Reply.live Polls</h1>
    <ol class="breadcrumb">
        <li><a href="{{ URL::route('backend.index') }}">Backend</a></li>
        <li><a href="{{ URL::route('backend.polls.index') }}">Polls</a></li>
        <li>{{ $poll->question }}</li>
    </ol>
@endsection

@section('content')
    <div class="col-xs-3">
        @include('backend.polls._partials.aside-single')
    </div>

    <div class="col-xs-9">
        @include('backend._partials.errors')
    </div>
@endsection

@section('scripts')
    {{--<script type="text/javascript" charset="UTF-8" src="{{ asset('js/backend/tables.js') }}"></script>--}}
@endsection