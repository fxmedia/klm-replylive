@if( isset($open_question) )
    {!! Form::model( $open_question, ['class'=>'form-horizontal user-admin', 'method'=>'patch', 'route' => ['backend.open_questions.update', $open_question->id]] ) !!}
@else
    {!! Form::open(['class'=>'form-horizontal user-admin', 'route' => 'backend.open_questions.store']) !!}
@endif
<div class="form-group {{ $errors->has('question') ? 'has-error' : '' }}">
    {!! Form::label('question','', ['class' => 'col-sm-3 control-label']) !!}
    <div class="col-sm-9">
        {!! Form::text('question', Input::old('question'),['class'=>'form-control']) !!}
    </div>
    <span class="help-block col-sm-12 col-md-offset-3">{!! $errors->first('question') !!}</span>
</div>

<div class="form-group {{ $errors->has('multiple_answers') ? 'has-error' : '' }}">
    {!! Form::label('multiple_answers','', ['class' => 'col-sm-3 control-label']) !!}
    <div class="col-sm-9">
        <div class="form-switch">
            <input class="on" id="multipleAnswersOn" type="radio" name="multiple_answers" value="1" @if( isset($open_question) && $open_question->multiple_answers == true) checked @endif>
            <input class="off" id="multipleAnswersOff" type="radio" name="multiple_answers" value="0" @if( !isset($open_question) || $open_question->multiple_answers != true) checked @endif>
            <div class="switch">
                <label class="on" for="multipleAnswersOff">On</label>
                <label class="off" for="multipleAnswersOn">Off</label>
            </div>
        </div>
        <span class="help-block col-sm-12 col-md-offset-3">{!! $errors->first('multiple_answers') !!}</span>
    </div>
</div>

<div class="form-group {{ $errors->has('tags') ? 'has-error' : '' }}">
    {!! Form::label('tags','', ['class' => 'col-sm-3 control-label']) !!}
    <div class="col-sm-9">
        {!! Form::text('tags', Input::old('tags'),['class'=>'form-control']) !!}
    </div>
    <span class="help-block col-sm-12 col-md-offset-3">{!! $errors->first('tags') !!}</span>
</div>

<div class="form-group">
    {!! Form::label('Groups','', ['class' => 'col-sm-3 control-label']) !!}
    <div class="col-sm-9">
        @forelse ($groups as $group )
            <label>
                @if( isset($open_question) )
                    {!! Form::checkbox('groups[' . $group->id . ']', $group->id, $open_question->isInGroup($group) ) !!}

                @else
                    {!! Form::checkbox('groups[' . $group->id . ']', $group->id ) !!}
                @endif
                {{ $group->name }}
            </label><br>
        @empty
            No user groups
        @endforelse
    </div>
</div>

<div class="form-group">
    <div class="col-sm-9 col-sm-offset-3">
        {!! Form::submit('Save', ['class'=>'btn btn-primary']) !!}
    </div>
</div>

{!! Form::close() !!}
