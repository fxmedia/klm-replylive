
<div class="">
    <div class="col-xs-12 setup-header">
        <h2>Who would you like to send {{ isset($email) ? $email->name : $sms->name }} to?</h2>
        <p>Filters will work in the following order:</p>
        <ul>
            <li>First users who pass the green filters will be added</li>
            <li>Then users who pass the red filters will be removed</li>
            <li>Individual user filters will overwrite the status of the group filters</li>
            <li>If you click on a remove button in the send-to list, it will not change any filters, but will remove the row you want.<br>N.B.: if you click on a filter after this, this action will be undone.</li>
        </ul>
    </div>
</div>
<div class="row setup">
    <div class="col-xs-2">
        <h3>Filters</h3>
        <div class="reset-filter-wrap">
            <a class="reset-filter" data-type="groups" href="#">Reset filters</a>
        </div>
        <div class="panel panel-default">
            <div class="panel-heading">General</div>
            <ul class="list-group">
                <li class="list-group-item hide-filter" data-state="0">Hide users without {{ (isset($email)) ? 'email' : 'phonenumber'  }}</li>
            </ul>
            <div class="panel-heading">Users</div>
            <ul class="list-group">
                <li class="list-group-item filter" data-state="0" data-filter="user_all">All users</li>
                <li class="list-group-item filter" data-state="0" data-filter="user_test">Test users</li>
                <li class="list-group-item filter" data-state="0" data-filter="user_no-test">Not test users</li>
            </ul>
            <div class="panel-heading">Groups</div>
            <ul class="list-group">
                @foreach($groups as $group)
                    <li class="list-group-item filter" data-state="0" data-filter="group_{{ $group->id }}">{{ $group->name }}</li>
                @endforeach
            </ul>
            <div class="panel-heading">Attending Status</div>
            <ul class="list-group">
                <li class="list-group-item filter" data-state="0" data-filter="status_attending">Attending</li>
                <li class="list-group-item filter" data-state="0" data-filter="status_waiting">Waiting</li>
                <li class="list-group-item filter" data-state="0" data-filter="status_pending">Pending</li>
                <li class="list-group-item filter" data-state="0" data-filter="status_cancelled">Cancelled</li>
                <li class="list-group-item filter" data-state="0" data-filter="status_no-status">No Status</li>
            </ul>

        </div>
    </div>
    <div class="col-xs-10">
        @include('backend._partials.errors')
    </div>
    <div class="col-xs-5">
        <h3>Filter individual users</h3>
        <div class="reset-filter-wrap">
            <a class="reset-filter" data-type="user" href="#">Reset individual filters</a>
        </div>
        <table id="listTableBody" class="setup-table table table-hover list-of-users_table">
            <tbody>
            @foreach($users as $user)
                <?php
                    $className = 'filter-user user_all';
                    foreach($groups as $group){
                        if($user->isInGroup($group)) $className.= ' group_'.$group->id;
                    }
                    if($user->test_user) $className.= ' user_test';
                    else $className.= ' user_no-test';
                    if($user->attending) $className.= ' status_'.$user->attending->status;
                    else $className.= ' status_no-status';

                    if((isset($email) && !$user->email) || (isset($sms) && !$user->phonenumber)) $className.= ' no-content';
                ?>
                <tr data-state="0"
                    data-id="{{ $user->id }}"
                    class="{{ $className }}">
                    <td>{{ $user->name }}</td>
                    @if(isset($email))
                        <td>({{ $user->email }})</td>
                    @else
                        <td>({{ $user->phonenumber }})</td>
                    @endif
                </tr>
            @endforeach
            </tbody>
        </table>
    </div>

    <div class="col-xs-5 setup-to-send-wrapper">
        <h3 class="text-center">You will send to:</h3>
        <div class="send-to-list">
            <table class="setup-table table table-hover send-to-list__table">
                <tbody id="sendTableBody"></tbody>
            </table>
        </div>
    </div>
</div>