@extends('backend.layout.app')

@section('header')
    <h1>Reply.live Learning Curves</h1>
    <ol class="breadcrumb">
        <li><a href="{{ URL::route('backend.index') }}">Backend</a></li>
        <li><a href="{{ URL::route('backend.learning_curves.index') }}">Learning Curves</a></li>
        <li>{{ $learning_curve->name }}</li>
    </ol>
@endsection

@section('content')
    <div class="col-xs-12">
        @include('backend.learning_curves._partials.learning_curve-form')
    </div>
@endsection

@section('scripts')
    <script type="text/javascript" charset="UTF-8" src="{{ asset('js/backend/tables.js') }}"></script>
@endsection
