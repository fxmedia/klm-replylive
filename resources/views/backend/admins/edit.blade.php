@extends('backend.layout.app')

@section('header')
    <h1>Reply.live Edit Admin</h1>
    <ol class="breadcrumb">
        <li><a href="{{ URL::route('backend.index') }}">Backend</a></li>
        <li><a href="{{ URL::route('backend.admins.index') }}">Admins</a></li>
        <li>Edit</li>
    </ol>
@endsection

@section('content')
    <div class="row">
        <div class="col-md8 col-md-offset-2">
            <div class="panel panel-default">
                <div class="panel-heading">Edit admin information</div>
                <div class="panel-body">
                    @include('backend._partials.errors')

                    {!! Form::open(['class' => 'form-horizontal', 'method' => 'patch', 'route' => ['backend.admins.update', $admin->id ]]) !!}

                    <div class="form-group">
                        <label for="name" class="col-md-4 control-label">Name</label>

                        <div class="col-md-6">
                            <input id="name" type="text" class="form-control" name="name" value="{{ $admin->name }}" required autofocus>
                        </div>
                    </div>

                    <div class="form-group">
                        <label for="email" class="col-md-4 control-label">E-Mail Address</label>

                        <div class="col-md-6">
                            <input id="email" type="email" class="form-control" name="email" value="{{ $admin->email }}" required>
                        </div>
                    </div>

                    <div class="form-group">
                        <label class="col-md-4 control-label">Subscribe to Notifications</label>

                        <div class="col-md-6">
                            <div class="form-switch">
                                <input class="on" id="notification_subscriptionOn" type="radio" name="notification_subscription" value="1" @if($admin->notification_subscription) checked @endif>
                                <input class="off" id="notification_subscriptionOff" type="radio" name="notification_subscription" value="0" @if(!$admin->notification_subscription) checked @endif>
                                <div class="switch">
                                    <label class="on" for="notification_subscriptionOff">On</label>
                                    <label class="off" for="notification_subscriptionOn">Off</label>
                                </div>
                            </div>
                        </div>
                    </div>

                    @if($is_me)

                        <div class="form-group">
                            <label for="old_password" class="col-md-4 control-label">Old Password</label>

                            <div class="col-md-6">
                                <input id="old_password" type="password" class="form-control" name="old_password" required>

                            </div>
                        </div>

                        <div class="form-group">
                            <label for="password" class="col-md-4 control-label">Password</label>

                            <div class="col-md-6">
                                <input id="password" type="password" class="form-control" name="password" required>

                            </div>
                        </div>

                        <div class="form-group">
                            <label for="password-confirm" class="col-md-4 control-label">Confirm Password</label>

                            <div class="col-md-6">
                                <input id="password-confirm" type="password" class="form-control" name="password_confirmation" required>
                            </div>
                        </div>

                    @endif

                    <div class="form-group">
                        <div class="col-md-6 col-md-offset-4">
                            <button type="submit" class="btn btn-primary">
                                Save
                            </button>
                        </div>
                    </div>

                    {!! Form::close() !!}

                </div>
            </div>
        </div>
    </div>
@endsection