{!! Form::open(['route'=>['backend.users.perform'], 'id'=>'usersTable', 'method' => 'POST']) !!}
<table class="table table-hover">
    <thead>
    @include('backend._partials.table-top-actions')
    <tr>
        <th>&nbsp;</th>
        <th>Status</th>
        <th>Name</th>
        <th>Token</th>
        <th>Contact</th>
        <th>Groups</th>
    </tr>
    </thead>
    <tbody>
    @foreach($users as $user)
        <tr id="user_{{$user->id}}">
            <td>{!! Form::checkbox('user[]', $user->id, false, ['class' => 'selectable ', 'id' => 'checkbox'.$user->id]) !!}</td>

             <td class="trigger-more">
                @switch( $user->attending['status'] )
                    @case('attending')
                        <span class="badge badge-success">Attending</span>
                        @break
                    @case('pending')
                        <span class="badge badge-danger">Pending</span>
                        @break
                    @case('waiting')
                        <span class="badge badge-warning">Waiting</span>
                        @break
                    @case('invited')
                        <span class="badge badge-info">Invited</span>
                        @break
                    @case('cancelled')
                        <span class="badge">Cancelled</span>
                        @break
                    @case('declined')
                        <span class="badge badge-purple">Declined</span>
                        @break
                @endswitch
            </td>

            <td class="trigger-more">{{ $user->name }}</td>
            <td class="trigger-more">{{ $user->token }}</td>
            <td class="trigger-more">
                <strong>E:</strong> - {{ $user->email }}<br/>
                <strong>P:</strong> - {{ $user->phonenumber }}
            </td>
            <td class="trigger-more">
                @foreach ( $user->groups as $group )
                    {{ $group->name }}<br/>
                @endforeach
            </td>
        </tr>
        <tr class="more">
            <td>&nbsp;</td>
            <td colspan="2">
                @if( $user->attending['status'] == 'pending' )
                    <a class="btn btn-xs btn-success form-action" href="#"
                       data-action="set_attending"
                       data-username="{{ $user->name }}"
                       data-id="{{ $user->id }}"
                       data-form="usersTable">
                        <span class="glyphicon glyphicon-check"></span>
                        Approve user
                    </a>
                    <a class="btn btn-xs btn-danger form-action" href="#"
                       data-action="set_declined"
                       data-username="{{ $user->name }}"
                       data-id="{{ $user->id }}"
                       data-form="usersTable">
                        <span class="glyphicon glyphicon-trash"></span>
                        Decline user
                    </a>
                @endif
            </td>
            <td colspan="3" class="actions">
                @if($eventDomain)<a class="btn btn-xs btn-info" href="{{ $eventDomain }}?token={{ $user->token }}" target="_blank"><span class="glyphicon glyphicon-link"></span>Open URL</a>@endif
                <a class="btn btn-xs btn-success" href="{{ route('backend.users.edit', [$user->id]) }}"><span class="glyphicon glyphicon-edit"></span>Edit</a>

                <a class="btn btn-xs btn-danger form-action" href="#"
                   data-action="delete"
                   data-username="{{ $user->name }}"
                   data-id="{{ $user->id }}"
                   data-form="usersTable">
                    <span class="glyphicon glyphicon-trash"></span>
                    Delete
                </a>
            </td>
        </tr>
    @endforeach
    </tbody>
    @include('backend.users._partials.table-bottom-actions')
</table>
{!! Form::close() !!}
