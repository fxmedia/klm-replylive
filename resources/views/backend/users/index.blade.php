@extends('backend.layout.app')

@section('header')

    <h1>Reply.live Users</h1>
    <ol class="breadcrumb">
        <li><a href="{{ URL::route('backend.index') }}">Backend</a></li>
        <li>Users</li>
    </ol>
@endsection

@section('content')
    <div class="col-xs-12 col-sm-3">
        @include('backend.users._partials.aside')
    </div>

    <div class="col-xs-12 col-sm-9">
        <nav class="navbar navbar-default" role="navigation">
            <div navbar-header>
                <a class="navbar-brand" href="#">Users</a>
            </div>


            {!! Form::open(['route'=>'backend.users.search', 'class'=>'navbar-form pull-right']) !!}
            <div class="form-group">
                {!! Form::text('search', Request::old('search'), ['placeholder'=>'Search', 'class'=>'form-control']) !!}
            </div>
            <button type="submit" class="btn btn-default"><span class="glyphicon glyphicon-search"></span></button>
            {!! Form::close() !!}
        </nav>

        @include('backend._partials.errors')

        @include('backend.users._partials.user-table')

    </div>
@endsection

@section('scripts')
    <script>
        (function(){
            var mainForm = document.getElementById('usersTable');

            if(mainForm)mainForm.addEventListener('submit', function(){

                var userRows = mainForm.querySelectorAll('[name="user[]"]:checked');
                var arr = [];
                if(userRows.length > 900){
                    for(var i=0; i<userRows.length; i++){
                        arr.push(userRows[i].value);
                        userRows[i].checked = false;

                    }
                    var hiddenVal = document.createElement('input');
                    hiddenVal.setAttribute('type','hidden');
                    hiddenVal.setAttribute('name', 'user_json');
                    hiddenVal.value = JSON.stringify(arr);

                    mainForm.appendChild(hiddenVal);

                } else {
                    // allow normal post
                }
            });
        })();
    </script>
    <script type="text/javascript" charset="UTF-8" src="{{ asset('js/backend/tables.js') }}"></script>
@endsection