@extends('backend.layout.app')

@section('header')

    <h1>Reply.live New User</h1>
    <ol class="breadcrumb">
        <li><a href="{{ URL::route('backend.index') }}">Backend</a></li>
        <li><a href="{{ URL::route('backend.users.index') }}">Users</a></li>
        <li>Create new user</li>
    </ol>
@endsection

@section('content')
    <div class="col-xs-12">
        @include('backend.users._partials.user-form')
    </div>
@endsection

@section('scripts')
    <script type="text/javascript" charset="UTF-8" src="{{ asset('js/backend/forms.js')  }}"></script>
@endsection