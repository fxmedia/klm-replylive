@extends( 'backend.layout.app' )

@section('header')
    <h1>Reply.live Timer</h1>
    <ol class="breadcrumb">
        <li><a href="{{ URL::route('backend.index') }}">Backend</a></li>
        <li>Timer</li>
    </ol>
@endsection

@section( 'content' )

<div class="timer">
    <h1>Display Timer</h1>
    <form action="{{ URL::route( 'backend.timer.submit' )  }}" method="post">
        {{ csrf_field() }}
        <input type="number" name="total_seconds" placeholder="Timer in seconds" />
        <input type="text" name="audio_url" placeholder="URL to audio file" />
        <input type="submit" class="btn btn-success" value="Timer start" />
    </form>

    <form action="{{ URL::route( 'backend.timer.stop' )  }}" method="post">
        {{ csrf_field() }}
        <input type="submit" class="btn btn-danger" value="Stop timer" />
    </form>
</div>

@stop
