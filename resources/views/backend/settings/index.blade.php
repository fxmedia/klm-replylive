@extends('backend.layout.app')

@section('header')

    <h1>Reply.live Settings</h1>
    <ol class="breadcrumb">
        <li><a href="{{ URL::route('backend.index') }}">Backend</a></li>
        <li>Settings</li>
    </ol>
@endsection

@section('content')
    <div class="col-xs-3">
        @include('backend.settings._partials.aside')
    </div>

    <div class="col-xs-9">
        @include('backend._partials.errors')
        
        {!! Form::open(['class' => 'form-horizontal', 'method' => 'post', 'route' => 'backend.settings.update']) !!}

        <ul class="nav nav-tabs" role="tablist">
            @foreach($groups as $group)
                <li role="presentation" class="{{ ($group->order ==0)? ' active' : '' }}"><a aria-controls="setting-group{{ $group->order }}" role="tab" data-toggle="tab" href="#setting-group{{ $group->order }}">{{ $group->name }}</a></li>
            @endforeach
            <li role="presentation" class="pull-right" style="margin-right: 8px;">
                {{--<div class="col-sm-1" style="margin-right: 20px;">--}}
                    {{--<button id="updateDefaults" class="btn btn-warning"><i class="glyphicon glyphicon-refresh"></i></button>--}}
                {{--</div>--}}
                <div class="col-sm-8">
                    {!! Form::submit('Save', ['class'=>'btn btn-primary']) !!}
                </div>
            </li>
        </ul>


        <div class="tab-content settings-content">
            @foreach($groups as $group)
                <div role="tabpanel" class="tab-pane{{ ($group->order ==0)? ' active' : '' }}" id="setting-group{{ $group->order }}">
                    @foreach($group->settings as $setting)
                        <div class="form-group">
                            {!! Form::label($setting->key,$setting->name, ['class' => 'col-sm-2 settings-label']) !!}
                            <div class="col-sm-1">
                                <a href="#" class="glyphicon glyphicon-info-sign info-icon" ><span>{{ $setting->description }}</span></a>
                            </div>
                            @if($setting->type === 'boolean')
                                <div class="col-sm-9">
                                    <div class="form-switch">
                                        <input class="on" id="{{ $setting->key }}On" type="radio" name="{{ $setting->key }}" value="1" @if($setting->value == '1') checked @endif>
                                        <input class="off" id="{{ $setting->key }}Off" type="radio" name="{{ $setting->key }}" value="0" @if(!$setting->value == '1') checked @endif>
                                        <div class="switch">
                                            <label class="on" for="{{ $setting->key }}Off">On</label>
                                            <label class="off" for="{{ $setting->key }}On">Off</label>
                                        </div>
                                    </div>
                                </div>
                            @else
                                <div class="col-sm-9">

                                    @if( strpos($setting->key, 'token') !== false )
                                        {!! Form::text($setting->key, $setting->value, ['placeholder' => $setting->placeholder, 'class' => 'form-control with-token']) !!}<!--
                                     --><div class="generate-token input-group-addon"><i class="glyphicon glyphicon-repeat"></i></div>
                                    @else
                                        {!! Form::text($setting->key, $setting->value, ['placeholder' => $setting->placeholder, 'class' => 'form-control']) !!}
                                    @endif
                                </div>
                            @endif
                        </div>
                    @endforeach
                </div>
            @endforeach
        </div>

        {!! Form::close() !!}
    </div>
@endsection


@section('scripts')
    @parent
    <script type="text/javascript" src="{{ asset( 'js/backend/bootstrap-switch.js' ) }}"></script>
    <script type="text/javascript" src="{{ asset('js/backend/walls.js') }}"></script>
@endsection
