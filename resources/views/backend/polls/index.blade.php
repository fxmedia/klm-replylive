@extends('backend.layout.app')

@section('header')
    <h1>Reply.live Polls</h1>
    <ol class="breadcrumb">
        <li><a href="{{ URL::route('backend.index') }}">Backend</a></li>
        <li>Polls</li>
    </ol>
@endsection

@section('content')
    <div class="col-xs-3">
        @include('backend.polls._partials.aside')
    </div>

    <div class="col-xs-9">
        @include('backend._partials.errors')

        @foreach($polls as $poll)
            <ul class="list-group  panel-default">
                <li class="list-group-item panel-heading">
                    {{ $poll->question }}
                </li>
                <li class="list-group-item list-group-heading" data-poll-id="{{ $poll->id }}">
                    <a class="toggle-poll-start-stop btn @if($poll->isActive) btn-danger @else btn-primary @endif btn-xs"><i class="fa @if($poll->isActive) fa-stop @else fa-play @endif"></i> <span>@if($poll->isActive) Stop Poll @else Start Poll @endif</span></a>

                    <span>Groups: {{ App\Group::printGroups( $poll->groups ) }}</span>

                    {!! Form::open(['method' => 'post', 'route' => ['backend.polls.remove', $poll->id], 'class' => 'pull-right', 'onsubmit' => 'return confirm("Please confirm to remove the selected poll?")']) !!}
                    <button class="btn btn-xs btn-danger"><span class="glyphicon glyphicon-trash"></span></button>
                    {!! Form::close() !!}

                    {!! Form::open(['method' => 'post', 'route' => ['backend.polls.reset.single', $poll->id], 'class' => 'pull-right', 'onsubmit' => 'return confirm("Are you sure you want to reset the score?")']) !!}
                    <button class="btn btn-xs btn-info"><span class="glyphicon glyphicon-retweet"></span></button>
                    {!! Form::close() !!}

                    <a href="{{ route('backend.polls.edit', [$poll->id]) }}" class="btn btn-success btn-xs pull-right"><span class="glyphicon glyphicon-edit"></span></a>

                    <span class="pull-right">Total Answers: <span class="answer-poll-{{ $poll->id }}">{{ count($poll->answers) }}</span>&nbsp;&nbsp;&nbsp;</span>
                </li>
                @foreach($poll->choices as $choice)
                    <li class="list-group-item">
                        <span class="badge pull-left">{{ $choice['choice'] }}</span>
                        &nbsp;&nbsp;&nbsp;{{ str_limit($choice['value'], 60) }}
                        <span class="pull-right">
                            Answers: <span class="answer-poll-{{ $poll->id }}-{{ $choice['choice'] }}">{{ $poll->result[ $choice['choice'] ] ?? 0}} </span>
                        </span>
                    </li>
                @endforeach
            </ul>
        @endforeach
        @if(count($polls) === 0)
            <h3>No Polls in the database.</h3>
            <p>Click on "Add Poll" in the menu on the left to create a new poll</p>
        @endif
    </div>
@endsection

@section('scripts')
    <script>
        var api_routes = {
            'other': {
                answerCount: '{{ URL::route('api.poll.status') }}'
            }
        };
        @foreach($polls as $poll)
            api_routes['{{ $poll->id }}'] = {
                start: '{{ URL::route('api.poll.start', [$poll->id]) }}',
                stop: '{{ URL::route('api.poll.stop') }}'
            };
        @endforeach
    </script>
    <script type="text/javascript" charset="UTF-8" src="{{ asset('js/backend/polls.js') }}"></script>
@endsection