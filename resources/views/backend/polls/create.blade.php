@extends('backend.layout.app')

@section('header')

    <h1>Reply.live New Poll</h1>
    <ol class="breadcrumb">
        <li><a href="{{ URL::route('backend.index') }}">Backend</a></li>
        <li><a href="{{ URL::route('backend.polls.index') }}">Polls</a></li>
        <li>Create new poll</li>
    </ol>
@endsection

@section('content')
    <div class="col-xs-12">
        @include('backend.polls._partials.poll-form')
    </div>
@endsection

@section('scripts')
    <script type="text/javascript" charset="UTF-8" src="{{ asset('js/backend/forms.js')  }}"></script>
@endsection