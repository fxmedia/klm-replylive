@if( isset($poll) )
    {!! Form::model( $poll, ['class'=>'form-horizontal user-admin', 'method'=>'patch', 'route' => ['backend.polls.update', $poll->id]] ) !!}
@else
    {!! Form::open(['class'=>'form-horizontal user-admin', 'route' => 'backend.polls.store']) !!}
@endif
<div class="form-group {{ $errors->has('question') ? 'has-error' : '' }}">
    {!! Form::label('question','', ['class' => 'col-sm-3 control-label']) !!}
    <div class="col-sm-9">
        {!! Form::text('question', Input::old('question'),['class'=>'form-control']) !!}
    </div>
    <span class="help-block col-sm-12 col-md-offset-3">{!! $errors->first('question') !!}</span>
</div>

<div class="form-group {{ $errors->has('tags') ? 'has-error' : '' }}">
    {!! Form::label('tags','', ['class' => 'col-sm-3 control-label']) !!}
    <div class="col-sm-9">
        {!! Form::text('tags', Input::old('tags'),['class'=>'form-control']) !!}
    </div>
    <span class="help-block col-sm-12 col-md-offset-3">{!! $errors->first('tags') !!}</span>
</div>

<div class="form-group">
    {!! Form::label('Groups','', ['class' => 'col-sm-3 control-label']) !!}
    <div class="col-sm-9">
        @forelse ($groups as $group )
            <label>
                @if( isset($poll) )
                    {!! Form::checkbox('groups[' . $group->id . ']', $group->id, $poll->isInGroup($group) ) !!}

                @else
                    {!! Form::checkbox('groups[' . $group->id . ']', $group->id ) !!}
                @endif
                {{ $group->name }}
            </label><br>
        @empty
            No user groups
        @endforelse
    </div>
</div>


<?php ?>
<div class="well form-group">
    {!! Form::label('choices','', ['class' => 'col-sm-3 control-label']) !!}
    <div class="col-sm-9">
        <?php $c = 0; $r = range('a', 'z') ?>
        @if(isset($poll) && is_array($poll->choices))
            @foreach($poll->choices as $val)
                <div class="form-group array-row" data-row="{{ $c }}">
                    <div class="col-sm-1">
                        <input type="text" class="key form-control" name="choices[{{ $c }}][choice]" value="{{ $val['choice'] }}" readonly />
                    </div>
                    <div class="col-sm-10">
                        <input type="text" class="value form-control" name="choices[{{ $c }}][value]" value="{{ $val['value'] }}" />
                    </div>
                    <div class="col-sm-1">
                        <div class="btn btn-danger remove-row" data-type="poll" data-row="{{ $c }}">Delete</div>
                    </div>
                </div>
                <?php $c++ ?>
            @endforeach
        @endif

        <div class="form-group extra-empty">
            <div class="col-sm-1">
                <input type="text" class="key form-control" name="choices[{{ $c }}][choice]" value="{{ $r[$c] }}" readonly data-replace="choice"/>
            </div>
            <div class="col-sm-10">
                <input type="text" class="value form-control" name="choices[{{ $c }}][value]" data-replace="value"/>
            </div>
            <div class="col-sm-1">
                <div class="btn btn-info add-row" data-type="poll" data-key="{{ $c }}">Add</div>
            </div>
        </div>
    </div>

</div>


<div class="form-group">
    <div class="col-sm-9 col-sm-offset-3">
        {!! Form::submit('Save', ['class'=>'btn btn-primary']) !!}
    </div>
</div>

{!! Form::close() !!}