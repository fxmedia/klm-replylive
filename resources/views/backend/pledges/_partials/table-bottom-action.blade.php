<tfoot>
    <tr>
        <td colspan="2">
            {!! Form::select('perform', [
                'export' => 'Export Pledges',
                'delete' => 'Delete Pledges'
            ], 'send_text', ['class'=>'form-control pull-left perform-selector']) !!}
        </td>
        <td>
            {!! Form::submit('Submit', ['class' => 'btn btn-md pull-left btn-primary']) !!}
        </td>
    </tr>
</tfoot>
