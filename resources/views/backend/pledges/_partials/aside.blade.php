<div class="panel panel-default">
    <div class="panel-heading"><i class="fa fa-download"></i> Download</div>
    <ul class="list-group">
        <li class="list-group-item"><a href="{{ route('backend.pledges.export') }}">Download all Pledges</a></li>
    </ul>


    <div class="panel-heading"><i class="glyphicon glyphicon-stats"></i> Statistics</div>
    <ul class="list-group">
        <li class="list-group-item"><span class="pull-right badge">{{ count($pledges) }}</span>Total pledges: </li>
    </ul>
</div>
