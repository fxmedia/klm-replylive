@extends('backend.layout.app')

@section('header')
    <h1>Reply.live Emails</h1>
    <ol class="breadcrumb">
        <li><a href="{{ URL::route('backend.index') }}">Backend</a></li>
        <li><a href="{{ URL::route('backend.emails.index') }}">Emails</a></li>
        <li><a href="{{ URL::route('backend.emails.setup', [$email->id]) }}">Setup</a></li>
        <li>Preview {{ $email->name }}</li>
    </ol>
@endsection

@section('content')
    <div class="col-xs-12">
        <h3>You're ready to send the email! Review the details below before sending your email</h3>
    </div>

    <div class="col-xs-12">
        <table class="table email-preview-table">
            <tbody>
                <tr class="preview-row">
                    <td>
                        <h4>List</h4>
                        <p>This email will be deliverd to <a href="#" data-toggle="modal" data-target="#modal-users">{{ count($sendUsers) }} users</a> and <a href="#" data-toggle="modal" data-target="#modal-skip">{{ count($skipUsers) }} users</a> without email address will be skipped.</p>
                    </td>
                </tr>
                <tr class="preview-row">
                    <td>
                        <h4>Subject line</h4>
                        <p>{{ $email->subject }}</p>
                    </td>
                </tr>
                <tr class="preview-row">
                    <td>
                        <h4>Replies</h4>
                        <p>All replies will go to <strong>{{ $email->reply_to }}</strong></p>
                    </td>
                </tr>
                <tr class="preview-row">
                    <td>
                        <h4>From</h4>
                        <p>This email will show that it's from <strong>{{ $email->from_name }}</strong> with the email address <strong>{{ $email->from_email }}</strong></p>
                    </td>
                </tr>
                <tr class="preview-row">
                    <td>
                        <h4>Template</h4>
                        <p>This email will use the <strong>{{ $email->view }}</strong> template.</p>
                    </td>
                </tr>
                <tr class="preview-row">
                    <td>
                        <h4>Attachment</h4>

                        @if (empty( $email->attachment))
                            <p>This email will not come with an attachment</p>
                        @else
                            <p>This email will come with <strong>{{ $email->attachment }}</strong> as attachment</p>
                        @endif
                    </td>
                </tr>
            </tbody>
        </table>
    </div>
    <div class="form-group">
        <div class="col-sm-6 breadcrumb breadcrumb-footer">
            <a href="{{ route('backend.emails.setup', [$email->id]) }}" class="btn btn-default">Back</a>
        </div>
        <div class="col-sm-6 breadcrumb breadcrumb-footer text-right">
            <a href="#" data-url="{{ route('backend.emails.ajax-send', [$email->id]) }}" class="btn btn-success" id="previewSend">Send!</a>
        </div>
    </div>

    <div class="modal fade" id="modal-users">
        <div class="modal-dialog modal-lg">
            <div class="modal-content">
                <div class="modal-header">
                    <button type="button" class="close" data-dismiss="modal" aria-label="Close"><span
                                aria-hidden="true">&times;</span></button>
                    <h4 class="modal-title">This email will be delivered to:</h4>
                </div>
                <div class="modal-body">

                    <table class="table table-hover">
                        <thead>
                        <th>Name</th>
                        <th>Email</th>
                        <tbody>
                        @foreach( $sendUsers as $user)
                            <tr>
                                <td>{{ $user->name }}</td>
                                <td>{{ $user->email }}</td>
                            </tr>
                        @endforeach
                        </tbody>
                    </table>

                </div>
            </div>
        </div>
    </div>

    <div class="modal fade" id="modal-skip">
        <div class="modal-dialog modal-lg">
            <div class="modal-content">
                <div class="modal-header">
                    <button type="button" class="close" data-dismiss="modal" aria-label="Close"><span
                                aria-hidden="true">&times;</span></button>
                    <h4 class="modal-title">This email will <strong>not</strong> be delivered to:</h4>
                </div>
                <div class="modal-body">

                    <table class="table table-hover">
                        <thead>
                        <th>Name</th>
                        <th>Email</th>
                        <tbody>
                        @foreach( $skipUsers as $user)
                            <tr>
                                <td>{{ $user->name }}</td>
                                <td>{{ $user->email }}</td>
                            </tr>
                        @endforeach
                        </tbody>
                    </table>

                </div>
            </div>
        </div>
    </div>

    @include('backend.emails._partials.batch-progress-modal')
@endsection

@section('scripts')
    <script type="text/javascript" charset="UTF-8" src="{{ asset('js/backend/message-batch-status.js') }}"></script>
    <script type="text/javascript" charset="UTF-8" src="{{ asset('js/backend/message-preview.js') }}"></script>
@endsection