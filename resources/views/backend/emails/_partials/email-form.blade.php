@if( isset($email) )
    {!! Form::model( $email, ['class'=>'form-horizontal user-admin', 'files'=>true, 'method'=>'patch', 'route' => ['backend.emails.update', $email->id]] ) !!}
@else
    {!! Form::open(['class'=>'form-horizontal user-admin', 'files'=>true, 'method' => 'POST', 'route' => 'backend.emails.store']) !!}
@endif

<div class="form-group">
    <div class="col-sm-3"></div>
    <div class="col-sm-9">
        <h3>General</h3>
    </div>
</div>

<div class="form-group {{ $errors->has('name') ? 'has-error' : '' }}">
    {!! Form::label('Email Name (for internal use)','', ['class' => 'col-sm-3 control-label']) !!}
    <div class="col-sm-9">
        {!! Form::text('name', Input::old('name'),['class'=>'form-control']) !!}
    </div>
    <span class="help-block col-sm-12 col-md-offset-3">{!! $errors->first('name') !!}</span>
</div>

<div class="form-group {{ $errors->has('view') ? 'has-error' : '' }}">
    {!! Form::label('Template name','', ['class' => 'col-sm-3 control-label']) !!}
    <div class="col-sm-9">
        {!! Form::select('view', $templateViews, Input::old('view'),['class'=>'form-control']) !!}
    </div>
    <span class="help-block col-sm-12 col-md-offset-3">{!! $errors->first('view') !!}</span>
</div>

<div class="form-group {{ $errors->has('action') ? 'has-error' : '' }}">
    {!! Form::label('action','', ['class' => 'col-sm-3 control-label']) !!}
    <div class="col-sm-9">
        <p>Does this email need to be sent automatically? If so select the appropriate action.</p>
        {!! Form::select('action', $actions, Input::old('action'),['class'=>'form-control']) !!}
    </div>
    <span class="help-block col-sm-12 col-md-offset-3">{!! $errors->first('action') !!}</span>
</div>

<div class="form-group">
    <div class="col-sm-3"></div>
    <div class="col-sm-9">
        <h3>Heading</h3>
    </div>
</div>

<div class="form-group {{ $errors->has('subject') ? 'has-error' : '' }}">
    {!! Form::label('Subject (max 200 characters)','', ['class' => 'col-sm-3 control-label']) !!}
    <div class="col-sm-9">
        {!! Form::text('subject', Input::old('subject'),['class'=>'form-control']) !!}
    </div>
    <span class="help-block col-sm-12 col-md-offset-3">{!! $errors->first('subject') !!}</span>
</div>

<div class="form-group {{ $errors->has('from_name') ? 'has-error' : '' }}">
    {!! Form::label('From Name','', ['class' => 'col-sm-3 control-label']) !!}
    <div class="col-sm-9">
        {!! Form::text('from_name', Input::old('from_name', $settings->email_default_from_name ),['class'=>'form-control']) !!}
    </div>
    <span class="help-block col-sm-12 col-md-offset-3">{!! $errors->first('from_name') !!}</span>
</div>

<div class="form-group {{ $errors->has('from_email') ? 'has-error' : '' }}">
    {!! Form::label('From Email','', ['class' => 'col-sm-3 control-label']) !!}
    <div class="col-sm-9">
        {!! Form::text('from_email', Input::old('from_email', $settings->email_default_from_email),['class'=>'form-control', 'placeholder' => 'The domain of this address needs to be white-labeled at Sendgrid (default is info@fxmedia.com)']) !!}
    </div>
    <span class="help-block col-sm-12 col-md-offset-3">{!! $errors->first('from_email') !!}</span>
</div>

<div class="form-group {{ $errors->has('reply_to') ? 'has-error' : '' }}">
    {!! Form::label('Reply Email','', ['class' => 'col-sm-3 control-label']) !!}
    <div class="col-sm-9">
        {!! Form::text('reply_to', Input::old('reply_to', $settings->email_default_reply),['class'=>'form-control']) !!}
    </div>
    <span class="help-block col-sm-12 col-md-offset-3">{!! $errors->first('reply_to') !!}</span>
</div>

<div class="form-group">
    <div class="col-sm-3"></div>
    <div class="col-sm-9">
        <h3>Content</h3>
    </div>
</div>

<div class="form-group {{ ($errors->has('header_image') || $errors->has('header_image_upload')) ? 'has-error' : ''}}">
    {!! Form::label('Header Image', '', ['class' => 'col-sm-3 control-label']) !!}
    <div class="col-sm-4">
        {!! Form::select('header_image', $images, Input::old('header_image'), ['class' => 'form-control']) !!}
    </div>
    <div class="col-sm-4">
        {!! Form::file('header_image_upload', ['class'=>'form-control']) !!}
    </div>
    <div class="col-sm-1" style="margin-top: 6px;">
        Resize {!! Form::checkbox('resize_header_image', null, ['class' => 'form-control']) !!}
    </div>
    <span class="help-block col-sm-12 col-md-offset-3">{!! $errors->first('header_image') !!}</span>
    <span class="help-block col-sm-12 col-md-offset-3">{!! $errors->first('header_image_upload') !!}</span>
</div>

<div class="form-group {{ ($errors->has('footer_image') || $errors->has('footer_image_upload')) ? 'has-error' : ''}}">
    {!! Form::label('Footer Image', '', ['class' => 'col-sm-3 control-label']) !!}
    <div class="col-sm-4">
        {!! Form::select('footer_image', $images, Input::old('footer_image'), ['class' => 'form-control']) !!}
    </div>
    <div class="col-sm-4">
        {!! Form::file('footer_image_upload', ['class'=>'form-control']) !!}
    </div>
    <div class="col-sm-1" style="margin-top: 6px;">
        Resize {!! Form::checkbox('resize_footer_image', null, ['class' => 'form-control']) !!}
    </div>
    <span class="help-block col-sm-12 col-md-offset-3">{!! $errors->first('footer_image') !!}</span>
    <span class="help-block col-sm-12 col-md-offset-3">{!! $errors->first('footer_image_upload') !!}</span>
</div>

<div class="form-group {{ ($errors->has('attachment') || $errors->has('attachment_upload')) ? 'has-error' : ''}}">
    {!! Form::label('Attachment', '', ['class' => 'col-sm-3 control-label']) !!}
    <div class="col-sm-4">
        {!! Form::select('attachment', $attachments, Input::old('attachment'), ['class' => 'form-control']) !!}
    </div>
    <div class="col-sm-4">
        {!! Form::file('attachment_upload', ['class'=>'form-control']) !!}
    </div>
    {{--<div class="col-sm-1" style="margin-top: 6px;">--}}
        {{--Resize {!! Form::checkbox('resize_attachment', null, ['class' => 'form-control']) !!}--}}
    {{--</div>--}}
    <span class="help-block col-sm-12 col-md-offset-3">{!! $errors->first('attachment') !!}</span>
    <span class="help-block col-sm-12 col-md-offset-3">{!! $errors->first('attachment_upload') !!}</span>
</div>


<div class="form-group {{ $errors->has('body_text') ? 'has-error' : '' }}">
    {!! Form::label('body_text','', ['class' => 'col-sm-3 control-label']) !!}
    <div class="col-sm-6">
        {!! Form::textarea('body_text', Input::old('body_text'),['class'=>'form-control', 'id' => 'body_text', 'placeholder' => 'Type here your body text. HTML-tags are allowed and required for enters']) !!}
    </div>
    <div class="col-sm-3">
        <p>Available data</p>
        <ul id="tag-list" class="tag-list" data-for="body_text">
            <li data-value="firstname">First name</li>
            <li data-value="lastname">Last name</li>
            <li data-value="token">Token</li>
            <li data-value="url">URL</li>
        </ul>
        <p>Extra tags are possible with [extra_xxx]</p>
        {{--<p id="tag-status"></p>--}}
    </div>
    <span class="help-block col-sm-12 col-md-offset-3">{!! $errors->first('body_text') !!}</span>
</div>


<div class="form-group {{ $errors->has('body_text') ? 'has-error' : '' }}">
    {!! Form::label('disclaimer_text','', ['class' => 'col-sm-3 control-label']) !!}
    <div class="col-sm-9">
        {!! Form::textarea('disclaimer_text', Input::old('disclaimer_text'),['class'=>'form-control', 'placeholder' => 'Type here your disclaimer text. HTML-tags are allowed and required for enters']) !!}
    </div>
    <span class="help-block col-sm-12 col-md-offset-3">{!! $errors->first('body_text') !!}</span>
</div>

<div class="form-group">
    <div class="col-sm-2 col-sm-offset-3 breadcrumb breadcrumb-footer">
        <a href="{{ URL::route('backend.emails.index') }}" class="btn btn-default">Back</a>
    </div>
    <div class="col-sm-7 breadcrumb breadcrumb-footer text-right">

        {!! Form::submit('Save', ['class'=>'btn btn-success']) !!}
    </div>
</div>


{!! Form::close() !!}