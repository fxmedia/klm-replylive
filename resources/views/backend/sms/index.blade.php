@extends('backend.layout.app')

@section('header')
    <h1>Reply.live Sms</h1>
    <ol class="breadcrumb">
        <li><a href="{{ URL::route('backend.index') }}">Backend</a></li>
        <li>Sms</li>
    </ol>
@endsection

@section('content')
    <div class="col-xs-3">
        @include('backend.sms._partials.aside')
    </div>
    <div class="col-xs-9">
        @include('backend._partials.errors')

        <table class="table table-hover">
            <thead>
            <tr>
                <th>Name</th>
                <th>Send on</th>
                <th>Actions</th>
            </tr>
            </thead>
            <tbody>
            @foreach($sms as $sm)
                <tr id="email_{{ $sm->id }}">
                    <td><strong>{{ $sm->name }}</strong></td>
                    <td>{{ ($sm->action !== 'noaction')? $sm->action : 'manually' }}</td>
                    <td>
                        <a class="btn btn-xs btn-success pull-left" href="{{ route('backend.sms.setup', [$sm->id]) }}">
                            <span class="glyphicon glyphicon-send"></span>Send
                        </a>
                        <a class="btn btn-xs btn-warning pull-left" href="{{ route('backend.sms.edit', [$sm->id]) }}">
                            <span class="glyphicon glyphicon-edit"></span>Edit
                        </a>
                        <a class="btn btn-xs btn-info pull-left" href="{{ route('backend.sms.log', [$sm->id]) }}">
                            <span class="glyphicon glyphicon-stats"></span>Log
                        </a>

                        {!! Form::open(['method'=>'DELETE', 'route' => ['backend.sms.remove', $sm->id], 'onsubmit' => 'return confirm("Please confirm to remove the selected sms?")']) !!}
                        <button class="btn btn-xs btn-danger btn-remove pull-left" type="submit">
                            <span class="glyphicon glyphicon-trash"></span> Delete
                        </button>
                        {!! Form::close() !!}
                    </td>
                </tr>
            @endforeach
            </tbody>
        </table>
    </div>
@endsection

@section('scripts')
@endsection