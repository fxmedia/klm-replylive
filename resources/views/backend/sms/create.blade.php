@extends('backend.layout.app')

@section('header')
    <h1>Reply.live Sms</h1>
    <ol class="breadcrumb">
        <li><a href="{{ URL::route('backend.index') }}">Backend</a></li>
        <li><a href="{{ URL::route('backend.sms.index') }}">Sms</a></li>
        <li>New Sms</li>
    </ol>
@endsection

@section('content')
    <div class="col-xs-12">
        @include('backend.sms._partials.sms-form')
    </div>
@endsection

@section('scripts')
    <script type="text/javascript" charset="UTF-8" src="{{ asset('js/backend/textarea-tag-lists.js') }}"></script>
@endsection