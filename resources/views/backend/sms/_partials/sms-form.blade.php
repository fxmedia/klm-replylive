@if( isset($sms) )
    {!! Form::model( $sms, ['class'=>'form-horizontal user-admin', 'method'=>'patch', 'route' => ['backend.sms.update', $sms->id]] ) !!}
@else
    {!! Form::open(['class'=>'form-horizontal user-admin', 'route' => 'backend.sms.store']) !!}
@endif

<div class="form-group">
    <div class="col-sm-3"></div>
    <div class="col-sm-9">
        <h3>General</h3>
    </div>
</div>

<div class="form-group {{ $errors->has('name') ? 'has-error' : '' }}">
    {!! Form::label('Sms Name (for internal use)','', ['class' => 'col-sm-3 control-label']) !!}
    <div class="col-sm-9">
        {!! Form::text('name', Input::old('name'),['class'=>'form-control']) !!}
    </div>
    <span class="help-block col-sm-12 col-md-offset-3">{!! $errors->first('name') !!}</span>
</div>

<div class="form-group {{ $errors->has('action') ? 'has-error' : '' }}">
    {!! Form::label('action','', ['class' => 'col-sm-3 control-label']) !!}
    <div class="col-sm-9">
        <p>Does this email need to be sent automatically? If so select the appropriate action.</p>
        {!! Form::select('action', $actions, Input::old('action'),['class'=>'form-control']) !!}
    </div>
    <span class="help-block col-sm-12 col-md-offset-3">{!! $errors->first('action') !!}</span>
</div>

<div class="form-group">
    <div class="col-sm-3"></div>
    <div class="col-sm-9">
        <h3>Heading</h3>
    </div>
</div>

<div class="form-group {{ $errors->has('subject') ? 'has-error' : '' }}">
    {!! Form::label('Sender (max 11 characters)','', ['class' => 'col-sm-3 control-label']) !!}
    <div class="col-sm-9">
        {!! Form::text('subject', Input::old('subject'),['class'=>'form-control']) !!}
    </div>
    <span class="help-block col-sm-12 col-md-offset-3">{!! $errors->first('subject') !!}</span>
</div>

<div class="form-group">
    <div class="col-sm-3"></div>
    <div class="col-sm-9">
        <h3>Content</h3>
    </div>
</div>

<div class="form-group {{ $errors->has('body_text') ? 'has-error' : '' }}">
    {!! Form::label('body_text','', ['class' => 'col-sm-3 control-label']) !!}
    <div class="col-sm-6">
        {!! Form::textarea('body_text', Input::old('body_text'),['class'=>'form-control', 'placeholder' => 'Type here your body text.', 'id' => 'body_text']) !!}
    </div>
    <div class="col-sm-3">
        <p>Available data</p>
        <ul id="tag-list" class="tag-list" data-for="body_text">
            <li data-value="firstname">First name</li>
            <li data-value="lastname">Last name</li>
            <li data-value="token">Token</li>
            <li data-value="url">URL</li>
        </ul>
        <p>Extra tags are possible with [extra_xxx]</p>
        {{--<p id="tag-status" class="tag-list" ></p>--}}
    </div>
    <span class="help-block col-sm-12 col-md-offset-3">{!! $errors->first('body_text') !!}</span>
</div>

<div class="form-group">
    <div class="col-sm-2 col-sm-offset-3 breadcrumb breadcrumb-footer">
        <a href="{{ URL::route('backend.sms.index') }}" class="btn btn-default">Back</a>
    </div>
    <div class="col-sm-7 breadcrumb breadcrumb-footer text-right">

        {!! Form::submit('Save', ['class'=>'btn btn-success']) !!}
    </div>
</div>


{!! Form::close() !!}