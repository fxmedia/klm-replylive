@extends('backend.layout.app')

@section('header')

    <h1>Reply.live Groups</h1>
    <ol class="breadcrumb">
        <li><a href="{{ URL::route('backend.index') }}">Backend</a></li>
        <li><a href="{{ URL::route('backend.groups.index') }}">Groups</a></li>
        <li>Create new group</li>
    </ol>
@endsection

@section('content')
    <div class="col-xs-12">
        @include('backend.groups._partials.group-form')
    </div>
@endsection