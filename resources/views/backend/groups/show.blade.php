@extends('backend.layout.app')

@section('header')

    <h1>Reply.live Groups</h1>
    <ol class="breadcrumb">
        <li><a href="{{ URL::route('backend.index') }}">Backend</a></li>
        <li><a href="{{ URL::route('backend.groups.index') }}">Groups</a></li>
        <li>Show {{ $groupName }}</li>
    </ol>
@endsection

@section('content')

    <div class="col-xs-3">
        @include('backend.groups._partials.aside')
    </div>

    <div class="col-xs-9">
        @include('backend._partials.errors')

        @include('backend.users._partials.user-table')
    </div>
@endsection


@section('scripts')
    <script>
        (function(){
            var mainForm = document.getElementById('usersTable');

            if(mainForm)mainForm.addEventListener('submit', function(){

                var userRows = mainForm.querySelectorAll('[name="user[]"]:checked');
                var arr = [];
                if(userRows.length > 900){
                    for(var i=0; i<userRows.length; i++){
                        arr.push(userRows[i].value);
                        userRows[i].checked = false;

                    }
                    var hiddenVal = document.createElement('input');
                    hiddenVal.setAttribute('type','hidden');
                    hiddenVal.setAttribute('name', 'user_json');
                    hiddenVal.value = JSON.stringify(arr);

                    mainForm.appendChild(hiddenVal);

                } else {
                    // allow normal post
                }
            });
        })();
    </script>
    <script type="text/javascript" charset="UTF-8" src="{{ asset('js/backend/tables.js') }}"></script>
@endsection