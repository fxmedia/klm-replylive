@include('backend._partials.errors')

@if( isset($group) )
    {!! Form::model( $group, ['class'=>'form-horizontal user-admin', 'method'=>'patch', 'route' => ['backend.groups.update', $group->id]] ) !!}
@else
    {!! Form::open(['class'=>'form-horizontal user-admin', 'route' => 'backend.groups.store', 'files'=>'true']) !!}
@endif

<div class="form-group {{ $errors->has('name') ? 'has-error' : '' }}">
    {!! Form::label('name','', ['class' => 'col-sm-3 control-label']) !!}
    <div class="col-sm-9">
        {!! Form::text('name', Input::old('name'),['class'=>'form-control']) !!}
    </div>
    <span class="help-block col-sm-12 col-md-offset-3">{!! $errors->first('name') !!}</span>
</div>

<div class="form-group {{ $errors->has('limit') ? 'has-error' : '' }}">
    {!! Form::label('limit', 'User limit', ['class' => 'col-sm-3 control-label']) !!}
    <div class="col-sm-3">
        {!! Form::text('limit', (isset($group)) ? $group->limit : 0 , ['class'=>'form-control']) !!}
    </div>
    <span class="help-block col-sm-12 col-md-offset-3">{!! $errors->first('limit') !!}</span>
</div>

<div class="form-group">
    <div class="col-sm-9 col-sm-offset-3">
        {!! Form::submit('Save', ['class'=>'btn btn-primary']) !!}
    </div>
</div>


{!! Form::close() !!}