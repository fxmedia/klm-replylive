@extends('backend.layout.app')

@section('header')

    <h1>Reply.live Groups</h1>
    <ol class="breadcrumb">
        <li><a href="{{ URL::route('backend.index') }}">Backend</a></li>
        <li>Groups</li>
    </ol>
@endsection

@section('content')

    <div class="col-xs-3">
        @include('backend.groups._partials.aside')
    </div>
    <div class="col-xs-9">
        {{--<nav class="navbar navbar-default" role="navigation">--}}
            {{--Some shit here--}}
        {{--</nav>--}}

        @include('backend._partials.errors')

        {!! Form::open(['route'=>['backend.groups.perform'], 'id'=>'groupsTable', 'method' => 'POST']) !!}
        <table class="table table-hover">
            <thead>
                @include('backend._partials.table-top-actions')
                <tr>
                    <th>&nbsp;</th>
                    <th>Name</th>
                    <th>&nbsp;</th>
                    <th>Limit</th>
                    <th>Users</th>
                    <th>Action</th>
                </tr>
            </thead>

            <tbody>
                <tr id="group_test">
                    <td>&nbsp;</td>
                    <td><a href="{{ route('backend.groups.test') }}">Test users</a></td>
                    <td>&nbsp;</td>
                    <td>&nbsp;</td>
                    <td>{{ isset($amountTestUsers)? $amountTestUsers : '??'  }}</td>
                    <td>&nbsp;</td>
                </tr>
                <tr id="group_0">
                    <td>&nbsp;</td>
                    <td><a href="{{ route('backend.groups.show', [0]) }}">Users without group</a></td>
                    <th>&nbsp;</th>
                    <td>&nbsp;</td>
                    <td>{{ isset($amountWithoutGroup)? $amountWithoutGroup : '??' }}</td>
                    <td>&nbsp;</td>
                </tr>
                @foreach($groups as $group)
                    <tr id="group_{{ $group->id }}">
                        <td>{!! Form::checkbox('group[]', $group->id, false, ['class' => 'selectable', 'id' => 'checkbox'.$group->id]) !!}</td>
                        <td><a href="{{ route('backend.groups.show', [$group->id]) }}">{{ $group->name }}</a></td>
                        <th>&nbsp;</th>
                        <td>{{ $group->limit }}</td>
                        <td>{{ count($group->users) }}</td>
                        <td>
                            <a class="btn btn-xs btn-success" href="{{ route('backend.groups.edit', [$group->id]) }}"><span class="glyphicon glyphicon-edit"></span></a>

                            <a class="btn btn-xs btn-danger form-action" href="#"
                               data-action="delete"
                               data-username="{{ $group->name }}"
                               data-id="{{ $group->id }}"
                               data-form="groupsTable">
                                <span class="glyphicon glyphicon-trash"></span>
                            </a>
                        </td>
                    </tr>
                @endforeach
            </tbody>
            @include('backend.groups._partials.table-bottom-action')
        </table>
        {!! Form::close() !!}
    </div>
@endsection

@section('scripts')
    <script type="text/javascript" charset="UTF-8" src="{{ asset('js/backend/tables.js') }}"></script>
@endsection