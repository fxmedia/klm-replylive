@extends('backend.layout.app')

@section('header')
    <h1>Reply.live Pushes</h1>
    <ol class="breadcrumb">
        <li><a href="{{ URL::route('backend.index') }}">Backend</a></li>
        <li>Pushes</li>
    </ol>
@endsection

@section('content')
    <div class="col-xs-3">
        @include('backend.pushes._partials.aside')
    </div>

    <div class="col-xs-9">
        @include('backend._partials.errors')

        @foreach($pushes as $push)
            <ul class="list-group  panel-default">
                <li class="list-group-item panel-heading">
                    {{ $push->name }}
                </li>
                <li class="list-group-item list-group-heading" data-push-id="{{ $push->id }}">
                    <a class="toggle-push-start-stop btn @if($push->isActive) btn-danger @else btn-primary @endif btn-xs"><i class="fa @if($push->isActive) fa-stop @else fa-play @endif"></i> <span>@if($push->isActive) Stop Push Module @else Start Push Module @endif</span></a>

                    <span>Groups: {{ App\Group::printGroups( $push->groups ) }}</span>

                    {!! Form::open(['method' => 'post', 'route' => ['backend.pushes.remove', $push->id], 'class' => 'pull-right', 'onsubmit' => 'return confirm("Please confirm to remove the selected poll?")']) !!}
                    <button class="btn btn-xs btn-danger"><span class="glyphicon glyphicon-trash"></span></button>
                    {!! Form::close() !!}

                    <a href="{{ route('backend.pushes.edit', [$push->id]) }}" class="btn btn-success btn-xs pull-right"><span class="glyphicon glyphicon-edit"></span></a>
                </li>
            </ul>
        @endforeach
    </div>

@endsection

@section('scripts')
    <script>
        var api_routes = {};
        @foreach($pushes as $push)
        api_routes['{{ $push->id }}'] = {
            start: '{{ route('api.push.start', $push->id) }}',
            stop: '{{ route('api.push.stop') }}',
        };
        @endforeach
    </script>
    <script type="text/javascript" charset="UTF-8" src="{{ asset('js/backend/pushes.js') }}"></script>
@endsection