@if( isset($push) )
    {!! Form::model( $push, ['class'=>'form-horizontal user-admin', 'files' => true, 'method'=>'patch', 'route' => ['backend.pushes.update', $push->id]] ) !!}
@else
    {!! Form::open(['class'=>'form-horizontal user-admin', 'files' => true, 'route' => 'backend.pushes.store']) !!}
@endif
<div class="form-group {{ $errors->has('name') ? 'has-error' : '' }}">
    {!! Form::label('name','', ['class' => 'col-sm-3 control-label']) !!}
    <div class="col-sm-9">
        {!! Form::text('name', Input::old('name'),['class'=>'form-control']) !!}
    </div>
    <span class="help-block col-sm-12 col-md-offset-3">{!! $errors->first('name') !!}</span>
</div>

<div class="form-group">
    {!! Form::label('Groups','', ['class' => 'col-sm-3 control-label']) !!}
    <div class="col-sm-9">
        @forelse ($groups as $group )
            <label>
                @if( isset($push) )
                    {!! Form::checkbox('groups[' . $group->id . ']', $group->id, $push->isInGroup($group) ) !!}

                @else
                    {!! Form::checkbox('groups[' . $group->id . ']', $group->id ) !!}
                @endif
                {{ $group->name }}
            </label><br>
        @empty
            No user groups
        @endforelse
    </div>
</div>

<div class="form-group {{ $errors->has('title') ? 'has-error' : '' }}">
    {!! Form::label('title','', ['class' => 'col-sm-3 control-label']) !!}
    <div class="col-sm-9">
        {!! Form::text('title', Input::old('title'),['class'=>'form-control']) !!}
    </div>
    <span class="help-block col-sm-12 col-md-offset-3">{!! $errors->first('title') !!}</span>
</div>

<div class="form-group {{ $errors->has('module') ? 'has-error' : '' }}">
    {!! Form::label('Template name','', ['class' => 'col-sm-3 control-label']) !!}
    <div class="col-sm-9">
        {!! Form::select('module', $modules, Input::old('module'),['class'=>'form-control']) !!}
    </div>
    <span class="help-block col-sm-12 col-md-offset-3">{!! $errors->first('module') !!}</span>
</div>

<div class="form-group {{ ($errors->has('image') || $errors->has('image_upload')) ? 'has-error' : ''}}">
    {!! Form::label('Image', '', ['class' => 'col-sm-3 control-label']) !!}
    <div class="col-sm-4">
        {!! Form::select('image', $images, Input::old('image'), ['class' => 'form-control']) !!}
    </div>
    <div class="col-sm-4">
        {!! Form::file('image_upload', ['class'=>'form-control']) !!}
    </div>
    <div class="col-sm-1" style="margin-top: 6px;">
        Resize {!! Form::checkbox('resize_image', null, ['class' => 'form-control']) !!}
    </div>
    <span class="help-block col-sm-12 col-md-offset-3">{!! $errors->first('image') !!}</span>
    <span class="help-block col-sm-12 col-md-offset-3">{!! $errors->first('image_upload') !!}</span>
</div>

<div class="form-group {{ $errors->has('copy') ? 'has-error' : '' }}">
    {!! Form::label('copy','', ['class' => 'col-sm-3 control-label']) !!}
    <div class="col-sm-9">
        {!! Form::textarea('copy', Input::old('copy'),['class'=>'form-control', 'placeholder' => 'html tags are allowed here']) !!}
    </div>
    <span class="help-block col-sm-12 col-md-offset-3">{!! $errors->first('copy') !!}</span>
</div>



<div class="form-group">
    <div class="col-sm-9 col-sm-offset-3">
        {!! Form::submit('Save', ['class'=>'btn btn-primary']) !!}
    </div>
</div>

{!! Form::close() !!}