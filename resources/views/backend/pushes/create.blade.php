@extends('backend.layout.app')

@section('header')
    <h1>Reply.live Pushes</h1>
    <ol class="breadcrumb">
        <li><a href="{{ URL::route('backend.index') }}">Backend</a></li>
        <li><a href="{{ URL::route('backend.pushes.index') }}">Pushes</a></li>
        <li>New Push</li>
    </ol>
@endsection

@section('content')
    <div class="col-xs-12">
        @include('backend.pushes._partials.push-form')
    </div>
@endsection

@section('scripts')
    <script type="text/javascript" charset="UTF-8" src="{{ asset('js/backend/forms.js')  }}"></script>
@endsection