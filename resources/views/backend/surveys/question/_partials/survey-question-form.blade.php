@if( isset($question) )
    {!! Form::model( $question, ['class'=>'form-horizontal user-admin', 'method'=>'patch', 'route' => ['backend.surveys.questions.update', $question->id]] ) !!}
    {!! Form::hidden('order', $question->order) !!}
@else
    {!! Form::open(['class'=>'form-horizontal user-admin', 'method'=>'post', 'route' => ['backend.surveys.questions.store', $survey->id]]) !!}
    {!! Form::hidden('order', count($survey->questions)) !!}
@endif

<div class="form-group {{ $errors->has('title') ? 'has-error' : '' }}">
    {!! Form::label('Question','', ['class' => 'col-sm-3 control-label']) !!}
    <div class="col-sm-9">
        {!! Form::text('title', Input::old('title'),['class'=>'form-control']) !!}
    </div>
    <span class="help-block col-sm-12 col-md-offset-3">{!! $errors->first('title') !!}</span>
</div>

<div class="form-group {{ $errors->has('type') ? 'has-error' : '' }}">
    {!! Form::label('Type','', ['class' => 'col-sm-3 control-label']) !!}
    <div class="col-sm-9">
        <select name="type" class="type-select form-control" data-class="survey-type-group">
            @foreach(config('surveys.question_types') as $type)
                <option value="{{ $type }}" @if(isset($question) && $type === $question->type) selected @endif>{{ $type }}</option>
            @endforeach
        </select>
    </div>
    <span class="help-block col-sm-12 col-md-offset-3">{!! $errors->first('type') !!}</span>
</div>

<div class="well form-group survey-type-group" data-type="in-between-screen">
    {!! Form::label('Content','', ['class' => 'col-sm-3 control-label']) !!}
    <div class="col-sm-9">
        {!! Form::text('data[content]', Input::old('data.content'),[ 'class' => 'form-control' ]) !!}
    </div>
</div>

<div class="well form-group survey-type-group" data-type="range">
    {!! Form::label('Min range Label','', ['class' => 'col-sm-3 control-label']) !!}
    <div class="col-sm-9">
        {!! Form::text('data[min_label]', Input::old('data.min_label'),[ 'class' => 'form-control' ]) !!}
    </div>
    {!! Form::label('Min range','', ['class' => 'col-sm-3 control-label']) !!}
    <div class="col-sm-9">
        {!! Form::number('data[min_range]', Input::old('data.min_range'),[ 'class' => 'form-control' ]) !!}
    </div>
    {!! Form::label('Max range Label','', ['class' => 'col-sm-3 control-label']) !!}
    <div class="col-sm-9">
        {!! Form::text('data[max_label]', Input::old('data.max_label'),[ 'class' => 'form-control' ]) !!}
    </div>
    {!! Form::label('Max range','', ['class' => 'col-sm-3 control-label']) !!}
    <div class="col-sm-9">
        {!! Form::number('data[max_range]', Input::old('data.max_range'),[ 'class' => 'form-control' ]) !!}
    </div>

</div>

<div class="well form-group survey-type-group" data-type="abcd">
    {!! Form::label('answers','', ['class' => 'col-sm-3 control-label']) !!}
    <div class="col-sm-9">


        <?php $c =0; $r = range('a', 'z') ?>
            @if( isset($question) && is_array($question->data) && ($question->type === 'abcd' || $question->type === 'abcd-multiple' ) )
                @foreach($question->data as $k => $ch)
                    <div class="form-group array-row" data-row="{{ $c }}">
                        <div class="col-sm-1">
                            <input type="text" class="key correct-key form-control" name="data[{{ $c }}][choice]" value="{{ $r[$c] }}" readonly />
                        </div>
                        <div class="col-sm-10">
                            <input type="text" class="value form-control" name="data[{{ $c }}][value]" value="{{ $ch['value'] }}" />
                        </div>
                        <div class="col-sm-1">
                            <div class="btn btn-danger remove-row" data-type="quiz_question" data-row="{{ $c }}">Delete</div>
                        </div>
                    </div>
                    <?php $c++; ?>
                @endforeach
            @endif

            <div class="form-group extra-empty">
                <div class="col-sm-1">
                    <input type="text" class="key correct-key form-control" name="data[{{ $c }}][choice]" value="{{ $r[$c] }}" readonly data-replace="choice"/>
                </div>
                <div class="col-sm-10">
                    <input type="text" class="value form-control" name="data[{{ $c }}][value]" data-replace="value"/>
                </div>
                <div class="col-sm-1">
                    <div class="btn btn-info add-row" data-type="survey_abcd" data-key="{{ $c }}">Add</div>
                </div>
            </div>
    </div>
</div>


<div class="form-group">
    <div class="col-sm-9 col-sm-offset-3">
        {!! Form::submit('Save', ['class'=>'btn btn-primary']) !!}
    </div>
</div>

{!! Form::close() !!}
