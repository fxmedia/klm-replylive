@extends('backend.layout.app')

@section('header')
    <h1>Reply.live Survey Questions</h1>
    <ol class="breadcrumb">
        <li><a href="{{ route('backend.index') }}">Backend</a></li>
        <li><a href="{{ route('backend.surveys.index') }}">Surveys</a></li>
        <li><a href="{{ route('backend.surveys.edit', [$survey->id]) }}" >{{ $survey->name }}</a></li>
        <li>New Question</li>
    </ol>
@endsection

@section('content')
    <div class="col-xs-12">
        @include('backend.surveys.question._partials.survey-question-form')

    </div>
@endsection

@section('scripts')
    <script type="text/javascript" charset="UTF-8" src="{{ asset('js/backend/forms.js') }}"></script>
@endsection