@extends('backend.layout.app')

@section('header')
    <h1>Reply.live Surveys</h1>
    <ol class="breadcrumb">
        <li><a href="{{ URL::route('backend.index') }}">Backend</a></li>
        <li>Surveys</li>
    </ol>
@endsection

@section('content')
    <div class="col-xs-3">
        @include('backend.surveys._partials.aside')
    </div>

    <div class="col-xs-9">
        @include('backend._partials.errors')

        @foreach($surveys as $survey)

            <ul class="list-group  panel-default">
                <li class="list-group-item panel-heading">
                    {{ $survey->name }}
                </li>
                <li class="list-group-item list-group-heading" data-survey-id="{{ $survey->id }}" data-takeover="{{ $survey->takeover }}">
                    <a class="toggle-survey-start-stop btn @if($survey->isActive) btn-danger @else btn-primary @endif btn-xs @if($survey->takeover) survey-takeover @endif"><i class="fa @if($survey->isActive) fa-stop @else fa-play @endif"></i> <span>@if($survey->isActive) Stop Survey @else Start Survey @endif</span></a>
                    <span>Takeover: {{ $survey->takeover ? 'yes' : 'no' }} </span>||
                    <span>Groups: {{ App\Group::printGroups( $survey->groups ) }}</span>

                    {!! Form::open(['method' => 'post', 'route' => ['backend.surveys.remove', $survey->id], 'class' => 'pull-right', 'onsubmit' => 'return confirm("Please confirm to remove the selected survey?")']) !!}
                    <button class="btn btn-xs btn-danger"><span class="glyphicon glyphicon-trash"></span></button>
                    {!! Form::close() !!}

                    {!! Form::open(['method' => 'post', 'route' => ['backend.surveys.reset.single', $survey->id], 'class' => 'pull-right', 'onsubmit' => 'return confirm("Are you sure you want to reset the score?")']) !!}
                    <button class="btn btn-xs btn-info"><span class="glyphicon glyphicon-retweet"></span></button>
                    {!! Form::close() !!}

                    <a href="{{ route('backend.surveys.edit', [$survey->id]) }}" class="btn btn-success btn-xs pull-right"><span class="glyphicon glyphicon-edit"></span></a>

                    {{--<span class="pull-right">Total Answers: <span class="answer-survey-{{ $survey->id }}">{{ count($survey->answers) }}</span>&nbsp;&nbsp;&nbsp;</span>--}}
                </li>
            </ul>

        @endforeach
        @if(count($surveys) === 0)
            <h3>No Surveys in the database.</h3>
            <p>Click on "Add Survey" in the menu on the left to create a new survey</p>
        @endif
    </div>

@endsection

@section('scripts')
    <script>
        var api_routes = {};
        @foreach($surveys as $survey)
        api_routes['{{ $survey->id }}'] = {
            start: '{{ route('api.survey.start', [$survey->id]) }}',
            stop: '{{ route('api.survey.stop', [$survey->id]) }}'
        };
        @endforeach
    </script>
    <script type="text/javascript" charset="UTF-8" src="{{ asset('js/backend/surveys.js') }}"></script>
@endsection