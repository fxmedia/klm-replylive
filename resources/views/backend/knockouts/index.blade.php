@extends('backend.layout.app')

@section('header')
    <h1>Reply.live Knockouts</h1>
    <ol class="breadcrumb">
        <li><a href="{{ URL::route('backend.index') }}">Backend</a></li>
        <li>Knockouts</li>
    </ol>
@endsection

@section('content')
    <div class="col-xs-3">
        @include('backend.knockouts._partials.aside')
    </div>

    <div class="col-xs-9">
        @include('backend._partials.errors')

        @foreach($knockouts as $knockout)
            <ul class="list-group panel-default">
                <li class="list-group-item panel-heading">
                    {{ $knockout->name }}
                </li>
                <li class="list-group-item list-grou-heading" data-knockout-id="{{ $knockout->id }}">
                    @if($knockout->isActive)
                        <a class="toggle-knockout-start-stop btn btn-danger btn-xs" data-question-index="{{ $knockout->activeData['question_index'] }}" @if( $knockout->activeData['locked'] ) data-check-answer="{{ $knockout->activeData['locked'] }}" @endif><i class="fa fa-stop"></i> <span>Stop Knockout</span></a>
                    @else
                        <a class="toggle-knockout-start-stop btn btn-primary btn-xs"><i class="fa fa-play"></i> <span>Start Knockout</span></a>
                    @endif

                    <span>Groups: {{ App\Group::printGroups( $knockout->groups ) }}</span>

                    {!! Form::open(['method' => 'post', 'route' => ['backend.knockouts.remove', $knockout->id], 'class' => 'pull-right', 'onsubmit' => 'return confirm("Please confirm to remove the selected knockout?")']) !!}
                    <button class="btn btn-xs btn-danger"><span class="glyphicon glyphicon-trash"></span></button>
                    {!! Form::close() !!}

                    {!! Form::open(['method' => 'post', 'route' => ['backend.knockouts.reset.single', $knockout->id], 'class' => 'pull-right', 'onsubmit' => 'return confirm("Are you sure you want to reset the score?")']) !!}
                    <button class="btn btn-xs btn-info"><span class="glyphicon glyphicon-retweet"></span></button>
                    {!! Form::close() !!}

                    <a href="{{ route('backend.knockouts.edit', [$knockout->id]) }}" class="btn btn-success btn-xs pull-right"><span class="glyphicon glyphicon-edit"></span></a>
                </li>

                @foreach($knockout->questions as $key => $question)
                    <li class="list-group-item">
                        <span class="badge pull-left">{{ $question->order }}</span>
                        <span class="pull-right">
                            <a class="toggle-question-start-stop btn btn-primary btn-xs" data-knockout-id="{{ $knockout->id }}" data-index="{{ $key }}"><i class="fa fa-play"></i> <span>Start Question</span></a>
                        </span>
                        &nbsp;&nbsp;&nbsp;{{ str_limit($question->title, 60) }}

                    </li>
                @endforeach
            </ul>
        @endforeach
        @if(count($knockouts) === 0)
            <h3>No Knockouts in the database.</h3>
            <p>Click on "Add Poll" in the menu on the left to create a new knockout</p>
        @endif
    </div>

    <div class="modal fade" id="knockoutModal" data-backdrop="static" data-keyboard="false">
        <div class="modal-dialog">
            <div class="modal-content">
                <div class="modal-header">
                    <button type="button" class="close" aria-label="Close"><span aria-hidden="true">&times;</span></button>
                    <h4 class="modal-title">Knockout: <span class="knockout-name"></span></h4>
                    <h5 class="modal-title">Players left: <span class="players-left"></span></h5>
                </div>
                <div class="modal-body">

                    {{--<div class="question-navigation" id="questionNavigation"></div>--}}

                    <div class="question-counter">Question <span class="question-number">1</span> of <span class="question-numbers">1</span></div>

                    <div class="question"><b>Question: <span class="question-name"></span></b></div>
                    {{--<a class="knockout-start btn btn-primary">Start knockout!</a>--}}
                    <span class="question-answers"></span>
                    <b>Correct answer: <span class="question-answer"></span></b><br/><br/>
                    <div class="modal-actions">
                        <span class="btn btn-primary check-answer">Check answer</span>
                        <span class="btn btn-default next" disabled="disabled">Next question</span>
                        {{--<span class="btn btn-info prev-result hide">Previous results</span>--}}
                        {{--<span class="btn btn-default page-number hide">1</span>--}}
                        {{--<span class="btn btn-info next-result hide">Next results</span>--}}
                        <span class="btn btn-success deciding-question hide">Show shootout question</span>
                        <span class="btn btn-warning check-deciding-question hide">Check shootout answers</span>
                        <span class="btn btn-success results hide">Show results</span>
                        <span class="btn btn-warning end hide">Stop the knockout</span>
                    </div>
                </div>
                <div class="modal-footer">
                    {{--<span class="btn btn-success score hide pull-right">Show score</span>--}}
                </div>
            </div>
        </div>
    </div>


@endsection

@section('scripts')
    <script>
        var api_routes = {
            other: {
                checkAnswer: '{{ route('api.knockout.check-answer') }}',
                getStatus : '{{ route('api.knockout.status') }}',
                stop: '{{ route('api.knockout.stop') }}',
                setDecidingQuestion: '{{ route('api.knockout.set-deciding-question') }}'
            }
        };
        @foreach($knockouts as $knockout)
            api_routes['{{ $knockout->id }}'] = {
                start: '{{ route('api.knockout.start', [$knockout->id]) }}',
                showQuestion: '{{ route('api.knockout.show-question', [$knockout->id]) }}',
                getData: '{{ route('api.knockout.get-data', [$knockout->id]) }}'
            };
        @endforeach
    </script>
    <script type="text/javascript" charset="UTF-8" src="{{ asset('js/backend/knockouts.js') }}"></script>
@endsection
