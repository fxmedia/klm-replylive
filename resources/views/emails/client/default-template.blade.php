<!DOCTYPE html>
<html lang="en">
<head>
    <meta content="text/html; charset=utf-8" http-equiv="Content-Type">
    <meta content="width=device-width, initial-scale=1" name="viewport">
    <meta content="telephone=no" name="format-detection">
    <meta content="IE=edge" http-equiv="X-UA-Compatible">
    <title></title><!--[if mso]>
    <style>body, table tr, table td, a, span, table.MsoNormalTable {
        font-family: verdana, arial, sans-serif !important !important;
    }</style><!==<![endif]-->

    <style type="text/css">

        body {
            margin: 0;
            padding: 0;
            -ms-text-size-adjust: 100%;
            -webkit-text-size-adjust: 100%;
            background-color: #ffffff;
            font-family: verdana, arial, sans-serif !important;
            font-weight: normal;
            font-size: 12px;
        }

        img {
            -ms-interpolation-mode: bicubic;
            outline: none;
            border-style: none;
            display: block;
        }

        table {
            border-collapse: collapse;
            border-spacing: 0;
            mso-table-lspace: 0pt;
            mso-table-rspace: 0pt;
        }

        table td {
            border-collapse: collapse;
        }

        a {
            font-weight: bold;
        }

        a:hover {

        }

        p, h1, h2, h3 {
            margin-top: 0;
            margin-bottom: 0;
        }

        p {
            line-height: 20px;
        }

        .header {
            background-color: #ffffff;
        }

        .header__content {
            padding: 0;
            text-align: left;
        }

        .block__content {
            padding-top:50px;
            padding-bottom:60px;
            padding-right:50px;
            padding-left:50px;
            text-align: left;
        }

        @media only screen and (max-width: 480px) {
            .mobile-full {
                width: 100% !important;
                display: block !important;
                float: none !important;
                height: auto !important;
            }

            .block__content {
                padding-top:30px !important;
                padding-bottom:40px !important;
                padding-right:10px !important;
                padding-left:10px !important;
                text-align: left !important;
            }

            img {
                height: auto !important;
            }

        }
    </style>
</head>

<body style="margin-top:0;margin-bottom:0;margin-right:0;margin-left:0;padding-top:0;padding-bottom:0;padding-right:0;padding-left:0;-ms-text-size-adjust:100%;-webkit-text-size-adjust:100%;background-color:#ffffff;font-family:verdana,arial,sans-serif!important;">
<center>
    <table border="0" cellpadding="0" cellspacing="0" class="header" style="border-collapse:collapse;border-spacing:0;mso-table-lspace:0pt;mso-table-rspace:0pt;background-color:#ffffff;height:100% !important; margin:0; padding:0; width:100% !important;" width="100%">
        <tr>
            <td align="center" style="border-collapse:collapse;">
                <table border="0" cellpadding="0" cellspacing="0" class="mobile-full" style="border-collapse:collapse;border-spacing:0;mso-table-lspace:0pt;mso-table-rspace:0pt;background-color:#ffffff;width:600px;" width="600">
                    <tr>
                        <td align="left" class="header__content" style="border-collapse:collapse;padding-top:0;padding-bottom:0;padding-right:0;padding-left:0;text-align:left;width:600px;" width="600">
                            <table border="0" cellpadding="0" cellspacing="0" style="border-collapse:collapse;border-spacing:0;mso-table-lspace:0pt;mso-table-rspace:0pt;" width="100%">
                                <tr>
                                    <td align="left" class="mobile-full" style="height:auto;" valign="bottom">
                                        @if($email->header_image)
                                            <img alt="" class="header__image mobile-full" height=""
                                                 src="{{ $email->embed($message, 'header_image') }}"
                                                 style="-ms-interpolation-mode:bicubic;outline-style:none;border-style:none;display:block;Margin-bottom:10px;height:auto;margin-top:0;margin-bottom:0;margin-right:0;margin-left:0;Margin:0;"
                                                 width="100%">
                                        @endif
                                    </td>
                                </tr>
                            </table>
                        </td>
                    </tr>
                </table>
            </td>
        </tr>
    </table>

    <table border="0" cellpadding="0" cellspacing="0" style="border-collapse:collapse;border-spacing:0;mso-table-lspace:0pt;mso-table-rspace:0pt;background-color:#ffffff;height:100%;color:#000000;font-family:verdana, arial, sans-serif;font-size:12px;" width="100%">
        <tr>
            <td align="center" style="border-collapse:collapse;color:#000000;">

                <table border="0" cellpadding="0" cellspacing="0" style="border-collapse:collapse;margin:0;border-spacing:0;mso-table-lspace:0pt;mso-table-rspace:0pt;background-color:#ffffff;color:#000000;" width="100%">
                    <tr>
                        <td align="center" style="border-collapse:collapse;color:#000000;">
                            <table border="0" cellpadding="0" cellspacing="0" class="mobile-full" style="border-collapse:collapse;border-spacing:0;mso-table-lspace:0pt;mso-table-rspace:0pt;background-color:#ffffff;color:#000000;" width="600">
                                <tr>
                                    <td class="block__content" style="border-collapse:collapse;padding-top:50px;padding-bottom:60px;padding-right:50px;padding-left:50px;text-align:left;font-family:verdana, arial, sans-serif;font-size:12px;">
                                        <p style="margin-top:0;color:#000000;font-family:verdana, arial, sans-serif;font-size:12px;">
                                            {!! $body_text !!}
                                        </p>
                                    </td>
                                </tr>
                            </table>
                        </td>
                    </tr>
                </table>
            </td>
        </tr>
    </table>

    <table border="0" cellpadding="0" cellspacing="0" class="header" style="border-collapse:collapse;border-spacing:0;mso-table-lspace:0pt;mso-table-rspace:0pt;background-color:#ffffff;" width="100%">
        <tr>
            <td align="center" style="border-collapse:collapse;">
                <table border="0" cellpadding="0" cellspacing="0" class="mobile-full" style="border-collapse:collapse;border-spacing:0;mso-table-lspace:0pt;mso-table-rspace:0pt;background-color:#ffffff;width:600px;" width="600">
                    <tr>
                        <td align="left" class="header__content" style="border-collapse:collapse;padding-top:0;padding-bottom:0;padding-right:0;padding-left:0;text-align:left;width:600px;" width="600">
                            <table border="0" cellpadding="0" cellspacing="0" style="border-collapse:collapse;border-spacing:0;mso-table-lspace:0pt;mso-table-rspace:0pt;" width="100%">
                                <tr>
                                    <td align="left" class="mobile-full" style="height:auto;" valign="bottom">
                                        @if($email->footer_image)
                                            <img alt="" class="footer__image mobile-full" height=""
                                                 src="{{ $email->embed($message, 'footer_image') }}"
                                                 style="-ms-interpolation-mode:bicubic;outline-style:none;border-style:none;display:block;Margin-bottom:0px;height:auto;margin-top:0;margin-bottom:0;margin-right:0;margin-left:0;Margin:0;"
                                                 width="100%">
                                        @endif
                                    </td>
                                </tr>
                            </table>
                        </td>
                    </tr>
                </table>
            </td>
        </tr>
    </table>

    @if($disclaimer_text)
        <table border="0" cellpadding="0" cellspacing="0" style="border-collapse:collapse;border-spacing:0;mso-table-lspace:0pt;mso-table-rspace:0pt;background-color:#ffffff;height:100%;color:#000000;font-family:verdana, arial, sans-serif;font-size:12px;" width="100%">
            <tr>
                <td align="center" style="border-collapse:collapse;color:#000000;">

                    <table border="0" cellpadding="0" cellspacing="0" style="border-collapse:collapse;margin:0;border-spacing:0;mso-table-lspace:0pt;mso-table-rspace:0pt;background-color:#ffffff;color:#000000;" width="100%">
                        <tr>
                            <td align="center" style="border-collapse:collapse;color:#000000;">
                                <table border="0" cellpadding="0" cellspacing="0" class="mobile-full" style="border-collapse:collapse;border-spacing:0;mso-table-lspace:0pt;mso-table-rspace:0pt;background-color:#ffffff;color:#000000;" width="600">
                                    <tr>
                                        <td class="block__content" style="border-collapse:collapse;padding-top:50px;padding-bottom:60px;padding-right:50px;padding-left:50px;text-align:left;font-family:verdana, arial, sans-serif;font-size:12px;">
                                            <p style="margin-top:0;color:#888888;font-family:verdana, arial, sans-serif;font-size:10px;">
                                                {!! $disclaimer_text !!}
                                            </p>
                                        </td>
                                    </tr>
                                </table>
                            </td>
                        </tr>
                    </table>
                </td>
            </tr>
        </table>
    @endif
</center>
</body>
</html>
