<!doctype html>
<html lang="en">
<head>
    <meta charset="utf-8">
    <meta http-equiv="X-UA-Compatible" content="IE=edge">
    <meta name="csrf-token" content="{{ csrf_token() }}">
    <meta name="viewport" content="width=device-width, maximum-scale=1, initial-scale=1, user-scalable=0">
    <title>V3 Reply.live - Display</title>
    <link rel="stylesheet" href="{{ asset('/css/display/display.css') }}">
    <link rel="shortcut icon" href="{{ asset('/images/favicon.png') }}">
</head>
<body>
<div id="root"></div>
<script id="replyLiveLoader" type="text/javascript" data-json="{{ $json }}"></script>
<script src="{{ asset('/js/display.js') }}"></script>
</body>
</html>
