@extends('frontend.client.layout.app')

@section('stylesheets')
    <link rel="stylesheet" type="text/css" href="{{ asset('css/lib/font-awesome/fontawesome.css') }}" />
@endsection

@section('header')
@endsection

@section('content')
    <div class="reply-live-takeover-posts">
        Trigger the posts
    </div>
    <div class="reply-live-takeover-posts">
        Trigger the posts
    </div>
@endsection

@section('scripts')
    <script id="replyLiveLoader" type="text/javascript" data-token="{{ $user->token }}"  data-rl="{{$dataPath}}" charset="UTF-8" src="{{$srcPath}}" ></script>
@endsection
