(function(){
    console.log('scanner page!');
    var HIDE_MESSAGE_TIMEOUT_TIME = 5000;

    var form;
    var barcode;
    var result;
    var period;
    var loader;
    var doingAjax = false;
    var hideMessageTimeout;

    //_____Helper_Functions________________________________________________________________________________________/
    var searchToObject = function() {
        var pairs = window.location.search.substring(1).split("&"),
            obj = {},
            pair,
            i;

        for ( i in pairs ) {
            if ( pairs.hasOwnProperty(i) && pairs[i] === "" ) continue;

            pair = pairs[i].split("=");
            obj[ decodeURIComponent( pair[0] ) ] = decodeURIComponent( pair[1] );
        }

        return obj;
    };

    //_____Event_Functions_________________________________________________________________________________________/
    var _onDocumentClick = function(e){
        if(!e.target.classList.contains('allow-click')){
            barcode.focus();
        }
    };

    var _onFormSubmit = function(event){
        event.preventDefault();

        var sendData = {
            method: form.getAttribute('method'),
            barcode: barcode.value
        };
        if(period) sendData.period = period;

        barcode.value = '';

        if(!doingAjax){
            doingAjax = true;
            _showOrHideLoader(true);

            $.ajax(form.getAttribute('action'), {
                method:'POST',
                data: sendData
            }).success(_onSubmitSuccess)
                .error(_onSubmitError);
        }

    };

    var _onSubmitSuccess = function(data){
        doingAjax = false;
        _showOrHideLoader(false);

        if(!data) return _onSubmitError('Error: Submit success, but no data return');
        if(!data.success) {
            return _showMessage('error', data.error.message);
        }

        if(data.warning){
            return _showMessage('warning', data.message);
        }

        _showMessage('success', 'Ticket scanned: '+ data.name);
    };

    var _onSubmitError = function(data){
        doingAjax = false;
        _showOrHideLoader(false);

        console.warn('submit error!', data);

        _showMessage('error', 'Oops, something went wrong while scanning');
    };

    //_____Functions_______________________________________________________________________________________________/
    var _showOrHideLoader = function(show){
        if(show){
            loader.classList.remove('hidden');
        } else {
            loader.classList.add('hidden');
            barcode.focus();
        }
    };

    var _showMessage = function(type, message){
        clearTimeout(hideMessageTimeout);

        result.innerHTML = message;
        result.className = 'show '+ type;

        hideMessageTimeout = setTimeout(_hideMessage, HIDE_MESSAGE_TIMEOUT_TIME);
    };

    var _hideMessage = function(){
        clearTimeout(hideMessageTimeout);

        result.className = '';
    };


    //_____Initialize______________________________________________________________________________________________/
    var _init = function(){
        form    = document.getElementById('scanForm');
        result  = document.getElementById('scanResult');
        barcode = document.getElementById('barcode');
        loader  = document.getElementById('loader');


        period = searchToObject()['period'];

        document.body.addEventListener('click', _onDocumentClick);
        form.addEventListener('submit', _onFormSubmit);
    };
    _init();
})();