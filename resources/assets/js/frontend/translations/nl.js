module.exports = {
    pledge: {
            errors: {
                amount: "Het bedrag moet hoger zijn dan 0 euro",
                name: "Uw naam is voor de registratie noodzakelijk. Anonimiteit voor het scherm kunt u hieronder aanvinken.",
                name_too_long: "Uw naam mag helaas niet langer zijn dan 70 tekens.",
            },
            anonymous: "Anoniem"
    },

    errors: {
        wrong_file_type: "Dit bestandstype wordt niet ondersteund",
        no_video_support: "Jouw browser ondersteunt geen HTML5 video's"
    },

    post: {
        open_posts: "Laat de berichten zien",
        chars_left_plural: "tekens over",
        chars_left_single: "teken over"
    },

    oq: {
        thanks: "Bedankt voor je antwoord!"
    },

    survey: {
        submit: "Verzenden",
        next: "Volgende vraag"
    },

    curve: {
        submit: "Verzenden",
        next: "Volgende vraag"
    },

    quiz: {
        end: "Quiz afsluiten",
        next: "Volgende vraag"
    },
};
