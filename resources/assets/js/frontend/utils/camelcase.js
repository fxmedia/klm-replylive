String.prototype.toCamelCase = function() {
    return this.trim().toLowerCase().replace(/[^a-zA-Z0-9\s]/g,' ').split(/[\s]{1,}/).map((a, i) => i > 0 ? a.charAt(0).toUpperCase() + a.substr(1) : a ).join('');
};
