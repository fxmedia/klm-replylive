let hasBlobConstructor = typeof(Blob) !== 'undefined' && (function () {
    try {
        return Boolean(new Blob());
    } catch (e) {
        return false;
    }
}());

let hasArrayBufferViewSupport = hasBlobConstructor && typeof(Uint8Array) !== 'undefined' && (function () {
    try {
        return new Blob([new Uint8Array(100)]).size === 100;
    } catch (e) {
        return false;
    }
}());

let hasToBlobSupport = (typeof HTMLCanvasElement !== "undefined" ? HTMLCanvasElement.prototype.toBlob : false);

let hasBlobSupport = (hasToBlobSupport || (typeof Uint8Array !== 'undefined' && typeof ArrayBuffer !== 'undefined' && typeof atob !== 'undefined'));

let hasReaderSupport = (typeof FileReader !== 'undefined' || typeof URL !== 'undefined');

export default class ImageCropping {
    static resize(file, maxDimensions, callback) {
        if (typeof maxDimensions === 'function') {
            callback = maxDimensions;
            maxDimensions = {
                width: 640,
                height: 480,
                orientation: -1
            };
        }
        //
        // let maxWidth  = maxDimensions.width;
        // let maxHeight = maxDimensions.height;

        if (!ImageCropping.isSupported() || !file.type.match(/image.*/)) {
            callback(file, false);
            return false;
        }

        if (file.type.match(/image\/gif/)) {
            // Not attempting, could be an animated gif
            callback(file, false);
            // TODO: use https://github.com/antimatter15/whammy to convert gif to webm
            return false;
        }

        let image = document.createElement('img');

        image.onload = (imgEvt) => {
            let w  = image.width;
            let h = image.height;
            let isTooLarge = false;

            if (w >= h && w > maxDimensions.width) {
                // width is the largest dimension, and it's too big.
                h *= maxDimensions.width / w;
                w = maxDimensions.width;
                isTooLarge = true;
            } else if (h > maxDimensions.height) {
                // either width wasn't over-size or height is the largest dimension
                // and the height is over-size
                w *= maxDimensions.height / h;
                h = maxDimensions.height;
                isTooLarge = true;
            }

            if (!isTooLarge) {
                // early exit; no need to resize
                callback(file, false);
                return;
            }

            let canvas = document.createElement('canvas');
            canvas.width = w;
            canvas.height = h;

            let ctx = canvas.getContext('2d');

            // do orientation!
            if (maxDimensions.orientation > 4) {
                canvas.width = h;
                canvas.height = w;
            }
            switch (maxDimensions.orientation) {
                case 2:
                    // horizontal flip
                    ctx.translate(w, 0);
                    ctx.scale(-1, 1);
                    break;
                case 3:
                    // 180° rotate left
                    ctx.translate(w, h);
                    ctx.rotate(Math.PI);
                    break;
                case 4:
                    // vertical flip
                    ctx.translate(0, h);
                    ctx.scale(1, -1);
                    break;
                case 5:
                    // vertical flip + 90 rotate right
                    ctx.rotate(0.5 * Math.PI);
                    ctx.scale(1, -1);
                    break;
                case 6:
                    // 90° rotate right
                    ctx.rotate(0.5 * Math.PI);
                    ctx.translate(0, -h);
                    break;
                case 7:
                    // horizontal flip + 90 rotate right
                    ctx.rotate(0.5 * Math.PI);
                    ctx.translate(w, -h);
                    ctx.scale(-1, 1);
                    break;
                case 8:
                    // 90° rotate left
                    ctx.rotate(-0.5 * Math.PI);
                    ctx.translate(-w, 0);
                    break;
            }

            ctx.drawImage(image, 0, 0, w, h);

            if (hasToBlobSupport) {
                canvas.toBlob((blob) => {
                    callback(blob, true);
                }, file.type);
            } else {
                let blob = ImageCropping._toBlob(canvas, file.type);
                callback(blob, true);
            }
        };
        ImageCropping._loadImage(image, file);

        return true;
    }

    static _toBlob(canvas, type) {
        let dataURI = canvas.toDataURL(type);
        let dataURIParts = dataURI.split(',');
        let byteString;
        if (dataURIParts[0].indexOf('base64') >= 0) {
            // Convert base64 to raw binary data held in a string:
            byteString = atob(dataURIParts[1]);
        } else {
            // Convert base64/URLEncoded data component to raw binary data:
            byteString = decodeURIComponent(dataURIParts[1]);
        }
        let arrayBuffer = new ArrayBuffer(byteString.length);
        let intArray = new Uint8Array(arrayBuffer);

        for (let i = 0; i < byteString.length; i += 1) {
            intArray[i] = byteString.charCodeAt(i);
        }

        let mimeString = dataURIParts[0].split(':')[1].split(';')[0];
        let blob = null;

        if (hasBlobConstructor) {
            blob = new Blob(
                [hasArrayBufferViewSupport ? intArray : arrayBuffer],
                {type: mimeString}
            );
        } else {
            let bb = new BlobBuilder();
            bb.append(arrayBuffer);
            blob = bb.getBlob(mimeString);
        }

        return blob;
    }

    static _loadImage(image, file, callback) {
        if (typeof(URL) === 'undefined') {
            let reader = new FileReader();
            reader.onload = function(evt) {
                image.src = evt.target.result;
                if (callback) { callback(); }
            };
            reader.readAsDataURL(file);
        } else {
            image.src = URL.createObjectURL(file);
            if (callback) { callback(); }
        }
    };

    static getOrientation(file, callback) {
        if(hasReaderSupport && callback){
            let reader = new FileReader();

            reader.onload = function(event) {
                let view = new DataView(event.target.result);

                if (view.getUint16(0, false) !== 0xFFD8) return callback(-2);

                let length = view.byteLength,
                    offset = 2;

                while (offset < length) {
                    let marker = view.getUint16(offset, false);
                    offset += 2;

                    if (marker === 0xFFE1) {
                        if (view.getUint32(offset += 2, false) !== 0x45786966) {
                            return callback(-1);
                        }
                        let little = view.getUint16(offset += 6, false) === 0x4949;
                        offset += view.getUint32(offset + 4, little);
                        let tags = view.getUint16(offset, little);
                        offset += 2;

                        for (let i = 0; i < tags; i++)
                            if (view.getUint16(offset + (i * 12), little) === 0x0112)
                                return callback(view.getUint16(offset + (i * 12) + 8, little));
                    }
                    else if ((marker & 0xFF00) !== 0xFF00) break;
                    else offset += view.getUint16(offset, false);
                }
                return callback(-1);
            };

            reader.readAsArrayBuffer(file.slice(0, 64 * 1024));
        } else {
            if(callback)callback(-1);
            else return -1;
        }

    }

    static isSupported() {
        return (
            (typeof(HTMLCanvasElement) !== 'undefined')
            && hasBlobSupport
            && hasReaderSupport
        );
    }
}