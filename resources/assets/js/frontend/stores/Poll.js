import { observable, action, computed } from 'mobx';

export default class PollStore {

    @observable data;
    @observable totalAnswers;
    @observable activeAnswer = { selected: null, choice: null};
    @observable isLoading = false;
    @observable showThanks = false;

    constructor(socket, mainStore) {
        this.data = {};
        this.totalAnswers = 0;
        this.mainStore = mainStore;
        this.socket = socket;
        this.listen();
    }

    @action addAnswer(choice) {
        if(!this.data.result[choice]) this.data.result[choice] = 0;
        this.data.result[choice]++;
        this.totalAnswers = this.calcTotalAnswers();
    }

    @action selectAnswer(answer) {
        let activeAnswer = {...this.activeAnswer};
        activeAnswer.selected = answer;
        this.activeAnswer = activeAnswer;
    }

    @action setAnswer(answer) {
        let activeAnswer = {...this.activeAnswer};
        activeAnswer.choice = answer;
        this.activeAnswer = activeAnswer;
        this.showThanks = true;
        this.isLoading = false;
    }

    @action resetAnswer() {
        this.activeAnswer = { selected: null, choice: null };
        this.isLoading = false;
        this.showThanks = false;
    }

    @action calcTotalAnswers() {
        if(!this.data || !this.data.result) return 0;
        return Object.keys(this.data.result).reduce((start, next) => start + this.data.result[next], 0);
    }

    @action start(data) {
        this.data = data;
        this.totalAnswers = this.calcTotalAnswers();
        this.mainStore.setActivePage("poll", true);
    }

    @action stop() {
        this.mainStore.closeTakeover();
        this.mainStore.setActivePage("home");
        this.data = {};
        this.resetAnswer();
    }

    @action submitAnswer() {
        this.isLoading = true;
        this.socket.connection.emit('poll_submit_answer', {choice: this.activeAnswer.selected, poll_id: this.data.id});
        //this.setAnswer(this.activeAnswer.selected);
    }

    @action listen() {
        this.socket.connection.on('poll_start', data => this.start(data));
        this.socket.connection.on('poll_stop', () => this.stop());
        this.socket.connection.on('poll_add_answer', data => this.addAnswer(data));
        this.socket.connection.on('poll_set_answer', answer => this.setAnswer(answer));
        this.socket.connection.on('poll_reset_answer', () => this.resetAnswer());
    }
}
