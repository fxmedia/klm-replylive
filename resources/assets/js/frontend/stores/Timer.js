import { observable, action } from 'mobx'

export default class TimerStore {

    @observable total_seconds
    @observable timestamp
    @observable audio_url

    constructor(socket, mainStore) {
        this.socket = socket;
        this.mainStore = mainStore;
        this.total_seconds = null;
        this.timestamp = null;
        this.audio_url = null;
        this.listen();
    }

    @action startTimer(data) {
        this.total_seconds = data.total_seconds;
        this.timestamp = Date.now();
        this.audio_url = data.audio_url;
        this.mainStore.setActivePage("timer");
    }

    @action stopTimer() {
        this.mainStore.setActivePage("home");
    }

    @action listen() {
        this.socket.connection.on('timer_start', data => this.startTimer(data));
        this.socket.connection.on('timer_stop', data => this.stopTimer(data));
    }
}
