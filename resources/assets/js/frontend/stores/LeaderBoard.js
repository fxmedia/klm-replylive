import { observable, action } from 'mobx'

export default class LeaderBoardStore {

    @observable results

    constructor(socket, mainStore) {
        this.socket = socket;
        this.mainStore = mainStore;
        this.results = [];
        this.listen();
    }

    // @action showLeaderBoard(data) {
    //     console.log("user list: ", data)
    //     .sort((a,b) => parseInt(a.points) < parseInt(b.points))
    //
    //     this.mainStore.setActivePage("lb", true);
    // }

    @action toggleResults(data) {
        if(this.mainStore.activePage == "lb") {
            this.mainStore.setActivePage("quiz");
        } else {
            this.results = data.results;
            this.mainStore.setActivePage("lb");
        }
    }

    @action listen() {
        //this.socket.connection.on('lb_show', data => this.showLeaderBoard(data));
        this.socket.connection.on('quiz_toggle_results', data => this.toggleResults(data));
    }
}
