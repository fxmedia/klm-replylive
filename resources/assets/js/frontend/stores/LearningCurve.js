import { observable, action, computed } from 'mobx'

export default class LearningCurveManagerStore {

    @observable data;
    @observable currentQuestionIdx;
    @observable activeAnswer = { choice: null, selected: null, checked: false };
    @observable prevAnswers = [];
    @observable phase = null;

    constructor(socket, mainStore) {
        this.socket = socket;
        this.mainStore = mainStore;
        this.data = null;
        this.currentAnswers = [];
        this.currentQuestionIdx = 0;
        this.listen();
    }

    @computed get activeQuestion() {
        if(!this.data) return null;

        return this.data.questions[this.currentQuestionIdx];
    }

    @computed get prevAnswer() {
        if(!this.data || !this.prevAnswers) return null;

        return this.prevAnswers[this.currentQuestionIdx];
    }

    @computed get isLastQuestion() {
        if(!this.data) return null;

        return this.currentQuestionIdx == this.data.questions.length - 1;
    }

    @computed get isCorrect() {
        if(!this.activeAnswer.checked) return false;

        return this.activeQuestion.correct_answer == this.activeAnswer.choice;
    }

    @action setAnswerToChecked() {
        let activeAnswer = {...this.activeAnswer};
        activeAnswer.checked = true;
        this.activeAnswer = activeAnswer;
    }

    @action addAnswer(answer) {
        this.currentAnswers.push(answer.choice);
        this.setAnswer(answer);
        if(this.phase == "post") {
            this.setAnswerToChecked();
        } else {
            this.loadNextQuestion();
        }
    }

    @action setAnswer(answer) {
        let activeAnswer = {...this.activeAnswer};
        activeAnswer.choice = answer.choice;
        activeAnswer.selected = null;
        this.activeAnswer = activeAnswer;
    }

    @action loadNextQuestion(data) {
        if(this.isLastQuestion && this.phase == "pre") {
            this.mainStore.closeTakeover();
            this.mainStore.setActivePage("home");
        }
        this.resetAnswer();
        this.currentQuestionIdx++;
    }

    @action selectAnswer(answer) {
        console.log("select answer in store", answer);
        let activeAnswer = {...this.activeAnswer};
        activeAnswer.selected = answer;
        this.activeAnswer = activeAnswer;
    }

    @action setPrevAnswers(answers) {
        this.prevAnswers = answers;
    }

    @action reset() {
        this.activeAnswer = { choice: null, selected: null, checked: false };
        this.prevAnswers = [];
        this.currentAnswers = [];
    }

    @action resetAnswer() {
        this.activeAnswer = { choice: null, selected: null, checked: false };
    }

    @action reboot(data) {
        if(data.question_index) {
            this.currentQuestionIdx = data.question_index;
        }
    }

    @action submitAnswer() {
        this.socket.connection.emit(
            'curve_submit_answer',
            {
                choice: this.activeAnswer.selected,
                learning_curve_question_id: this.activeQuestion.id,
                phase: this.phase,
                curve_id: this.data.id
            }
        );
        this.setAnswer( { choice: this.activeAnswer.selected } );
    }

    @action start(data) {
        console.log("start individual: ", data);
        this.reset();
        if(data.reboot) {
            this.reboot(data.reboot);
        }
        // block reboot if user has already all of the questions
        if(this.currentQuestionIdx >= data.curve.questions.length) return;
        if(data.mine) {
            this.setPrevAnswers(data.mine);
        }
        this.data = data.curve;
        this.phase = data.phase;
        this.mainStore.setActivePage("curve", true);
    }

    @action stop() {
        this.mainStore.closeTakeover();
        this.mainStore.setActivePage("home");
        this.data = null;
        this.currentQuestionIdx = 0;
    }

    @action listen() {
        this.socket.connection.on('curve_start', data => this.start(data));
        this.socket.connection.on('curve_stop', () => this.stop());
        this.socket.connection.on('curve_add_answer', answer => this.addAnswer(answer));
        this.socket.connection.on('curve_reset_answer', () => this.resetAnswer());
    }
}
