import { observable, action } from 'mobx'

export default class DisplayStore {

    @observable activePage;
    @observable settings;
    @observable totalUsers;
    @observable background;

    constructor(socket) {
        this.activePage = "idle";
        this.settings = {};
        this.namespace = "display";
        this.totalUsers = null;
        this.socket = socket;
        this.listen();
    }

    @action closeTakeover() {

    }

    @action setActivePage(page) {
        this.activePage = page;
    }

    @action setSettings(settings) {
        this.settings = settings;
    }

    @action setBackground(background) {
        this.background = background;
    }

    @action setTotalUsers(data) {
        this.totalUsers = data.total;
    }

    @action setDisplay(display) {
        console.log("setting display with settings: ", display);
        this.settings = display.data.settings;
        if(display.page) {
            this.setActivePage(display.page);
        }
    }

    @action setError(data) {
        console.log("error with the display: ", data.message);
    }

    @action listen() {
        this.socket.connection.on('display_set', display => this.setDisplay(display));
        this.socket.connection.on('display_set_page', data => this.setActivePage(data.name));
        this.socket.connection.on('users_set_total', data => this.setTotalUsers(data));
        this.socket.connection.on('display_error', data => this.setError(data));
    }
}
