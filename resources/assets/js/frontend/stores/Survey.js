import { observable, action, computed } from 'mobx'

export default class SurveyStore {

    @observable data;
    @observable currentQuestionIdx;
    @observable activeAnswer = { selected: null, choice: null };
    @observable phase;
    @observable isLoading = false;
    @observable activeOpenAnswer = "";

    constructor(socket, mainStore) {
        this.socket = socket;
        this.mainStore = mainStore;
        this.data = null;
        this.currentQuestionIdx = 0;
        this.phase = null;
        this.backgroundState = [];
        this.listen();
    }

    @computed get activeQuestion() {
        if(!this.data) return null;

        return this.data.questions[this.currentQuestionIdx];
    }

    @computed get isLastQuestion() {
        if(!this.data) return null;

        return this.currentQuestionIdx >= this.data.questions.length - 1;
    }

    @action updateOpenAnswer(answer) {
        this.activeOpenAnswer = answer;
    }

    @action setNextQuestion() {
        this.currentQuestionIdx++;
        this.socket.connection.emit('survey_save_next_question_idx', { survey_id: this.data.id });
    }

    @action addAnswer(answer) {
        if(!this.data.answers) this.data.answers = [];
        this.data.answers.push(answer);
        this.resetAnswer();
        if(this.isLastQuestion ) {
            if (this.data.outro) {
                return this.setPhase("outro");
            }
            else {
                return this.stop();
            }
        }
        this.currentQuestionIdx++;
        if(!this.data.takeover) {
            this.updateBackground();
        }
    }

    @action selectAnswer(answer) {
        let activeAnswer = {...this.activeAnswer};
        activeAnswer.selected = answer;
        this.activeAnswer = activeAnswer;
    }

    @action setAnswer(answer) {
        let activeAnswer = {...this.activeAnswer};
        activeAnswer.choice = answer;
        this.activeAnswer = activeAnswer;
    }

    @action updateBackground() {
        let currentBackgroundIdx = this.backgroundState.findIndex(survey => survey.id == this.data.id);
        if(!this.backgroundState[currentBackgroundIdx].question_index) this.backgroundState[currentBackgroundIdx].question_index = 0;
        this.backgroundState[currentBackgroundIdx].question_index++;
    }

    @action openSurvey(surveyId) {
        const selectedSurvey = this.backgroundState.find(survey => survey.id == surveyId);
        if(!selectedSurvey) return false;

        this.data = selectedSurvey;
        this.currentQuestionIdx = selectedSurvey.question_index || 0;
        this.mainStore.setActivePage("survey");
    }

    @action reboot(data) {
        if(data.question_index) {
            this.currentQuestionIdx = data.question_index;
            this.setPhase("questions");
        }
    }

    @action setPhase(phase) {
        this.phase = phase;
    }

    @action submitAnswer(answer) {
        this.socket.connection.emit(
            'survey_submit_answer',
            {input: answer, survey_question_id: this.activeQuestion.id, survey_id: this.data.id }
        );
        this.isLoading = true;
        //this.activeAnswer = { selected: null, choice: null };
        //this.setAnswer(answer);
    }

    @action startBackground(data) {
        console.log("background: ", data);
        this.backgroundState.push(data);
    }

    @action stopBackground(data) {
        const idx = this.backgroundState.findIndex(item => item.id == data.survey_id);
        let backgroundState = [...this.backgroundState];
        backgroundState.splice(idx, 1);
        this.backgroundState = backgroundState;
    }

    @action rebootBackground(background) {
        console.log("loading old background state: ", background);
        this.backgroundState = background;
    }

    @action resetAnswer() {
        this.activeAnswer = { selected: null, choice: null };
        this.updateOpenAnswer("");
        console.log("resetting answer: ", this.activeOpenAnswer);
        this.isLoading = false;
    }

    @action start(data) {
        if(data.question_index) {
            this.currentQuestionIdx = parseInt(data.question_index);
        }
        if(data.reboot) {
            this.reboot(data.reboot);
        } else if(data.intro) {
            this.setPhase("intro");
        }
        // block reboot if user has already all of the questions
        if(this.currentQuestionIdx >= data.questions.length) return;

        this.data = data;
        this.mainStore.setActivePage("survey", true);
    }

    @action stop() {
        this.mainStore.closeTakeover();
        this.mainStore.setActivePage("home");
        this.data = null;
        this.currentQuestionIdx = 0;
        this.setPhase("questions");
    }

    @action listen() {
        this.socket.connection.on('survey_start', data => this.start(data));
        this.socket.connection.on('survey_start_background', data => this.startBackground(data));
        this.socket.connection.on('survey_stop_background', data => this.stopBackground(data));
        this.socket.connection.on('survey_reboot_background', data => this.rebootBackground(data));
        this.socket.connection.on('survey_stop', () => this.stop());
        this.socket.connection.on('survey_add_answer', answer => this.addAnswer(answer));
        this.socket.connection.on('survey_reset_answer', () => this.resetAnswer());
    }
}
