import { observable, action } from 'mobx'
import mediaPreLoader from '../utils/mediaPreLoader'
import Debug from './../utils/debug'

const preLoadArray = [
    //'http://localhost:8000/media/image/TrfokcQqXF2v9LSggM49/post/1516032662-SampleVideo_1280x720_1mb.mp4-1516032662.gif'
];
const videoPreLoadArray = [];
const cacheVideo = '';

export default class UserStore {

    @observable activePage;
    @observable user;
    @observable closablePage;
    @observable background;
    @observable error;

    constructor(socket, api) {
        this.activePage = "home";
        this.user = {};
        this.showPoints = true;
        this.namespace = "user";
        this.socket = socket;
        this.api = api;
        this.listen();

        mediaPreLoader.loadImage(preLoadArray);
        mediaPreLoader.loadVideo(videoPreLoadArray);
        mediaPreLoader.cacheVideo(cacheVideo);
    }

    @action addPoints(points) {
        if(!this.showPoints) return;
        let user = {...this.user};
        user.extra = (user.extra && !Array.isArray(user.extra)) ? user.extra : { points: 0 };
        const currentPoints = (user.extra.points) ? user.extra.points : 0;
        user.extra.points = currentPoints + points;
        this.user = user;
        //this.socket.connection.emit('user_update', user);
    }

    @action addGroup(data) {
        Debug.logL('Store' ,"adding group..");
        let user = {...this.user};
        if(!user.groups) user.groups = [];
        if(!user.groups.find(group => group.id == data.group_id)) {
            user.groups.push({ id: data.group_id });
            this.user = user;
        }
    }

    @action deleteUser(data) {
        if(this.user.id == data.user_id) {
            Debug.logL('Store' ,"you just got deleted!");
            window.location.reload(true);
        }
    }

    @action removeGroup(data) {
        let user = {...this.user};
        Debug.logL('Store' ,data)
        const groupIdx = user.groups.findIndex(group => group.id == data.group_id);
        if(groupIdx > -1) {
            user.groups.splice(groupIdx, 1);
            this.user = user;
        }
        Debug.logL('Store' ,"user groups: ", user.groups);
    }

    @action setActivePage(page, takeover, closable) {
        this.activePage = page;
        this.closablePage = closable;
        Debug.logL('Store' ,"Switching page to: ", page);
        if(takeover) {
            document.body.classList.add("reply-live-takeover");
        }
    }

    @action setBackground(background) {
        this.background = background;
    }

    @action closeTakeover() {
        document.body.classList.remove("reply-live-takeover");
    }

    @action setTestUser(data) {
        let user = {...this.user};
        user.test_user = data.test_user;
        this.user = user;
        Debug.logL('Store' ,this.user);
    }

    @action setUserAttending(data) {
        let user = {...this.user};
        user.attending = data.attending
        this.user = user;
        Debug.logL('Store', this.user);
    }

    @action setUser(user) {
        Debug.logL('Store',"this user: ", user.data);
        this.user = user.data;
    }

    @action setUserImage(data) {
        let user = {...this.user};
        user.image = data.image;
        this.user = user;
    }

    @action submitPicture(image, cb) {
        const formData = new FormData();
        formData.append('file', image);
        formData.append('user_id', this.user.id);

        // set loader
        this.api.defaults.onUploadProgress = cb;


        /**
         * TODO need error handling from server -> response.success check
         */
        return this.api.post("user/picture", formData)
        .then(res => this.setUserImage(res.data))
        .catch(err => Debug.logL('Store' ,err))
    }

    @action updateUser(user) {
        this.user = user;
        Debug.logL('Store' ,this.user);
    }

    @action userScanned(data) {
        Debug.logL('Store','i got scanned!', data);
    }

    @action setError(data) {
        Debug.logL('Store',"error on the node server: ", data.message);
        this.error = data.message;
        setTimeout(() => this.error = null, 5000);
    }

    @action setOffline() {
        Debug.logL('Store' ,"going offline..");
        location.href = "https://reply.live"; // lol hard coded
    }

    @action listen() {
        this.socket.connection.on('user_add_group', data => this.addGroup(data));
        this.socket.connection.on('user_remove_group', data => this.removeGroup(data));
        this.socket.connection.on('user_set', user => this.setUser(user));
        this.socket.connection.on('user_delete', user => this.deleteUser(user));
        this.socket.connection.on('user_set_test', data => this.setTestUser(data));
        this.socket.connection.on('user_set_attending', data => this.setUserAttending(data));
        this.socket.connection.on('user_update', user => this.updateUser(user));
        this.socket.connection.on('user_scan', data => this.userScanned(data));
        this.socket.connection.on('test_check', data => Debug.logL('Store' ,"Test connection :: ", data));
        this.socket.connection.on('user_add_image', data => this.setUserImage(data));
        this.socket.connection.on('user_offline', () => this.setOffline());
        this.socket.connection.on('user_error', data => this.setError(data));
    }
}
