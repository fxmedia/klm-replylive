import { observable, action } from 'mobx'

export default class PledgeStore {

    @observable pledges = [];

    constructor(socket, mainStore) {
        this.socket = socket;
        this.mainStore = mainStore;
        this.listen();
    }

    @action add(pledge) {
        console.log("adding pledge..", pledge);
        let pledges = [...this.pledges];
        pledges.push(pledge);
        this.pledges = pledges;
    }

    @action fetchAll() {
        this.socket.connection.emit('pledge_fetch_all');
    }

    @action removePledges(data) {
        let pledges = [...this.pledges];
        data.pledge_ids.forEach(pledge_id => {
            const removedPledgeIdx = this.pledges.findIndex(pledge => pledge.id == pledge_id);
            pledges.splice(removedPledgeIdx, 1);
        })
        this.pledges = pledges;
    }

    @action setPledges(pledges) {
        this.pledges = pledges;
    }

    @action submit(pledge) {
        this.socket.connection.emit('pledge_submit', pledge);
    }

    @action listen() {
        this.socket.connection.on('pledge_add', pledge => this.add(pledge));
        this.socket.connection.on('pledge_remove', data => this.removePledges(data));
        this.socket.connection.on('pledge_set_all', pledges => this.setPledges(pledges));
    }
}
