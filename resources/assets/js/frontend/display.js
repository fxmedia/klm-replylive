import Debug from './utils/debug'
Debug.setOptions({
    enabled: true,
    enabledLevels: {
        mediaPreLoader: 'color: purple;',
        Store: 'font-weight:bold;color:brown;'
    }
});
window._Debug = Debug;

import React from 'react';
import ReactDOM from 'react-dom';
import { inject, observer, Provider } from 'mobx-react';

import Header from './display/components/layout/Header';
import PageContainer from './display/pages/PageContainer';
import translations from './translations/en';

import createStores from './stores';

const displayStores = createStores("displays");

@inject('stores', 't') @observer
class App extends React.Component {

    getStyling(bg){
        if(!bg) return "App";
        return `App custom-bg ${bg}`;
    }

    renderPage() {
        const { activePage } = this.props.stores.main;
        if(!activePage) return null;

        const ActivePageComponent = PageContainer[activePage] || PageContainer["home"];
        return <ActivePageComponent />
    }

    render() {
        const { token } = this.props.stores.main.settings;
        const { background } = this.props.stores.main;

        if(!token) return null;

        return (
            <div className={ this.getStyling(background)}>
                <div className="color-overlay" />
                <Header secondaryLogo={false} />
                <div className="body">
                    {this.renderPage()}
                </div>
            </div>
        )
    }
}

ReactDOM.render(
    <Provider stores={displayStores} t={translations}>
        <App />
    </Provider>,
    document.getElementById("root")
);