import React from 'react';
import ConfirmBox from './ConfirmBox';

export default function MultipleChoiceAnswer(props) {

    const getStyling = () => {
        let base = "MultipleChoiceAnswer";

        if(props.isSelected && !props.isConfirmed) {
            base += " active";
        } else if(props.isConfirmed) {
            base += " confirmed";
        } else if (props.isInActive) {
            base += " inactive";
        }
        return base;
    }

    return (
        <li
            className={getStyling()}
            onClick={(props.block || props.isSelected) ? false : props.handleSelect}
        >
            <div className="choice">
                <span>{(props.isConfirmed) ? <i className="fa fa-check"></i> : props.choice}</span>
            </div>
            <a className={"answer"}>
                <p className="text">{props.text}</p>
                <ConfirmBox
                    active={props.isSelected && !props.isConfirmed}
                    confirmAnswer={props.handleConfirm}
                />
            </a>
        </li>
    )
}
