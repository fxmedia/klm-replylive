import React from 'react';

export default (props) => {
  return (
    <div className="ribon__wrap ribon__wrap--20 ribon__wrap--wait">
        <h1>{ props.text }</h1>
    </div>
  )
}
