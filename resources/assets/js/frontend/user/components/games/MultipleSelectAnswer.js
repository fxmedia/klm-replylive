import React from 'react';

export default function MultipleSelectAnswer(props) {
    return (
        <li className={(props.isSelected) ? "MultipleChoiceAnswer active" : "MultipleChoiceAnswer"} onClick={props.handleSelect}>
            <div className="choice">
                <span>{(props.isSelected) ? <i className="fa fa-check"></i> : props.choice}</span>
            </div>
            <a className="answer">
                <p className="text">{props.text}</p>
            </a>
        </li>
    )
}
