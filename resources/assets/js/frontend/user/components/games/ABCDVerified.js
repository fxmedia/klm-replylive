import React from 'react';
import MultipleChoiceVerify from './MultipleChoiceVerify';
import { inject } from 'mobx-react';

@inject("t")
export default class ABCDVerified extends React.Component {

    handleAnswer(isConfirmed) {
        if(!isConfirmed) {
            // cancel current selection
            console.log("cancel current selection");
            return this.props.handleSelect(null);
        }
        this.props.handleAnswer(this.props.activeAnswer.selected);
    }

    handleSelect(answer) {
        this.props.handleSelect(answer);
    }

    render() {
        return (
            <ol className="ABCDVerified">
                {this.props.question.choices.map((answer) =>
                    <MultipleChoiceVerify
                        isSelected={this.props.activeAnswer.selected === answer.choice}
                        handleSelect={this.handleSelect.bind(this, answer.choice)}
                        handleConfirm={this.handleAnswer.bind(this)}
                        activeAnswer={this.props.activeAnswer}
                        correctAnswer={this.props.question.correct_answer}
                        choice={answer.choice}
                        isCorrect={this.props.isCorrect}
                        text={answer.value}
                        key={answer.choice}
                    />
                )}
            </ol>
        )
    }
}
