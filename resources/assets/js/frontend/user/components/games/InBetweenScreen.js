import React from 'react'
import { inject } from 'mobx-react'
import SubmitButton from './SubmitButton'

const InBetweenScreen = inject("t")((props) => {
    return (
        <div className="InBetweenScreen">
            <p>{props.data.content}</p>
            <SubmitButton
                text={props.t.survey.next}
                handleSubmit={props.handleNextQuestion}
            />
        </div>
    )
})

export default InBetweenScreen
