import React from 'react';

export default class InputRange extends React.Component {

    sliderMouseDownEvent(event) {
        let target = event.currentTarget;

        let mouseMove = (ev) => {
            this.move(ev.pageX, target);
        };
        //event.persist();
        mouseMove(event);
        // on mouse down, start listening to mouse move on the whole document,
        // and on mouse up, remove that again
        document.addEventListener('mousemove', mouseMove);
        document.addEventListener('mouseup', function bb() {
            document.removeEventListener('mouseup', bb);
            document.removeEventListener('mousemove', mouseMove);
        });
    }

    sliderTouchStartEvent(event) {
        let target = event.currentTarget;
        event.preventDefault();
        let touchMove = (ev) => {
            ev.preventDefault();
            this.move(ev.touches[0].pageX, target);
        };
        //event.persist();
        touchMove(event);
        // on touch start, start listening to touch move on the whole document,
        // and on touch up, remove that again
        document.addEventListener('touchmove', touchMove);
        document.addEventListener('touchend', function te() {
            document.removeEventListener('touchend', te);
            document.removeEventListener('touchmove', touchMove);
        });
    }

    move(pageX, element) {
        // now we got the cursorX position compared to the document, from either
        // touch or mouse. next step is to compare it to the element
        let elementLeft = element.getBoundingClientRect().left;

        let position = (pageX - elementLeft) / element.offsetWidth;

        if (position < 0) position = 0;
        else if (position > 1) position = 1;
        // what we have now is the cursor position on the slider horizontally from the left
        let max = parseInt(this.props.max);
        let min = parseInt(this.props.min);


        let value = (position * (max - min)) + min;
        if (this.props.roundValues) value = Math.round(value);

        //sliderData.filler.style.width = ;

        // put the value now somwhere in a hidden input field or something...
        this.props.handleInput(value);
    }


    render() {

        const width = (this.props.value - this.props.min) / (this.props.max - this.props.min) * 100 + '%';

        return (
            <div className="slider" onMouseDown={this.sliderMouseDownEvent.bind(this)}
                 onTouchStart={this.sliderTouchStartEvent.bind(this)}>
                <div className="slider-center"/>
                <div className="filler" style={{width}}>
                    <div className="border">
                        <span>{this.props.value}</span>
                    </div>
                </div>
            </div>
        )
    }
}
