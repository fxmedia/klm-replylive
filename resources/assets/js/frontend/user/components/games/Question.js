import React from 'react'

export default (props) => {
  return (
    <div className="Question">
        <p>{props.text}</p>
        {props.children}
    </div>
  )
}
