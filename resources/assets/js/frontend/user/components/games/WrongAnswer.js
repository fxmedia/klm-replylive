import React from 'react';
import config from "../../../config";

export default function WrongAnswerIcon(props) {
  return (
      <div className="AnswerOutcome wrong">
        <img src={`${config.base_url}/images/wrong.png`} />
        <h2>{props.text.header}</h2>
        <p>{props.text.description}</p>
    </div>
  )
}
