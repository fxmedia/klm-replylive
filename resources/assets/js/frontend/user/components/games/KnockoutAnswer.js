import React from 'react'

export default function KnockoutAnswer(props) {
    if(props.activeAnswer && props.activeAnswer.checked && props.choice != props.activeAnswer.choice) return null;
    if(props.activeAnswer && props.activeAnswer.checked && props.choice == props.activeAnswer.choice) {
        return (
            <div className={"KnockoutAnswer correct"} onClick={props.handleSelect}>
                <p className="text">Nice, goedzo!</p>
            </div>
        )
    }

    return (
      <div className={"KnockoutAnswer"} onClick={props.handleSelect}>
          <p className="text">
              <strong>
                  {props.text}
              </strong>
          </p>
      </div>
    )
}
