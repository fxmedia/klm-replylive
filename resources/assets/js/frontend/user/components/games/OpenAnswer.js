import React from 'react';
import { inject } from 'mobx-react';

import SubmitButton from './SubmitButton';
import Error from '../Error';

@inject("t")
export default class OpenAnswer extends React.Component {

    handleInput(e) {
        if(this.props.limit && e.target.value.length > this.props.limit) {
            return false;
        }
        this.props.handleChange(e.target.value);
    }

    render() {
        return (
            <div className="OpenAnswer">
                <textarea value={this.props.value} onChange={this.handleInput.bind(this)} placeholder={this.props.t.survey.open_answer_placeholder} />
                {(this.props.limit)
                    ? <p className="chars-left">
                        {this.props.limit - this.props.value.length}
                        &nbsp;
                        {((this.props.limit - this.props.value.length) == 1)
                            ? this.props.t.general.chars_left_single
                            : this.props.t.general.chars_left_plural
                        }
                    </p>
                    : null
                }
                <SubmitButton
                    text={(this.props.isLast) ? this.props.t.survey.submit : this.props.t.survey.next}
                    handleSubmit={this.props.handleAnswer}
                    disabled={!this.props.value || this.props.value.length < 1}
                />
            </div>
        )
    }
}
