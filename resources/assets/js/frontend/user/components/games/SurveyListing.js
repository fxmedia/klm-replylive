import React from 'react';

export default function SurveyListing(props) {
    return (
        <div className="SurveyListing" onClick={props.handleClick}>
            <h2>{props.data.name}</h2>
            <p>Nice survey, click to continue!</p>
        </div>
    )
}
