import React from 'react'
import ConfirmBox from './ConfirmBox'

export default function MultipleChoiceVerify(props) {

    const renderStyling = () => {
        if(props.activeAnswer.selected && props.activeAnswer.selected !== props.choice) {
            return "MultipleChoiceAnswer inactive";
        } else {
            return "MultipleChoiceAnswer";
        }
    }

    return (
      <li className={renderStyling()} onClick={props.handleSelect}>
          <div className="choice">
              <span>{props.choice}</span>
          </div>
          <a className={"answer"}>
              <p className="text">{props.text}</p>
          </a>
      </li>
    )
}
