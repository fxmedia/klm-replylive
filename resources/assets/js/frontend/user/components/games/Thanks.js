import React from 'react'

export default function Thanks(props) {
    if(!props.show) return null;

    return (
        <div className="Thanks text-wrapper">
            <h2>{props.message}</h2>
        </div>
    )
}
