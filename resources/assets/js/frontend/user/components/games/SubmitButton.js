import React from 'react';

export default function SubmitButton(props) {
    if(props.hide) return null;
    
    return (
        <div
            className={(props.disabled) ? "SubmitButton disabled" : "SubmitButton"}
            onClick={(!props.disabled) ? props.handleSubmit : false}>
            <span>{props.text}</span>
        </div>
    )
}
