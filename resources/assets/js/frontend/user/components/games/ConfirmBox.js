import React from 'react';
import { inject } from 'mobx-react';

const ConfirmBox = inject("t")((props) => {
    return (
        <div className="ConfirmBox">
            <div className="confirm-choice decline" onClick={props.confirmAnswer.bind(null, false)}>
                <p className="icon icon-decline">{props.t.curve.confirm_decline}</p>
            </div>
            <div className="confirm-choice confirm" onClick={props.confirmAnswer.bind(null, true)}>
                <p className="icon icon-confirm">{props.t.curve.confirm_confirm}</p>
            </div>
        </div>
    )
});

export default ConfirmBox
