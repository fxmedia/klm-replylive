import React from 'react';

export default function Points(props) {
    return (
        <div className="Points">
            <p>Total points: {props.total}</p>
        </div>
    )
}
