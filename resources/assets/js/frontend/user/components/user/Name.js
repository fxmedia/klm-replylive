import React from 'react';

export default function UserName({ user }) {
    if(!user) return null;

    return (
        <p>{user.firstname} {user.lastname}</p>
    )
}
