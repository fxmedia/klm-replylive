class Snackbar extends React.Component {
    constructor(props) {
        super(props)
        this.state = { show: true }
    }

    componentWillMount() {
        if(localStorage.getItem("firstVisit") && localStorage.getItem("firstVisit") == 1) {
            this.setState({ show: false });
        }
    }

    componentDidMount() {
        localStorage.setItem("firstVisit", 1);
    }

    handleClose() {
        this.setState({ show: false });
    }

    render() {
        if(!this.state.show) return null;

        return (
            <div className="snackbar" onClick={this.handleClose.bind(this)}>
                <p>
                    {this.props.message}
                </p>
                <div className="close-snackbar">X</div>
            </div>
        )
    }
}

export default Snackbar
