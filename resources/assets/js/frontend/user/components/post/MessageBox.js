import React from 'react';

export default function MessageBox(props) {

    const getCharsLeft = () => {
        return props.limit - props.message.length;
    }

    const handleChange = (e) => {
        if(e.target.value.length > props.limit) {
            const shortened = e.target.value.substring(0, props.limit);
            props.handleChange(shortened);
        } else {
            props.handleChange(e.target.value);
        }
    }

    const charsLeft = getCharsLeft();

    return (
        <div className="MessageBox">
            <input type="text" onChange={handleChange} value={props.message} placeholder={props.placeholder} />
            <p className="chars-left">
                {charsLeft}&nbsp;
                {(charsLeft != 1)
                    ? props.t.post.chars_left_plural
                    : props.t.post.chars_left_single
                }
            </p>
        </div>
    )
}
