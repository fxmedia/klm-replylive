import React from 'react';
import { inject, observer } from 'mobx-react';
import config from '../../../config';
import ImageUpload from '../utils/ImageUpload';
import MessageBox from './MessageBox';

@inject("stores", "t") @observer
export default class PostForm extends React.Component {

    constructor(props) {
        super(props);
        this.state = {
            message: "",
            image: "",
            progress: 0,
            loading: false,
            image_path: '',
            isVideo: false
        };
    }

    handleChange(key, value) {
        this.setState({ [key]: value });
    }

    handleSubmit(e) {
        e.preventDefault();
        if(!this.state.message && !this.state.image) return false;
        this.props.stores.post.submitPost(this.state);

        this.setState({ message: "", image: "", image_path: "" });
    }

    handleProgress(e) {
        if(e && e.lengthComputable) {
            const progress = (e.loaded / e.total) * 100;
            this.setState({ progress })
        }
    }

    handleClosePreview() {
        console.log("closing preview..");
        this.setState({ image: "", image_path: "" });
    }

    /**
     * TODO needs response error handling --> response.success
     * @param file
     * @param isVideo
     */
    handleUpload(file, isVideo) {
        this.setState({ loading: true, progress: 0 });
        if(isVideo) {
            this.setState({ image: URL.createObjectURL(file), isVideo });
            //setTimeout(() => document.getElementById("video-preview").load(), 2500);
            this.props.stores.post
                .submitFile(config.video_endpoint, file, this.handleProgress.bind(this))
                .catch(err => {
                    this.setError();
                })
                .then(res => {
                    if(res && res.data.success) {
                        this.setState({ image: res.data.gif, image_path: res.data.image_path, loading: false, isVideo: false })
                    } else {
                        this.setError();
                    }
                })
        } else {
            this.props.stores.post
                .submitFile(config.image_endpoint, file, this.handleProgress.bind(this))
                .catch(err => {
                    this.setError();
                })
                .then(res => {
                    if(res && res.data.success) {
                        this.setState({ image: res.data.image, image_path: res.data.image_path, loading: false, isVideo })
                    } else {
                        this.setError();
                    }
                })
        }
    }

    setError() {
        this.props.stores.main.setError({ message: this.props.t.errors.file_too_big })
        this.setState({
            image: "",
            progress: 0,
            loading: false,
            image_path: "",
            isVideo: false
        });
    }

    render() {
        return (
            <form className="PostForm" onSubmit={this.handleSubmit.bind(this)}>
                <ImageUpload
                    allowed={config.allowedFileTypes}
                    store={this.props.stores.post}
                    preview={true}
                    isVideo={this.state.isVideo}
                    image={this.state.image_path}
                    loading={this.state.loading}
                    progress={this.state.progress}
                    handleUpload={this.handleUpload.bind(this)}
                    handleClosePreview={this.handleClosePreview.bind(this)}
                />
                <div className="input-container">
                    <MessageBox
                        handleChange={this.handleChange.bind(this, "message")}
                        message={this.state.message}
                        limit={config.maxMessageSize}
                        t={this.props.t}
                        placeholder={this.props.t.post.form_placeholder}
                    />
                    <div className="submit-wrapper" onClick={this.handleSubmit.bind(this)}>
                        <div className="submit-icon"></div>
                    </div>
                </div>
                <input type="submit" />
            </form>
        )
    }
}
