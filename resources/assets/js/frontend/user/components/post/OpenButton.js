import React from 'react'
import config from "../../../config";

export default function OpenButton(props) {
    return (
        <div className="OpenButton" onClick={props.handleOpen}>
            <div className="body">
                <div className="icon-wrapper">
                    <img src={`${config.base_url}/images/${props.icon}`} />
                </div>
                <div className="text-wrapper">
                    <span>{props.t.post.open_posts}</span>
                </div>
            </div>
        </div>
    )
}
