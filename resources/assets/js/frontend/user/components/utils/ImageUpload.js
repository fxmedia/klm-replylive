import React from 'react';
import { inject } from 'mobx-react';

import ErrorMessage from './ErrorMessage';
import Image from './Image';
import Loader from './Loader';
import ImageCropping from '../../../utils/ImageCropping';
import config from '../../../config';

@inject("stores", "t")
export default class ImageUpload extends React.Component {

    constructor(props) {
        super(props);
        this.videoTypes = ["video/quicktime", "video/mp4", "video/webm", "video/ogg", "video/mpeg"];
        this.state = { error: null };
    }

    handleSubmit(e) {
        e.preventDefault();
        e.target.submit();
    }

    handleChange(e) {
        this.setState({ error: null });
        const file = e.target.files[0];
        if(!file || this.props.allowed.indexOf(file.type) === -1) {
            return this.props.stores.main.setError({message: this.props.t.errors.wrong_file_type});
        }
        if(this.videoTypes.indexOf(file.type) > -1) {
            this.props.handleUpload(file, true);
        } else {
            if(ImageCropping.isSupported()){
                ImageCropping.getOrientation(file,(orientation)=>{
                    ImageCropping.resize(file,{
                        width: config.otherOptions.upload_max_size || 640,
                        height: config.otherOptions.upload_max_size || 640,
                        orientation: orientation
                    }, (blob) => {
                        this.props.handleUpload(blob, false);
                    })
                });
            } else {
                this.props.handleUpload(file, false);
            }
        }
    }

    renderError() {
        if(!this.state.error) return null;

        return <ErrorMessage text={this.state.error} />
    }

    renderPreview() {
        if(!this.props.preview || (!this.props.loading && !this.props.image)) return null;

        if(this.props.isVideo) {
            return (
                <div className="preview">
                    <div className="image-wrapper">
                        <video id="video-preview" width="400">
                          <source src={this.props.image} />
                          {this.props.t.errors.no_video_support}
                        </video>
                        <div className="delete-preview" onClick={this.props.handleClosePreview}></div>
                    </div>
                    {(this.props.loading)
                        ? <div className="post-asset-loader-wrapper">
                            <Loader progress={this.props.progress} />
                          </div>
                        : null
                    }
                </div>
            )
        }

        return (
            <div className="preview">
                <div className="image-wrapper">
                    <div className="image-with-icon">
                        {this.props.image ? <img src={this.props.image} /> : null}
                        <div className="delete-preview" onClick={this.props.handleClosePreview}></div>
                    </div>
                </div>
                {(this.props.loading)
                    ? <div className="post-asset-loader-wrapper">
                        <Loader progress={this.props.progress} />
                      </div>
                    : null
                }
            </div>
        );
    }

    render() {
        return(
            <div className="ImageUpload">
                <div className="input-wrapper">
                    <i className="upload-icon fa fa-camera"></i>
                    <input type="file" name="video" onChange={this.handleChange.bind(this)} />
                </div>
                {this.renderPreview()}
                {this.renderError()}
            </div>
        )
    }
}
