import React from 'react';
import { Circle } from 'rc-progress';

export default function Loader(props) {
    return (
        <div className="PostLoader">
            <Circle percent={props.progress} strokeWidth="2" strokeColor="#f00" />
            <div className="percent-wrapper">
                <span>{Math.floor(props.progress)}</span>
            </div>
        </div>
    )
}
