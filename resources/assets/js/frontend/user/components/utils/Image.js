import React from 'react';
import { inject } from 'mobx-react';
import config from '../../../config';

@inject("stores")
export default class Image extends React.Component {

    render() {
        if(!this.props.filename) return null;

        return <img src={ config.base_url + this.props.filename } />
    }

}
