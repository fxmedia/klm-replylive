import React from 'react';
import { inject, observer } from 'mobx-react';

import MultipleChoiceAnswer from '../components/games/MultipleChoiceAnswer';
import Question from '../components/games/Question';
import Thanks from '../components/games/Thanks';
import Loader from '../components/Loader';
import SubmitButton from '../components/games/SubmitButton';
import ProgressBar from '../components/games/ProgressBar';

import RightAnswer from '../components/games/RightAnswer';
import WrongAnswer from '../components/games/WrongAnswer';


@inject('stores', 't') @observer
export default class QuizPage extends React.Component {

    handleConfirm(isConfirmed) {
        if(!isConfirmed) {
            this.props.stores.quiz.selectAnswer(null);
        } else {
            this.props.stores.quiz.submitAnswer();
        }
    }

    handleSelect(answer) {
        this.props.stores.quiz.selectAnswer(answer);
    }

    handleNextQuestion() {
        if(this.props.stores.quiz.isLastQuestion) {
            this.props.stores.main.closeTakeover();
            this.props.stores.main.setActivePage("home");
        } else {
            this.props.stores.quiz.loadNextQuestion();
        }
    }

    renderRightWrong(isCorrect) {
        return (isCorrect)
        ? <RightAnswer text={this.props.t.quiz.right} />
        : <WrongAnswer text={this.props.t.quiz.wrong} />
    }

    render() {
        const {
            activeQuestion,
            currentQuestionIdx,
            data,
            isLastQuestion,
            activeAnswer,
            isCorrect,
            isLoading,
            showThanks
        } = this.props.stores.quiz;

        if(!activeQuestion || !data) return <p>No Quiz Found</p>;
        if(activeAnswer.checked) return this.renderRightWrong(isCorrect);

        return (
            <div className="QuizPage game-wrapper">
                <ProgressBar currentNum={currentQuestionIdx} totalNum={data.questions.length} />
                <Question text={activeQuestion.title} />
                <ol className={"ABCD"}>
                    {activeQuestion.choices.map(answer =>
                        <MultipleChoiceAnswer
                            isSelected={activeAnswer.selected == answer.choice}
                            isConfirmed={activeAnswer.choice == answer.choice}
                            handleSelect={this.handleSelect.bind(this, answer.choice)}
                            choice={answer.choice}
                            text={answer.value}
                            key={answer.choice}
                            block={activeAnswer.choice != null}
                            handleConfirm={this.handleConfirm.bind(this)}
                            isInActive={activeAnswer.choice && activeAnswer.choice != answer.choice}
                        />
                    )}
                </ol>
                <Thanks show={showThanks} message={this.props.t.quiz.thanks}/>
                {(isLoading) ? <Loader /> : null}
            </div>
        )
    }
}
