import React from 'react';
import { inject, observer } from 'mobx-react';

import KnockoutAnswer from '../components/games/KnockoutAnswer';
import Question from '../components/games/Question';
import Image from '../components/utils/Image';

import RightAnswer from '../components/games/RightAnswer';
import WrongAnswer from '../components/games/WrongAnswer';
import SubmitButton from '../components/games/SubmitButton';

@inject('stores', 't') @observer
export default class KnockoutPage extends React.Component {

    constructor(props) {
        super(props);
        this.state = { deciding_answer: "", deciding_confirm: false };
    }

    handleDecidingAnswer(e) {
        this.setState({ deciding_answer: e.target.value });
    }

    handleDecidingSubmit() {
        if(!this.state.deciding_confirm) {
            this.props.stores.knockout.submitDecidingAnswer(this.state.deciding_answer);
        }
        this.setState({ deciding_confirm: true });
        this.props.stores.main.setActivePage("waiting");
    }

    handleSelect(input) {
        this.props.stores.knockout.submitAnswer(input);
    }

    renderDecidingQuestion() {
        const { deciding_question } = this.props.stores.knockout.data;

        return (
            <div className="KnockoutPage game-wrapper">
                <Question text={deciding_question} />
                <div className="deciding-question">
                    <input type="number"
                        onChange={this.handleDecidingAnswer.bind(this)}
                        value={this.state.deciding_answer}
                    />
                    <SubmitButton
                        handleSubmit={this.handleDecidingSubmit.bind(this)}
                        text={this.props.t.knockout.submit}
                    />
                </div>
            </div>
        )
    }

    renderRightWrong(knockedOut) {
        return (knockedOut)
        ? <WrongAnswer text={this.props.t.knockout.wrong} />
        : <RightAnswer text={this.props.t.knockout.right} />
    }


    renderWinner(winner) {
        if(!winner) return null;

        if(winner.find(user => user.id == this.props.stores.main.user.id)) {
            return (
                <div className="KnockoutPage game-wrapper">
                    <div className="outcome-wrapper">
                        <Image filename="/images/trophy.png" />
                        <h2>{this.props.t.knockout.congratulations}</h2>
                        <p>{this.props.t.knockout.win_challenge}</p>
                    </div>
                </div>
            )
        } else {
            return (
                <div className="KnockoutPage game-wrapper">
                    <div className="outcome-wrapper">
                        <Image filename="/images/lost.png" />
                        <h2>{this.props.t.knockout.lost}</h2>
                        <p>{this.props.t.knockout.lost_challenge}</p>
                    </div>
                </div>
            )
        }
    }

    render() {
        const {
            activeQuestion,
            knockedOut,
            activeAnswer,
            phase,
            winner
        } = this.props.stores.knockout;
        if(activeAnswer.checked) return this.renderRightWrong(knockedOut);
        if(phase == "deciding_question") return this.renderDecidingQuestion();
        if(phase == "winner") return this.renderWinner(winner);
        if(!activeQuestion) return <p>No Knockout Found</p>;

        console.log("new active answer: ", activeAnswer.choice)

        return (
            <div className={(activeAnswer.choice) ? `KnockoutPage ${activeAnswer.choice} game-wrapper` : "KnockoutPage game-wrapper"}>
                <Question text={activeQuestion.title} />
                <div className="answers">
                    {activeQuestion.choices.map(answer =>
                        <KnockoutAnswer
                            key={answer.choice}
                            choice={answer.choice}
                            text={answer.value}
                            handleSelect={this.handleSelect.bind(this, answer.choice)}
                            activeAnswer={activeAnswer}
                            t={this.props.t.knockout.answer}
                        />
                    )}
                </div>
            </div>
        )
    }
}
