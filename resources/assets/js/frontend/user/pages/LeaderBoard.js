import React from 'react';
import { inject, observer } from 'mobx-react';

@inject("stores") @observer
export default class LeaderBoardPage extends React.Component {
    render() {
        const { users } = this.props.stores.lb;
        return (
            <div className="LeaderBoardPage game-wrapper">
                {users.map(user => <div key={user.firstname}>{user.firstname} {user.lastname} - {user.points}</div>)}
            </div>
        )
    }
}
