import React from 'react';
import { inject, observer } from 'mobx-react';

@inject('t') @observer
export default class Waiting extends React.Component {
    render() {
        return (
            <div className="WaitingPage">
                <div className="loader-wrapper">
                    <div className="lds-css ng-scope"><div style={{width:"100%", height: "100%"}} className="lds-ripple"><div></div><div></div></div>
                    </div>
                    <span>{this.props.t.waiting.please_wait}</span>
                </div>
            </div>
        )
    }
}
