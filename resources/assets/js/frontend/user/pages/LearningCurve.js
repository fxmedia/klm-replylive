import React from 'react';
import { inject, observer } from 'mobx-react';

import ABCDCurve from '../components/games/ABCDCurve';
import SubmitButton from '../components/games/SubmitButton';
import Question from '../components/games/Question';
import ProgressBar from '../components/games/ProgressBar';

@inject('stores', 't') @observer
export default class LearningCurvePage extends React.Component {

    constructor(props) {
        super(props);
        this.state = { showScore: false };
    }

    calcScore() {
        const { prevAnswers, currentAnswers, data } = this.props.stores.curve;
        let score = { prev: 0, current: 0 };
        data.questions.forEach((question, idx) => {
            if(question.correct_answer == prevAnswers[idx]) {
                score.prev++;
            }
            if(question.correct_answer == currentAnswers[idx]) {
                score.current++;
            }
        })
        return score;
    }

    handleAnswer(isConfirmed) {
        if(isConfirmed) {
            this.props.stores.curve.submitAnswer();
        } else {
            this.props.stores.curve.selectAnswer(null);
        }
    }

    handleSelect(input) {
        console.log("input: ", input)
        this.props.stores.curve.selectAnswer(input);
    }

    handleNextQuestion() {
        if(this.props.stores.curve.isLastQuestion && this.props.stores.curve.phase == "post") {
            this.setState({ showScore: true });
        } else {
            this.props.stores.curve.loadNextQuestion();
        }
    }

    renderAnswers(phase) {
        const { activeQuestion, isLastQuestion, isCorrect, activeAnswer } = this.props.stores.curve;
        if(!activeQuestion) return null;

        return (
            <ABCDCurve
                handleSelect={this.handleSelect.bind(this)}
                handleAnswer={this.handleAnswer.bind(this)}
                question={activeQuestion}
                activeAnswer={activeAnswer}
                isCorrect={isCorrect}
                isLast={isLastQuestion}
                phase={phase}
            />
        )
    }

    renderPrevAnswer(prevAnswer) {
        if(!prevAnswer) return null;

        const { activeAnswer, activeQuestion } = this.props.stores.curve;

        let styling = "prevAnswer";
        if(activeAnswer.checked && prevAnswer == activeQuestion.correct_answer) {
            styling += " correct";
        } else if(activeAnswer.checked && prevAnswer !== activeQuestion.correct_answer) {
            styling += " wrong";
        }

        return (
            <div className={styling}>
                <p>Previous answer: <span>{prevAnswer}</span></p>
            </div>
        )
    }

    renderScore() {
        const score = this.calcScore();
        const totalQuestions = this.props.stores.curve.data.questions.length;

        return (
            <div className="ScoreBoard game-wrapper">
                <div className="text-wrapper">
                    <h2>{this.props.t.curve.results}</h2>
                    <p>{this.props.t.curve.results_intro}</p>
                </div>
                <div className="score-container">
                    <div className="score current">
                        <h3>{this.props.t.curve.current_score}</h3>
                        <span>{score.current} / {totalQuestions}</span>
                    </div>
                    <div className="score previous">
                        <h3>{this.props.t.curve.previous_score}</h3>
                        <span>{score.prev} / {totalQuestions}</span>
                    </div>
                </div>
                <SubmitButton
                    text={this.props.t.general.go_home}
                    handleSubmit={() => this.props.stores.main.closeTakeover()}
                />
            </div>
        )
    }

    render() {
        const {
            activeQuestion,
            currentQuestionIdx,
            data,
            isLastQuestion,
            prevAnswer,
            phase,
            activeAnswer
        } = this.props.stores.curve;

        if(!activeQuestion || !data) return null;

        if(this.state.showScore) return this.renderScore();

        return (
            <div className={`LearningCurvePage game-wrapper ${phase}`}>
                <ProgressBar currentNum={currentQuestionIdx} totalNum={data.questions.length} />
                <Question text={activeQuestion.title} />
                {this.renderPrevAnswer(prevAnswer)}
                {this.renderAnswers(phase)}
                <SubmitButton
                    hide={phase == "pre"}
                    disabled={!activeAnswer.checked}
                    text={(!isLastQuestion) ? this.props.t.curve.next : this.props.t.curve.results}
                    handleSubmit={(activeAnswer.checked) ? this.handleNextQuestion.bind(this) : false}
                />
            </div>
        )
    }
}
