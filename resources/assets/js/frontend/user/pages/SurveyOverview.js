import React from 'react';
import { inject, observer } from 'mobx-react';

import SurveyListing from '../components/games/SurveyListing';

@inject('stores', 't') @observer
export default class SurveyOverviewPage extends React.Component {

    openSurvey(idx) {
        this.props.stores.survey.openSurvey(idx);
    }

    render() {
        const { backgroundState } = this.props.stores.survey;

        if(!backgroundState) return null;

        console.log(backgroundState)

        return (
            <div className="SurveyOverviewPage game-wrapper">
                <h1>Survey Overview</h1>
                {backgroundState
                    .filter((survey, idx) => survey.question_index < survey.questions.length)
                    .map((survey, idx) =>
                    <SurveyListing data={survey} key={survey.id} handleClick={this.openSurvey.bind(this, survey.id)} />
                )}
            </div>
        )
    }
}
