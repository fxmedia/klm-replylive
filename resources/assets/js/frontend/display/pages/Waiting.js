import React from 'react';

export default function WaitingPage(props) {
    return (
        <div className="wrapper wrapper--waiting">
            <p>Waiting</p>
        </div>
    )
}
