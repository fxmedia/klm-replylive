import React from 'react';
import { inject, observer } from 'mobx-react';

import KnockoutAnswer from '../components/games/KnockoutAnswer';
import Question from '../components/games/Question';
import GameLabel from '../components/games/Label';
import Image from '../components/utils/Image';


@inject('stores', 't') @observer
export default class KnockoutPage extends React.Component {

    // renderWinnersLosers(players_left, final) {
    //     if(!final) return null;
    //
    //     return (
    //         <div className="final">
    //             <div className="overview winners">
    //                 <h2>Winners</h2>
    //                 {final.winners.map(winner => <p key={winner.id}>{winner.firstname}</p>)}
    //             </div>
    //             <div className="overview losers">
    //                 <h2>Losers</h2>
    //                 {final.losers.map(loser => <p key={loser.id}>{loser.firstname}</p>)}
    //             </div>
    //         </div>
    //     )
    // }

    renderNoWinners() {
        // should do something on the body as well to fill whole background with red overlay....

        return (
            <div className="KnockoutPage game-wrapper">
                <div className="center-vertical">
                    <div className="outcome-header">
                        <div className="game-icon">
                            <Image filename="/images/cross.png" />
                        </div>
                        <div className="title-wrapper">
                            <h1>Game over!<br/>All players were eliminated</h1>
                        </div>
                    </div>
                </div>
            </div>
        )
    }

    renderWinner(winner) {
        if(!winner) return null;

        return (
            <div className="KnockoutPage game-wrapper">
                <div className="outcome-header">
                    <div className="game-icon">
                        <Image filename="/images/trophy-display.png" />
                    </div>
                    <div className="title-wrapper">
                        <h1>
                            {(winner.length == 1)
                                ? this.props.t.knockout.we_have_winner
                                : this.props.t.knockout.multiple_winners
                            }
                        </h1>
                    </div>
                </div>
                {winner.map(user =>
                    <div className="winner-wrapper">
                        <div className="avatar" style={{ backgroundImage: `url(${user.image_path || "/images/avatar.png"})` }} />
                        <div className="name">
                            <p>{ user.name }</p>
                        </div>
                    </div>
                )}

            </div>
        )
    }

    renderDecidingQuestion() {
        const { data } = this.props.stores.knockout;

        return (
            <div className="KnockoutPage game-wrapper">
                <Question text={data.deciding_question} />
                <div className="text-wrapper">
                    <p>{this.props.t.knockout.answer_mobile}</p>
                </div>
            </div>
        )
    }

    render() {
        const { activeQuestion, activeAnswer, final, phase, winner, players_left } = this.props.stores.knockout;

        if(phase === "deciding_question") return this.renderDecidingQuestion();
        if(phase === "winner") return this.renderWinner(winner);

        if(final && players_left === 0){
            return this.renderNoWinners();
        }

        if(!activeQuestion) return <p>Knockout not found</p>;

        const playerTotal = `${players_left} ${this.props.t.knockout.players_left}`;

        return (
            <div className={(activeAnswer.checked) ? `KnockoutPage checked game-wrapper` : "KnockoutPage game-wrapper"}>
                <GameLabel text={playerTotal} />
                <Question text={activeQuestion.title} />
                <div className={(activeQuestion.choices.length > 2) ? "answers-container many-answers" : "answers-container"}>
                    <ol className="answers">
                        {activeQuestion.choices.map(answer =>
                            <KnockoutAnswer
                                answer={answer.value}
                                choice={answer.choice}
                                key={answer.choice}
                                correct={answer.choice === activeQuestion.correct_answer}
                            />
                        )}
                    </ol>
                </div>
            </div>
        )
    }
};
