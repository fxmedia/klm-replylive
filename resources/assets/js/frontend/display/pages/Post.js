import React                from 'react';
import { inject, observer } from 'mobx-react';
import Masonry              from 'react-masonry-component';
import Post                 from '../components/post/Post';
import PostHighlight        from '../components/post/PostHighlight';

@inject("stores", "t") @observer
export default class PostPage extends React.Component {
    componentDidMount() {
        this.props.stores.post.fetchAll();
    }

    handleHighlight(post_id) {
        this.props.stores.post.highlightPost({ post_id });
    }

    renderHighlight(highlight) {
        if(!highlight) return null;
        const post = this.props.stores.post.feed.find(post => post.id == highlight);

        return <PostHighlight data={post} handleClick={this.handleHighlight.bind(this, post.id)} highlight={true} />
    }

    render() {
        const masonryOptions = {
            fitWidth: true
        }
        const { highlight, feed } = this.props.stores.post;
        return (
            <div className="PostPage">
                <div className={(highlight) ? "posts-container highlight" : "posts-container"}>
                    <Masonry
                        options={masonryOptions}
                    >
                        {feed
                            .filter(post => post.visible)
                            .reverse()
                            .map(post =>
                                <Post
                                    key={post.id}
                                    data={post}
                                    handleClick={(highlight) ? false : this.handleHighlight.bind(this, post.id)}
                                />
                            )
                        }
                    </Masonry>
                </div>
                {this.renderHighlight(highlight)}
            </div>
        )
    }
}
