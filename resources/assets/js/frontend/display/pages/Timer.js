import React from 'react';
import { inject, observer } from 'mobx-react';

@inject("stores") @observer
export default class TimerPage extends React.Component {

    constructor(props) {
        super(props);
        this.state = { timestamp: null, remaining: 0 }
        this.update = null;
        this.audio = null;
    }

    componentDidMount() {
        this.startTimer();
        const { timestamp } = this.props.stores.timer;
        this.setState({ timestamp });
    }

    componentWillUnmount() {
        if(this.audio) {
            this.audio.pause();
        }
        clearInterval(this.update);
    }

    componentDidUpdate() {
        // old timer
        if(this.props.stores.timer.timestamp !== this.state.timestamp) {
            clearInterval(this.update);
            if(this.audio) {
                this.audio.pause();
            }
            this.setState({ timestamp: this.props.stores.timer.timestamp });
            this.startTimer();
        }
    }

    parseTimeLeft(time) {
        const seconds = time % 60;
        const minutes = Math.floor((time / 60) % 60);
        return (
            <span>
                {(minutes.toString().length > 1) ? minutes : "0" + minutes }:
                {(seconds.toString().length > 1) ? seconds : "0" + seconds}
            </span>
        )
    }

    startTimer() {
        const start = Date.now();
        const { total_seconds, audio_url } = this.props.stores.timer;
        if(audio_url) {
            this.audio = new Audio(audio_url);
            this.audio.play();
        }
        this.update = setInterval(() => {
            const totalTime = parseInt(total_seconds);
            var delta = Date.now() - start; // milliseconds elapsed since start
            var elapsed = (Math.floor(delta / 1000)); // in seconds
            var remaining = totalTime - elapsed;

            if(remaining < 0) {
                clearInterval(this.update);
                this.setState({ remaining: 0 });
                if(this.audio) {
                    this.audio.pause();
                }
            } else {
                this.setState({ remaining });
            }
        }, 250); // update about every second
    }

    render() {
        if(!this.state.timestamp) return null;

        return (
            <div className="TimerPage">
                <div className="timer-wrapper">
                    <div className="lds-css ng-scope"><div style={{width:"100%", height: "100%"}} className="lds-ripple"><div></div><div></div></div>
                    </div>
                    <span className="timer">{this.parseTimeLeft(this.state.remaining)}</span>
                </div>
            </div>
        )
    }
}
