import React from 'react';
import { inject, observer } from 'mobx-react';

import AnswerTotal from '../components/games/AnswerTotal';
import OpenQuestionAnswer from '../components/games/OpenQuestionAnswer';
import Question from '../components/games/Question';

@inject('stores', 't') @observer
export default class OpenQuestionPage extends React.Component {

    constructor(props) {
        super(props);
        this.FADE_LIMIT = 4;
        this.positions = [];
    }

    handleHighlight(id) {
        const { highlight } = this.props.stores.oq;
        if(highlight != id) {
            this.props.stores.oq.highlightAnswer({ open_question_answer_id: id });
        } else {
            this.props.stores.oq.highlightAnswer({open_question_answer_id: null });
        }
    }

    shouldAnswerFade(idx, total) {
        return ((idx < total - this.FADE_LIMIT) && (total > this.FADE_LIMIT))
    }

    getHighlightData(id, result) {
        if(!id) return null;

        return result.find(answer => answer.id == id);
    }

    buildZones(result) {
        let zones = [[],[],[],[]];
        let currentZone = 0;
        for(let i = 0; i < result.length; i++) {
            let currentAnswer = result[i];
            zones[currentZone].push(
                <OpenQuestionAnswer
                    key={currentAnswer.id}
                    data={currentAnswer}
                    fade={this.shouldAnswerFade(i, result.length)}
                    handleClick={this.handleHighlight.bind(this, currentAnswer.id)}
                />
            )
            currentZone = (currentZone < 3) ? currentZone + 1 : 0;
        }
        return zones;
    }

    render() {
        const { question, result } = this.props.stores.oq.data;
        const { totalAnswers, highlight } = this.props.stores.oq;

        if(!question || !result) return <p>OpenQuestion not found</p>;

        return (
            <div className={(highlight) ? "OpenQuestionPage game-wrapper highlight" : "OpenQuestionPage game-wrapper"}>
                <div className={(result.length > 0) ? "question-wrapper with-answers" : "question-wrapper"}>
                    <Question text={question}/>
                </div>
                <OpenQuestionAnswer
                    hide={!highlight}
                    highlight={true}
                    data={this.getHighlightData(highlight, result)}
                    handleClick={this.handleHighlight.bind(this, null)}
                />
                <div className="answers">
                    {this.buildZones(result).map(zone =>
                        <div className="zone">
                            {zone}
                        </div>
                    )}
                </div>
            </div>
        )
    }
};
