import React from 'react';
import { inject, observer } from 'mobx-react';

import Balloon from '../components/games/Balloon';
import StaticBalloon from '../components/games/StaticBalloon';

@inject('stores', 't') @observer
export default class PledgePage extends React.Component {

    componentDidMount() {
        this.props.stores.pledge.fetchAll();
    }

    render() {
        const { pledges } = this.props.stores.pledge;
        console.log("rendering pledges..", pledges)

        return (
            <div className="PledgePage">
                <Balloon pledges={pledges} t={this.props.t} />
                <StaticBalloon />
            </div>
        )
    }
};
