import React from 'react';

export default function QuizAnswer(props) {
    const renderStyling = () => {
        let base = "QuizAnswer";
        if(props.correct && props.showResults) {
            base += " correct";
        } else if(!props.correct && props.showResults) {
            base += " wrong";
        }
        return base;
    }

    return (
        <div className={renderStyling()}>
            <div className="choice"><span>{props.choice}</span></div>
            <div className="answer">
                <p>{props.answer}</p>
            </div>
        </div>
    )
}
