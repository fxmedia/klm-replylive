import React from 'react';

export default function GameLabel(props) {
    return (
        <div className="GameLabel">
            <span>{props.text}</span>
        </div>
    )
}
