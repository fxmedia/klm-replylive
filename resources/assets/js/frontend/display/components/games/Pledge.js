import React from 'react';
import Transition from 'react-addons-css-transition-group'

export default function Pledge(props) {
    if(!props.data) return null;

    return (
        <Transition
            transitionName={"fade"}
            transitionAppear={true}
            transitionAppearTimeout={500}
            transitionEnter={false}
            transitionLeave={true}
            transitionLeaveTimeout={300}
        >
            <div className={"Pledge"}>
                <p>{(!props.data.anonymous) ? props.data.name : props.t.pledge.anonymous}</p>
            </div>
        </Transition>
    )
}
