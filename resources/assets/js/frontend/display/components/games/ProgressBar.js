import React from 'react';
import Odometer from 'react-odometerjs';

export default function ProgressBar(props) {
    const progressPercentage = (props.value > 0) ? (props.value / props.total) * 100 : 0;

    return (
        <div className="ProgressBar">
            <div className="bar">
                <div style={{ width: progressPercentage + "%" }} className="completed"></div>
            </div>
            <div className={progressPercentage == 100 ? "odometer-wrapper full-styling-fix" : "odometer-wrapper"}>
                <Odometer value={progressPercentage} format={"d"} duration={300} />
                <span className="percentage">%</span>
            </div>
        </div>
    )
}
