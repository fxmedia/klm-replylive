import React from 'react';
import Odometer from 'react-odometerjs';

export default function AnswerTotal(props) {
    return (
        <div className="AnswerTotal">
            <div className="text-wrapper">
                <div className="total-wrapper">
                    <Odometer value={props.total} format={"d"} duration={300} />
                    <div className="total-users"> {(props.totalUsers) ? `/${props.totalUsers}` : null}</div>
                </div>
                <div className="label-wrapper">
                    <span>{(props.total === 1) ? "answer" : "answers"} given</span>
                </div>
            </div>
        </div>
    )
}
