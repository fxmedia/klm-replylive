import React from 'react';

export default function Question(props) {
    return (
        <div className="Question">
            <p>
                {props.text}
            </p>
        </div>
    )
}
