import React from 'react';

import Pledge from './Pledge';

export default function Balloon(props) {

    const BASE_SCALE = 0.7;
    const MAX_SCALE = 1.3;
    const MAX_FLOAT = 60;
    const MAX_PLEDGES = 60;
    const MAX_SHOW_PLEDGES = 5;
    const GROWTH_PER_PLEDGE = (MAX_SCALE - BASE_SCALE) / MAX_PLEDGES;
    const FLOAT_PER_PLEDGE = 0.25 * (MAX_FLOAT / MAX_PLEDGES);

    const renderSize = (total) => {
        const newScale = Math.min(BASE_SCALE + (total * GROWTH_PER_PLEDGE), MAX_SCALE);
        const newFloat = Math.min(total * FLOAT_PER_PLEDGE, MAX_FLOAT);
        return {
            transform: `scale(${newScale}) translateY(-${newFloat}%)`,
            transition: "transform 0.3s linear"
        }
    }

    const shouldPledgeRender = (idx, total) => {
        return (idx <= (MAX_SHOW_PLEDGES - 1) && total <= MAX_SHOW_PLEDGES)
        || (idx >= (total - MAX_SHOW_PLEDGES) && total > MAX_SHOW_PLEDGES)
    }

    return (
        <div className="Balloon" style={renderSize(props.pledges.length)}>
            <div className="pledge-container">
                <h2>Dank voor uw 'Pledge'</h2>
                <div className="pledges">
                    {props.pledges.map((pledge, idx) =>
                        (shouldPledgeRender(idx, props.pledges.length))
                        ? <Pledge key={pledge.id} data={pledge} t={props.t} />
                        : null
                    )}
                </div>
            </div>
        </div>
    )
}
