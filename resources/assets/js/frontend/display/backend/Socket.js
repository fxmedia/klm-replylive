import socketio from 'socket.io-client';
import config from './../../config';

export default class Socket {
    constructor(namespace) {
        console.log("config: ", config);
        this.connection = socketio(config.websocket + '/'+ namespace);
        this.connection.on('connect', display => {
            console.log("The display connection is live! :D", this.connection.id);
            this.connection.emit('token_set', this.parseToken());
        });
    }

    parseToken() {
        return location.pathname.replace('/display/', '');
    }
}
