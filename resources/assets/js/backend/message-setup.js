(function(){
    console.log('Message-Setup.js');
    var users = [];
    var idInput;
    var filters;
    var hideFilter;
    var listTableBody;
    var sendTableBody;
    var filterSettings = {};
    var hideFilterState = false;
    var resetFilters;

    var _onTopFilterClick = function(){
        var state = this.getAttribute('data-state');

        state = (parseInt(state)+1)%2;
        hideFilterState = state;

        this.setAttribute('data-state', state);

        if(state === 1){
            listTableBody.classList.add('hide-no-content');
            this.classList.add('list-group-item-success');
        }
        else{
            listTableBody.classList.remove('hide-no-content');
            this.classList.remove('list-group-item-success');
        }

        _buildSendTable();
    };

    var _onFilterClick = function(){
        var filter = this.getAttribute('data-filter');
        var state = this.getAttribute('data-state');
        var split = filter.split('_')[0];

        state = (parseInt(state)+1)%3;
        if(!filterSettings[split]) filterSettings[split] = {};
        filterSettings[split][filter] = state;

        this.setAttribute('data-state', state);

        this.classList.remove('list-group-item-success');
        this.classList.remove('list-group-item-danger');

        if(state === 1) this.classList.add('list-group-item-success');
        if(state === 2) this.classList.add('list-group-item-danger');

        _buildSendTable();
    };

    var _onUserFilterClick = function(){
        var state = this.getAttribute('data-state');

        state = (parseInt(state)+1)%3;
        this.setAttribute('data-state', state);

        this.classList.remove('bg-success');
        this.classList.remove('bg-danger');

        if(state === 1) this.classList.add('bg-success');
        if(state === 2) this.classList.add('bg-danger');

        _buildSendTable();
    };

    var _onResetFilterClick = function(e){
        e.preventDefault();

        var type = this.getAttribute('data-type');
        var i, k, l;

        switch(type){
            case 'groups':
                for(i=0; i<filters.length; i++){
                    filters[i].setAttribute('data-state', '0');
                    filters[i].classList.remove('list-group-item-success');
                    filters[i].classList.remove('list-group-item-danger');
                }
                hideFilter.setAttribute('data-state', '0');
                hideFilter.classList.remove('list-group-item-success');
                hideFilter.classList.remove('list-group-item-danger');

                for(k in filterSettings){
                    for(l in filterSettings[k]){
                        if(filterSettings[k].hasOwnProperty(l))filterSettings[k][l] = 0;
                    }
                }
                hideFilterState = 0;
                break;
            case 'user':
                for(i=0; i<users.length; i++){
                    users[i].setAttribute('data-state', '0');
                    users[i].classList.remove('bg-success');
                    users[i].classList.remove('bg-danger');
                }
                break;
        }

        _buildSendTable();
    };

    var _onSendTableClick = function(e){
        e.preventDefault();

        if(e.target.classList.contains('item-remove')){
            // remove from table
            var row = e.target.parentNode.parentNode;
            row.parentNode.removeChild(row);

            // remove from submit input

            var ids = idInput.value.split(',');
            var index = ids.indexOf( row.getAttribute('data-id') );

            if(index > -1){
                ids.splice(index, 1);

                idInput.value = ids.toString();
            }
        }
    };

    var _getSetFiltersValues = function(){
        var g = ['user', 'group', 'status'];
        var _filtersArray = [];
        var _subArr = [];

        var i, k;
        for(i=0; i<g.length; i++){
            _subArr = [];
            if(filterSettings[ g[i] ]) for(k in filterSettings[ g[i] ]){
                if( filterSettings[ g[i] ].hasOwnProperty(k) &&
                    filterSettings[ g[i] ][k] !== 0          ){

                    _subArr.push({
                        name: k,
                        val: filterSettings[g[i]][k]
                    });
                }
            }

            if(_subArr.length) _filtersArray.push(_subArr);
        }

        return _filtersArray;
    };

    var _buildSendTable = function(){
        sendTableBody.innerHTML = "";
        var i, j, k, state,
            groupHasPositiveValue = false,
            shouldShow =  false,
            hasClassFromGroup = false,
            td,
            id,
            idList = [],
            filters = _getSetFiltersValues();

        // first check if any of these settings are actually set, else shouldshow = true
        // for(k in filterSettings['user']){
        //     if(filterSettings['user'].hasOwnProperty(k)) usersFilterSet = true;
        // }

        // console.log('-----------------------');
        for(i=0; i<users.length; i++){
            state = parseInt(users[i].getAttribute('data-state'));
            shouldShow = true;


            // first difficult block
            if(filters.length) for(j=0; j<filters.length; j++){
                hasClassFromGroup = false;
                groupHasPositiveValue = false;

                if(shouldShow) for(k=0; k<filters[j].length; k++){
                    if(users[i].classList.contains(filters[j][k].name) ){
                        hasClassFromGroup = true;


                        if(filters[j][k].val === 2) shouldShow = false;
                    }

                    if(filters[j][k].val === 1) groupHasPositiveValue = true;
                }

                if(!hasClassFromGroup && groupHasPositiveValue) shouldShow = false;

            } else shouldShow = false;


            // second overwrite with individual
            if(state === 1) {
                shouldShow = true;
            }
            else if(state === 2){
                shouldShow = false;
            }
            //if(i===0)console.log('Ronny should show by | Own State',shouldShow);


            // STEP 3: first check if "hide users without filter is checked"
            // last skip users without address when hide filter is active
            if(hideFilterState && users[i].classList.contains('no-content')) shouldShow = false;

            if(shouldShow){
                id = users[i].getAttribute('data-id');

                td = document.createElement('tr');
                td.setAttribute('data-id', id);
                td.innerHTML = users[i].innerHTML + '<td class="showOnHover text-danger"><span class="glyphicon glyphicon-remove item-remove"></span></td>';
                idList.push(id);

                sendTableBody.appendChild(td);

            }
        }
        if(idList.length) idInput.value = idList.toString();
        else idInput.value = '';
    };

    var _init = function(){
        var i, dataFilter, split;
        idInput = document.getElementById('idList');
        listTableBody = document.getElementById('listTableBody');

        sendTableBody = document.getElementById('sendTableBody');
        sendTableBody.addEventListener('click', _onSendTableClick);

        filters = document.getElementsByClassName('filter');
        for(i=0; i<filters.length; i++){
            dataFilter = filters[i].getAttribute('data-filter');
            split = dataFilter.split('_')[0];

            if(!filterSettings[split]) filterSettings[split] = {};
            filterSettings[split][dataFilter] = parseInt( filters[i].getAttribute('data-state') ) || 0;

            filters[i].addEventListener('click', _onFilterClick);
        }

        hideFilter = document.getElementsByClassName('hide-filter')[0];
        hideFilter.addEventListener('click', _onTopFilterClick);

        resetFilters = document.getElementsByClassName('reset-filter');
        for(i=0; i<resetFilters.length; i++){
            resetFilters[i].addEventListener('click', _onResetFilterClick);
        }

        users = document.getElementsByClassName('filter-user');
        for(i=0; i<users.length; i++){
            users[i].addEventListener('click', _onUserFilterClick);
        }
    };
    _init();
})();