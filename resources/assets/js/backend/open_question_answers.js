(function(scope, $){
    console.log('open_question_answers.js');

    class _OpenQuestionAnswer {
        constructor() {
            this.routes = api_routes;
            this.highlightBtns = $('.toggle-open_question_answer-highlight');

            this.highlightBtns.on('click', this.toggleHighlight.bind(this));
        }

        getRoute(open_question_answer_id, key) {
            console.log("id: ", open_question_answer_id);
            return this.routes[open_question_answer_id][key];
        }

        toggleHighlight(event){
            var el = $(event.currentTarget),
                open_question_answerId = el.data('open_question_answer-id'),
                isHighlight = el.hasClass('btn-success');

            !isHighlight
                ? this.setHighlight(el, open_question_answerId)
                : this.stopHighlight();
        }

        setHighlight(el, open_question_answerId){
            var route = this.getRoute(open_question_answerId, 'highlight');

            $.ajax(route, {
                method: 'POST'
            }).success(data => {
                if(!data || !data['success']) return this.error(data);

                this.highlightBtns.removeClass('btn-success').addClass('btn-primary');

                el.addClass('btn-success').removeClass('btn-primary');

            }).error(this.error);
        }

        stopHighlight(){

            var route = this.getRoute('other','lowlight');

            $.ajax(route, {
                method: 'POST'
            }).success(data => {
                if(!data || !data['success']) return this.error(data);

                this.highlightBtns.removeClass('btn-success').addClass('btn-primary');
            }).error(this.error);
        }


        error(xhr, status, errorThrown) {
            console.log(xhr, status, errorThrown);

            if(xhr && xhr.hasOwnProperty('success') && typeof xhr.success !== 'function') {
                if(xhr.hasOwnProperty('error')) {
                    xhr.error = (xhr.error && xhr.error.message)? xhr.error.message : xhr.error || 'Whoops, something went wrong';
                    return Notifications.error(xhr.error);
                }
            }

            if(xhr && xhr.readyState === 0) {
                return Notifications.error('Internet error, make sure you are connected to the internet');
            }

            if(xhr && xhr.readyState === 4) {
                return Notifications.error(errorThrown);
            }

            return Notifications.error('Whoops, something went wrong.');
        }

    }

    scope.open_question_answer = new _OpenQuestionAnswer();
})(this, $);
