(function(scope, $){
    console.log('LearningCurves.js');

    class _LearningCurve {
        constructor() {
            this.routes = api_routes;
            this.currentActiveIds = {};
            this.$startStopBtns = $('.toggle-learning_curve-start-stop');

            this.$startStopBtns.on('click', this.toggleLearningCurve.bind(this)).each((i, el) => {
                el = $(el);
                if(el.hasClass('btn-danger')){
                    this.currentActiveIds[ el.parent().data('learning_curve-id') ] = {takeover: el.parent().data('takeover') };
                }
            });

        }

        getRoute(id, key) {
            return this.routes[id][key];
        }


        toggleLearningCurve(event){
            event.preventDefault();

            var el = $(event.currentTarget),
                learning_curveId = el.parent().data('learning_curve-id'),
                phase = el.data('phase'),
                isActive = el.hasClass('btn-danger');

            isActive
                ? this.stopLearningCurve(el, phase)
                : this.startLearningCurve(el,learning_curveId, phase);
        }

        stopLearningCurve(el, phase) {
            var route = this.getRoute('other', 'stop');

            $.ajax(route,{
                method: 'POST'
            }).success(data => {
                if(!data || !data['success']) return this.error(data);

                var phase = el.data('phase');
                el.toggleClass('btn-danger btn-primary')
                    .find('i').toggleClass('fa-stop fa-play')
                    .parent().find('span').text('Start ' + phase.charAt(0).toUpperCase() + phase.slice(1) + ' Learning Curve');
                    // .parent().parent().find('.toggle-pause')
                    // .addClass('hidden')
                    // .addClass('btn-info').removeClass('btn-success')
                    // .find('i').removeClass('glyphicon-play').addClass('glyphicon-pause')
                    // .parent().find('span').text('Pause');
            }).error(this.error);
        }

        startLearningCurve(el, learning_curveId, phase) {
            var route = this.getRoute(learning_curveId, 'start_' + phase);

            var currentActive = $('.toggle-learning_curve-start-stop.btn-danger').length > 0;

            if(currentActive && !confirm('There is already a learning_curve active, start a new learning_curve?')) {
                return;
            } else if (currentActive) {
                var activeButtons = $('.toggle-learning_curve-start-stop.btn-danger');
                this.stopLearningCurve(activeButtons, phase);
            }

            $.ajax(route, {
                method: 'POST'
            }).success(data => {
                if(!data || !data['success']) return this.error(data);

                $('.toggle-learning_curve-start-stop')
                .each(function() {
                    var phase = $( this ).data('phase');
                    $( this ).addClass('btn-primary').removeClass('btn-danger')
                    .find('i').addClass('fa-play').removeClass('fa-stop')
                    .parent().find('span').text('Start ' + phase.charAt(0).toUpperCase() + phase.slice(1) + ' Learning Curve');
                });

                var phase = el.data('phase');
                el.toggleClass('btn-danger btn-primary')
                    .find('i').toggleClass('fa-stop fa-play')
                    .parent().find('span').text('Stop ' + phase.charAt(0).toUpperCase() + phase.slice(1) + ' Learning Curve');
            }).error(this.error);
        }


        error(xhr, status, errorThrown) {
            console.log(xhr, status, errorThrown);

            if(xhr && xhr.hasOwnProperty('success') && typeof xhr.success !== 'function') {
                if(xhr.hasOwnProperty('error')) {
                    xhr.error = (xhr.error && xhr.error.message)? xhr.error.message : xhr.error || 'Whoops, something went wrong';
                    return Notifications.error(xhr.error);
                }
            }

            if(xhr && xhr.readyState === 0) {
                return Notifications.error('Internet error, make sure you are connected to the internet');
            }

            if(xhr && xhr.readyState === 4) {
                return Notifications.error(errorThrown);
            }

            return Notifications.error('Whoops, something went wrong.');
        }
    }

    scope.learning_curve = new _LearningCurve();
})(this, $);
