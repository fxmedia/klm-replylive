$(function() {

    $('#updateDefaults').on('click', function ( e ) {
        e.preventDefault();
        var form = $($('form').get(0));
        form.append(
            $('<input>').attr('type', 'hidden').attr('name', 'updateDefaults').val('1')
        );

        form.submit();
    });
    
    if(document.location.pathname.indexOf('create') > -1 ) {
        $('input#folder').on('keyup', updateImage);

        $('input#name').on('keyup', function(e){
            var name = e.currentTarget.value;

            console.log(name);

            name = name.toLowerCase();
            name = name.replace(/\W+/g, '_');

            $('input#folder').val(name);

            updateImage.apply($('input#folder'));
        });
    }

    function updateImage()
    {
        var folder = $(this).val();

        $('input[name="theme[theme__path-img]"]').val('"../../images/' + folder + '"');
    }

    if( $("[name='is_enabled']").length > 0) {
        $("[name='is_enabled']")
            .bootstrapSwitch({
                onSwitchChange: function (ev, state) {
                    ev.currentTarget.value = state ? "on" : "off";
                    if(state) {
                        $('.is_locked').removeClass('hidden');
                    } else {
                        $('.is_locked').addClass('hidden');
                    }
                }
            });
    }

    if( $("[name='is_locked']").length > 0) {
        $("[name='is_locked']")
            .bootstrapSwitch({
                onSwitchChange: function (ev, state) {
                    ev.currentTarget.value = state ? "on" : "off";
                }
            });
    }
    
    if($('#is_enabled').length > 0 && $('#is_enabled').val() == "0") {
        $('.is_locked').addClass('hidden');
    }
    
    $('.generate-token').on('click', function(){
        $(this).parent().find('input').val(generateToken(20));
    });

    var generateToken = function( length ){
        length = length || 64;
        var token    = "";
        var possible = "ABCDEFGHIJKLMNOPQRSTUVWXYZabcdefghijklmnopqrstuvwxyz0123456789";

        for( var i = 0; i < length; i++ ) {
            token += possible.charAt(Math.floor(Math.random() * possible.length));
        }

        return token;
    };

});