(function(scope, $){
    console.log('Surveys.js');

    class _Survey {
        constructor() {
            this.routes = api_routes;
            this.currentActiveIds = {};
            this.$startStopBtns = $('.toggle-survey-start-stop');

            this.$startStopBtns.on('click', this.toggleSurvey.bind(this)).each((i, el) => {
                el = $(el);
                if(el.hasClass('btn-danger')){
                    this.currentActiveIds[ el.parent().data('survey-id') ] = {takeover: el.parent().data('takeover') };
                }
            });

        }

        getRoute(id, key) {
            return this.routes[id][key];
        }


        toggleSurvey(event){
            event.preventDefault();

            var el = $(event.currentTarget),
                surveyId = el.parent().data('survey-id'),
                isActive = el.hasClass('btn-danger');

            isActive
                ? this.stopSurvey(el, surveyId)
                : this.startSurvey(el,surveyId);
        }

        stopSurvey(el, surveyId) {
            var route = this.getRoute(surveyId, 'stop');

            $.ajax(route,{
                method: 'POST'
            }).success(data => {
                if(!data || !data['success']) return this.error(data);

                el.toggleClass('btn-danger btn-primary')
                    .find('i').toggleClass('fa-stop fa-play')
                    .parent().find('span').text('Start Survey');
                    // .parent().parent().find('.toggle-pause')
                    // .addClass('hidden')
                    // .addClass('btn-info').removeClass('btn-success')
                    // .find('i').removeClass('glyphicon-play').addClass('glyphicon-pause')
                    // .parent().find('span').text('Pause');


                delete this.currentActiveIds[surveyId];
            }).error(this.error);
        }

        startSurvey(el, surveyId) {
            var route = this.getRoute(surveyId, 'start');

            var currentTakeover = el.hasClass('survey-takeover');
            var currentActive = $('.toggle-survey-start-stop.btn-danger.survey-takeover').length > 0;

            if(currentActive && currentTakeover && !confirm('There is already a survey active, start a new survey?')) {
                return;
            }

            $.ajax(route, {
                method: 'POST'
            }).success(data => {
                if(!data || !data['success']) return this.error(data);

                for(var k in this.currentActiveIds){
                    if(this.currentActiveIds.hasOwnProperty(k) &&
                        this.currentActiveIds.takeover) delete this.currentActiveIds[k];
                }
                if(currentTakeover){
                    $('.toggle-survey-start-stop.survey-takeover')
                        .addClass('btn-primary').removeClass('btn-danger')
                        .find('i').addClass('fa-play').removeClass('fa-stop')
                        .parent().find('span').text('Start Survey');
                    //.parent().parent().find('.toggle-pause').addClass('hidden');
                }

                el.toggleClass('btn-danger btn-primary')
                    .find('i').toggleClass('fa-stop fa-play')
                    .parent().find('span').text('Stop Survey');

                this.currentActiveIds[surveyId] = {
                    takeover: el.hasClass('survey-takeover')
                }

            }).error(this.error);
        }


        error(xhr, status, errorThrown) {
            console.log(xhr, status, errorThrown);

            if(xhr && xhr.hasOwnProperty('success') && typeof xhr.success !== 'function') {
                if(xhr.hasOwnProperty('error')) {
                    xhr.error = (xhr.error && xhr.error.message)? xhr.error.message : xhr.error || 'Whoops, something went wrong';
                    return Notifications.error(xhr.error);
                }
            }

            if(xhr && xhr.readyState === 0) {
                return Notifications.error('Internet error, make sure you are connected to the internet');
            }

            if(xhr && xhr.readyState === 4) {
                return Notifications.error(errorThrown);
            }

            return Notifications.error('Whoops, something went wrong.');
        }
    }

    scope.survey = new _Survey();
})(this, $);