/* global console, confirm */

console.log('Init Quiz.js');

$(function() {

    var $modal = $('#quiz'),
        $quiz = null,
        $question = null,
        $page = 1,
        $questionNumber = 0,
        $questionUri = '',
        $lockUri = '',
        $quizResultsUri = '',
        $quizScoreUri = '',
        $quizShowUri = '',
        $quizEndUri = '',
        $quizScorePageUri = '';


    var resetQuestion = function()
    {
        $modal.find('#questionNavigation').html( '' );
        $modal.find('.end').addClass('hide');
        $modal.find('.question-counter').hide();
        $modal.find('.question-name').html( '' );
    };

    var createPagination = function( numberOfItems, currentItem ) {
        var $menu = $('#questionNavigation');
        // reset previous items
        $menu.html('');
        for( var $q = 0; $q < numberOfItems; $q++) {
            var $link = $('<a>').addClass('item').data({question: $quiz.questions[$q], questionId: $q}).html($q + 1);

            if ($questionNumber > $q)
                $link.addClass('old');
            if ($questionNumber == $q)
                $link.addClass('active');
            if ($questionNumber < $q)
                $link.addClass('new');

            if($quiz.questions[$questionNumber] == $question)
                $link.addClass('selected');

            $menu.append( $link );
        }
    };

    var fillQuestion = function()
    {
        $.ajax({
            url: $questionUri + '/' + $question.id,
            method: 'POST',
            success: function() {

                $modal.find('.question-number').html( $questionNumber + 1 );
                createPagination( $quiz.questions.length, $questionNumber );
                $modal.find('.question-counter').show();
                $modal.find('.score').removeClass('hide');
                $modal.find('.question-name').html( $question.question );

                var listAnswers = $('<ol type="A">');
                for( var answer in $question.answers )
                {
                    listAnswers.append( $('<li>').html($question.answers[answer]) );
                }
                $modal.find('.question-answers').html( listAnswers );
            }
        });
    };

    $('#quizzes').on('click', '.open-modal', function( event )
    {
        event.preventDefault();

        $questionUri = $(this).data('question');
        $lockUri = $(this).data('lock');
        $quizResultsUri = $(this).data('results');
        $quizEndUri = $(this).data('end');
        $quizScoreUri = $(this).data('score');
        $quizShowUri = $(this).data('quiz');
        $quizScorePageUri = $(this).data('scorepage');

        $page = 1;

        $.ajax({
            url: $(this).attr('href'),
            method: 'POST',
            success: function( response ) {
                if( response.data )
                {
                    $quiz = response.data.quiz;
                    $questionNumber = 0;
                    $modal.find('.quiz-name').html( $quiz.name );

                    // Reset any loaded modals before
                    resetQuestion();
                    $modal.find('.modal-actions').addClass('hide');
                    $modal.find('.score').addClass('hide');

                    // Check if the quiz contains a question
                    if( $quiz.questions.length === 0 )
                    {
                        $modal.find('.quiz-start').hide();
                        $modal.find('.question-answers').html('Unable to start the quiz. There are no questions!');
                    } else {
                        $modal.find('.quiz-start').show();
                        $modal.find('.question-answers').html('');
                    }

                    $modal.modal({
                        backdrop: 'static',
                        keyboard: false,
                        show: true
                    });
                }
            }
        });
    });

    $('#quiz').on('click', '.prev-result, .next-result', function( event ) {

        if( $(this).hasClass('prev-result') ) {
            if( $page > 1 ) {
                $page--;
            }
        } else {
            $page++;
        }

        var uri = $quizScorePageUri.replace('0/score_page', $quiz.id + 'score_page')

        console.log(uri);

        $.ajax({
            url: uri,
            data: {page: $page},
            success: function( data )
            {
                $('.page-number').html($page);
            }
        });

    } );

    $('#quiz').on('click', '.score', function( event )
    {
        event.preventDefault();

        var uri = $quizScoreUri;

        if( $modal.find('.score').html() === 'Show score' )
        {
            $modal.find('.score').html('Show Quiz');
            $modal.find('.lock').addClass('hide');
            $modal.find('.next').addClass('hide');
            $modal.find('.prev-result').removeClass('hide');
            $modal.find('.page-number').removeClass('hide');
            $modal.find('.next-result').removeClass('hide');
        } else {
            $modal.find('.score').html('Show score');
            $modal.find('.lock').removeClass('hide');
            $modal.find('.next').removeClass('hide');
            $modal.find('.prev-result').addClass('hide');
            $modal.find('.page-number').addClass('hide');
            $modal.find('.next-result').addClass('hide');
            $page = 1;
            $('.page-number').html($page);
            uri = $quizShowUri;
        }

        $.ajax({
            url: uri,
            success: function( data )
            {
                console.log( data );
            }
        });

    });

    $('#quiz').on('click', '.quiz-start', function( event )
    {
        event.preventDefault();
        var $activeQuestion = 0;

        $modal.find('.quiz-start').hide();
        $modal.find('.modal-actions').removeClass('hide');
        $modal.find('.lock').show();
        if( ($quiz.questions.length - 1) > $questionNumber ) {
            $modal.find('.next').show().attr('disabled','disabled');
        }
        $question = $quiz.questions[$questionNumber];
        fillQuestion();
    });

    $('#quiz').on('click', '.lock', function()
    {
        $.ajax({
            url: $lockUri + '/' + $question.id,
            method: 'post',
            success: function() {

                $modal.find('.lock').hide();
                if( ($quiz.questions.length - 1) === $questionNumber ) {
                    $modal.find('.results').removeClass('hide');
                } else {
                    $modal.find('.next').removeAttr('disabled');
                }

            }
        });
    });

    $('#quiz').on('click', '.next', function()
    {
        $questionNumber++;
        $modal.find('.lock').show();
        if( ($quiz.questions.length - 1) === $questionNumber )
        {
            $modal.find('.next').hide();
        } else {
            $modal.find('.next').attr('disabled','disabled');
        }

        $question = $quiz.questions[$questionNumber];
        fillQuestion();
    });

    $('#quiz').on('click', '.results', function()
    {
        $.ajax({
            url: $quizResultsUri,
            method: 'post',
            success: function () {
                resetQuestion();
                var icon = $('<span>').addClass('fa fa-bar-chart');
                $modal.find('.question-answers').html( icon );

                $modal.find('.results').addClass('hide');
                $modal.find('.end').removeClass('hide');
            }
        });

    });

    $('#quiz').on('click', '.end, .close', function()
    {
        var end = confirm('Are you sure to end this quiz?');

        if( end === true ) {
            $.ajax({
                url: $quizEndUri,
                method: 'post',
                success: function () {
                    $modal.modal('hide');
                    document.location.reload();
                }
            });
        }
    });

    $('#quiz').delegate( 'a.old, a.active', 'click', function ( e ) {
        e.preventDefault();
        var question = $(this).data('questionId');
        $question = $quiz.questions[question];
        fillQuestion();
    });


    $('#quizzes').on('click', '.remove-quiz', function( e ) {
        e.preventDefault();
        var conf = confirm('Are you sure to delete the quiz with all related questions and answers?');
        if( conf ) {
            $(this).parent().submit();
        }
    });
});