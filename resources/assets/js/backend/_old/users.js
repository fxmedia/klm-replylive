$(function() {

    var $form =  $('#usersTable'),
        $messageSimulateUri = messageSimulateUri,
        $messageSendUri = messageSendUri,
        $deleteUserUri = deleteUserUri,
        $users = null,
        $userIds = null,
        $action = null,
        $modal = null,
        $sending = 0,
        $send = 1,
        $checked = false,
        $progress = {
            valuemin: 0,
            valuemax: 100,
            valuenow: 0,
            with: '0%'
        },
        $settings = {},
        $maySend = true;

    var inputTemplate = function( type, properties, listener )
    {
        var group = $('<div>').addClass('form-group'),
            label = $('<label>').html( properties.label),
            input = null;

        if( type === 'textarea' ) {
            input = $('<'+type+'>').addClass('form-control').attr('name',properties.name).text(properties.value);
        } else {
            input = $('<input>').attr('type',type).addClass('form-control').attr('name',properties.name).val(properties.value);
        }

        if(listener) {
            input.on('keyup', listener);
        }

        return group.append(label).append(input);
    };

    var parseInput = function(e) {
        var valid = /\[(url|personalUrl|firstname|lastname|username)\]/gi;
        var all = /\[[a-z0-9\s]*\]/gi;
        var str = $(this).val();
        var m;
        var count_valid = 0;
        var count_all = 0;

        while ((m = all.exec(str)) !== null) {
            if (m.index === all.lastIndex) {
                all.lastIndex++;
            }
            count_all++;
        }
        m = null;
        while ((m = valid.exec(str)) !== null) {
            if (m.index === valid.lastIndex) {
                valid.lastIndex++;
            }
            count_valid++;
        }
        var w = (count_all === count_valid) ? 'all' : '[' + count_valid + ']';
        $('#content-validation')
            .html('Detected [' + count_all + '] tags in the content, of which ' + w + ' are valid')
            .removeClass('text-success text-danger')
            .addClass((count_all === count_valid) ? 'text-success' : 'text-danger');
    };

    var sendMail = function( user_id )
    {
        console.log('sendmail : ', user_id);

        $.ajax({
            url: $messageSendUri,
            method: 'post',
            data: { user_id: user_id, type: $action.val(), settings: $settings },
            success: function( response ) {

                if( response.data )
                {
                    var user = response.data.user;
                    $('#user_' + user.id).find('input[name*="users"]').prop('checked',false).addClass('hide');
                    $('#user_' + user.id).find('.fa-paper-plane').removeClass('hide');
                }

                $progress.valuenow = (100 / $sending) * $send;
                $progress.with = $progress.valuenow + '%';

                $('.progress-bar').attr('role','progressbar').attr('aria-valuenow',$progress.valuenow).attr('aria-valuemin',$progress.valuemin).attr('aria-valuemax',$progress.valuemax).css('width',$progress.with);

                var index = $userIds.indexOf(user_id);
                $userIds.splice(index,1);

                if( $userIds.length > 0 ) {
                    $('.send').html( $send++ );

                    if( $maySend ) {
                        sendMail( $userIds[0] );
                    }
                } else {
                    $('.progress-bar').removeClass('progress-bar-striped active');
                    $('.progress-stats').html('Completed!');
                    $('#cancel').html('Close');
                }

            }
        });
    };

    $('#showPerPage').on('change', function() {
        $.ajax({
            url: updateShowUri,
            method: 'POST',
            data: { per_page: $(this).val() },
            success: function( response ) {
                if( response == 'true' ) {
                    location.reload();
                }
            }
        });
    });

    // Toggle all form table
    $form.on('click', '.select-all', function( e ) {
        e.preventDefault();
        $checked = !$checked;
        if( $checked ) {
            $(this).html('De-select all');
        } else {
            $(this).html('Select all');
        }
        $form.find('input[name*="users"]').not('.hide').prop('checked', $checked );
    });

    // Reset all form table
    $form.on('click', '.reset-status', function( e ) {
        e.preventDefault();
        $('.fa-paper-plane').each( function() {
            $(this).not('.hide').click();
        });
    });

    // Reset already send to send again
    $form.on('click', '.fa-paper-plane', function() {
        $(this).addClass('hide');
        $(this).next().removeClass('hide');
    });

    // Check checkbox on click rule
    $form.on('click','tr', function( selector )
    {
        if( selector.target.tagName !== 'INPUT' ) {
            var checked = $(this).find('input[name*="users"]').is(':checked');
            $(this).find('input[name*="users"]').prop('checked', !checked);
        }
    });

    // Submit the form
    $form.submit( function( event )
    {
        event.preventDefault();

        $action = $form.find('select[name="perform"] option:selected');
        $users = $('input[name*="users"]:checked');
        $modal = $('#users');

        // small resets
        $maySend = true;
        $modal.on('hide.bs.modal', function () {
            $maySend = false;
        });
        $('#cancel').html('Cancel');
        $('#submit').show();

        var userIds = [];
        for( var u = 0; $users.length > u; u++ )
        {
            var user = $($users[u]);
            userIds.push( user.val() );
        }
        $userIds = userIds;

        switch( $action.val() )
        {
            case 'delete':
                var message = 'Are you sure to remove ' + $users.length + ' users?';

                $modal.find('.modal-title').html( $action.text() );
                $modal.find('.modal-body').html( message );
                $modal.find('#submit').addClass('btn-danger').removeClass('btn-primary').html( 'Delete users' );
                break;
            case 'send_mail':
                $.ajax({
                    url: $messageSimulateUri,
                    method: 'post',
                    data: { users: $userIds, type: $action.val() },
                    success: function( response ) {
                        var message = $('<div>').attr('id','mailSettings');
                        message.append( $('<p>').html('Your selection will sent ' + response.data.sending + ' ' + response.data.type + '!' ) );
                        message.append( inputTemplate('text', {label: 'From: ', name: 'from_name', value: response.data.settings.email__from_name} ) );
                        message.append( inputTemplate('text', {label: 'Email: ', name: 'from_email', value: response.data.settings.email__from_email} ) );
                        message.append( inputTemplate('text', {label: 'Subject: ', name: 'subject', value: response.data.settings.email__subject} ) );
                        message.append( inputTemplate('textarea', {label: 'Content: ', name: 'content', value: response.data.settings.email__body}, parseInput ) );
                        message.append( $('<label>').attr('id', 'content-validation') );

                        $modal.find('.modal-title').html( $action.text() );
                        $modal.find('.modal-body').html( message );
                        $modal.find('#submit').removeClass('btn-danger').addClass('btn-primary').html( 'Send' );
                    }
                });
                break;
            case 'send_text':
                $.ajax({
                    url: $messageSimulateUri,
                    method: 'post',
                    data: { users: $userIds, type: $action.val() },
                    success: function( response ) {
                        var message = $('<div>').attr('id','mailSettings');
                            message.append( $('<p>').html('Your selection will sent ' + response.data.sending + ' ' + response.data.type + '!' ) );
                            message.append( inputTemplate('text', {label: 'Subject: ', name: 'subject', value: response.data.settings.sms__subject} ) );
                            message.append( inputTemplate('textarea', {label: 'Content: ', name: 'content', value: response.data.settings.sms__body}, parseInput ) );
                            message.append( $('<label>').attr('id', 'content-validation') );

                        $modal.find('.modal-title').html( $action.text() );
                        $modal.find('.modal-body').html( message );
                        $modal.find('#submit').removeClass('btn-danger').addClass('btn-primary').html( 'Send' );
                    }
                });
                break;
        }

        $modal.modal({
            show: true
        });

        setTimeout(function(){
            parseInput.apply($('textarea[name="content"]'));
        }, 700);

    });

    $('#users').on('click', '#submit', function( event ) {
        event.preventDefault();

        if($('#content-validation').hasClass('text-danger')) {
            if(!confirm('Are you sure you want to send this message with invalid tags?')) {
                return false;
            }
        }

        switch( $action.val() )
        {
            case 'delete':
                document.getElementById('submit').className = 'btn btn-disabled';
                $.ajax({
                    url: $deleteUserUri,
                    method: 'delete',
                    data: { users: $userIds, type: $action.val() },
                    success: function( response ) {
                        if (response === true) {
                            window.location.reload();
                        }
                    }
                });
                break;
            case 'send_mail':
            case 'send_text':
                var subject = $modal.find('input[name*="subject"]').val();
                var body = $modal.find('textarea[name*="content"]').val();

                $.ajax({
                    url: $messageSendUri,
                    method: 'post',
                    data: { users: $userIds, subject: subject, body: body },
                    success: function( response ) {
                        $('#mailSettings').empty().html('Completed!');
                        $('#cancel').html('Close');
                        $('#submit').remove();
                    }
                });
                break;
        }
    });

    $('.btn-importfile').click(function(e) {
        e.preventDefault();
        $('.importfile').click();
    });
});