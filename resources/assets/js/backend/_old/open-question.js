console.log('Init Open question');

class OpenQuestion {

    constructor() {
        this.answerInterval = null;
        this.updateRoute = $('#open-question').data('update-route');
        try {
            this.lastId = lastPostId;
        } catch (e) {
            this.lastId = 0;
        }

        $('.toggle-question-start-stop').on('click', this.toggleQuestion.bind(this));
        $('.remove-open-question').on('click', this.deleteQuestion.bind(this));
        $('.highlight-answer').on('click', this.highlightAnswer.bind(this));
        $('.reset-question').on('click', this.resetQuestion.bind(this));

        if($('.toggle-question-start-stop.btn-danger').length > 0) {
            this.startUpdate();
        }

        if(document.location.pathname.indexOf('/list') > -1) {
            setInterval(this.fetchAnswers.bind(this), 2000);
        }
    }

    deleteQuestion(event) {
        var el = $(event.currentTarget);
        if(!confirm('Are you sure you want to delete this question with all its answers?')) {
            return false;
        }

        $.ajax(el.data('route'), {
            type: 'DELETE'
        }).success(data => {
            document.location.reload();
        }).error(this.error);
    }

    resetQuestion(event) {
        var el = $(event.currentTarget);
        if(!confirm('Are you sure you want to permanently delete all answers given to this question?')) {
            return false;
        }

        $.ajax(el.data('route'), {
            type: 'POST'
        }).success(data => {
            document.location.reload();
        }).error(this.error);
    }

    startUpdate(timeout = 2000) {
        if(!this.answerInterval) {
            this.answerInterval = setInterval(this.checkAnswers.bind(this), timeout);
        }
    }

    stopUpdate() {
        if(this.answerInterval) {
            this.answerInterval = clearInterval(this.answerInterval);
        }
    }

    toggleQuestion(event) {
        var el = $(event.currentTarget),
            isActive = el.hasClass('btn-danger');

        isActive
            ? this.stopQuestion(el)
            : this.startQuestion(el);
    }

    stopQuestion(el) {
        var route = el.data('end');

        $.ajax(route, {
            method: 'POST'
        }).success(data => {
            if(!data || !data['success']) throw new Error('No success');

            el.toggleClass('btn-danger btn-primary')
                .find('i').toggleClass('fa-stop fa-play')
                .parent().find('span').text('Start Question')
                .find('i').removeClass('glyphicon-play').addClass('glyphicon-pause');

            this.stopUpdate();

        }).error(this.error);
    }

    startQuestion(el) {
        var route = el.data('start');
        var currentActive = $('.toggle-question-start-stop.btn-danger').length > 0;

        if(currentActive && !confirm('There is already a question active, do you wish to proceed to this question?')) {
            return;
        }

        var withResults = el.parent().parent().find('.with-results').prop('checked');

        $.ajax(route, {
            method: 'POST',
            data: {with_results: withResults}
        }).success(data => {
            if(!data || !data['success']) return this.error(data);

            $('.toggle-question-start-stop')
                .addClass('btn-primary').removeClass('btn-danger')
                .find('i').addClass('fa-play').removeClass('fa-stop')
                .parent().find('span').text('Start Question');

            el.toggleClass('btn-danger btn-primary')
                .find('i').toggleClass('fa-stop fa-play')
                .parent().find('span').text('Stop Question');

            this.startUpdate();

        }).error(this.error);
    }

    highlightAnswer(event) {
        var el = $(event.currentTarget),
            route = el.data('route');

        //if($('.highlighted').length > 0 && !confirm('If you highlight this answer, the current highlighted one will close')) {
        //    return;
        //}

        $.ajax(route, {
            method: 'POST',
        }).success(data => {
            if(!data || !data['success']) this.error(data);

            $('.highlighted').removeClass('highlighted')
                .find('.glyphicon')
                .addClass('glyphicon-zoom-in')
                .removeClass('glyphicon-zoom-out');

            if(data.state) {
                el
                    .parent().parent().addClass('highlighted')
                    .find('.glyphicon')
                    .removeClass('glyphicon-zoom-in')
                    .addClass('glyphicon-zoom-out');
            }

        }).error(this.error);
    }

    error(xhr, status, errorThrown) {
        console.log(xhr, status, errorThrown);

        if(xhr && xhr.hasOwnProperty('success') && typeof xhr.success !== 'function') {
            if(xhr.hasOwnProperty('error')) {
                xhr.error = xhr.error || 'Whoops, something went wrong';
                return Notifications.error(xhr.error);
            }
        }

        if(xhr && xhr.readyState === 0) {
            return Notifications.error('Internet error, make sure you are connected to the internet');
        }

        if(xhr && xhr.readyState === 4) {
            return Notifications.error(errorThrown);
        }

        return Notifications.error('Whoops, something went wrong.');
    }

    checkAnswers() {
        var el = $('.toggle-question-start-stop.btn-danger');

        if(el.length < 1) return;

        $.ajax(this.updateRoute, {
            method: 'POST'
        }).success(data => {
            if(!data || !data['success']) return this.error(data);

            for(var key in data.answers) {
                $('.question_' + key).find('.answer-count').text(data.answers[key]);
            }
        }).error(err => {
            //console.warn('Ping answers error: ', err);
        });
    }

    fetchAnswers() {
        var route = $('#open-question-answers').data('update-route');

        $.ajax(route, {
            method: 'POST',
            data: {last_id: this.lastId}
        }).success(data => {
            if(!data || !data['success']) return this.error(data);

            data.answers.forEach(answer => {
                if($('[data-answer-id="' + answer.id + '"]').length === 0) {
                    var row = $('<tr>').data('answer-id', answer.id);
                    row.append($('<td>').text(answer.answer));
                    row.append($('<td>').text(answer.user));
                    row.append($('<td width="160">').text(answer.created_at));
                    row.append($('<td width="50"></td>').append(
                        $('<a data-route="/api/open-question/' + answer.top_question_id + '/highlight/' + answer.id + '" title="Highlight Answer" class="highlight-answer btn btn-info btn-xs"><span class="glyphicon glyphicon-zoom-in"></span></a>')
                            .on('click', this.highlightAnswer.bind(this))
                    ));
                    $('tbody').prepend(row);

                    this.lastId = answer.id;
                }
            });

        }).error(this.error);
    }

}

window.oq = new OpenQuestion();