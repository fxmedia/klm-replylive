/* global console, confirm */
console.log('Init Poll.js');

class Poll {

    constructor() {
        this.routes = poll_routes;
        this.answerInterval = null;

        $('.toggle-poll-start-stop').on('click', this.toggleQuestion.bind(this));
        $('.toggle-pause').on('click', this.togglePause.bind(this));
        $('.reset-question-answers').on('click', this.resetQuestion.bind(this));

        if($('.toggle-poll-start-stop.btn-danger').length > 0) {
            this.startUpdate();
        }
    }

    startUpdate(timeout = 2000) {
        if(!this.answerInterval) {
            this.answerInterval = setInterval(this.checkAnswers.bind(this), timeout);
        }
    }

    stopUpdate() {
        if(this.answerInterval) {
            this.answerInterval = clearInterval(this.answerInterval);
        }
    }

    getRoute(poll_id, key) {
        return this.routes[poll_id][key];
    }

    toggleQuestion(event) {
        var el = $(event.currentTarget),
            questionId = el.parent().data('question-id'),
            pollId = el.parent().data('poll-id'),
            isActive = el.hasClass('btn-danger');

        isActive
            ? this.stopQuestion(el, pollId)
            : this.startQuestion(el, pollId, questionId);
    }

    stopQuestion(el, pollId) {
        var route = this.getRoute(pollId, 'end');

        $.ajax(route, {
            method: 'POST'
        }).success(data => {
            if(!data || !data['success']) throw new Error('No success');

            el.toggleClass('btn-danger btn-primary')
                .find('i').toggleClass('fa-stop fa-play')
                .parent().find('span').text('Start Poll')
                .parent().parent().find('.toggle-pause')
                .addClass('hidden')
                .addClass('btn-info').removeClass('btn-success')
                .find('i').removeClass('glyphicon-play').addClass('glyphicon-pause')
                .parent().find('span').text('Pause');

            this.stopUpdate();

        }).error(this.error);
    }

    startQuestion(el, pollId, questionId) {
        var route = this.getRoute(pollId, 'setQuestion');

        var currentActive = $('.toggle-poll-start-stop.btn-danger').length > 0;

        if(currentActive && !confirm('There is already a poll active, do you wish to proceed to this question?')) {
            return;
        }

        $.ajax(route + '/' + questionId, {
            method: 'POST',
            data: {results: true}
        }).success(data => {
            if(!data || !data['success']) return this.error(data);

            $('.toggle-poll-start-stop')
                .addClass('btn-primary').removeClass('btn-danger')
                .find('i').addClass('fa-play').removeClass('fa-stop')
                .parent().find('span').text('Start Poll')
                .parent().parent().find('.toggle-pause').addClass('hidden');

            el.toggleClass('btn-danger btn-primary')
                .find('i').toggleClass('fa-stop fa-play')
                .parent().find('span').text('Stop Poll')
                .parent().parent().find('.toggle-pause').removeClass('hidden');

            this.startUpdate();

        }).error(this.error);
    }

    togglePause(event) {
        var el = $(event.currentTarget),
            questionId = el.parent().data('question-id'),
            pollId = el.parent().data('poll-id'),
            isPaused = el.hasClass('btn-success');

        isPaused
            ? this.unpause(el, pollId)
            : this.pause(el, pollId);
    }

    pause(el, pollId) {
        var route = this.getRoute(pollId, 'pause');

        $.ajax(route, {
            method: 'POST',
            data: {action: 'pauze'}
        }).success(data => {
            el.toggleClass('btn-success btn-info')
                .find('i').toggleClass('glyphicon-play glyphicon-pause')
                .parent().find('span').text('Continue');

        }).error(this.error)
    }

    unpause(el, pollId) {
        var route = this.getRoute(pollId, 'pause');

        $.ajax(route, {
            method: 'POST',
            data: {action: 'continue'}
        }).success(data => {
            el.toggleClass('btn-success btn-info')
                .find('i').toggleClass('glyphicon-play glyphicon-pause')
                .parent().find('span').text('Pause');

        }).error(this.error)
    }

    checkAnswers() {
        var route = this.getRoute('other', 'answerCount');

        $.ajax(route, {
            method: 'GET'
        }).success(data => {
            if(!data || !data['success']) return this.error(data);

            for(var key in data.answers) {
                $('.question_' + key).find('.answer_count').text(data.answers[key]);
            }
        }).error(err => {
            //console.warn('Ping answers error: ', err);
        });
    }

    error(xhr, status, errorThrown) {
        console.log(xhr, status, errorThrown);

        if(xhr && xhr.hasOwnProperty('success') && typeof xhr.success !== 'function') {
            if(xhr.hasOwnProperty('error')) {
                xhr.error = xhr.error || 'Whoops, something went wrong';
                return Notifications.error(xhr.error);
            }
        }

        if(xhr && xhr.readyState === 0) {
            return Notifications.error('Internet error, make sure you are connected to the internet');
        }

        if(xhr && xhr.readyState === 4) {
            return Notifications.error(errorThrown);
        }

        return Notifications.error('Whoops, something went wrong.');
    }

    resetQuestion(event) {
        var el = $(event.currentTarget),
            url = el.data('uri');

        if(!confirm('Are you sure you want to delete all answers for this question?')) return;

        $.ajax(url, {method: 'POST'}).success(data => {
            if(!data || !data['success']) return this.error(data);

            el.parent().find('.answer_count').text('0');
        }).error(this.error);

    }

}

var poll = new Poll();



//
//$(function() {
//
//    var $modal = $('#poll'),
//        $poll = null,
//        $question = null,
//        $results = false;
//        $questionNumber = 0,
//        $questionUri = '',
//        $lockUri = '',
//        $pollResultsUri = '',
//        $pollScoreUri = '',
//        $pollShowUri = '',
//        $pollPauzeUri = '',
//        $pollEndUri = '';
//
//    var resetQuestion = function()
//    {
//        $modal.find('#questionNavigation').html( '' );
//        $modal.find('.end').addClass('hide');
//        $modal.find('.question-counter').hide();
//        $modal.find('.question-name').html( '' );
//    };
//
//    var createPagination = function( numberOfItems, currentItem ) {
//        $menu = $('#questionNavigation');
//        // reset previous items
//        $menu.html('');
//        for( var $q = 0; $q < numberOfItems; $q++) {
//            $link = $('<a>').addClass('item').data({question: $poll.questions[$q], questionId: $q}).html($q + 1);
//
//            if ($questionNumber > $q)
//                $link.addClass('old');
//            if ($questionNumber == $q)
//                $link.addClass('active');
//            if ($questionNumber < $q)
//                $link.addClass('new');
//
//            if($poll.questions[$questionNumber] == $question)
//                $link.addClass('selected');
//
//            $menu.append( $link );
//        }
//    };
//
//    var fillQuestion = function()
//    {
//        $.ajax({
//            url: $questionUri + '/' + $question.id,
//            method: 'POST',
//            data: { results: $results },
//            success: function() {
//                $results = false;
//                $modal.find('.question-number').html( $questionNumber + 1 );
//                createPagination( $poll.questions.length, $questionNumber );
//                $modal.find('.question-counter').show();
//                $modal.find('.score').removeClass('hide');
//                $modal.find('.question-name').html( $question.question );
//                $modal.find('.continue-poll').removeClass('continue-poll').addClass('pauze-poll').html('Pauze Poll');
//
//                var listAnswers = $('<ol type="A">');
//                for( var answer in $question.answers )
//                {
//                    listAnswers.append( $('<li>').html($question.answers[answer]) );
//                }
//                $modal.find('.question-answers').html( listAnswers );
//            }
//        });
//    };
//
//    $('#polls').on('click', '.open-modal', function( event )
//    {
//        event.preventDefault();
//
//        $questionUri = $(this).data('question');
//        $pollResultsUri = $(this).data('results');
//        $pollResetUri = $(this).data('reset');
//        $pollEndUri = $(this).data('end');
//        $pollPauzeUri = $(this).data('pauze');
//
//        $.ajax({
//            url: $(this).attr('href'),
//            method: 'POST',
//            success: function( response ) {
//                if( response.data )
//                {
//                    $poll = response.data.poll;
//                    $questionNumber = 0;
//                    $modal.find('.poll-name').html( $poll.name );
//
//                    // Reset any loaded modals before
//                    resetQuestion();
//                    $modal.find('.modal-actions').addClass('hide');
//                    $modal.find('.score').addClass('hide');
//
//                    // Check if the quiz contains a question
//                    if( $poll.questions.length === 0 )
//                    {
//                        $modal.find('.poll-start').hide();
//                        $modal.find('.question-answers').html('Unable to start the poll. There are no questions!');
//                    } else {
//                        $modal.find('.poll-start').show();
//                        $modal.find('.reset').hide();
//                        $modal.find('.question-answers').html('');
//                    }
//
//                    $modal.modal({
//                        backdrop: 'static',
//                        keyboard: false,
//                        show: true
//                    });
//                }
//            }
//        });
//    });
//
//    $('#poll').on('click', '.score', function( event )
//    {
//        event.preventDefault();
//
//        var uri = $pollScoreUri;
//
//        if( $modal.find('.score').html() === 'Show score' )
//        {
//            $modal.find('.score').html('Show Quiz');
//            $modal.find('.lock').addClass('hide');
//            $modal.find('.next').addClass('hide');
//        } else {
//            $modal.find('.score').html('Show score');
//            $modal.find('.lock').removeClass('hide');
//            $modal.find('.next').removeClass('hide');
//            uri = $pollShowUri;
//        }
//
//        $.ajax({
//            url: uri,
//            success: function( data )
//            {
//                console.log( data );
//            }
//        });
//
//    });
//
//    $('#poll').on('click', '.poll-start', function( event )
//    {
//        event.preventDefault();
//        $activeQuestion = 0;
//
//        $modal.find('.poll-start').hide();
//        $modal.find('.modal-actions').removeClass('hide');
//        $modal.find('.lock').show();
//        //if( ($poll.questions.length - 1) > $questionNumber ) {
//        //    $modal.find('.next').show().attr('disabled','disabled');
//        //}
//        $question = $poll.questions[$questionNumber];
//        fillQuestion();
//    });
//
//    $('#poll').on('click', '.lock', function()
//    {
//        $.ajax({
//            url: $lockUri + '/' + $question.id,
//            method: 'post',
//            success: function() {
//
//                $modal.find('.lock').hide();
//                if( ($poll.questions.length - 1) === $questionNumber ) {
//                    $modal.find('.results').removeClass('hide');
//                } else {
//                    //$modal.find('.next').removeAttr('disabled');
//                }
//
//            }
//        });
//    });
//
//    $('#poll').on('click', '.next', function()
//    {
//        $questionNumber++;
//        $modal.find('.lock').show();
//        $modal.find('.reset').hide();
//        if( ($poll.questions.length - 1) === $questionNumber )
//        {
//            $modal.find('.next').hide();
//        } else {
//            //$modal.find('.next').attr('disabled','disabled');
//        }
//
//        $question = $poll.questions[$questionNumber];
//        fillQuestion();
//    });
//
//    $('#poll').on('click', '.results', function()
//    {
//        $.ajax({
//            url: $pollResultsUri,
//            method: 'post',
//            success: function () {
//                resetQuestion();
//                var icon = $('<span>').addClass('fa fa-bar-chart');
//                $modal.find('.question-answers').html( icon );
//
//                $modal.find('.results').addClass('hide');
//                $modal.find('.end').removeClass('hide');
//            }
//        });
//
//    });
//
//
//    $('#poll').on('click', '.pauze-poll', function () {
//        $(this).addClass('continue-poll').removeClass('pauze-poll').html('Continue poll');
//
//        $.ajax({
//            url: $pollPauzeUri,
//            method: 'post',
//            data: {
//                action: 'pauze'
//            }
//        });
//    });
//
//    $('#poll').on('click', '.continue-poll', function () {
//        $(this).addClass('pauze-poll').removeClass('continue-poll').html('Pauze poll');
//        $.ajax({
//            url: $pollPauzeUri,
//            method: 'post',
//            data: {
//                action: 'continue'
//            }
//        });
//    });
//
//    $('#poll').on('click', '.end, .close', function()
//    {
//
//        var end = confirm('Are you sure to end this poll?');
//
//        if( end === true ) {
//            $.ajax({
//                url: $pollEndUri,
//                method: 'post',
//                success: function () {
//                    $modal.modal('hide');
//                    document.location.reload();
//                }
//            });
//        }
//    });
//
//    $('#poll').delegate( '.reset', 'click', function( e ) {
//        e.preventDefault();
//
//        var uri = $pollResetUri.replace('0/reset', $question.id + '/reset');
//        $.ajax({
//            url: uri,
//            method: 'POST',
//            success: function( e ) {
//                console.log( e );
//            }
//        })
//
//    });
//
//    $('#poll').delegate( 'a.old, a.active', 'click', function ( e ) {
//        e.preventDefault();
//        var question = $(this).data('questionId');
//        $modal.find('.reset').show();
//        $question = $poll.questions[question];
//        $results = true;
//        fillQuestion();
//    });
//
//
//    $('#polls').on('click', '.remove-quiz', function( e ) {
//        e.preventDefault();
//        var conf = confirm('Are you sure to delete the quiz with all related questions and answers?');
//        if( conf ) {
//            $(this).parent().submit();
//        }
//    });
//});
