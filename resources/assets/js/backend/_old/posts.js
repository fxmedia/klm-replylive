/* global console, newPostsUri, lastPostId, toggleViewUri, destroyUri */

console.clear();

var $interval = 1000,
    $lastPostId = lastPostId,
    $toggleViewUri = toggleViewUri,
    $destroyUri = destroyUri,
    $form = $('#posts');

var checkForNewPosts = function()
{
    $.ajax({
        url: newPostsUri,
        method: 'post',
        data: { last_id: $lastPostId },
        success: function( response )
        {
            if( response.data )
            {
                var posts = response.data.posts;
                for( var post in posts )
                {
                    // Create elements for row
                    post = posts[post];
                    console.log('Post', post);

                    var viewUrl = $toggleViewUri,
                        destoryUrl = $destroyUri,
                        row = $('<tr>').attr('id','post_' + post.id),
                        column1 = $('<td>'),
                            toggle_hide = $('<a>').attr('href', viewUrl.replace('0', post.id) ).addClass('toggle-hide'),
                                viewIcon = $('<i>').addClass('fa fa-eye'),
                        column2 = $('<td>').html( post.message),
                        columnImg = $('<td>').append($('<img>').attr('src', post.file).attr('style', 'max-height: 100px; max-width: 200px;')),
                        column3 = $('<td>').html( post.type ),
                        columnName =  $('<td>').html( post.user.firstname + ' ' + post.user.lastname ),
                        column4 = $('<td>'),
                            destroy = $('<a>').attr('href', destoryUrl.replace('0', post.id)).addClass('btn btn-danger btn-xs'),
                                destroyIicon = $('<i>').addClass('fa fa-trash-o');

                    if( post.visible === 0 ) {
                        viewIcon.addClass('fa-eye-slash').removeClass('fa-eye');
                    }

                    // Add buttons to columns
                    toggle_hide.append(viewIcon);
                    column1.append(toggle_hide);
                    destroy.append(destroyIicon);
                    column4.append(destroy);

                    // Add columns to row
                    row.append(column1).append(column2).append(columnImg).append(column3).append(columnName).append(column4);
                    $form.prepend( row );

                    if( ! post.visible )
                    {
                        row.addClass('greyed-out');
                    }
                    $lastPostId = post.id;
                }
            }
        }
    });

    setTimeout( function() { checkForNewPosts(); }, $interval);
};

$(function() {

    checkForNewPosts();

    $form.delegate('.toggle-hide', 'click', function( event ) {
        event.preventDefault();

        var url = $(this).attr('href'),
            icon = $(this).find('i.fa'),
            row = $(this).parent().parent();

        if( icon.hasClass( 'fa-eye-slash' ) )
        {
            $.ajax({
                url: url,
                method: 'post',
                success: function( response ) {
                    if( response.data )
                    {
                        var post = response.data.post;
                        if( post.visible === 1 ) {
                            row.removeClass('greyed-out');
                            icon.removeClass('fa-eye-slash');
                            icon.addClass('fa-eye');
                            row.find('.btn-danger').remove();
                        }
                    }
                }
            });
        }

    });

    $form.delegate('.btn-danger', 'click', function( event ) {
        event.preventDefault();

        if(!confirm('Are you sure you want to remove this post completely?')) return false;

        $.ajax({
            url: $(this).attr('href'),
            method: 'post',
            success: function( response ) {
                $('#post_' + response.data.post.id).remove();
            }
        });
    });


});