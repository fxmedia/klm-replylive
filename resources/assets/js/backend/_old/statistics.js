class Statistics {

    constructor() {
        this.element = $('#statistics');

        this.interval = setInterval(this.update.bind(this), 4000);
        this.update();
    }

    update() {
        if(document.location.pathname.indexOf('/login') > -1) {
            return this.element.hide();
        }
        this.element.show();

        $.ajax('/api/statistics').success(data => {
            if(!data || !data.success) {
                this.element.find('.connected').hide();
                this.element.find('.disconnected').show();

                return;
            }

            this.element.find('.connected').show();
            this.element.find('.disconnected').hide();
            this.element.find('.users-online').text(data.stats.active_connections);
            this.element.find('.posts').text(data.stats.posts);
        }).error(err => {
            this.element.find('.connected').hide();
            this.element.find('.disconnected').show();
        });
    }

    stop() {
        clearInterval(this.interval);
    }

}

window.Statistics = new Statistics();