/* global console, showUri, storeUri, editUri, id, uploadUri, uploadResultUri, questionType */

console.log( 'init Question.js' );

$( function () {

    var $questionID = null,
        $id         = id,
        $mTitle     = $( '#Question' ).find( '.modal-title' ),
        $mBody      = $( '#Question' ).find( '.modal-body' );

    var questionForm = function ( data ) {
        var uri    = editUri,
            action = uri.replace( '0/edit', $questionID + '/edit' );
        if ( undefined === data ) {
            action = storeUri;
            data   = {
                question:  '',
                answer:    '',
                shootout_answer: '',
                answers:   {
                    a:           '',
                    b:           '',
                    c:           '',
                    d:           '',
                    answer_type: 'open',
                    min_range:   0,
                    max_range:   10
                },
                image:     '',
                imagename: '',
                points:    ''
            };
        }

        var imageUri = {};
        if ( data.image ) {
            imageUri = {'background-image': 'url(' + data.image + ')'};
        }

        var list = [];
        switch ( questionType ) {
            case 'poll':
                list = [ 'a', 'b', 'c', 'd', 'e', 'f', 'g', 'h', 'i', 'j','k','l']; //5 answers for HGC wall
                break;
            case 'quiz':
                list = [ 'a', 'b', 'c', 'd' ];
                break;
            case 'hat':
                list = [ 'a', 'b' ];
                break;
            case 'questionnaire':
                list = [];
                break;
            default:
                list = [ 'a', 'b', 'c', 'd' ];
        }

        var posibleAnswers = $( '<select>' ).attr( 'name', 'answer' ).addClass( 'form-control' );
        for ( var i in list ) {
            posibleAnswers.append( $( '<option>' ).val( list[ i ] ).html( list[ i ] ) );
        }
        posibleAnswers.val( data.answer );


        var form        = $( '<form>' ).attr( 'id', 'newQuestion' ).attr( 'method', 'post' ).attr( 'action', action ),
            columnLeft  = $( '<div>' ).addClass( 'pull-left col-xs-6' ),
            columnRight = $( '<div>' ).addClass( 'pull-right col-xs-6' ),
            input       = $( '<input>' ).attr( 'type', 'file' ).attr( 'id', 'fileupload' ).attr( 'name', 'file' ),
            type        = $( '<input>' ).attr( 'type', 'hidden' ).attr( 'name', 'type' ).val( questionType ),
            h_image     = $( '<input>' ).attr( 'type', 'hidden' ).attr( 'name', 'image' ).val( data.imagename ),
            quiz        = $( '<input>' ).attr( 'type', 'hidden' ).attr( 'name', questionType + '_id' ).val( $id ),
            question    = $( '<div>' ).addClass( 'form-group' )
                .append( $( '<label>' ).html( 'Question' ).addClass( 'col-xs-3' ) )
                .append( $( '<div>' ).addClass( 'col-xs-9' )
                    .append( $( '<input>' ).attr( 'type', 'text' ).attr( 'name', 'question' ).addClass( 'form-control' ).val( data.question ) )
                ),
            answer      = $( '<div>' ).addClass( 'form-group' )
                .append( $( '<label>' ).html( 'Answer' ).addClass( 'col-xs-3' ) )
                .append( $( '<div>' ).addClass( 'col-xs-9' )
                    .append( posibleAnswers )
                ),
            points      = $( '<div>' ).addClass( 'form-group' )
                .append( $( '<label>' ).html( 'Points' ).addClass( 'col-xs-3' ) )
                .append( $( '<div>' ).addClass( 'col-xs-3' )
                    .append( $( '<input>' ).attr( 'type', 'number' ).attr( 'name', 'points' ).addClass( 'form-control' ).val( data.points ) )
                ),
            image       = $( '<div>' ).addClass( 'form-group' )
                .append( $( '<label>' ).html( 'Image' ).addClass( 'col-xs-3' ) )
                .append( $( '<div>' ).addClass( 'col-xs-9' )
                    .append( $( '<div>' ).addClass( 'image' ).css( imageUri ) )
                    .append( $( '<div>' ).addClass( 'image-loader' ) )
                    .append( input )
                ),
            answers     = $( '<div>' );

        for ( var a in list ) {
            var answerLabel = $( '<label>' ).html( ' Answer ' + list[ a ] ).addClass( 'col-xs-3' ),
                answerInput = $( '<div>' ).addClass( 'col-xs-9' ).append( $( '<input>' ).attr( 'type', 'text' ).attr( 'name', 'answers[' + list[ a ] + ']' ).addClass( 'form-control' ).val( data.answers[ list[ a ] ] ) );

            answers.append( $( '<div>' ).addClass( 'form-group' ).append( answerLabel ).append( answerInput ) );
        }

        if( questionType == 'quiz' ) {
            var SOanswerLabel = $( '<label>' ).html( 'Shootout Answer' ).addClass( 'col-xs-3' ),
                SOanswerInput = $( '<div>' ).addClass( 'col-xs-9' ).append( $( '<input>' ).attr( 'type', 'text' ).attr( 'name', 'shootout_answer' ).addClass( 'form-control' ).val( data.shootout_answer ) );

            answers.append( $( '<div>' ).addClass( 'form-group' ).append( SOanswerLabel ).append( SOanswerInput ) );
        }


        columnLeft.append( quiz )
            .append( type )
            .append( h_image )
            .append( question )
            .append( answers );

        if ( questionType == 'quiz' || questionType == 'hat' ) {
            columnLeft.append( points );
            columnRight.append( answer );
        }

        if ( questionType == 'quiz' ) {
            columnRight.append( image );
            columnRight.append()
        }


        /** Questionnaire form builder **/
        if ( questionType == 'questionnaire' ) {
            var answerType =
                    $( '<div>' ).addClass( 'form-group' )
                        .append(
                            $( '<label>' ).html( 'Answer type' ).addClass( 'col-xs-3' )
                        ).append(
                        $( '<div>' ).addClass( ' col-xs-9' ).append(
                            $( '<select>' ).attr( 'name', 'answer_type' ).addClass( 'form-control' )
                                .append( $( '<option>' ).val( 'open' ).html( 'Open answer' ) )
                                .append( $( '<option>' ).val( 'abcd' ).html( 'A B C D' ) )
                                .append( $( '<option>' ).val( 'range' ).html( 'Range slider' ) )
                                .append( $('<option>').val('multiple').html('A B C D Multiple') )
                                .val( data.answers.answer_type || 'open' )
                                .on( 'change', function ( e ) {
                                    columnRight.empty();
                                    columnRight.append( questionnaireHtml( $( this ).val() ) );
                                } )
                        )
                    );

            function questionnaireHtml ( type ) {
                var base = $( '<div>' );

                function addAnswer(a) {
                    if(!a) return;
                    var answerLabel = $( '<label>' ).html( ' Answer ' + a ).addClass( 'col-xs-3' ),
                        answerInput = $( '<div>' ).addClass( 'col-xs-9' ).append( $( '<input>' ).attr( 'type', 'text' ).attr( 'name', a ).addClass( 'form-control' ).val( data.answers[ a ] ) );
                    base.append( $( '<div>' ).addClass( 'form-group' ).append( answerLabel ).append( answerInput ) );
                    base.find('select[name=answer]').append( $( '<option>' ).val( a ).html( a ) );
                }

                switch ( type ) {
                    case 'open':

                        break;
                    case 'abcd':
                    case 'multiple':
                        var list = [ null, 'a', 'b', 'c', 'd' ],
                            copy = $.extend({}, data.answers);

                        delete copy.answer_type;
                        delete copy.max_range;
                        delete copy.min_range;

                        $.each(copy, function(k, val) {
                            if(val.length > 0 && list.indexOf(k) === -1) {
                                list.push(k);
                            }
                        });

                        var posibleAnswers = $( '<select>' ).attr( 'name', 'answer' ).addClass( 'form-control' );
                        for ( var a in list ) {
                            addAnswer(list[a]);
                            posibleAnswers.append( $( '<option>' ).val( list[ a ] ).html( list[ a ] ) );
                        }
                        posibleAnswers.val( data.answer );

                        var answer      = $( '<div>' ).addClass( 'form-group' )
                            .append( $( '<label>' ).html( 'Answer' ).addClass( 'col-xs-3' ) )
                            .append( $( '<div>' ).addClass( 'col-xs-9' )
                                .append( posibleAnswers )
                            );

                        base.append( answer );
                        base.append( $( '<div>' ).addClass( 'form-group' ).append( $('<label>').html('Add more').addClass( 'col-xs-3' ) ).append(
                            $('<div>').addClass( 'add-more-btn col-xs-9' ).html('+')
                                .on('click', function(e) {
                                    var lastItem = list[list.length - 1];
                                    var nextItem = lastItem.substring(0,lastItem.length-1)+String.fromCharCode(lastItem.charCodeAt(lastItem.length-1)+1);
                                    console.log(lastItem, nextItem);
                                    list.push(nextItem);
                                    addAnswer(nextItem);
                                })
                        ) );
                        break;
                    case 'range':
                        base.append( $( '<div>' ).addClass( 'form-group' ).append(
                            $( '<label>' ).html( 'Min range' ).addClass( 'col-xs-3' ),
                            $( '<div>' ).addClass( ' col-xs-9' ).append(
                                $( '<input>' ).attr( 'name', 'min_range' ).addClass( 'form-control' ).val( data.answers.min_range || 0 )
                            )
                        ) );
                        base.append( $( '<div>' ).addClass( 'form-group' ).append(
                            $( '<label>' ).html( 'Max range' ).addClass( 'col-xs-3' ),
                            $( '<div>' ).addClass( ' col-xs-9' ).append(
                                $( '<input>' ).attr( 'name', 'max_range' ).addClass( 'form-control' ).val( data.answers.max_range || 10 )
                            )
                        ) );
                        break;
                }

                return base;
            }

            // trying to add answer to questionnaire
            // list = [ 'a', 'b', 'c', 'd' ];
            // var posibleAnswers = $( '<select>' ).attr( 'name', 'answer' ).addClass( 'form-control' );
            // for ( var i in list ) {
            //     posibleAnswers.append( $( '<option>' ).val( list[ i ] ).html( list[ i ] ) );
            // }
            // posibleAnswers.val( data.answer );
            //
            // var answer      = $( '<div>' ).addClass( 'form-group' )
            //     .append( $( '<label>' ).html( 'Answer' ).addClass( 'col-xs-3' ) )
            //     .append( $( '<div>' ).addClass( 'col-xs-9' )
            //         .append( posibleAnswers )
            //     );

            columnLeft.append( answerType );
            columnRight.append( questionnaireHtml( data.answers.answer_type ) );
            //columnRight.append( answer );
        }

        form.append( columnLeft ).append( columnRight ).append( $( '<div>' ).addClass( 'clearfix' ) );

        input.fileupload( {
            url:              uploadUri,
            dataType:         'json',
            start:            function () {
                $( '.image-loader' ).removeClass( 'hide' );
            },
            done:             function ( e, data ) {
                $( '.image-loader' ).addClass( 'hide' );
                $( '.image' ).css( {'background-image': 'url(' + data._response.result.filepath + ')'} );
                $( 'input[name*="image"]' ).val( data._response.result.filename );
            },
            replaceFileInput: true
        } );

        return form;
    };

    var makeQuestion = function () {

        $.ajax( {
            url:     $( '#Question' ).find( 'form' ).attr( 'action' ),
            method:  'POST',
            data:    $( '#Question' ).find( 'form' ).serialize(),
            success: function ( response ) {
                if ( response.data ) {
                    if ( response.data.action === 'update' ) {
                        console.log( $( 'li[data-*="' + response.data.question.id + '"]' ) );
                    }
                    if ( response.data.action === 'new' ) {
                        var data        = response.data.question,
                            $destroyUri = destoryUri.replace( '0/destroy', data.id + '/destroy' ),
                            $resetUri   = resetUri.replace( '0/reset', data.id + '/reset' ),
                            answers     = $( '<small>' ).addClass( 'lightgrey' ).append( ' - ( <span id="answers_' + data.id + '">0</span> answers )' ),
                            question    = $( '<div>' ).addClass( 'question' ).attr( 'id', 'question_' + data.id ).html( data.question ).append( answers ),
                            controls    = $( '<span>' ).addClass( 'pull-right' ),
                            edit        = $( '<span>' ).addClass( 'btn btn-success btn-xs edit-question' ).data( {'question-id': data.id} ).html( $( '<i>' ).addClass( 'glyphicon glyphicon-edit' ) ),
                            reset       = $( '<span>' ).addClass( 'btn btn-warning btn-xs reset-question' ).data( {
                                'question-id': data.id,
                                uri:           $resetUri
                            } ).html( $( '<i>' ).addClass( 'glyphicon glyphicon-edit' ) ),
                            destroy     = $( '<span>' ).addClass( 'btn btn-danger btn-xs remove-question' ).data( {
                                'question-id': data.id,
                                uri:           $destroyUri
                            } ).html( $( '<i>' ).addClass( 'glyphicon glyphicon-trash' ) ),
                            clearfix    = $( '<div>' ).addClass( 'clearfix' ),
                            row         = question.append(
                                controls.append( edit )
                                    .append( reset )
                                    .append( destroy )
                            ).append( clearfix );

                        $( '.questions' ).append( row );
                    }
                    //if(questionType === 'poll') {
                    //    $('.add-question').hide();
                    //}
                    $( '#Question' ).modal( 'hide' );
                }
            }
        } );
    };

    // Show results
    $('body').on('click', '.results-question', function () {
        var $uri = $(this).data('uri');

        $.ajax({
            url: $uri,
            method: 'GET',
            success: function (response) {
                $mTitle.html( 'Result');
                var html = "";

                $.each(response, function(k, v){
                    html += "<div>" + v + "</div>";
                });

                $mBody.html(html);
                console.log(response);
                $('#Question').find('.btn-primary').html('Save changes');
                $('#Question').modal('show');
            }
        });
    });

    // Edit question
    $( 'body' ).delegate( '.edit-question', 'click', function () {
        $questionID = $( this ).data( 'question-id' );
        var uri     = showUri;

        $.ajax( {
            url:     uri.substring( 0, uri.length - 1 ) + $questionID,
            method:  'GET',
            success: function ( response ) {
                $mTitle.html( 'Edit ' + questionType + ' question: ' + response.data.question.question );
                $mBody.html( questionForm( response.data.question ) );
                if( questionType === 'poll') {
                    $mBody.prepend('<p>If you want to create the creative ladder/coloured poll, please  fill out ALL 10 answers.</p>\n<p>When you would like to create a ‘normal’ poll, please fill out maximum 5 answers</p><br/>\n\n\n');
                }
                $( '#Question' ).find( '.btn-primary' ).html( 'Save changes' );
                $( '#Question' ).modal( 'show' );
            }
        } );
    } );

    // Reset question
    $( 'body' ).delegate( '.reset-question', 'click', function ( e ) {
        e.preventDefault();

        var conf = confirm( 'Are you sure to reset this question with all answers?' );

        if ( conf ) {
            $.ajax( {
                url:     $( this ).data( 'uri' ),
                method:  'POST',
                success: function ( e ) {
                    $( '#answers_' + e.question.id ).html( '0' );
                }
            } );
        }
    } );

    // Destroy question
    $( 'body' ).delegate( '.remove-question', 'click', function ( e ) {
        e.preventDefault();

        var conf = confirm( 'Are you sure to delete this question with possible answers?' );

        if ( conf ) {
            $.ajax( {
                url:     $( this ).data( 'uri' ),
                method:  'POST',
                success: function ( e ) {
                    $( '#question_' + e.question.id ).remove();
                }
            } );
        }
    } );

    // Show modal with question form
    $( '#add-question' ).on( 'click', function ( event ) {
        event.preventDefault();

        $mTitle.html( 'Add new ' + questionType + ' question' );
        $( '#Question' ).find( '.btn-primary' ).html( 'Create' );
        $mBody.html( questionForm() );

        $( '#Question' ).modal( 'show' );
    } );


    // Create new question
    $( '#Question' ).find( '.btn-primary' ).click( makeQuestion );
} );
