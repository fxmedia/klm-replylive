/* global console, confirm */

console.log('Init Hat.js');

$(function() {

    var $modal = $('#hat'),
        $hat = null,
        $question = null,
        $hatNumber = 0,
        $hatUri = '',
        $lockUri = '',
        $hatResultsUri = '',
        $hatScoreUri = '',
        $hatShowUri = '',
        $hatEndUri = '',
        $page = 1,
        $hatScorePageUri = '',
        $activeQuestion = 0;

    var resetQuestion = function()
    {
        $modal.find('#questionNavigation').html( '' );
        $modal.find('.end').addClass('hide');
        $modal.find('.question-counter').hide();
        $modal.find('.question-name').html( '' );
    };

    var createPagination = function( numberOfItems, currentItem ) {
        $menu = $('#questionNavigation');
        // reset previous items
        $menu.html('');
        for( var $q = 0; $q < numberOfItems; $q++) {
            $link = $('<a>').addClass('item').data({question: $hat.questions[$q], questionId: $q}).html($q + 1);

            if ($hatNumber > $q)
                $link.addClass('old');
            if ($hatNumber == $q)
                $link.addClass('active');
            if ($hatNumber < $q)
                $link.addClass('new');

            if($hat.questions[$hatNumber] == $hat)
                $link.addClass('selected');

            $menu.append( $link );
        }
    }

    var fillQuestion = function()
    {
        $.ajax({
            url: $hatUri + '/' + $question.id,
            method: 'POST',
            success: function() {
                $modal.find('.question-number').html( $hatNumber + 1 );
                createPagination( $hat.questions.length, $hatNumber );
                $modal.find('.question-counter').show();
                $modal.find('.score').removeClass('hide');
                $modal.find('.question-name').html( $question.question );

                var listAnswers = $('<ol type="A">');
                for( var answer in $question.answers )
                {
                    listAnswers.append( $('<li>').html($question.answers[answer]) );
                }
                $modal.find('.question-answers').html( listAnswers );
            }
        });
    };

    $('#hats').on('click', '.open-modal', function( event )
    {
        event.preventDefault();

        $hatUri = $(this).data('question');
        $lockUri = $(this).data('lock');
        $hatResultsUri = $(this).data('results');
        $hatEndUri = $(this).data('end');
        $hatScoreUri = $(this).data('score');
        $hatShowUri = $(this).data('hat');
        $hatScorePageUri = $(this).data('scorepage');

        $page = 1;

        $.ajax({
            url: $(this).attr('href'),
            method: 'POST',
            success: function( response ) {
                if( response.data )
                {
                    $hat = response.data.hat;
                    console.log('Open model received hat::', $hat);
                    $hatNumber = 0;
                    $modal.find('.hat-name').html( $hat.name );

                    // Reset any loaded modals before
                    resetQuestion();
                    $modal.find('.modal-actions').addClass('hide');
                    $modal.find('.score').addClass('hide');

                    // Check if the quiz contains a question
                    if( $hat.questions.length === 0 )
                    {
                        $modal.find('.hat-start').hide();
                        $modal.find('.question-answers').html('Unable to start the quiz. There are no questions!');
                    } else {
                        $modal.find('.hat-start').show();
                        $modal.find('.question-answers').html('');
                    }

                    $modal.modal({
                        backdrop: 'static',
                        keyboard: false,
                        show: true
                    });
                }
            }
        });
    });

    $('#hat').on('click', '.prev-result, .next-result', function( event ) {

        if( $(this).hasClass('prev-result') ) {
            if( $page > 1 ) {
                $page--;
            }
        } else {
            $page++;
        }

        var uri = $hatScorePageUri.replace('0/score_page', $hat.id + 'score_page');

        console.log(uri);

        $.ajax({
            url: uri,
            data: {page: $page},
            success: function( data )
                 {
                     $('.page-number').html($page);
                 }
        });

    } );

    $('#hat').on('click', '.score', function( event )
    {
        event.preventDefault();

        var uri = $hatScoreUri;

        if( $modal.find('.score').html() === 'Show score' )
        {
            $modal.find('.score').html('Show Hat');
            $modal.find('.lock').addClass('hide');
            $modal.find('.next').addClass('hide');
            $modal.find('.prev-result').removeClass('hide');
            $modal.find('.page-number').removeClass('hide');
            $modal.find('.next-result').removeClass('hide');
        } else {
            $modal.find('.score').html('Show score');
            $modal.find('.lock').removeClass('hide');
            $modal.find('.next').removeClass('hide');
            $modal.find('.prev-result').addClass('hide');
            $modal.find('.page-number').addClass('hide');
            $modal.find('.next-result').addClass('hide');
            $page = 1;
            $('.page-number').html($page);
            uri = $hatShowUri;
        }

        $.ajax({
            url: uri,
            success: function( data )
            {
                console.log( data );
            }
        });

    });

    $('#hat').on('click', '.hat-start', function( event )
    {
        event.preventDefault();
        $activeQuestion = 0;

        $modal.find('.hat-start').hide();
        $modal.find('.modal-actions').removeClass('hide');
        $modal.find('.lock').show();
        if( ($hat.questions.length - 1) > $hatNumber ) {
            $modal.find('.next').show().attr('disabled','disabled');
        }
        $question = $hat.questions[$hatNumber];
        fillQuestion();
    });

    $('#hat').on('click', '.lock', function()
    {
        $.ajax({
            url: $lockUri + '/' + $hat.id,
            method: 'post',
            success: function() {

                $modal.find('.lock').hide();
                if( ($hat.questions.length - 1) === $hatNumber ) {
                    $modal.find('.results').removeClass('hide');
                } else {
                    $modal.find('.next').removeAttr('disabled');
                }

            }
        });
    });

    $('#hat').on('click', '.next', function()
    {
        $hatNumber++;

        console.log('Next hat question %s',$hatNumber, $hat);

        $modal.find('.lock').show();
        if( ($hat.questions.length - 1) === $hatNumber )
        {
            $modal.find('.next').hide();
        } else {
            $modal.find('.next').attr('disabled','disabled');
        }


        $question = $hat.questions[$hatNumber];
        fillQuestion();
    });

    $('#hat').on('click', '.results', function()
    {
        $.ajax({
            url: $hatResultsUri,
            method: 'post',
            success: function () {
                resetQuestion();
                var icon = $('<span>').addClass('fa fa-bar-chart');
                $modal.find('.question-answers').html( icon );

                $modal.find('.results').addClass('hide');
                $modal.find('.end').removeClass('hide');
            }
        });

    });

    $('#hat').on('click', '.end, .close', function()
    {
        var end = confirm('Are you sure to end this hat?');

        if( end === true ) {
            $.ajax({
                url: $hatEndUri,
                method: 'post',
                success: function () {
                    $modal.modal('hide');
                    document.location.reload();
                }
            });
        }
    });

    $('#hat').delegate( 'a.old, a.active', 'click', function ( e ) {
        e.preventDefault();
        var question = $(this).data('questionId');
        $hat = $hat.questions[question];
        fillQuestion();
    });


    $('#hats').on('click', '.remove-hat', function( e ) {
        e.preventDefault();
        var conf = confirm('Are you sure to delete the quiz with all related questions and answers?');
        if( conf ) {
            $(this).parent().submit();
        }
    });
});