console.log('Init Messages.js');

var listOfUsersSelectionInitialized = false;
var groupsSelectionInitialized = false;
var pasteSelectionInitialized = false;

$(document).ready(function () {
    $('#show-users').on('click', function () {
        initializeListOfUsersSelection();
    });

    $('#show-groups').on('click', function () {
        initializeGroupsSelection();
    });

    $('#show-paste').on('click', function () {
        initializePasteSelection();
    });

    $('.group-row').on('click', function () {
        $(this).toggleClass('success');
    });

    $('.tab-item').on('click', function () {
        $('.tab-item').removeClass('active');
        $(this).addClass('active');
    });

    $('#setup-users .select-filter').on('click', handleUserlistFilters);
    $('#setup-groups .select-filter').on('click', handleGroupsFilters);
    $('#setup-paste .select-filter').on('click', handlePasteFilters);

    function handleUserlistFilters() {
        var filter = $(this).data('filter');

        $('#setup-users .user-row').each(function (k, row) {
            if ($(row).data('filter-' + filter) == "1") {
                $(row).detach().appendTo('#setup-users .send-to-list__table tbody');
            }
        });
    }

    function handlePasteFilters() {
        var filter = $(this).data('filter');

        $('#setup-paste .send-to-list__table .user-row').each(function (k, row) {
            if ($(row).data('filter-' + filter) != "1") {
                $(row).addClass('disabled');
            }
        });
    }

    function handleGroupsFilters() {
        var filter = $(this).data('filter');
        $('#setup-groups .send-to-list__table .user-row').each(function (k, row) {
            if ($(row).data('filter-' + filter) != "1") {
                $(row).addClass('disabled');
            }
        });
    }

    function initializeListOfUsersSelection() {

        if (!listOfUsersSelectionInitialized) {
            console.log('initializeListOfUsersSelection');

            var userRows = $('#users .user-row').clone();

            $('.list-of-users tbody').append(userRows);

            userRows.on('click', function () {
                toggleUserRow($(this));
            });

            listOfUsersSelectionInitialized = true;
        }

        $('.setup-container').addClass('hide');
        $('#setup-users').removeClass('hide');
    }

    function toggleUserRow(row) {
        if (row.parent().parent().hasClass('send-to-list__table')) {
            row.detach().appendTo('.list-of-users__table tbody');
        } else {
            row.detach().appendTo('#setup-users .send-to-list__table tbody');
        }
    }

    function initializeGroupsSelection() {
        if (!groupsSelectionInitialized) {
            console.log('initializeGroupsSelection');

            $('.group-row').on('click', function () {
                toggleGroupRow($(this));
            });

            groupsSelectionInitialized = true;
        }

        $('.setup-container').addClass('hide');
        $('#setup-groups').removeClass('hide');
    }

    function toggleGroupRow(row) {
        var userRowsWithThisGroup;

        if (row.hasClass('selected')) {
            userRowsWithThisGroup = $('#setup-groups .user-in-' + row.attr('id'));
            userRowsWithThisGroup.remove();
            row.removeClass('selected');
        } else {
            userRowsWithThisGroup = $('#users .user-in-' + row.attr('id')).clone();
            $('#setup-groups .send-to-list__table tbody').append(userRowsWithThisGroup);
            userRowsWithThisGroup.on('click', function () {
                $(this).toggleClass('disabled');
            });
            row.addClass('selected');
        }
    }

    function initializePasteSelection() {

        if (!pasteSelectionInitialized) {
            console.log('initializePasteSelection');

            $('#submit-emails-from-paste').on('click', function () {

                $('#setup-paste .send-to-list__table tbody').empty();

                var pastedListOfUsers = $('#emails-textarea').val().split('\n');

                pastedListOfUsers.forEach(function (email) {
                    var invalidEmailDiv = document.createElement('li');
                    var invalidEmailDivContent = document.createTextNode(email);
                    invalidEmailDiv.appendChild(invalidEmailDivContent);
                    invalidEmailDiv.className = '.user-row';
                    invalidEmailDiv.content = email;

                    if ($('#users .user-row[data-email="' + email + '"]').length) {
                        var row = $('#users .user-row[data-email="' + email + '"]').clone();
                        $('#setup-paste .send-to-list__table tbody').append(row);
                        row.on('click', function () {
                            $(this).toggleClass('disabled');
                        });
                    } else {
                        $('.invalid-emails').removeClass('hide');
                        $('#invalid-emails-list').append(invalidEmailDiv);
                    }
                });
            });

            pasteSelectionInitialized = true;
        }

        $('.setup-container').addClass('hide');
        $('#setup-paste').removeClass('hide');
    }

    initializeListOfUsersSelection();

    $('#setup-ready-btn, #goto-preview').on('click', setupReadyBtnHandler);
    $('#preview-ready-btn').on('click', submitEmail);

    function setupReadyBtnHandler() {
        var userRows = $('.setup-container:not(.hide)').find('.send-to-list .user-row');

        if (userRows.length == 0) {
            $('#no-users-alert').removeClass('hide');

            return;
        }

        var userIDs = [],
            notSending = 0;

        userRows.each(function (k, row) {
            if (($(row).data('email') && $(row).data('email').length) ||
                ($(row).data('sms') && $(row).data('sms').toString().length)) {
                if (!$(row).hasClass('disabled')) {
                    userIDs.push($(row).attr('id').replace('user_', ''));
                }
            } else {
                notSending++;
            }
        });

        if (notSending !== 0) {
            if (!confirm("There are " + notSending + " users without an email address, they will not be added to the list. Continue?")) return false;
        }

        if (userIDs.length === 0) {
            $('#no-users-alert').removeClass('hide');

            return;
        }

        $.ajax({
            url: $(this).data('url'),
            method: 'POST',
            data: {ids:  JSON.stringify(userIDs) },
            success: function (data) {
                window.location.href = data.url;
            },
            error: function (error) {
                console.error(error);
            }
        });
    }

    function submitEmail() {
        if (!confirm('Are you sure?')) return;

        var modal = $('#status');
        modal.modal();

        $.ajax({
            url: $(this).data('url'),
            method: 'POST',
            data: {},
            success: function (data) {

                $('#result-total').text(count);

                modal.find('#cancel').on('click', function () {
                    document.location.href = data.history;
                });

                var interval = setInterval(function () {
                    var body = modal
                        .find('tbody');

                    var pingbackData = {
                        batch: data.batch
                    };

                    if (data.email_id) {
                        pingbackData.email_id = data.email_id;
                    }

                    if (data.sms_id) {
                        pingbackData.sms_id = data.sms_id;
                    }

                    $.ajax({
                        url: data.pingback,
                        method: 'POST',
                        data: pingbackData,
                        success: function (result) {
                            console.log('Pingback data', result);
                            if (!result.success || !result.logs.length) return;

                            $('#result-done').text(result.logs.length);

                            if (result.logs.length >= count) {
                                clearInterval(interval);
                                $('#waiting-for-response').addClass('hide');
                            }

                            $.each(result.logs, function (k, row) {
                                if (body.find('#log_' + row.id).length === 0) {
                                    body.append(
                                        $('<tr>').attr('id', 'log_' + row.id)
                                            .append($('<td>').html(row.user.name))
                                            .append($('<td>').html(data.email_id ? row.user.email : row.user.phonenumber))
                                            .append($('<td>').html(row.error))
                                    )
                                }
                            });
                        }
                    });
                }, 300);
            },
            error: function (error) {
                console.error(error);
            }
        });
    }

    $('.removeUpload').on('click', function () {
        var el = $(this),
            url = el.data('url'),
            type = el.data('type');

        $.ajax({
            url: url,
            method: 'POST',
            data: {type: type},
            success: function () {
                el.parent().text('None');
            }
        });
    });

    $('.btn-remove').on('click', function () {
        if(!confirm('Are you sure?')) return false;
        $(this).closest('form').submit();
    });

    /** Parse body text input to look for tags and validate them **/
    $('#body_text').on('keyup', function () {
        var valid = /\[(url|firstname|lastname|extra_[a-z0-9\s\_]*)\]/gi;
        var all = /\[[a-z0-9\s\_]*\]/gi;
        var str = $(this).val();
        var m;
        var count_valid = 0;
        var count_all = 0;

        while ((m = all.exec(str)) !== null) {
            if (m.index === all.lastIndex) {
                all.lastIndex++;
            }
            count_all++;
        }
        m = null;
        while ((m = valid.exec(str)) !== null) {
            if (m.index === valid.lastIndex) {
                valid.lastIndex++;
            }
            count_valid++;
        }
        var w = (count_all === count_valid) ? 'all' : '[' + count_valid + ']';

        $('#tag-status').html('Detected [' + count_all + '] tags in the content, of which ' + w + ' are valid').removeClass('text-success text-danger')
            .addClass((count_all === count_valid) ? 'text-success' : 'text-danger');
    });

    /** Insert tags into the body text on click **/
    $('#tag-list').find('li').on('click', function () {
        var tag = $(this).data('value');
        insertAtCaret('body_text', '[' + tag + ']');
    });

    function insertAtCaret(areaId, text) {
        var txtarea = document.getElementById(areaId);
        if (!txtarea) {
            return;
        }

        var scrollPos = txtarea.scrollTop;
        var strPos = 0;
        var br = ((txtarea.selectionStart || txtarea.selectionStart == '0') ?
            "ff" : (document.selection ? "ie" : false ) );
        if (br == "ie") {
            txtarea.focus();
            var range = document.selection.createRange();
            range.moveStart('character', -txtarea.value.length);
            strPos = range.text.length;
        } else if (br == "ff") {
            strPos = txtarea.selectionStart;
        }

        var front = (txtarea.value).substring(0, strPos);
        var back = (txtarea.value).substring(strPos, txtarea.value.length);
        txtarea.value = front + text + back;
        strPos = strPos + text.length;
        if (br == "ie") {
            txtarea.focus();
            var ieRange = document.selection.createRange();
            ieRange.moveStart('character', -txtarea.value.length);
            ieRange.moveStart('character', strPos);
            ieRange.moveEnd('character', 0);
            ieRange.select();
        } else if (br == "ff") {
            txtarea.selectionStart = strPos;
            txtarea.selectionEnd = strPos;
            txtarea.focus();
        }

        txtarea.scrollTop = scrollPos;
    }


    $('#view-template').on('click', function () {
        console.log('view template clicked');

        $('#modal-preview').modal('show');

        var id = $(this).attr('data-id');

        $.ajax({
            url: '/backend/emails/' + id + '/htmlPreview',
            method: 'GET',
            success: function (data) {
                $('#preview-container').append(data);
            },
            error: function (error) {
                console.error(error);
            }
        });

    });

    $('#save-and-view-template').on('click', function () {
        console.log('save-and-view-template');

        //need another way to save, because submit() redirects to previous page after you save. Maybe remove redirecting?
        // $('#edit-form').submit();

        $('#modal-preview').modal('show');

        var id = $(this).attr('data-id');

        $.ajax({
            url: '/backend/emails/' + id + '/htmlPreview',
            method: 'GET',
            success: function (data) {
                $('#preview-container').append(data);
            },
            error: function (error) {
                console.error(error);
            }
        });
    });

    $('#modal-preview').on('hide.bs.modal', function () {
        console.log('Fired at start of hide event!');

        $('#preview-container').empty();
    });

    $('#send-test-btn').on('click', function () {
        var pastedListOfUsers = $('.test-textarea').val().split('\n');

        $.ajax({
            url: $(this).data('url'),
            method: 'POST',
            data: {data: pastedListOfUsers},
            success: function (data) {
                console.log('data: ', data);
            }
        });
    });
});