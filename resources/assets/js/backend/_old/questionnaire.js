/* global console, confirm */

console.log( 'Init questionnaire.js' );

$( function () {

    $( '.toggle-questionnaire' ).on( 'click', function ( e ) {
        e.preventDefault();

        var el       = $( this ),
            startUrl = $( this ).data( 'start' ),
            stopUrl  = $( this ).data( 'end' ),
            curve = $( this ).data( 'curve'),
            starting = el.hasClass( 'btn-primary' ),
            url      = starting ? startUrl : stopUrl;

        if ( !confirm( 'Are you sure you wish to ' + (starting ? 'start' : 'stop') + ' this questionnaire ?' ) ) {
            return false;
        }

        $.ajax( {
            url:     url,
            method:  'POST',
            data: {
                curve: curve
            },
            success: function ( response ) {
                console.log(curve);
                if ( starting ) {
                    el.removeClass( 'btn-primary' ).addClass( 'btn-danger' );
                    el.find( '.fa-play' ).removeClass( 'fa-play' ).addClass( 'fa-stop' );
                    el.find( 'span' ).html( 'Stop questionnaire' );
                    if(curve == true) {
                        el.find( 'span' ).html( 'Stop questionnaire with learning curve' );
                    } else {
                        el.find( 'span' ).html( 'Stop questionnaire' );
                    }
                } else {
                    el.removeClass( 'btn-danger' ).addClass( 'btn-primary' );
                    el.find( '.fa-stop' ).removeClass( 'fa-stop' ).addClass( 'fa-play' );
                    if(curve == true) {
                        el.find( 'span' ).html( 'Start questionnaire with learning curve' );
                    } else {
                        el.find( 'span' ).html( 'Start questionnaire' );
                    }
                }
            }
        } );

    } );

} );
