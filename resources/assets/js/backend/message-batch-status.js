(function(scope, $){
    console.log('MessageBatchStatus.js');

    class _MessageBatchStatus{
        constructor(){
            this.$modal = $('#statusModal');

            this.$resultDone = $('#result-done');
            this.$resultTotal = $('#result-total');

            this.$table = $('#batchTableBody');

            this.$btnCancel = $('#batchStatusCancel');
            this.$btnCancel.on('click', this.closeModal.bind(this));

            this.$waitingForResponse = $('#waiting-for-response');

            this.batch = 0;
            this.email_id = 0;
            this.sms_id = 0;
            this.statusUrl = '';
            this.logUrl = '';
            this.totalUsers = 0;

            this.updateInterval = null;
            this.doingAjax = false;
        }
        stopCurrentPolling(){
            this.stopUpdate()
        }

        startUpdate(timeout = 1000) {
            if(!this.updateInterval) {
                this.updateInterval = setInterval(this.checkStatus.bind(this), timeout);
            }
        }

        stopUpdate() {
            if(this.updateInterval) {
                this.updateInterval = clearInterval(this.updateInterval);
            }
        }

        startPolling(data){
            this.stopCurrentPolling();

            this.batch = data.batch;
            if(data.email_id){
                this.email_id = data.email_id;
                this.sms_id = 0;
            } else {
                this.email_id = 0;
                this.sms_id = data.sms_id;
            }
            this.statusUrl = data.statusUrl;
            this.logUrl = data.logUrl;
            this.totalUsers = data.totalUsers;

            this.$resultDone.html('0');
            this.$resultTotal.html(this.totalUsers);

            this.$table.html('');
            this.$waitingForResponse.removeClass('hidden');

            this.$modal.modal('show');

            this.startUpdate();
        }

        closeModal(){
            this.stopUpdate();
            this.$modal.modal('hide');
        }

        checkStatus(){
            if(!this.doingAjax){
                if(this.statusUrl){
                    this.doingAjax = true;
                    $.ajax(this.statusUrl,{
                        method: 'POST',
                        data:{
                            email_id: this.email_id,
                            batch: this.batch,
                            sms_id: this.sms_id
                        }
                    }).success(data => {
                        if(!data || !data['success']) return this.error(data);
                        this.doingAjax = false;

                        this.updateTable(data.logs);


                    }).error(this.error)

                } else {
                    this.stopUpdate();
                    Notifications.error('Unable to check batch status without status url');
                }
            }
        }

        updateTable(logs){

            this.$resultTotal.text(logs.length);
            var done = 0;

            $.each(logs, (k, row) => {
                if(row.status === 'done'){
                    // set/update in dom
                    if(this.$table.find('#batch_log_'+row.id).length === 0){
                        this.$table.append(
                            $('<tr>').attr('id', 'batch_log_'+row.id)
                                .append($('<td>').html(row.user.name))
                                .append($('<td>').html(this.email_id ? row.user.email : row.user.phonenumber))
                                .append($('<td>').html(row.status))
                                .append($('<td>').html(row.error))
                        );
                    }

                    done++;
                }
            });

            this.$resultDone.text(done);

            if(done === logs.length){
                console.log('done!');
                this.$waitingForResponse.addClass('hidden');
                this.stopUpdate();
            }
        }


        error(xhr, status, errorThrown) {
            this.doingAjax = false;
            this.stopUpdate();
            console.log(xhr, status, errorThrown);

            if(xhr && xhr.hasOwnProperty('success') && typeof xhr.success !== 'function') {
                if(xhr.hasOwnProperty('error')) {
                    xhr.error = (xhr.error && xhr.error.message)? xhr.error.message : xhr.error || 'Whoops, something went wrong';
                    return Notifications.error(xhr.error);
                }
            }

            if(xhr && xhr.readyState === 0) {
                return Notifications.error('Internet error, make sure you are connected to the internet');
            }

            if(xhr && xhr.readyState === 4) {
                return Notifications.error(errorThrown);
            }

            return Notifications.error('Whoops, something went wrong.');
        }
    }

    scope.messageBatchStatus = new _MessageBatchStatus();
})(this, $);