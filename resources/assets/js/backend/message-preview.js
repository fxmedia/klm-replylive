(function(scope, $){
    console.log('MessagePreview.js');

    class _MessagePreview {
        constructor(){
            this.clickedSend = false;
            $('#previewSend').on('click',this.clickSend.bind(this))
        }

        clickSend(e){
            e.preventDefault();
            var el = $(e.currentTarget);
            var url = el.data('url');

            if(!this.clickedSend){
                this.clickedSend = true;

                $.ajax(url,{
                    method: 'POST'
                }).success(data => {
                    if(!data || !data['success']) return this.error(data);

                    if(scope.messageBatchStatus){
                        console.log('start polling with data', data);
                        scope.messageBatchStatus.startPolling(data);

                    } else {
                        alert('Email is being send, but there is an error in polling the progress. Please check the log page for the current progress');
                    }

                }).error(this.error);

            } else {
                console.log('you already clicked send!');
            }
        }

        error(xhr, status, errorThrown) {
            this.clickedSend = false;
            console.log(xhr, status, errorThrown);

            if(xhr && xhr.hasOwnProperty('success') && typeof xhr.success !== 'function') {
                if(xhr.hasOwnProperty('error')) {
                    xhr.error = (xhr.error && xhr.error.message)? xhr.error.message : xhr.error || 'Whoops, something went wrong';
                    return Notifications.error(xhr.error);
                }
            }

            if(xhr && xhr.readyState === 0) {
                return Notifications.error('Internet error, make sure you are connected to the internet');
            }

            if(xhr && xhr.readyState === 4) {
                return Notifications.error(errorThrown);
            }

            return Notifications.error('Whoops, something went wrong.');
        }
    }

    scope.messagePreview = new _MessagePreview();
})(this, $);