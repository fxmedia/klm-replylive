<?php

use App\Rules\stringOrArray;

return [
    'admins' => [
        'store' => [
            'name' => 'required|string|max:255',
            'email' => 'required|string|email|max:255|unique:admins',
            'notification_subscription' => 'boolean',
            'password' => 'required|string|min:6|confirmed'
        ],
        'update' => [
            'name' => 'required|string|max:255',
            'email' => 'required|email|max:255',
            'notification_subscription' => 'boolean'
        ],
        'updateSelf' => [
            'name' => 'required|string|max:255',
            'email' => 'required|email|max:255',
            'notification_subscription' => 'boolean',
            'password' => 'required|confirmed|min:6|string',
            'old_password' => 'required|min:6|string'
        ],
        'import' => [
            'name' => 'required|string|max:255',
            'email' => 'required|email|max:255', // no need for unique, will update? / nothing? if exist
            'notification_subscription' => 'boolean'
        ]
    ],
    'users' => [
        'store' => [
            'firstname' => 'string|max:35|required',
            'lastname' => 'nullable|string|max:37',
            'phonenumber' => 'max:50',
            'email' => 'nullable|email|max:254',
            'image' => 'mimes:jpeg,bmp,png,jpg',
            'test_user' => 'boolean',
            'extra.*.key' => 'nullable|max:255',
            'extra.*.value' => ['nullable', new stringOrArray]
        ],
        'import' => [
            'firstname' => 'required|string|max:35',
            'lastname' => 'nullable|string|max:37',
            'phonenumber' => 'max:50',
            'email' => 'nullable|email|max:254',
            'test_user' => 'boolean|nullable',
            'token' => 'nullable|string|max:12|min:12'
        ],
        'update' => [
            'firstname' => 'nullable|string|max:35',
            'lastname' => 'nullable|string|max:37',
            'phonenumber' => 'max:50',
            'email' => 'required|email|max:254',
            'test_user' => 'boolean',
            'token' => 'nullable|string|max:12|min:12'
        ],
        'extra_field' => 'max:255',
        'register' => [
            'firstname' => 'required|string|max:35',
            'lastname' => 'required|string|max:70',
            'email' => 'required|string|max:70',
            'company' => 'string|nullable|max:70'
        ]
    ],
    'groups' => [
        'name' => 'max:255|required|string',
        'limit' => 'numeric'
    ],
    'settings' => [
        'name' => 'max:255|required|string',
        'key' => 'max:255|required|string',
        'value' => 'required',
        'type' => 'max:255|required|string',
        'settingGroup' => 'string|max:255', // if not given, setting will go advanced
        'placeholder' => 'max:255|string',
        'validation' => 'max:255|string', // if not given, will be automatic same as this thing
        'description' => 'max:255|string'
    ],
    'settingDefaultValidation' => 'max:255|string',

    'posts' => [
        'frontend' => [
            'user_id' => 'numeric|required',
            'data.message' => 'string|nullable',
            'data.image' => 'string|nullable'
        ]
    ],

    'polls' => [
        'store' => [
            'question' => 'string|max:255|required',
            'tags' => 'string|max:255|nullable',
            'choices.*.choice' => 'nullable|max:4',
            'choices.*.value' => 'string|nullable'
        ],
        'import' => [
            'poll' => [
                'question' => 'string|max:255|required',
                'tags' => 'string|max:255|nullable',
            ],
            'choice' => [
                'choice' => 'required|max:4',
                'value' => 'string|required'
            ]
        ]
    ],
    'quizzes' => [
        'store' => [
            'name' => 'string|max:255|required',
            'tags' => 'string|max:255|nullable'
        ],
        'import' => [
            'quiz' => [
                'name' => 'string|max:255|required',
                'tags' => 'string|max:255|nullable',
            ],
            'question' => [
                'title' => 'string|max:255|required',
                'points' => 'integer|nullable',
                'order' => 'integer|nullable',
                'correct_answer' => 'required|max:4'
            ],
            'choices' => [
                'choice' => 'required|max:4',
                'value' => 'string|required'
            ]
        ],
        'questions' => [
            'store' => [
                'title' => 'string|max:255|required',
                'order' => 'integer',
                'points' => 'integer|required',
                'image' => 'string|max:255|nullable',
                'image_upload' => 'nullable|mimes:jpeg,bmp,png,jpg',
                'correct_answer' => 'string|max:255|required',
                'choices.*.choice' => 'nullable|max:4',
                'choices.*.value' => 'string|nullable'
            ]
        ]
    ],
    'learning_curves' => [
        'store' => [
            'name' => 'string|max:255|required',
            'tags' => 'string|max:255|nullable'
        ],
        'import' => [
            'learning_curve' => [
                'name' => 'string|max:255|required',
                'tags' => 'string|max:255|nullable',
            ],
            'question' => [
                'title' => 'string|max:255|required',
                'order' => 'integer|nullable',
                'correct_answer' => 'required|max:4'
            ],
            'choices' => [
                'choice' => 'required|max:4',
                'value' => 'string|required'
            ]
        ],
        'questions' => [
            'store' => [
                'title' => 'string|max:255|required',
                'order' => 'integer',
                'correct_answer' => 'string|max:255|required',
                'choices.*.choice' => 'nullable|max:4',
                'choices.*.value' => 'string|nullable'
            ]
        ]
    ],
    'knockouts' => [
        'store' => [
            'name' => 'string|max:255|required',
            'tags' => 'string|max:255|nullable',
            'deciding_question' => 'string|max:255|required',
            'deciding_answer' => 'integer|required'
        ],
        'import' => [
            'knockout' => [
                'name' => 'string|max:255|required',
                'tags' => 'string|max:255|nullable',
                'deciding_question' => 'string|max:255|required',
                'deciding_answer' => 'integer|required'
            ],
            'question' => [
                'title' => 'string|max:255|required',
                'order' => 'integer|nullable',
                'correct_answer' => 'required|max:4'
            ],
            'choices' => [
                'choice' => 'required|max:4',
                'value' => 'string|required'
            ]
        ],
        'questions' => [
            'store' => [
                'title' => 'string|max:255|required',
                'order' => 'integer',
                'correct_answer' => 'string|max:255|required',
                'choices.*.choice' => 'nullable|max:4',
                'choices.*.value' => 'string|nullable'
            ]
        ]
    ],
    'surveys' => [
        'store' => [
            'name' => 'string|max:255|required',
            'tags' => 'string|max:255|nullable',
            'takeover' => 'boolean',
            'intro' => 'string|nullable',
            'outro' => 'string|nullable'
        ],
        'import' => [
            'survey' => [
                'name' => 'string|max:255|required',
                'tags' => 'string|max:255|nullable',
                'takeover' => 'integer|required',
                'intro' => 'string|nullable',
                'outro' => 'string|nullable'
            ],
            'question' => [
                'title' => 'string|max:255|required',
                'type' => 'string|required|in:abcd,abcd-multiple,range,open,in-between-screen',
                'order' => 'integer|nullable',
                'data' => 'json|nullable'
            ]
        ],
        'questions' => [
            'abcd' => [
                'title' => 'string|max:255|required',
                'type' => 'string|required|in:abcd',
                'order' => 'integer|nullable',
                'data.*.choice' => 'nullable|max:4',
                'data.*.value' => 'string|nullable'
            ],
            'abcd-multiple' => [
                'title' => 'string|max:255|required',
                'type' => 'string|required|in:abcd-multiple,',
                'order' => 'integer|nullable',
                'data.*.choice' => 'nullable|max:4',
                'data.*.value' => 'string|nullable'
            ],
            'range' => [
                'title' => 'string|max:255|required',
                'type' => 'string|required|in:range',
                'order' => 'integer|nullable',
                'data.min_range' => 'integer|required',
                'data.max_range' => 'integer|required',
                'data.min_label' => 'string|nullable',
                'data.max_label' => 'string|nullable'

            ],
            'open' => [
                'title' => 'string|max:255|required',
                'type' => 'string|required|in:open',
                'order' => 'integer|nullable'
                // something with data here
            ],
            'in-between-screen' => [
                'title' => 'string|max:255|required',
                'type' => 'string|required|in:in-between-screen',
                'data.content' => 'string|nullable',
                'order' => 'integer|nullable'
            ]
        ]
    ],
    'open_questions' => [
        'store' => [
            'question' => 'string|max:255|required',
            'tags' => 'string|max:255|nullable',
            'multiple_answers' => 'boolean'
        ],
        'import' => [
            'question' => 'string|max:255|required',
            'tags' => 'string|max:255|nullable',
            'multiple_answers' => 'integer|required'
        ]
    ],

    'pushes' => [
        'store' => [
            'name' => 'required|string|max:255',
            'title' => 'string|max:255|nullable',
            'copy' => 'string|nullable',
            'module' => 'string|max:255|nullable',
            'image' => 'string|max:255|nullable',
            'image_upload' => 'nullable|mimes:jpeg,bmp,png,jpg'
        ],
    ],

    'timer' => [
        'total_seconds' => 'numeric|required',
        'audio_url' => 'string|nullable',
    ],
    'emails' => [
        'store' => [
            'name' => 'string|max:255|required',
            'subject' => 'string|max:200|required',
            'reply_to' => 'string|max:255|nullable',
            'from_name' => 'string|max:255|required',
            'from_email' => 'string|max:255|required',
            'action' => 'string|max:255|nullable',
            'view' => 'string|max:255|required',
            'attachment' => 'string|max:255|nullable',
            'body_text' => 'string|nullable',
            'disclaimer_text' => 'string|nullable',
            'header_image' => 'string|max:255|nullable',
            'footer_image' => 'string|max:255|nullable',
            'header_image_upload' => 'nullable|mimes:jpeg,bmp,png,jpg',
            'footer_image_upload' => 'nullable|mimes:jpeg,bmp,png,jpg',
            'attachment_upload' => 'nullable|file|max:10000' // 10mb
        ]
    ],
    'sms' => [
        'store' => [
            'name' => 'string|max:255|required',
            'subject' => 'string|max:11|required',
            'body_text' => 'string|nullable',
            'action' => 'string|max:255|nullable'
        ]
    ]
];
