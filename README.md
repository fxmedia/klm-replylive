[TOC]

# The New New Reply.live #

This Reply.live project is the latest version of which all interactive elements are seperated from the Event Website. The event website only has to include one small piece of JavaScript, and it will load all elements of Reply.Live on it (or at least that is the plan...)


## Setup as Developer ##

- Clone Repository to your local environment
- change to the root-directory of your local environment and run ....

```
npm install
composer install
gulp dev
nodemon server/server.js
```
- `cp .env.example .env` and fill in your own local url & database connection data
- `php artisan key:generate`
- `php artisan migrate`
- for now, create a public/uploads/encoded directory (for uploaded gids) @todo: catch error and create directory and move to storage path
- server dependencies (mac) -> windows users: https://www.ffmpeg.org/download.html
```
brew install ffmpeg
```

### Folder structure modules ###
The folder structure can be a bit overwhelming, but all modules and components follow the same structure. So if you know the structure of one module, you know all of them. Below is an example of the tipical structure of a module in reply.live

```
replylive3.0
.
+- app
|	+- Export
|	|	+-- [module]Export.php
|	|
|	+- Http
|	|	+- Controllers
|	|		+- Api
|	|		|	+-- [module]Controller.php
|	|		|
|	|		+- Backend
|	|			+-- [module]Controller.php
|	|
|	+- Import
|	|	+-- [module]Import.php
|	|
|	+-	Report
|	|	+-- [module]Report.php
|	|
|	+-- [module].php // the Model
|
+- config
|	+-- [module].php // not for every module neccasary
|
+- database
|	+- factories
|	|	+-- [module]Factory.php // for testing to build random db-fields
|	|
|	+- migrations
|	|	+-- xxxx_xx_xx_xxxxx_create_[module]_table.php // and/or other migrations
|	|
|	+- seeds
|		+-- [module]Seeder.php // to seed db with random or default fields
|
+- resources
|	+- js
|	|	// stuff to fill here
|	|
|	+- sass
|	|	// stuff to fill here
|	|
|	+- views
|		+- backend
|			+- [module]
|				+-- // here you find all blade templates for backend
|
+- routes
|	+-- api.php // write here all api call routes
|	+-- web.php // write here all your backend routes
|
+- server
	+ [module].js
```


# Changelog

## v.1.0.1
- Added custom stringOrArray validation rule
- Fixed a bug where, if a user was saved in the backend, exta fields with multiple values would only store the last value.

## v1.0.0
- Release