const config = require('./config');
const app = require('express')();
const bodyParser = require('body-parser');
const server = require('http').createServer(app);
const io = require('socket.io')(server);


const Administrator = require('./Admin');
const admin = new Administrator(io);

const jsonParser = bodyParser.json();

// main connection with backend
app.post('/api/action', jsonParser, (req, res) => {
    console.log(req.body);
    const manager = admin.getManager(req.body.action);
    if(admin.managers[manager]) {
        admin.managers[manager].handle(req.body.action, req.body.data);
    } else {
        admin.broadcast(req.body.action, req.body.data);
    }
    res.send({success: true});
});

app.post('/api/status', jsonParser, (req, res) => {
    let response = {};
    switch(req.body.action) {
        case 'app_status':
            response = admin.managers.app.status();
            break;
        case 'poll_status':
            response = admin.managers.poll.status();
            break;
        case 'post_highlight_status':
            // get post id of the highlighted post
            response = admin.managers.post.getHighlightStatus();
            break;
        case 'quiz_status':
            // get post id of the highlighted post
            response = admin.managers.quiz.status();
            break;
        case 'knockout_status':
            // get post id of the highlighted post
            response = admin.managers.knockout.status();
            break;
        case 'survey_status':
            // get array of survey ids of the active surveys
            response = admin.managers.survey.status();
            break;
        case 'curve_status':
            // get array of survey ids of the active surveys
            response = admin.managers.curve.status();
            break;
        case 'oq_status':
            // get post id of the highlighted post
            response = admin.managers.oq.status();
            break;
        case 'oq_highlight_status':
            // get post id of the highlighted post
            response = admin.managers.oq.getHighlightStatus();
            break;
        case 'push_status':
            // get array of pushed ids of the active pusheds
            response = admin.managers.push.status();
            break;
        case 'display_page_status':
            // get post id of the highlighted post
            response = admin.managers.display.status();
            break;
    }

    res.send(response);
});


server.listen(config.options.port);
console.log("Server listening on port "+ config.options.port);
