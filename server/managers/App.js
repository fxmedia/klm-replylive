const GameManager = require('./Game');

class AppManager extends GameManager {
    constructor(Admin) {
        super(Admin, "app");
    }

    handle(action, data) {
        switch(action) {
            case 'app_offline':
                this.setOffline(action, data);
                break;
        }
    }

    setOffline(action, data) {
        this.Admin.broadcastToChannel('user', 'user_offline');
    }

    status() {
        const connected_users = Object.keys(this.Admin.connections.user).length;
        const active_module = (this.Admin.activeAction) ? this.Admin.getManager(this.Admin.activeAction.action) : "none";

        return { online: true, connected_users, active_module }
    }
}

module.exports = AppManager;
