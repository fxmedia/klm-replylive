const GameManager = require('./Game');

class PledgeManager extends GameManager {
    constructor(Admin) {
        super(Admin, "pledge");
    }

    handle(action, data) {
        switch(action) {
            case 'pledge_remove':
                this.removePledges(action, data);
                break;
            default:
                this.Admin.broadcast(action, data);
        }
    }

    removePledges(action, data) {
        this.Admin.broadcastToChannel("display", action, data);
    }

    fetchAll(channel, client) {
        this.Admin.fetch('/pledge/all')
        .then(result => this.Admin.channels[channel].to(client.id).emit('pledge_set_all', result.data.pledges))
        .catch(err => console.log("Pledges could not be fetched..", err))
    }

    submit(pledge, user) {
        this.Admin.save('/pledge/submit', pledge, user.id)
        .then(result => {
            console.log("data; ", result.data);
            this.Admin.channels.user.to(user.id).emit('pledge_add', result.data.pledge);
            this.Admin.broadcastToChannel("display", "pledge_add", result.data.pledge);
        })
        .catch(err => console.log(err))
    }
}

module.exports = PledgeManager;
