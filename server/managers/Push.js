const GameManager = require('./Game');

class PushManager extends GameManager {
    constructor(Admin) {
        super(Admin, "push");
    }

    handle(action, data) {
        switch(action) {
            case 'push_start':
                this.start(action, data);
                break;
            case 'push_stop':
                this.stop(action, data);
                break;
        }
    }

    start(action, data) {
        console.log("start push: ", data);
        this.setBroadcastLevel(data);
        const extraction = {
            user: this.extract(data, ["name", "title", "copy", "module", "image_path", "id"]),
            display: {}
        }
        this.broadcastToUsers(action, extraction.user);
        this.Admin.setActiveAction({ action, data: extraction });
    }

    stop(action, data) {
        if(this.isActive()) {
            this.broadcastToUsers(action);
            this.Admin.setActiveAction(null);
        }
    }

    status() {
         if(!this.Admin.activeAction || this.Admin.getManager(this.Admin.activeAction.action) !== this.namespace) {
            return { push_id: false };
        }
        return {
            push_id: this.Admin.activeAction.data.user.id
        };
    }
}

module.exports = PushManager;
