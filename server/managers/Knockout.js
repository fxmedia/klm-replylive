const GameManager = require('./Game');

class KnockoutManager extends GameManager {
    constructor(Admin) {
        super(Admin, "knockout");
        this.users = [];
        this.participants = [];
        this.currentState = {
            questionIdx: 0,
            locked: false,
            phase: "multiple_choice",
            players_left: 999
        };
        // create private state so it will not be sent to users
        this.privateState = {
            answers: [],
            knockouts: [],
            deciding_answers: []
        };
    }

    addAnswer(answer) {
        const existingAnswerIdx = this.privateState.answers.findIndex(_answer => answer.user_id == _answer.user_id);
        if(existingAnswerIdx === -1) {
            this.privateState.answers.push(answer);
        } else {
            this.privateState.answers[existingAnswerIdx] = answer;
        }
    }

    addDecidingAnswer(answer) {
        this.privateState.deciding_answers.push(answer);
    }

    checkAnswers(action, data) {
        if(this.currentState.phase == "deciding_question") {
            this.checkDecidingAnswers(action, data);
        } else {
            this.currentState.locked = true;
            const currentQuestion = this.Admin.activeAction.data.user.knockout.questions[this.currentState.questionIdx];
            this.privateState.answers.forEach(answer => {
                if(answer.choice != currentQuestion.correct_answer) {
                    this.privateState.knockouts.push(answer.user_id);
                }
            });

            // knockout inactive users
            this.users.forEach(user => {
                console.log("answers: ", this.privateState.answers);
                if(!this.privateState.answers.find(answer => answer.user_id == user.id)) {
                    this.privateState.knockouts.push(user.id);
                }
            });

            const players_left = this.participants.length - this.privateState.knockouts.length;

            console.log("participants: ", this.participants.length)
            console.log("knockouts: ", this.privateState.knockouts.length)

            if(players_left === 1) {
                // show winner
                const winner = this.participants.find(p => this.privateState.knockouts.indexOf(p.id) === -1);
                this.broadcastToUsers("knockout_set_winner", { winner: [winner] });
                this.Admin.broadcastToChannel("display", "knockout_set_winner", { winner: [winner] });
                this.Admin.setActiveAction({
                    action: "knockout_set_winner",
                    data: {
                        user: { winner: [winner], knockout: {id: this.Admin.activeAction.data.user.knockout.id }},
                        display: { winner: [winner], knockout: {id: this.Admin.activeAction.data.user.knockout.id }}
                    }
                });
                console.log(this.Admin.activeAction)
            } else {
                // show that nobody won (yet)
                this.currentState.players_left = players_left;
                this.broadcastToUsers(action);
                this.Admin.broadcastToChannel(
                    "display",
                    "knockout_set_score",
                    { score: this.calcFinal(), players_left }
                );
            }
        }
    }

    checkDecidingAnswers(action, data) {
        console.log("check deciding: ", this.Admin.activeAction.data.display.knockout);
        console.log("racers: ", this.privateState.deciding_answers);
        const correct_answer = this.Admin.activeAction.data.display.knockout.deciding_answer;
        let candidates = [];
        let winningOffset = 99999999;
        this.privateState.deciding_answers.forEach(a => {
            const offset = Math.abs(parseInt(a.answer) - parseInt(correct_answer));
            if(offset <= winningOffset) {
                candidates.push({ user_id: a.user_id, answer: a.answer, offset });
                winningOffset = offset;
            }
        });
        const winners = candidates.filter(candidate => candidate.offset <= winningOffset);
        console.log(winners);
        const winnerData = this.Admin.managers.user.getUsers()
            .filter(user => winners.find(_user => user.id == _user.user_id));
        console.log("and the winner is: ", winnerData);

        this.broadcastToUsers("knockout_set_winner", { winner: winnerData })
        this.Admin.broadcastToChannel("display", "knockout_set_winner", { winner: winnerData });
        this.Admin.setActiveAction({
            action: "knockout_set_winner",
            data: {
                user: { winner: winnerData, knockout: {id: this.Admin.activeAction.data.user.knockout.id }},
                display: { winner: winnerData, knockout: {id: this.Admin.activeAction.data.user.knockout.id }}
            }
        });
    }

    calcFinal() {
        let final = { winners: [], losers: [] };
        const users = this.calcParticipatingUsers();
        users.forEach(user => {
            if(this.privateState.knockouts.indexOf(user.id) === -1) {
                final.winners.push(user);
            } else {
                final.losers.push(user);
            }
        });
        return final;
    }

    calcParticipatingUsers() {
        let users = this.Admin.managers.user.getUsers();
        return users.filter(user =>
            this.Admin.managers.user.hasMatchingGroup(this.group_ids, user.groups)
            && this.Admin.managers.user.isAttending(user)
        );
    }

    cleanUpRound() {
        // set remaining users so next calculation is less heavy
        this.users = this.users.filter(user => this.privateState.knockouts.indexOf(user.id) === -1);
        this.privateState.answers = [];
    }

    getKnockOutMap() {
        return this.privateState.knockouts;
    }

    getTotalKnockouts() {
        return this.privateState.knockouts.length;
    }

    handle(action, data) {
        switch(action) {
            case 'knockout_start':
                this.start(action, data);
                break;
            case 'knockout_stop':
                this.stop(action, data);
                break;
            case 'knockout_check_answer':
                this.checkAnswers(action, data);
                break;
            case 'knockout_show_question':
                this.showQuestion(action, data);
                break;
            case 'knockout_show_final':
                this.showFinal(action, data);
                break;
            case 'knockout_deciding_question':
                this.setDecidingQuestion(action, data);
                break;
        }
    }

    setDecidingQuestion(action, data) {
        this.currentState.phase = "deciding_question";
        this.broadcastToUsers(action, data, this.privateState.knockouts);
        this.Admin.broadcastToChannel("display", action, data);
        this.Admin.setActiveAction({
            action: "knockout_deciding_question",
            data: {
                user: { phase: this.currentState.phase, knockout: this.Admin.activeAction.data.user.knockout},
                display: { phase: this.currentState.phase, knockout: this.Admin.activeAction.data.display.knockout}
            }
        });
    }

    status() {
        if(!this.Admin.activeAction || this.Admin.getManager(this.Admin.activeAction.action) !== this.namespace) {
            return { knockout_id: false };
        }
        return {
            knockout_id: this.Admin.activeAction.data.user.knockout.id,
            question_index: this.currentState.questionIdx,
            locked: this.currentState.locked,
            players_left: this.participants.length - this.privateState.knockouts.length
        };
    }

    reset() {
        this.currentState = {
            questionIdx: 0,
            locked: false,
            phase: "multiple_choice",
            players_left: 999
        };
        this.privateState = {
            answers: [],
            knockouts: [],
            deciding_answers: []
        }
    }

    showFinal(action, data) {
        const final = this.calcFinal();
        this.Admin.broadcastToChannel("display", action, final);
    }

    showQuestion(action, data) {
        this.cleanUpRound();
        this.currentState.questionIdx = data.question_index;
        this.currentState.locked = false;
        this.broadcastToUsers(action, data, this.privateState.knockouts);
        this.Admin.broadcastToChannel("display", action, data);
    }

    start(action, data) {
        this.reset();
        this.setBroadcastLevel(data.knockout);
        this.participants = this.calcParticipatingUsers();
        this.users = this.calcParticipatingUsers();

        let extraction = {
            user: this.extract(data, [
                {key: 'knockout', keys: ["questions", "deciding_question", "id"]},
                {key: 'question_index', keys: null}
            ]),
            display: this.extract(data, [
                {key: 'knockout', keys: ["questions", "id", "deciding_question", "deciding_answer"]},
                {key: 'question_index', keys: null}
            ])
        };
        extraction.display.players_left = this.participants.length;
        console.log("extraction: ", extraction.display);
        this.broadcastToUsers(action, extraction.user);
        this.Admin.broadcastToChannel("display", action, extraction.display);
        this.Admin.setActiveAction({ action, data: extraction });
    }

    stop(action, data) {
        if(this.isActive()) {
            this.broadcastToUsers(action);
            this.Admin.broadcastToChannel("display", action);
            this.Admin.setActiveAction(null);
        }
    }

    submitAnswer(answer, userSocket) {
        this.Admin.save('/knockout/answer', answer, userSocket.id)
        .catch(err => {
            this.Admin.sendError(userSocket, "The backend has some problems, contact someone at FX..");
            const userId = this.Admin.getConnection("user", userSocket.id).id;
            this.Admin.broadcastToUsers([userId], 'knockout_reset_answer');
        })
        .then(result => {
            if(result.data.success) {
                this.addAnswer({choice: result.data.answer.choice, user_id: result.data.answer.user_id});

                // update all of the user connections
                this.Admin.broadcastToUsers([result.data.answer.user_id], 'knockout_set_answer', answer.choice)
            } else {
                this.Admin.sendError(userSocket, "The Answer could not be submitted, please try again..");
                const userId = this.Admin.getConnection("user", userSocket.id).id;
                this.Admin.broadcastToUsers([userId], 'knockout_reset_answer');
            }
        })
    }

    submitDecidingAnswer(data, user) {
        const userData = this.Admin.getConnection("user", user.id);
        this.addDecidingAnswer({answer: parseInt(data.answer), user_id: userData.id});
    }
}

module.exports = KnockoutManager;
