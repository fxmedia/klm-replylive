const GameManager = require('./Game');

class OpenQuestionManager extends GameManager {
    constructor(Admin) {
        super(Admin, "oq");
        this.result_users = [];
        this.result = [];
        this.currentState = { highlight: null };
    }

    updateRebootSettings(userId, multiple_answers) {
        // if multiple answers are not allowed, add them to the result_users array to shut them out
        if(!multiple_answers)  {
            this.result_users.push(userId);
        }
    }

    deleteAnswers(action, data) {
        this.result = this.result.filter(answer => data.open_question_answer_ids.indexOf(answer.id.toString()) === -1);
        this.Admin.broadcastToChannel("display", action, data);
    }

    getHighlightStatus() {
        return { open_question_answer_id: this.currentState.highlight };
    }

    handle(action, data) {
        switch(action) {
            case 'oq_start':
                this.start(action, data);
                break;
            case 'oq_stop':
                this.stop(action);
                break;
            case 'oq_highlight_answer':
                this.highlightAnswer(action, data);
                break;
            case 'oq_delete_answer':
                this.deleteAnswers(action, data);
                break;
        }
    }

    highlightAnswer(action, data) {
        this.currentState.highlight = data.open_question_answer_id;
        this.Admin.broadcastToChannel("display", action, data);
    }

    reset() {
        this.result_users = [];
        this.result = [];
        this.currentState.highlight = null;
    }

    start(action, data) {
        this.reset();
        this.setBroadcastLevel(data);
        const extraction = {
            user: this.extract(data, ["question", "multiple_answers", "id"]),
            display: this.extract(data, ["question", "result", "id"])
        }
        if(data.result && data.result.length && data.result.length > 0) {
            this.result = data.result.slice();
        }
        if(data.result_users && data.result_users.length > 0) {
            this.result_users = data.result_users;
        }
        this.broadcastToUsers(action, extraction.user, data.result_users);
        this.Admin.broadcastToChannel("display", action, extraction.display);
        this.Admin.setActiveAction({ action, data: extraction });
    }

    status() {
        if(!this.Admin.activeAction || this.Admin.getManager(this.Admin.activeAction.action) != this.namespace) {
            return { open_question_id: false };
        }
        return { open_question_id: this.Admin.activeAction.data.user.id };
    }

    stop(action) {
        if(this.isActive()) {
            this.broadcastToUsers(action, {}, this.result_users);
            this.Admin.broadcastToChannel("display", action);
            this.Admin.setActiveAction(null);
        }
    }

    submitAnswer(answer, userSocket) {
        this.Admin.save('/open-question/answer', answer, userSocket.id)
        .catch(err => {
            this.Admin.sendError(userSocket, "The backend failed..");
            const userId = this.Admin.getConnection("user", userSocket.id).id;
            this.Admin.broadcastToUsers([userId], 'oq_reset_answer');
        })
        .then(result => {
            if(result.data.success) {
                this.Admin.broadcastToUsers([result.data.answer.user_id], "oq_show_thanks");
                const { multiple_answers } = this.Admin.activeAction.data.user;

                if(!this.result.find(answer => answer.user_id == result.data.answer.user_id) || multiple_answers) {
                    this.Admin.channels.display.emit('oq_add_answer', result.data.answer);
                    this.result.push(result.data.answer);
                }
                this.updateRebootSettings(result.data.answer.user_id, multiple_answers);
            } else {
                this.Admin.sendError(userSocket, "The answer could not be saved correctly..");
                const userId = this.Admin.getConnection("user", userSocket.id).id;
                this.Admin.broadcastToUsers([userId], 'oq_reset_answer');
            }
        })
    }
}

module.exports = OpenQuestionManager
