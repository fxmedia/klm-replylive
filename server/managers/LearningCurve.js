const GameManager = require('./Game');

class LearningCurveManager extends GameManager {
    constructor(Admin) {
        super(Admin, "curve");
        this.currentState = { filter: ['users'], users: {} };
    }

    handle(action, data) {
        switch(action) {
            case 'curve_start':
                this.start(action, data);
                break;
            case 'curve_stop':
                this.stop(action, data);
                break;
        }
    }

    start(action, data) {
        this.setBroadcastLevel(data);
        const extraction = {
            user: this.extract(data, [
                {key: 'curve', keys: ["questions", "id"]},
                {key: 'phase', keys: null}
            ]),
            display: {}
        };
        this.broadcastToUsers(action, extraction.user);
        this.Admin.setActiveAction({ action, data: extraction });
    }

    status() {
        if(!this.Admin.activeAction || this.Admin.getManager(this.Admin.activeAction.action) != this.namespace) {
            return { curve_id: false };
        }

        return { curve_id: this.Admin.activeAction.data.user.curve.id, phase: this.Admin.activeAction.data.user.phase };
    }

    stop(action, data) {
        if(this.isActive()) {
            this.currentState.users = {};
            this.broadcastToUsers(action, this.Admin.activeAction.data.user);
            this.Admin.setActiveAction(null);
        }
    }

    submitAnswer(answer, userSocket) {
        console.log("sending: ", answer)
        this.Admin.save('/curve/answer', answer, userSocket.id)
        .catch(err => {
            this.Admin.sendError(userSocket, "The backend has some problems, contact someone at FX..");
        })
        .then(result => {
            if(result.data.success) {
                this.Admin.broadcastToUsers([result.data.answer.user_id], 'curve_add_answer', result.data.answer);
                const userId = result.data.answer.user_id;
                const curveId = result.data.curve_id;

                if(!this.currentState.users[userId]) this.currentState.users[userId] = {};
                if(!this.currentState.users[userId][curveId]) this.currentState.users[userId][curveId] = {question_index: 0};
                this.currentState.users[userId][curveId].question_index++;
            } else {
                this.Admin.sendError(userSocket, "The Answer could not be submitted, please try again..");
                const userId = this.Admin.getConnection("user", userSocket.id).id;
                this.Admin.broadcastToUsers([userId], 'curve_reset_answer');
            }
        })
    }
}

module.exports = LearningCurveManager;
