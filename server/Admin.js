const axios = require('axios');
const config = require('./config');

const KnockoutManager = require('./managers/Knockout');
const OpenQuestionManager = require('./managers/OpenQuestion');
const AppManager = require('./managers/App');
const PollManager = require('./managers/Poll');
const PostManager = require('./managers/Post');
const PushManager = require('./managers/Push');
const PledgeManager = require('./managers/Pledge');
const QuizManager = require('./managers/Quiz');
const SurveyManager = require('./managers/Survey');
const LearningCurveManager = require('./managers/LearningCurve');
const UserManager = require('./managers/User');
const DisplayManager = require('./managers/Display');

class Administrator {

    constructor(socket) {
        this.api = axios.create(config.axios);
        this.channels = {user: socket.of('/users'), display: socket.of('/displays')};
        this.connections = {user: {}, display: {}};
        this.managers = null;
        this.users = [];
        this.activeAction = null;
        this.backgroundState = {};
        this.loadManagers();
    }

    addToBackground(key, data) {
        if(!this.backgroundState[key]) this.backgroundState[key] = [];
        this.backgroundState[key].push(data);
    }

    removeFromBackground(key, id) {
        const idx = this.backgroundState[key].findIndex(item => item.id == id);
        this.backgroundState[key].splice(idx, 1);
    }

    broadcast(action, data) {
        Object.keys(this.channels)
        .forEach(channel => {
            if(channel == "user") {
                this.broadcastToFilter(action, data);
            } else {
                this.channels[channel].emit(action, data)
            }
        });
    }

    broadcastToChannel(channel, action, data) {
        if(channel == "user") {
            this.broadcastToFilter(action, data);
        } else {
            this.channels[channel].emit(action, data);
        }
    }

    broadcastToFilter(action, data, filter_ids = [], group_ids = []) {
        let connectionIds = Object.keys(this.connections.user)
        .filter(key => {
            const userId = this.connections.user[key].id;
            const user = this.managers.user.users.find(user => user.id == userId);
            const user_group_ids = user.groups.map(group => group.id);
            return filter_ids.indexOf(this.connections.user[key].id) === -1
            && this.managers.user.hasMatchingGroup(group_ids, user_group_ids)
            && this.managers.user.isAttending(this.connections.user[key])
        });
        connectionIds.forEach(id => this.channels.user.to(id).emit(action, data))
    }

    broadcastToIndividuals(action, data, answers, group_ids) {
        const connectionIds = Object.keys(this.connections.user)
        .filter(key =>
            this.managers.user.hasMatchingGroup(group_ids, this.connections.user[key].group_ids)
            && this.managers.user.isAttending(this.connections.user[key])
        );

        connectionIds.forEach(id => {
            let currentUserId = this.getConnection("user", id).id;
            let currentUserData = answers[currentUserId];
            let myData = Object.assign({}, data);
            myData.mine = currentUserData;
            this.channels.user.to(id).emit(action, myData);
        });
    }

    broadcastToUsers(userIds, action, data) {
        userIds.forEach(userId => {
            const userConnectionIds = this.getUserConnectionIds(userId);
            userConnectionIds.forEach(connId => {
                // update all of the socket connections (multiple tabs / devices / etc..)
                this.channels.user.to(connId).emit(action, data);
            })
        })
    }

    sendError(userSocket, message) {
        console.log(message)
        const userId = this.getConnection("user", userSocket.id).id;
        this.broadcastToUsers([userId], 'user_error', { message });
    }

    fetch(route) {
        return this.api.get(route);
    }

    getActiveModule() {
        if(this.activeAction) {
            return this.activeAction.module;
        }
    }

    getConnection(channel, id) {
        return this.connections[channel][id];
    }

    getManager(action) {
        return action.split('_')[0];
    }

    getUserConnectionIds(userId) {
        return Object.keys(this.connections.user)
        .filter(key => this.connections.user[key].id == userId)
    }

    getUserConnectionId(userId) {
        return Object.keys(this.connections.user)
        .find(key => this.connections.user[key].id == userId)
    }

    loadManagers() {
        // construct managers with Admin context to give them more autonomy
        this.managers = {
            app: new AppManager(this),
            display: new DisplayManager(this.channels.display, this),
            knockout: new KnockoutManager(this),
            oq: new OpenQuestionManager(this),
            poll: new PollManager(this),
            post: new PostManager(this),
            push: new PushManager(this),
            pledge: new PledgeManager(this),
            quiz: new QuizManager(this),
            survey: new SurveyManager(this),
            curve: new LearningCurveManager(this),
            user: new UserManager(this.channels.user, this)
        }
    }

    removeConnection(channel, id) {
        delete this.connections[channel][id];
    }

    save(route, data, userId) {
        return this.api.post(route, {data, user_id: this.getConnection("user", userId).id });
    }

    setActiveAction(data) {
        this.activeAction = data;
        if(data) {
            const activePage = this.getManager(data.action);
            this.managers.display.setPage(activePage);
        } else {
            this.managers.display.setPage("idle");
        }
    }

    setConnection(channel, id, data) {
        this.connections[channel][id] = data;
    }

    setConnectionAttribute(channel, id, data, attribute) {
        this.connections[channel][id][attribute] = data;
    }
}

module.exports = Administrator;
