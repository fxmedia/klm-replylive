<?php

use Illuminate\Database\Seeder;
use Illuminate\Support\Str;
use App\SettingGroup;
use App\Setting;


class fillDefaultSettings extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        //
        foreach(Config::get('default-settings.settingGroups') as $group){
            if(!SettingGroup::where('name', '=', $group['name'])->exists()){
                DB::table('setting_groups')->insert([
                    'name' => $group['name'],
                    'order' => $group['order']
                ]);
            }
        }

        foreach(Config::get('default-settings.settings') as $setting){
            if(!Setting::where('key', '=', $setting['key'])->exists()) {
                if(isset($setting['auto_generate']) && $setting['auto_generate']){
                    $setting['value'] = Str::random(20);
                }

                DB::table('settings')->insert([
                    'setting_group_id' => $setting['group_id'],
                    'name' => $setting['name'],
                    'key' => $setting['key'],
                    'value' => $setting['value'],
                    'type' => $setting['type'],
                    'placeholder' => $setting['placeholder'],
                    'validation' => $setting['validation'],
                    'description' => $setting['description']
                ]);
            }
        }
    }
}
