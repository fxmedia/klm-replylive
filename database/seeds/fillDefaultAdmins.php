<?php

use Illuminate\Database\Seeder;
use App\Admin;

class fillDefaultAdmins extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        //
        foreach(Config::get('admins') as $admin){
            if(!Admin::where('email', '=', $admin['email'])->exists()){
                DB::table('admins')->insert([
                    'name' => $admin['name'],
                    'email' => $admin['email'],
                    'password' => $admin['password'],
                    'created_at' => date("Y-m-d H:i:s"),
                    'updated_at' => date("Y-m-d H:i:s")
                ]);
            }
        }

    }
}
