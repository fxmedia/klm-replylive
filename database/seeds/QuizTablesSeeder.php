<?php

use Illuminate\Database\Seeder;

class QuizTablesSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        //
        $this->command->info('Seeding the database with random quizzes');
        factory('App\Quiz', 3)->create()->each(function($q){
            $q->questions()->save(factory('App\QuizQuestion')->make());
            $q->questions()->save(factory('App\QuizQuestion')->make());
            $q->questions()->save(factory('App\QuizQuestion')->make());
        });
    }
}
