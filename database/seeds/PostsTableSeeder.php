<?php

use Illuminate\Database\Seeder;
use App\User;

class PostsTableSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        //
        $this->command->info('Seeding Database with random Posts');
        if(User::count() == 0){
            // make a user if it does not exist
            $this->command->info('No users found: creating 1 random user first');
            factory('App\User',1)->create();
        }

        factory('App\Post',30)->create();
    }
}
