<?php

use Illuminate\Database\Seeder;

class LearningCurveTablesSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        //
        $this->command->info('Seeding the database with random learning curves');
        factory('App\LearningCurve', 3)->create()->each(function($q){
            $q->questions()->save(factory('App\LearningCurveQuestion')->make());
            $q->questions()->save(factory('App\LearningCurveQuestion')->make());
            $q->questions()->save(factory('App\LearningCurveQuestion')->make());
        });
    }
}
