<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateLearningCurveTables extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('learning_curves', function(Blueprint $table) {
            $table->increments('id');
            $table->string('name');
            $table->string('tags')->nullable();
            $table->timestamps();
        });

        Schema::create('learning_curve_questions', function(Blueprint $table) {
            $table->increments('id');
            $table->integer('learning_curve_id')->unsigned();
            $table->foreign('learning_curve_id')->references('id')->on('learning_curves')->onDelete('cascade');
            $table->string('title');
            $table->string('correct_answer', 1);
            $table->text('choices');
            $table->timestamps();
        });

        Schema::create('learning_curve_answers', function(Blueprint $table) {
            $table->increments('id');
            $table->integer('learning_curve_question_id')->unsigned();
            $table->foreign('learning_curve_question_id')->references('id')->on('learning_curve_questions')->onDelete('cascade');
            $table->integer('user_id')->unsigned();
            $table->foreign('user_id')->references('id')->on('users')->onDelete('cascade');
            $table->string('choice', 1);
            $table->string('phase', 5);
            $table->timestamps();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('learning_curve_answers');
        Schema::dropIfExists('learning_curve_questions');
        Schema::dropIfExists('learning_curves');
    }
}
