<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateKnockoutTables extends Migration
{
    public function up()
    {
        Schema::create('knockouts', function(Blueprint $table) {
            $table->increments('id');
            $table->string('name');
            $table->timestamps();
        });

        Schema::create('knockout_questions', function(Blueprint $table) {
            $table->increments('id');
            $table->integer('knockout_id')->unsigned();
            $table->foreign('knockout_id')->references('id')->on('knockouts')->onDelete('cascade');
            $table->string('title');
            $table->string('correct_answer', 1);
            $table->text('choices');
            $table->timestamps();
        });

        Schema::create('knockout_answers', function(Blueprint $table) {
            $table->increments('id');
//            $table->integer('knockout_id')->unsigned();
//            $table->foreign('knockout_id')->references('id')->on('knockouts')->onDelete('cascade');
            $table->integer('knockout_question_id')->unsigned();
            $table->foreign('knockout_question_id')->references('id')->on('knockout_questions')->onDelete('cascade');
            $table->integer('user_id')->unsigned();
            $table->foreign('user_id')->references('id')->on('users')->onDelete('cascade');
            $table->string('choice', 1);
            $table->timestamps();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('knockout_answers');
        Schema::dropIfExists('knockout_questions');
        Schema::dropIfExists('knockouts');
    }
}
