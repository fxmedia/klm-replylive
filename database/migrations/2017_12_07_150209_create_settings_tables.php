<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateSettingsTables extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        //
        Schema::create('setting_groups', function(Blueprint $table){
            $table->increments('id');
            $table->string('name');
            $table->integer('order')->default(0);
        });


        Schema::create('settings', function(Blueprint $table){
            $table->increments('id');
            $table->integer('setting_group_id')->unsigned();
            $table->foreign('setting_group_id')->references('id')->on('setting_groups')->onDelete('cascade')->onUpdate('cascade');
            $table->string('name');
            $table->string('key', 191)->unique();
            $table->string('value')->nullable();
            $table->string('type')->default('text');
            $table->string('placeholder');
            $table->string('validation');
            $table->string('description');
        });

        Artisan::call('db:seed', [
            '--class' => 'fillDefaultSettings',
            '--force' => true
        ]);
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        //
        Schema::drop('settings');
        Schema::drop('setting_groups');
    }
}
