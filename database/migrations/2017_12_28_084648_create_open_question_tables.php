<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateOpenQuestionTables extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('open_questions', function(Blueprint $table) {
            $table->increments('id');
            $table->string('question', 255);
            $table->text('settings')->nullable();
            $table->timestamps();
        });

        Schema::create('open_question_answers', function(Blueprint $table) {
            $table->increments('id');
            $table->integer('open_question_id')->unsigned();
            $table->foreign('open_question_id')->references('id')->on('open_questions')->onDelete('cascade');
            $table->integer('user_id')->unsigned();
            $table->foreign('user_id')->references('id')->on('users')->onDelete('cascade');
            $table->text('answer');
            $table->timestamps();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::drop('open_questions');
        Schema::drop('open_question_answers');
    }
}
