<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateSurveyTables extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('surveys', function(Blueprint $table) {
            $table->increments('id');
            $table->string('name');
            $table->boolean('takeover');
            $table->text('intro')->nullable();
            $table->text('outro')->nullable();
            $table->timestamps();
        });

        Schema::create('survey_questions', function(Blueprint $table) {
            $table->increments('id');
            $table->integer('survey_id')->unsigned();
            $table->foreign('survey_id')->references('id')->on('surveys')->onDelete('cascade');
            $table->enum('type', config('surveys.question_types'));
            $table->string('title');
            $table->text('data')->nullable();
            $table->timestamps();
        });

        Schema::create('survey_answers', function(Blueprint $table) {
            $table->increments('id');
//            $table->integer('survey_id')->unsigned();
//            $table->foreign('survey_id')->references('id')->on('surveys')->onDelete('cascade');
            $table->integer('survey_question_id')->unsigned();
            $table->foreign('survey_question_id')->references('id')->on('survey_questions')->onDelete('cascade');
            $table->integer('user_id')->unsigned();
            $table->foreign('user_id')->references('id')->on('users')->onDelete('cascade');
            $table->text('input');
            $table->timestamps();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('survey_answers');
        Schema::dropIfExists('survey_questions');
        Schema::dropIfExists('surveys');
    }
}
