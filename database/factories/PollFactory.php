<?php

use Faker\Generator as Faker;

$factory->define(App\Poll::class, function (Faker $faker) {
    return [
        //
        'question' => $faker->text(100),
        'choices' => [
            [
                'choice' => 'a',
                'value' => $faker->text(20)
            ],
            [
                'choice' => 'b',
                'value' => $faker->text(20)
            ],
            [
                'choice' => 'c',
                'value' => $faker->text(20)
            ],
            [
                'choice' => 'd',
                'value' => $faker->text(20)
            ]
        ]
    ];
});
