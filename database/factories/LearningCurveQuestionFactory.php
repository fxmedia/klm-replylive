<?php

use Faker\Generator as Faker;

$factory->define(App\LearningCurveQuestion::class, function (Faker $faker) {
    return [
        'title' => $faker->text(100) . '?',
        'correct_answer' => $faker->randomElement(['a','b','c','d']),
        'choices' => [
            [
                'choice' => 'a',
                'value' => $faker->text(20)
            ],
            [
                'choice' => 'b',
                'value' => $faker->text(20)
            ],
            [
                'choice' => 'c',
                'value' => $faker->text(20)
            ],
            [
                'choice' => 'd',
                'value' => $faker->text(20)
            ]
        ]
    ];
});
