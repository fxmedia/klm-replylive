<?php

use Faker\Generator as Faker;
use App\User;

$factory->define(App\Post::class, function (Faker $faker) {
    $user = User::inRandomOrder()->first();


    return [
        //
        'user_id' => $user->id,
        'type' => 'comment',
        'message' => $faker->text('255'),
        'visible' => 1
    ];
});
