<?php

namespace App\Listeners;

use App\Events\NewUserRegistered;
use App\Senders\AdminEmailSender;
use App\Setting;

//use Illuminate\Queue\InteractsWithQueue;
//use Illuminate\Contracts\Queue\ShouldQueue;

class SendAdminUserRegistered
{
    /**
     * Create the event listener.
     *
     * @return void
     */
    public function __construct()
    {
        //
    }

    /**
     * Handle the event.
     *
     * @param NewUserRegistered $event
     * @return void
     */
    public function handle(NewUserRegistered $event)
    {
        //
        $settings = Setting::getSettingsAsObject();

        $user = $event->getUser();
        $data = [
            'user' => $user,
            'username' => $user->name,
            'user_email' => $user->email,
            'user_id' => $user->id,
            'registered_at' => $user->created_at,
            'token' => $settings->media_token
        ];

        $email = (object) [
            'subject' => 'New user registered for '. $settings->event_name,
            'reply_to' => $settings->email_default_reply ?? 'info@fxmedia.com',
            'from_email' => $settings->email_default_from_email ?? 'info@fxmedia.com',
            'from_name' => $settings->email_default_from_name ?? 'FXmedia',
            'view' => 'emails.admin.new-register'
        ];

        AdminEmailSender::sendSubscribers($email, $data);
    }
}