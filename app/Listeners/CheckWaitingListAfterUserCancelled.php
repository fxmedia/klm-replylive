<?php

namespace App\Listeners;

use App\Attending;
use App\Events\UserSetCancelled;
use App\Senders\EmailSender;
//use Illuminate\Queue\InteractsWithQueue;
//use Illuminate\Contracts\Queue\ShouldQueue;

class CheckWaitingListAfterUserCancelled
{
    /**
     * Create the event listener.
     *
     * @return void
     */
    public function __construct()
    {
        //
    }

    /**
     * Handle the event.
     *
     * @param UserSetCancelled $event
     * @return void
     */
    public function handle(UserSetCancelled $event)
    {

        $cancelledUser = $event->getUser();
        $attendingWaiting = Attending::getFirstWaiting($cancelledUser->attending->group_id);

        if($attendingWaiting !== null){
            // change status to 'appending' and send user an email
            $newAttendingStatus = Attending::determineAttendingStatus($attendingWaiting->user);

            if($newAttendingStatus === 'attending'){

                $attendingWaiting->status = 'attending';

                if($attendingWaiting->save()){
                    EmailSender::sendAction($attendingWaiting->user, 'moved_from_waiting_to_attending');
                }

            }

        }
    }
}
