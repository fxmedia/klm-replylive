<?php

namespace App\Listeners;

use App\Events\UserSetAttending;
use App\Senders\EmailSender;
//use Illuminate\Queue\InteractsWithQueue;
//use Illuminate\Contracts\Queue\ShouldQueue;

class SendMailUserAttending
{
    /**
     * Create the event listener.
     *
     * @return void
     */
    public function __construct()
    {
        //
    }

    /**
     * Handle the event.
     *
     * @param UserSetAttending $event
     * @return void
     */
    public function handle(UserSetAttending $event)
    {
        $user = $event->getUser();

        EmailSender::sendAction($user, 'attending');
    }
}
