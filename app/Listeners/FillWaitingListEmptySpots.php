<?php

namespace App\Listeners;

use App\Attending;
use App\Events\WaitingListLimitChanged;
use App\Group;
use App\Senders\EmailSender;
use App\Setting;
use Illuminate\Support\Collection;

//use Illuminate\Queue\InteractsWithQueue;
//use Illuminate\Contracts\Queue\ShouldQueue;

class FillWaitingListEmptySpots
{
    /**
     * Create the event listener.
     *
     * @return void
     */
    public function __construct()
    {
        //
    }

    /**
     * Handle the event.
     *
     * @param  WaitingListLimitChanged  $event
     * @return void
     */
    public function handle(WaitingListLimitChanged $event)
    {
        // master limit and group limits should not be used simultaneously

        $masterLimit = Setting::getValue('event_attending_limit');
        if($masterLimit > 0){
            $amountAttendingUsers = Attending::whereStatus('attending')->count();

            if($amountAttendingUsers < $masterLimit) {
                $waitingUsers = $this->getWaitingUsers($masterLimit - $amountAttendingUsers, null);

                if(count($waitingUsers)){
                    $this->setUsersAttending($waitingUsers);
                }
            }
        } else {
            $groupsWithLimit = Group::where('limit','>','0')->get();
            foreach($groupsWithLimit as $group){
                $amountAttendingUsers = Attending::whereGroupId($group->id)->whereStatus('attending')->count();

                if($amountAttendingUsers < $group->limit){
                    $waitingUsers = $this->getWaitingUsers($group->limit - $amountAttendingUsers, $group->id);


                    if(count($waitingUsers)){
                        // set them all to attending!
                        $this->setUsersAttending($waitingUsers);
                    }

                }
            }
        }
    }

    /**
     * @param int $limit
     * @param int|null $groupId
     * @return \Illuminate\Database\Eloquent\Collection|\Illuminate\Support\Collection|static[]
     */
    protected function getWaitingUsers(int $limit, int $groupId = null){
        if($groupId === null){
            return Attending::whereStatus('waiting')
                ->orderBy('updated_at', 'asc')
                ->take($limit)->get();
        }

        return Attending::whereGroupId($groupId)
            ->whereStatus('waiting')
            ->orderBy('updated_at', 'asc')
            ->take($limit)->get();
    }

    /**
     * @param Collection $waitingUsers
     */
    protected function setUsersAttending(Collection $waitingUsers){
        foreach ($waitingUsers as $attending){
            $attending->status = 'attending';

            if($attending->save()){
                // send mail!
                EmailSender::sendAction($attending->user, 'moved_from_waiting_to_attending');
            }
        }
    }
}
