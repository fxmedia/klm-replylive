<?php

namespace App;

use Illuminate\Database\Eloquent\Model;
use Illuminate\Support\Str;

/**
 * App\User
 *
 * @property-read mixed $name
 * @property-read \Illuminate\Database\Eloquent\Collection|\App\Group[] $groups
 * @property-read \Illuminate\Database\Eloquent\Collection|\App\PollAnswer[] $pollAnswers
 * @property-read \Illuminate\Database\Eloquent\Collection|\App\Post[] $posts
 * @mixin \Eloquent
 * @property int $id
 * @property string $token
 * @property string $firstname
 * @property string|null $lastname
 * @property string|null $email
 * @property string|null $phonenumber
 * @property string|null $image
 * @property array $extra
 * @property int $test_user
 * @property \Carbon\Carbon|null $created_at
 * @property \Carbon\Carbon|null $updated_at
 * @method static \Illuminate\Database\Eloquent\Builder|\App\User whereCreatedAt($value)
 * @method static \Illuminate\Database\Eloquent\Builder|\App\User whereEmail($value)
 * @method static \Illuminate\Database\Eloquent\Builder|\App\User whereExtra($value)
 * @method static \Illuminate\Database\Eloquent\Builder|\App\User whereFirstname($value)
 * @method static \Illuminate\Database\Eloquent\Builder|\App\User whereId($value)
 * @method static \Illuminate\Database\Eloquent\Builder|\App\User whereImage($value)
 * @method static \Illuminate\Database\Eloquent\Builder|\App\User whereLastname($value)
 * @method static \Illuminate\Database\Eloquent\Builder|\App\User wherePhonenumber($value)
 * @method static \Illuminate\Database\Eloquent\Builder|\App\User whereTestUser($value)
 * @method static \Illuminate\Database\Eloquent\Builder|\App\User whereToken($value)
 * @method static \Illuminate\Database\Eloquent\Builder|\App\User whereUpdatedAt($value)
 * @property-read mixed $group_ids
 * @property-read mixed $image_path
 * @property-read \App\Attending $attending
 * @property-read \Illuminate\Database\Eloquent\Collection|\App\Pledge[] $pledges
 */
class User extends Model
{
    protected $fillable = ['firstname', 'lastname', 'email', 'phonenumber', 'image', 'test_user', 'extra'];

    protected $appends = ['name',  'image_path'];

    /**
     * Set database fields as different type.
     * E.g. Array will be stored as serialized array and will be unserialized when retrieved
     * @var array
     */
    protected $casts = [
        'extra' => 'array'
    ];

    protected $with = ['attending', 'groups'];

    public function addPoints($points) {
        $extra = $this->extra;
        if(isset($this->extra['points'])) {
            $extra['points'] = $this->extra['points'] + $points;
        } else {
            $extra['points'] = $points;
        }
        $this->extra = $extra;
        $this->save();
    }

    public function addExtraValue($attribute, $value) {
        $extra = $this->extra;
        $extra[$attribute] = $value;
        $this->extra = $extra;
        $this->save();
    }

    public function getNameAttribute() {
        if(!empty($this->lastname)) {
            return $this->firstname . ' ' . $this->lastname;
        }
        return $this->firstname;
    }

    public function getImagePathAttribute(){
        if(!isset($this->image)) return null;


        $token = Setting::getValue('media_token');
        return route('frontend.media.image', ['token'=>$token, 'group'=>'user', 'file'=>$this->image]);
    }

    public function attending() {
        return $this->hasOne('App\Attending');
    }

    public function groups(){
        return $this->belongsToMany('App\Group');
    }

    public function pollAnswers() {
        return $this->hasMany('App\PollAnswer');
    }

    public function pledges() {
        return $this->hasMany('App\Pledge');
    }

    public function posts() {
        return $this->hasMany('App\Post');
    }

    /**
     * Get all users except the ones that are pending or declined
     *
     * @return \Illuminate\Database\Eloquent\Collection|static[]
     */
    public static function getUsersWithoutPendingOrDeclined(){
        return User::whereDoesntHave('attending', function($query){
            return $query->where('status','pending')->orWhere('status','declined');
        })->get();
    }

    /**
     * Get all test users
     *
     * @return mixed
     */
    public static function getTestUsers(){
        return User::where('test_user', '=', 1)->get();
    }

    /**
     * Get all users without a group
     *
     * @return mixed
     */
    public static function getUsersNoGroup(){
        return User::doesntHave('groups')->get();
    }

    /**
     * isingroup
     *
     * @param group $group
     * @return bool true if user is attached to given group
     */
    public function isInGroup(Group $group){
        $groups = [];
        foreach ($this->groups as $userGroup) {
            $groups[] = $userGroup->id;
        }

        return in_array($group->id, $groups);
    }

    /**
     * createToken is a function to create a new random token for a user,
     * or checks if given token already exists
     *
     * @param string $token Token to check if already exists in db
     *
     * @return string
     */
    public static function createToken($token = null){
        $configLength = config('users.token_length');
        if($token !== null && strlen($token) === $configLength){
            if(! User::where('token',$token)->exists()) return $token;
        }
        // do we want to do a check if it already exists in the db? 62^12 would be random enough to not make that happen...
        return Str::random($configLength);
    }

    /**
     * verifyUserToken is a function to check if a user with the given token exists.
     * This function is being used by the verifyUserToken middleware
     *
     * @param string $token User token to check
     * @return bool
     */
    public static function verifyUserToken($token = null){
        if($token === null) return false;

        return User::whereToken($token)->exists();
    }

}
