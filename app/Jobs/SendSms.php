<?php

namespace App\Jobs;

use App\Senders\SmsSender;
use App\Sms;
use App\SmsLog;
use App\User;
use Illuminate\Bus\Queueable;
use Illuminate\Queue\SerializesModels;
use Illuminate\Queue\InteractsWithQueue;
use Illuminate\Contracts\Queue\ShouldQueue;
use Illuminate\Foundation\Bus\Dispatchable;
use Illuminate\Support\Facades\Log;
use Exception;

class SendSms implements ShouldQueue
{
    use Dispatchable, InteractsWithQueue, Queueable, SerializesModels;

    /**
     * The number of seconds the job can run before timing out.
     *
     * @var int
     */
    public $timeout = 20;
    /**
     * The number of times the job may be attempted.
     *
     * @var int
     */
    public $tries = 5;

    protected $sms;
    protected $user;
    protected $log;

    /**
     * Create a new job instance.
     *
     * @param Sms $sms
     * @param User $user
     * @param SmsLog $log
     *
     * @return void
     */
    public function __construct(Sms $sms, User $user, SmsLog $log)
    {
        //
        $this->sms = $sms;
        $this->user = $user;
        $this->log = $log;
    }

    /**
     * Execute the job.
     *
     * @return void
     */
    public function handle()
    {
        //
        $statusList = config('logs.status');
        $error = null;
        $result = null;

        $this->log->status = $statusList[1];
        $this->log->save();

        try{
            $result = SmsSender::_send($this->sms, $this->user);

            if(property_exists($result, 'failed') && $result->failed === true){
                $error = $result->error;
            } else {
                $result = $result->recipients->totalSentCount;
            }
        } catch (Exception $e){
            Log::info('Failed to send sms: ' . $this->user->phonenumber . ' - Error: ' . $e->getMessage() . ':' . $e->getLine());
            $error = $e->getMessage() . ': ' . $e->getLine();
        }

        $this->log->error = $error;
        $this->log->result = $result;
        $this->log->status = $statusList[2];
        $this->log->save();
    }
}
