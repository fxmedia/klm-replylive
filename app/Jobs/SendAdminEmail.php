<?php

namespace App\Jobs;

use App\Email;
use App\Senders\AdminEmailSender;
use Illuminate\Bus\Queueable;
use Illuminate\Queue\SerializesModels;
use Illuminate\Queue\InteractsWithQueue;
use Illuminate\Contracts\Queue\ShouldQueue;
use Illuminate\Foundation\Bus\Dispatchable;
use Exception;

class SendAdminEmail implements ShouldQueue
{
    use Dispatchable, InteractsWithQueue, Queueable, SerializesModels;

    /**
     * The number of seconds the job can run before timing out.
     *
     * @var int
     */
    public $timeout = 20;
    /**
     * The number of times the job may be attempted.
     *
     * @var int
     */
    public $tries = 5;

    protected $email;
    protected $address;
    protected $data;
    /**
     * Create a new job instance.
     *
     * @param \stdClass $email
     * @param string $address
     * @param array $data
     *
     * @return void
     */
    public function __construct(\stdClass $email, string $address, array $data)
    {
        //
        $this->email = $email;
        $this->address = $address;
        $this->data = $data;
    }

    /**
     * Execute the job.
     *
     * @return void
     */
    public function handle()
    {
        //
        try{
            AdminEmailSender::_send($this->email, $this->address, $this->data);
        } catch(Exception $e){}
    }
}
