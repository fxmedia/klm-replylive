<?php

namespace App\Jobs;

use App\Email;
use App\EmailLog;
use App\Senders\EmailSender;
use App\User;
use Illuminate\Bus\Queueable;
use Illuminate\Queue\SerializesModels;
use Illuminate\Queue\InteractsWithQueue;
use Illuminate\Contracts\Queue\ShouldQueue;
use Illuminate\Foundation\Bus\Dispatchable;
use Exception;
use Illuminate\Support\Facades\Log;

class SendEmail implements ShouldQueue
{
    use Dispatchable, InteractsWithQueue, Queueable, SerializesModels;

    /**
     * The number of seconds the job can run before timing out.
     *
     * @var int
     */
    public $timeout = 20;
    /**
     * The number of times the job may be attempted.
     *
     * @var int
     */
    public $tries = 5;

    protected $email;
    protected $user;
    protected $log;

    /**
     * SendEmail constructor.
     *
     * @param Email $email
     * @param User $user
     * @param EmailLog $log
     *
     * @return void
     */
    public function __construct(Email $email, User $user, EmailLog $log)
    {
        //
        $this->email = $email;
        $this->user = $user;
        $this->log = $log;
    }

    /**
     * Execute the job.
     *
     * @return void
     */
    public function handle()
    {
        //
        $statusList = config('logs.status');
        $error = null;

        $this->log->status = $statusList[1];
        $this->log->save();

        try{
            EmailSender::_send($this->email, $this->user);
        } catch(Exception $e){
            Log::info('Failed to send email: ' . $this->user->email . ' for Email with ID ' . $this->email->id . ' - Error: ' . $e->getMessage());
            $error = $e->getMessage();
        }

        $this->log->error = $error;
        $this->log->status = $statusList[2];

        $this->log->save();
    }
}