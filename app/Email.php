<?php

namespace App;

use Illuminate\Database\Eloquent\Model;
use Symfony\Component\Finder\Finder;

/**
 * App\Email
 *
 * @property int $id
 * @property string $name
 * @property string $subject
 * @property string|null $reply_to
 * @property string $from_name
 * @property string $from_email
 * @property string|null $action
 * @property string $view
 * @property string|null $attachment
 * @property string|null $body_text
 * @property string|null $disclaimer_text
 * @property string|null $header_image
 * @property string|null $footer_image
 * @property \Carbon\Carbon|null $created_at
 * @property \Carbon\Carbon|null $updated_at
 * @property-read \Illuminate\Database\Eloquent\Collection|\App\EmailLog[] $logs
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Email whereAction($value)
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Email whereAttachment($value)
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Email whereBodyText($value)
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Email whereCreatedAt($value)
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Email whereDisclaimerText($value)
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Email whereFooterImage($value)
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Email whereFromEmail($value)
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Email whereFromName($value)
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Email whereHeaderImage($value)
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Email whereId($value)
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Email whereName($value)
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Email whereReplyTo($value)
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Email whereSubject($value)
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Email whereUpdatedAt($value)
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Email whereView($value)
 * @mixin \Eloquent
 */
class Email extends Model
{
    //
    protected $table = 'emails';

    protected $fillable = ['name', 'subject', 'reply_to', 'from_name', 'from_email', 'action', 'view', 'attachment', 'body_text', 'disclaimer_text', 'header_image', 'footer_image'];

    /**
     * @return \Illuminate\Database\Eloquent\Relations\HasMany
     */
    public function logs(){
        return $this->hasMany('App\EmailLog');
    }

    /**
     * Returns list of implemented automated actions
     * @return array
     */
    public static function getActions(){
        return [
            'noaction' => 'No action',
            'register' => 'After registration, when user has been set on pending',
            'attending' => 'User status has been set on attending',
            'waiting' => 'User status has been set on waiting list',
            'cancelled'=> 'User status has been set to cancelled and is not attending',
            'moved_from_waiting_to_attending' => 'When User has been moved from waiting list to attending',
            'resend_personal_url' => 'Resend the personal url when the user forgot it',
            'register_declined' => 'When an admin denied the registration of a new user'
        ];
    }

    /**
     * @param string $image e.g. header_image or footer_image
     *
     * @return string
     */
    public function imagePath( $image )
    {
        return storage_path('uploads/'. config('storage.email_images') .'/'. $this->{$image});
    }

    /**
     * This function uses the default embed function of the Mail Message class
     * to embed images inline into the email. However, this functionality does not
     * exist in preview mode, so a fallback has to be used
     *
     * @param $message
     * @param string $imageType
     * @return string
     */
    public function embed( $message, $imageType )
    {
        if(!isset($this->preview) || !$this->preview) {
            return $message->embed( $this->imagePath( $imageType ) );
        }

        $data = base64_encode(file_get_contents($this->imagePath($imageType)));
        $ext = pathinfo($this->imagePath($imageType), PATHINFO_EXTENSION);

        return "data:image/{$ext};base64,{$data}";
    }


    /**
     * @param string $action
     * @return Model|null|static
     */
    public static function getByAction($action)
    {
        return Email::whereAction($action)->first();
    }

    /**
     * @return array Array of existing images for mail
     */
    public static function getImageList(){
        $images = [];
        $images[''] = '';
        $path = storage_path('uploads/'. config('storage.email_images') .'/');

        if(is_dir($path)){
            $finder = new Finder();
            $dir = $finder->in($path);

            foreach($dir as $file){
                if($file->isFile()){
                    $images[$file->getBasename()] = $file->getBasename();
                }
            }
        }

        return $images;
    }

    /**
     * @return array Array of existing attachments
     */
    public static function getAttachmentsList(){
        $attachments = [];
        $attachments[''] = '';
        $path = storage_path('uploads/'. config('storage.email_attachments') .'/');

        if(is_dir($path)){
            $finder = new Finder();
            $dir = $finder->in($path);

            foreach($dir as $file){
                if($file->isFile()){
                    $attachments[$file->getBasename()] = $file->getBasename();
                }
            }
        }
        return $attachments;
    }

    /**
     * @return array
     */
    public static function getLayoutOptions(){
        $templates = [];

        $finder = new Finder();
        $dir = $finder->in(base_path('resources/views/emails/client/'));

        foreach($dir as $file){
            if($file->isFile()){
                $basename = str_replace('.blade.php', '', $file->getBasename());
                $templates['emails.client.'.$basename] = $basename;
            }
        }

        return $templates;
    }
}
