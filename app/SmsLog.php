<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

/**
 * App\SmsLog
 *
 * @property int $id
 * @property int $user_id
 * @property int $sms_id
 * @property int|null $batch
 * @property string|null $error
 * @property string|null $result
 * @property \Carbon\Carbon|null $created_at
 * @property \Carbon\Carbon|null $updated_at
 * @property-read \App\Sms $sms
 * @property-read \App\User $user
 * @method static \Illuminate\Database\Eloquent\Builder|\App\SmsLog whereBatch($value)
 * @method static \Illuminate\Database\Eloquent\Builder|\App\SmsLog whereCreatedAt($value)
 * @method static \Illuminate\Database\Eloquent\Builder|\App\SmsLog whereError($value)
 * @method static \Illuminate\Database\Eloquent\Builder|\App\SmsLog whereId($value)
 * @method static \Illuminate\Database\Eloquent\Builder|\App\SmsLog whereResult($value)
 * @method static \Illuminate\Database\Eloquent\Builder|\App\SmsLog whereSmsId($value)
 * @method static \Illuminate\Database\Eloquent\Builder|\App\SmsLog whereUpdatedAt($value)
 * @method static \Illuminate\Database\Eloquent\Builder|\App\SmsLog whereUserId($value)
 * @mixin \Eloquent
 * @property string $status
 * @method static \Illuminate\Database\Eloquent\Builder|\App\SmsLog whereStatus($value)
 */
class SmsLog extends Model
{
    //
    protected $table = 'sms_log';
    protected $fillable = ['user_id', 'sms_id', 'batch', 'status', 'error', 'result'];

    /**
     * @return \Illuminate\Database\Eloquent\Relations\BelongsTo
     */
    public function user()
    {
        return $this->belongsTo('App\User');
    }

    /**
     * @return \Illuminate\Database\Eloquent\Relations\BelongsTo
     */
    public function sms()
    {
        return $this->belongsTo('App\Sms');
    }
}
