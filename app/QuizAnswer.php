<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

/**
 * App\QuizAnswer
 *
 * @property-read \App\QuizQuestion $question
 * @property-read \App\Quiz $quiz
 * @property-read \App\User $user
 * @mixin \Eloquent
 * @property int $id
 * @property int $quiz_id
 * @property int $question_id
 * @property int $user_id
 * @property string $choice
 * @property \Carbon\Carbon|null $created_at
 * @property \Carbon\Carbon|null $updated_at
 * @method static \Illuminate\Database\Eloquent\Builder|\App\QuizAnswer whereChoice($value)
 * @method static \Illuminate\Database\Eloquent\Builder|\App\QuizAnswer whereCreatedAt($value)
 * @method static \Illuminate\Database\Eloquent\Builder|\App\QuizAnswer whereId($value)
 * @method static \Illuminate\Database\Eloquent\Builder|\App\QuizAnswer whereQuestionId($value)
 * @method static \Illuminate\Database\Eloquent\Builder|\App\QuizAnswer whereQuizId($value)
 * @method static \Illuminate\Database\Eloquent\Builder|\App\QuizAnswer whereUpdatedAt($value)
 * @method static \Illuminate\Database\Eloquent\Builder|\App\QuizAnswer whereUserId($value)
 * @property int $quiz_question_id
 * @method static \Illuminate\Database\Eloquent\Builder|\App\QuizAnswer whereQuizQuestionId($value)
 */
class QuizAnswer extends Model
{
    protected $fillable = ["quiz_question_id", "user_id", "choice"];
    protected $table = "quiz_answers";

    public function user() {
        return $this->belongsTo('App\User');
    }

    public function question() {
        return $this->belongsTo('App\QuizQuestion');
    }

}
