<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

/**
 * App\OpenQuestion
 *
 * @property int $id
 * @property string $question
 * @property array $settings
 * @property \Carbon\Carbon|null $created_at
 * @property \Carbon\Carbon|null $updated_at
 * @property-read \Illuminate\Database\Eloquent\Collection|\App\OpenQuestionAnswer[] $answers
 * @method static \Illuminate\Database\Eloquent\Builder|\App\OpenQuestion whereCreatedAt($value)
 * @method static \Illuminate\Database\Eloquent\Builder|\App\OpenQuestion whereId($value)
 * @method static \Illuminate\Database\Eloquent\Builder|\App\OpenQuestion whereQuestion($value)
 * @method static \Illuminate\Database\Eloquent\Builder|\App\OpenQuestion whereSettings($value)
 * @method static \Illuminate\Database\Eloquent\Builder|\App\OpenQuestion whereUpdatedAt($value)
 * @mixin \Eloquent
 * @property-read mixed $group_ids
 * @property string|null $tags
 * @property int $multiple_answers
 * @property-read mixed $result
 * @property-read mixed $result_users
 * @property-read \Illuminate\Database\Eloquent\Collection|\App\Group[] $groups
 * @method static \Illuminate\Database\Eloquent\Builder|\App\OpenQuestion whereMultipleAnswers($value)
 * @method static \Illuminate\Database\Eloquent\Builder|\App\OpenQuestion whereTags($value)
 */
class OpenQuestion extends Model
{
    protected $fillable = ['question', 'tags', 'multiple_answers'];

    protected $appends = ['result', 'result_users', 'group_ids'];

    public function getResultAttribute() {
        return $this->answers;
    }

    public function getResultUsersAttribute(){
        return $this->answers()->pluck('user_id')->toArray();
    }

    public function getGroupIdsAttribute(){
        return $this->groups()->pluck('id')->toArray();
    }

    public function answers() {
        return $this->hasMany('App\OpenQuestionAnswer');
    }

    public function groups(){
        return $this->belongsToMany('App\Group');
    }

    /**
     * isInGroup
     *
     * @param group $group
     * @return bool true if user is attached to given group
     */
    public function isInGroup(Group $group){
        $groups = [];
        foreach ($this->groups as $userGroup) {
            $groups[] = $userGroup->id;
        }

        return in_array($group->id, $groups);
    }
}
