<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

/**
 * App\PollAnswer
 *
 * @property-read \App\Poll $poll
 * @property-read \App\User $user
 * @mixin \Eloquent
 * @property int $id
 * @property int $poll_id
 * @property int $user_id
 * @property string $choice
 * @property \Carbon\Carbon|null $created_at
 * @property \Carbon\Carbon|null $updated_at
 * @method static \Illuminate\Database\Eloquent\Builder|\App\PollAnswer whereChoice($value)
 * @method static \Illuminate\Database\Eloquent\Builder|\App\PollAnswer whereCreatedAt($value)
 * @method static \Illuminate\Database\Eloquent\Builder|\App\PollAnswer whereId($value)
 * @method static \Illuminate\Database\Eloquent\Builder|\App\PollAnswer wherePollId($value)
 * @method static \Illuminate\Database\Eloquent\Builder|\App\PollAnswer whereUpdatedAt($value)
 * @method static \Illuminate\Database\Eloquent\Builder|\App\PollAnswer whereUserId($value)
 */
class PollAnswer extends Model
{
    protected $fillable = ["poll_id", "user_id", "choice"];
    protected $table = "poll_answers";

    public function user() {
        return $this->belongsTo('App\User');
    }

    public function poll() {
        return $this->belongsTo('App\Poll');
    }
}
