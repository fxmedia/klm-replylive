<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

/**
 * App\LearningCurveAnswer
 *
 * @property-read \App\LearningCurveQuestion $question
 * @property-read \App\LearningCurve $learning_curve
 * @property-read \App\User $user
 * @mixin \Eloquent
 * @property int $id
 * @property int $learning_curve_id
 * @property int $question_id
 * @property int $user_id
 * @property string $choice
 * @property \Carbon\Carbon|null $created_at
 * @property \Carbon\Carbon|null $updated_at
 * @method static \Illuminate\Database\Eloquent\Builder|\App\LearningCurveAnswer whereChoice($value)
 * @method static \Illuminate\Database\Eloquent\Builder|\App\LearningCurveAnswer whereCreatedAt($value)
 * @method static \Illuminate\Database\Eloquent\Builder|\App\LearningCurveAnswer whereId($value)
 * @method static \Illuminate\Database\Eloquent\Builder|\App\LearningCurveAnswer whereQuestionId($value)
 * @method static \Illuminate\Database\Eloquent\Builder|\App\LearningCurveAnswer whereLearningCurveId($value)
 * @method static \Illuminate\Database\Eloquent\Builder|\App\LearningCurveAnswer whereUpdatedAt($value)
 * @method static \Illuminate\Database\Eloquent\Builder|\App\LearningCurveAnswer whereUserId($value)
 * @property int $learning_curve_question_id
 * @method static \Illuminate\Database\Eloquent\Builder|\App\LearningCurveAnswer whereLearningCurveQuestionId($value)
 * @property string $phase
 * @method static \Illuminate\Database\Eloquent\Builder|\App\LearningCurveAnswer wherePhase($value)
 */
class LearningCurveAnswer extends Model
{
    protected $fillable = ["learning_curve_question_id", "user_id", "choice"];
    protected $table = "learning_curve_answers";

    public function user() {
        return $this->belongsTo('App\User');
    }

    public function question() {
        return $this->belongsTo('App\LearningCurveQuestion');
    }

}
