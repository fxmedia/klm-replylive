<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

/**
 * App\SettingGroup
 *
 * @property-read \Illuminate\Database\Eloquent\Collection|\App\Setting[] $settings
 * @mixin \Eloquent
 * @property int $id
 * @property string $name
 * @property int $order
 * @method static \Illuminate\Database\Eloquent\Builder|\App\SettingGroup whereId($value)
 * @method static \Illuminate\Database\Eloquent\Builder|\App\SettingGroup whereName($value)
 * @method static \Illuminate\Database\Eloquent\Builder|\App\SettingGroup whereOrder($value)
 */
class SettingGroup extends Model
{
    /**
     * The table associated with the model.
     *
     * @var string
     */
    protected $table = 'setting_groups';

    /**
     * Indicates if the model should be timestamped.
     *
     * @var bool
     */
    public $timestamps = false;

    /**
     * Get the settings for the setting group.
     */
    public function settings()
    {
        return $this->hasMany(Setting::class);
    }
}
