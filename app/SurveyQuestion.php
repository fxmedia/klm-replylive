<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

/**
 * App\SurveyQuestion
 *
 * @mixin \Eloquent
 * @property int $id
 * @property int $survey_id
 * @property string $type
 * @property string $title
 * @property array|null $data
 * @property \Carbon\Carbon|null $created_at
 * @property \Carbon\Carbon|null $updated_at
 * @method static \Illuminate\Database\Eloquent\Builder|\App\SurveyQuestion whereCreatedAt($value)
 * @method static \Illuminate\Database\Eloquent\Builder|\App\SurveyQuestion whereData($value)
 * @method static \Illuminate\Database\Eloquent\Builder|\App\SurveyQuestion whereId($value)
 * @method static \Illuminate\Database\Eloquent\Builder|\App\SurveyQuestion whereSurveyId($value)
 * @method static \Illuminate\Database\Eloquent\Builder|\App\SurveyQuestion whereTitle($value)
 * @method static \Illuminate\Database\Eloquent\Builder|\App\SurveyQuestion whereType($value)
 * @method static \Illuminate\Database\Eloquent\Builder|\App\SurveyQuestion whereUpdatedAt($value)
 * @property int $order
 * @method static \Illuminate\Database\Eloquent\Builder|\App\SurveyQuestion whereOrder($value)
 * @property-read \Illuminate\Database\Eloquent\Collection|\App\SurveyAnswer[] $answers
 * @property-read \App\Survey $survey
 */
class SurveyQuestion extends Model
{
    //
    protected $fillable = ['type', 'title', 'data', 'order'];
    protected $table = "survey_questions";
    protected $casts = [
        'data' => 'array'
    ];

    public function survey(){
        return $this->belongsTo('App\Survey');
    }

    public function answers(){
        return $this->hasMany('App\SurveyAnswer');
    }
}
