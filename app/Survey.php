<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

/**
 * App\Survey
 *
 * @property-read \Illuminate\Database\Eloquent\Collection|\App\SurveyAnswer[] $answers
 * @mixin \Eloquent
 * @property int $id
 * @property string $name
 * @property int $takeover
 * @property string|null $intro
 * @property string|null $outro
 * @property \Carbon\Carbon|null $created_at
 * @property \Carbon\Carbon|null $updated_at
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Survey whereCreatedAt($value)
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Survey whereId($value)
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Survey whereIntro($value)
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Survey whereName($value)
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Survey whereOutro($value)
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Survey whereTakeover($value)
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Survey whereUpdatedAt($value)
 * @property string|null $tags
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Survey whereTags($value)
 * @property-read \Illuminate\Database\Eloquent\Collection|\App\Group[] $groups
 * @property-read mixed $group_ids
 * @property-read \Illuminate\Database\Eloquent\Collection|\App\SurveyQuestion[] $questions
 * @property-read array $user_progress
 */
class Survey extends Model
{
    protected $fillable = ['name', 'tags', 'takeover', 'intro', 'outro'];
    protected $appends = ['group_ids', 'user_progress'];

    protected $with = ['questions'];

    public function getGroupIdsAttribute(){
        return $this->groups()->pluck('id')->toArray();
    }

    /**
     * @return array 'user_id' => 'question_index'
     */
    public function getUserProgressAttribute(){
        $user_progress = [];
        foreach($this->questions as $key => $question){
            foreach($question->answers as $answer){
                $user_progress[$answer->user_id] = $key;
            }
        }
        return $user_progress;
    }

    public function questions() {
        return $this->hasMany('App\SurveyQuestion')->orderBy('order');
    }

    public function groups(){
        return $this->belongsToMany('App\Group');
    }


    /**
     * isInGroup
     *
     * @param group $group
     * @return bool true if user is attached to given group
     */
    public function isInGroup(Group $group){
        $groups = [];
        foreach ($this->groups as $userGroup) {
            $groups[] = $userGroup->id;
        }

        return in_array($group->id, $groups);
    }
}
