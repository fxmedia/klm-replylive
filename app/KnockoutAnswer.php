<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

/**
 * App\KnockoutAnswer
 *
 * @property int $id
 * @property int $knockout_id
 * @property int $question_id
 * @property int $user_id
 * @property string $choice
 * @property \Carbon\Carbon|null $created_at
 * @property \Carbon\Carbon|null $updated_at
 * @property-read \App\Knockout $knockout
 * @property-read \App\KnockoutQuestion $question
 * @property-read \App\User $user
 * @method static \Illuminate\Database\Eloquent\Builder|\App\KnockoutAnswer whereChoice($value)
 * @method static \Illuminate\Database\Eloquent\Builder|\App\KnockoutAnswer whereCreatedAt($value)
 * @method static \Illuminate\Database\Eloquent\Builder|\App\KnockoutAnswer whereId($value)
 * @method static \Illuminate\Database\Eloquent\Builder|\App\KnockoutAnswer whereKnockoutId($value)
 * @method static \Illuminate\Database\Eloquent\Builder|\App\KnockoutAnswer whereQuestionId($value)
 * @method static \Illuminate\Database\Eloquent\Builder|\App\KnockoutAnswer whereUpdatedAt($value)
 * @method static \Illuminate\Database\Eloquent\Builder|\App\KnockoutAnswer whereUserId($value)
 * @mixin \Eloquent
 * @property int $knockout_question_id
 * @method static \Illuminate\Database\Eloquent\Builder|\App\KnockoutAnswer whereKnockoutQuestionId($value)
 */
class KnockoutAnswer extends Model
{
    protected $fillable = ["knockout_question_id", "user_id", "choice"];
    protected $table = "knockout_answers";

    public function user() {
        return $this->belongsTo('App\User');
    }

    public function question() {
        return $this->belongsTo('App\KnockoutQuestion');
    }
}
