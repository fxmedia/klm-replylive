<?php

namespace App\Import;

use App\Group;
use App\Survey;
use App\SurveyQuestion;
use Validator;

class SurveyImport extends BaseImport
{
    public function getHeaders(){
        return ['id', 'survey', 'tags', 'takeover', 'intro', 'outro', 'groups', 'question' , 'type', 'order' ,'data'];
    }

    private function getGroupsIds($groups = null){
        if(isset($groups) && strlen($groups) > 0){
            if(count($names = explode(',',$groups)) > 0){
                $groupIds = [];

                foreach($names as $name){
                    // if given group doesn't exist, create it!
                    if(!$dbGroup = Group::where('name', trim($name))->first()){
                        $dbGroup = new Group;
                        $dbGroup->name = trim($name);
                        $dbGroup->save();
                    }
                    // store the id in the array
                    $groupIds[] = $dbGroup->id;
                }

                return $groupIds;
            }
        }

        return [];
    }

    public function import(){
        $skipCount = 0;
        $addCount = 0;
        $addQuestionCount = 0;
        $warnings = [];
        $errors = [];

        $idLink = []; // for surveys

        foreach($this->data as $key => $obj){
            if(
                $obj->id == null &&
                ($obj->survey == null || $obj->question == null)
            ) {
                // skip, cause it misses identifier fields
                $warnings[] = 'Skipped row '. ($key+1) . ': neither survey nor question';
                $skipCount++;
                continue;
            }

            $data = [];
            foreach($obj as $k => $v){
                if($v !== null && $k !== 'id')$data[$k] = $v;
            }

            if($obj->survey != null){
                $data['name'] = $obj->survey;

                $validation = Validator::make($data, config('validation.surveys.import.survey'));

                if($validation->fails()){
                    $errors[] = 'Validation failed on row '. ($key+1) .' (' . $validation->messages()->first() . ')';
                    $skipCount++;
                    continue;
                }
                //dd($validation->getData());

                $survey = new Survey($validation->getData());

                if($survey->save()){
                    $idLink[$obj->id] = $survey->id;

                    $groupIds = $this->getGroupsIds($obj['groups']);
                    $survey->groups()->sync($groupIds);

                    $addCount++;
                } else {
                    $errors[] = 'Unable to save Survey on row '. ($key+1);
                    $skipCount++;
                }
            } else {
                $data['title'] = $obj->question;

                $validation = Validator::make($data, config('validation.surveys.import.question'));

                if($validation->fails()){
                    $errors[] = 'Validation failed on row '. ($key+1) .' (' . $validation->messages()->first() . ')';
                    $skipCount++;
                    continue;
                }

                if(!isset($idLink[$obj->id])){
                    $errors[] = "Question link with survey id on row ". ($key+1) . ' is not set correctly';
                    $skipCount++;
                    continue;
                }

                $vData = $validation->getData();
                $vData['data'] = json_decode($data['data'], true) ?? [];

                $question = new SurveyQuestion($vData);
                $question->survey_id = $idLink[$obj->id];

                if($question->save()){
                    $addQuestionCount++;
                } else {
                    $errors[] = 'Unable to save Question on row '. ($key+1);
                    $skipCount++;
                }
            }
        }

        return [
            'errors' => $errors,
            'warnings' => $warnings,
            'messages' => [
                $skipCount . ' Row'. ($skipCount == 1? '' : 's') .' skipped',
                $addCount . ' Survey'. ($addCount == 1? '' : 's') .' added',
                'With '. $addQuestionCount . ' Question'. ($addCount == 1? '' : 's')
            ]
        ];
    }
}