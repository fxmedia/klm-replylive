<?php

namespace App\Import;

use App\Admin;
use Validator;
use Hash;

class AdminImport extends BaseImport
{

    public function getHeaders()
    {
        return ['name', 'email', 'notification_subscription'];
    }

    /**
     * array['errors'] array of error fields.
     * array['warnings'] array of warnings
     * array['status'] array of statuses
     *
     * @return array $arr
     */
    public function import()
    {
        $skipCount = 0;
        $addCount = 0;
        $warnings = [];
        $errors = [];


        foreach($this->data as $key => $admin){
            if($admin->email == null || $admin->name == null){
                $warnings[] = 'Skipped row '. ($key+1) . ': no email or name found';
                $skipCount++;
                continue;
            }
            $existingAdmin = Admin::where('email','=',$admin->email)->first();

            if($existingAdmin !== null){
                $warnings[] = 'Skipped row '. ($key+1) . ': admin with email '. $admin->email . ' already exists';
                $skipCount++;
                continue;
            }
            $data = [
                'name' =>$admin->name,
                'email' => $admin->email,
                'password' => Hash::make(str_random(8)),
                'notification_subscription' => $admin->notification_subscription
            ];

            $validation = Validator::make($data, config('validation.admins.import'));

            if($validation->fails()){
                // dd($validation->errors());
                $errors[] = 'Validation failed on row '. ($key+1) .': email or name is invalid';
                $skipCount++;
                continue;
            }

            // now add this to a user
            $newAdmin = Admin::create($data);

            if(!$newAdmin->save()){
                // fail!
                $errors[] = 'Failed saving row'. ($key+1) .' for unknown reason: '. $admin->email;
                continue;
            }

            $addCount++;
        }

        return [
            'errors' => $errors,
            'warnings' => $warnings,
            'messages' => [
                $skipCount . ' Row'. ($skipCount == 1? '' : 's') .' skipped',
                $addCount . ' Admin'. ($addCount == 1? '' : 's') .' added'
            ]
        ];

    }
}