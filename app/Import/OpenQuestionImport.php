<?php

namespace App\Import;

use App\Group;
use App\OpenQuestion;
use Validator;

class OpenQuestionImport extends BaseImport
{

    public function getHeaders(){
        return ['question', 'tags', 'multiple_answers', 'groups'];
    }

    private function getGroupsIds($groups = null){
        if(isset($groups) && strlen($groups) > 0){
            if(count($names = explode(',',$groups)) > 0){
                $groupIds = [];

                foreach($names as $name){
                    // if given group doesn't exist, create it!
                    if(!$dbGroup = Group::where('name', trim($name))->first()){
                        $dbGroup = new Group;
                        $dbGroup->name = trim($name);
                        $dbGroup->save();
                    }
                    // store the id in the array
                    $groupIds[] = $dbGroup->id;
                }

                return $groupIds;
            }
        }

        return [];
    }

    public function import(){
        $skipCount = 0;
        $addCount = 0;
        $addChoiceCount = 0;
        $warnings = [];
        $errors = [];

        foreach($this->data as $key => $obj){
            if(
                $obj->question == null){
                // skip, cause it misses identifier fields
                $warnings[] = 'Skipped row '. ($key+1) . ': no question found';
                $skipCount++;
                continue;
            }

            $data = [];
            foreach($obj as $k => $v){
                if($v !== null && $k !== 'id')$data[$k] = $v;
            }
            $validation = Validator::make($data,config('validation.open_questions.import'));
            if($validation->fails()){
                $errors[] = 'Validation failed on row '. ($key+1) .' (' . $validation->messages()->first() . ')';
                $skipCount++;
                continue;
            }

            $open_question = new OpenQuestion;

            $open_question->fill($validation->getData());

            if($open_question->save()){
                $groupIds = $this->getGroupsIds($obj['groups']);
                $open_question->groups()->sync($groupIds);

                $addCount++;
            }
        }

        return [
            'errors' => $errors,
            'warnings' => $warnings,
            'messages' => [
                $skipCount . ' Row'. ($skipCount == 1? '' : 's') .' skipped',
                $addCount . ' OpenQuestion'. ($addCount == 1? '' : 's') .' added',
                'With '. $addChoiceCount . ' Choice'. ($addCount == 1? '' : 's')
            ]
        ];
    }
}
