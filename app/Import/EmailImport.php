<?php

namespace App\Import;

use App\Email;
use Validator;

class EmailImport extends BaseImport
{
    public function getHeaders(){
        return ['name', 'subject', 'reply_to', 'from_name', 'from_email', 'action', 'view', 'attachment', 'body_text', 'disclaimer_text', 'header_image', 'footer_image'];
    }

    public function import(){
        $skipCount = 0;
        $addCount = 0;
        $warnings = [];
        $errors = [];

        foreach($this->data as $key => $obj){

            if($obj->name === null){
                $warnings[] = 'Skipped row '. ($key+1) . ': missing name field';
                $skipCount++;
                continue;
            }

            $data = [];
            foreach($obj as $k => $v){
                if($v !== null && $k !== 'id')$data[$k] = $v;
            }

            $validation = Validator::make($data, config('validation.emails.store'));

            if($validation->fails()){
                $errors[] = 'Validation failed on row '. ($key+1) .' (' . $validation->messages()->first() . ')';
                $skipCount++;
                continue;
            }

            $email = new Email($validation->getData());

            if($email->save()){
                $addCount++;
            } else {
                $errors[] = 'Unable to save email on row '. ($key+1);
                $skipCount++;
            }
        }

        return [
            'errors' => $errors,
            'warnings' => $warnings,
            'messages' => [
                $skipCount . ' Row'. ($skipCount == 1? '' : 's') .' skipped',
                $addCount . ' Email'. ($addCount == 1? '' : 's') .' added'
            ]
        ];
    }
}