<?php

namespace App\Import;

use App\Attending;
use App\User;
use App\Group;
use App\Setting;
use App\NodeJS\Command;
use Validator;

class UserImport extends BaseImport
{
    private $knownFields = [
        'firstname',
        'lastname',
        'token',
        'email',
        'phonenumber',
        'test_user',
        'groups'
    ];

    public function getHeaders(){
        return [];
    }

    private function getGroupsIds($groups = null){
        if(isset($groups) && strlen($groups) > 0){
            if(count($names = explode(',',$groups)) > 0){
                $groupIds = [];

                foreach($names as $name){
                    // if given group doesn't exist, create it!
                    if(!$dbGroup = Group::where('name', trim($name))->first()){
                        $dbGroup = new Group;
                        $dbGroup->name = trim($name);
                        $dbGroup->save();
                    }
                    // store the id in the array
                    $groupIds[] = $dbGroup->id;
                }

                return $groupIds;
            }
        }

        return [];
    }

    /**
     * array['errors'] array of error fields.
     * array['warnings'] array of warnings
     * array['status'] array of statuses
     *
     * @return array $arr
     */
    public function import()
    {
        $skipCount = 0;
        $updateCount = 0;
        $addCount = 0;
        $warnings = [];
        $errors = [];

        $settingAttachGroups = Setting::getValue('attach_groups');
        $settingOverwriteToken = Setting::getValue('overwrite_token');
        $settingAttachExtras = Setting::getValue('attach_extras');

        foreach($this->data as $key => $obj){
            $isUpdate = false;
            if(
                $obj->email == null &&
                $obj->lastname == null &&
                $obj->firstname == null &&
                $obj->phonenumber == null ){
                // skip, cause it misses identifier fields
                $warnings[] = 'Skipped row '. ($key+1) . ': no identifiers found';
                $skipCount++;
                continue;
            }

            //obj to arr and splitting default fields from extra fields
            $data = [];
            $extra = [];
            foreach($obj as $k => $v){
                if (in_array($k, $this->knownFields)) {
                    $data[$k] = $v;
                } else {
                    if($v !== null)$extra[$k] = $v;
                }
            }
            // test_user datatype fix
            $data['test_user'] = (isset($data['test_user']) && $data['test_user'] != 0)? true : false;

            // validate fields
            $validation = Validator::make($data, config('validation.users.import'));

            if($validation->fails()){
                $errors[] = 'Validation failed on row '. ($key+1) .' (' . $validation->messages()->first() . ')';
                continue;
            }

            // check for existing user
            if($obj->email !== null && $user = User::where('email',$obj->email)->first() ){
                $isUpdate = true;
            } else {
                // create new user
                $user = new User;
                $user->extra = [];
            }

            // get the groups
            $groupIds =  $this->getGroupsIds($data['groups']);
            unset($data['groups']);

            // set the token
            if(!(isset($user->token) && !$settingOverwriteToken)){
                $user->token = User::createToken($data['token'] ?? $user->token);
            }
            unset($data['token']);

            // set extra data
            if($settingAttachExtras){
                $user->extra = array_merge($user->extra, $extra);
            } else {
                $user->extra = $extra;
            }

            // set all other data
            $user->fill($data);

            // save the user!
            if($user->save()){

                // set the groups
                if($settingAttachGroups){
                    $user->groups()->syncWithoutDetaching($groupIds);
                } else {
                    $user->groups()->sync($groupIds);
                }

                 $attending = Attending::whereUserId( $user->id )->first();
                if ( $attending == null ) {
                    $attending = new Attending();
                    $attending->user_id = $user->id;
                    $attending->status = 'invited';

                    if ( $attending->save() ) {
                        $user = $user->fresh();
                    }
                }

                if($isUpdate) {
                    Command::make('user_update', $user->toArray());
                    $updateCount++;
                }
                else {
                    Command::make('user_add', $user->toArray());
                    $addCount ++;
                }

            } else {
                $errors[] = 'Failed saving row'. ($key+1);
                $skipCount++;
            }
        }

        return [
            'errors' => $errors,
            'warnings' => $warnings,
            'messages' => [
                $skipCount . ' Row'. ($skipCount == 1? '' : 's') .' skipped',
                $updateCount . ' User'. ($updateCount == 1? '' : 's') .' updated',
                $addCount . ' User'. ($addCount == 1? '' : 's') .' added'
            ]
        ];
    }
}
