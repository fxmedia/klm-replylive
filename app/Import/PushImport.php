<?php

namespace App\Import;

use App\Group;
use App\Push;
use Validator;

class PushImport extends BaseImport
{

    public function getHeaders(){
        return ['name', 'title', 'copy', 'groups', 'image', 'module'];
    }

    private function getGroupsIds($groups = null){
        if(isset($groups) && strlen($groups) > 0){
            if(count($names = explode(',',$groups)) > 0){
                $groupIds = [];

                foreach($names as $name){
                    // if given group doesn't exist, create it!
                    if(!$dbGroup = Group::where('name', trim($name))->first()){
                        $dbGroup = new Group;
                        $dbGroup->name = trim($name);
                        $dbGroup->save();
                    }
                    // store the id in the array
                    $groupIds[] = $dbGroup->id;
                }

                return $groupIds;
            }
        }

        return [];
    }

    public function import(){
        $skipCount = 0;
        $addCount = 0;
        $warnings = [];
        $errors = [];


        foreach($this->data as $key => $obj){
            if(
                $obj->name == null
            ) {
                // skip, cause it misses identifier fields
                $warnings[] = 'Skipped row '. ($key+1) . ': no name given';
                $skipCount++;
                continue;
            }

            $data = [];
            foreach($obj as $k => $v){
                if($v != null && $k !== 'id')$data[$k] = $v;
            }

            $validation = Validator::make($data,config('validation.pushes.store'));

            if($validation->fails()){
                $errors[] = 'Validation failed on row '. ($key+1) .' (' . $validation->messages()->first() . ')';
                $skipCount++;
                continue;
            }

            $poll = new Push($validation->getData());


            if($poll->save()){

                $groupIds = $this->getGroupsIds($obj['groups']);
                $poll->groups()->sync($groupIds);

                $addCount++;
            }
        }

        return [
            'errors' => $errors,
            'warnings' => $warnings,
            'messages' => [
                $skipCount . ' Row'. ($skipCount == 1? '' : 's') .' skipped',
                $addCount . ' Push'. ($addCount == 1? '' : 'es') .' added'
            ]
        ];
    }
}