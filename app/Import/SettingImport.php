<?php

namespace App\Import;

use App\Setting;
use App\SettingGroup;
use Validator;
use DB;

class SettingImport extends BaseImport
{

    public function getHeaders()
    {
        return ['name', 'key', 'value'];
    }

    /**
     * array['errors'] array of error fields.
     * array['warnings'] array of warnings
     * array['status'] array of statuses
     *
     * @return array $arr
     */
    public function import()
    {
        $skipCount = 0;
        $addCount = 0;
        $updateCount = 0;
        $warnings = [];
        $errors = [];

        foreach($this->data as $key => $setting){
            $existingSetting = Setting::where('key','=',$setting->key)->first();

            //obj to arr
            $data = [];
            foreach($setting as $k => $v){
                $data[$k] = $v;
            }

            if($existingSetting !== null){
                /// update this one

                $validation = Validator::make($data, [
                    'value' => config('validation.settings.value')
                ]);

                if($validation->fails()){
                    $errors[] = 'Update setting '. $setting->key .' failed due to invalid value: '. $setting->value;
                    continue;
                }

                $existingSetting->value = $data['value'];

                if($existingSetting->save()){
                    $updateCount++;
                    continue;
                }

                $errors[] = 'Failed saving change for '. $setting->key ;
                continue;
            }

            // fill empty spots
            if($data['placeholder'] == null) $data['placeholder'] = '';
            if($data['validation'] == null) $data['validation'] = config('validation.settingDefaultValidation');
            if($data['description'] == null) $data['description'] = '';

            // no existing, so make new
            $validation = Validator::make($data, config('validation.settings'));
            if($validation->fails()){

                $errors[] = 'Validation failed at row '. ($key+1);
                $skipCount++;
                continue;
            }

            // first find setting group by name
            $sGroup = SettingGroup::where('name','=',$setting->settinggroup)->first();

            $group_id = 2; // --> default group_id = advanced settings
            if($sGroup !== null)$group_id = $sGroup->id;


            $data = $validation->getData();
            $data['setting_group_id'] = $group_id;

            unset($data['settinggroup']);

            /**
             * TODO: fix commented. the db::table insert is a workaround for now...
             */
//            $s = Setting::create($data); ///////// shit crashes here.....
//            //dd($data);
//            $data['setting_group_id'] = $group_id;
//
//            if(!$s->save()){
//                // fail!
//                $errors[] = 'Failed saving row'. ($key+1) .' for unknown reason'. $data['key'];
//                continue;
//            }
            // for some reason abofe sdoesnt work... below does work...

            if(!DB::table('settings')->insert($data)){
                // fail!
                $errors[] = 'Failed saving row'. ($key+1) .' for unknown reason'. $data['key'];
                continue;
            }
            $addCount++;


        }

        return [
            'errors' => $errors,
            'warnings' => $warnings,
            'messages' => [
                $skipCount . ' Row'. ($skipCount == 1? '' : 's') .' skipped',
                $updateCount . ' Setting'. ($updateCount == 1? '' : 's') .' updated',
                $addCount . ' Setting'. ($addCount == 1? '' : 's') .' added'
            ]
        ];
    }
}