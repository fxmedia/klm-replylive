<?php

namespace App;

use Illuminate\Database\Eloquent\Model;
use Symfony\Component\Finder\Finder;

/**
 * App\QuizQuestion
 *
 * @property-read \Illuminate\Database\Eloquent\Collection|\App\QuizAnswer[] $answers
 * @property-read \App\Quiz $quiz
 * @mixin \Eloquent
 * @property int $id
 * @property int $quiz_id
 * @property string $title
 * @property string $correct_answer
 * @property int $points
 * @property array $choices
 * @property \Carbon\Carbon|null $created_at
 * @property \Carbon\Carbon|null $updated_at
 * @method static \Illuminate\Database\Eloquent\Builder|\App\QuizQuestion whereChoices($value)
 * @method static \Illuminate\Database\Eloquent\Builder|\App\QuizQuestion whereCorrectAnswer($value)
 * @method static \Illuminate\Database\Eloquent\Builder|\App\QuizQuestion whereCreatedAt($value)
 * @method static \Illuminate\Database\Eloquent\Builder|\App\QuizQuestion whereId($value)
 * @method static \Illuminate\Database\Eloquent\Builder|\App\QuizQuestion wherePoints($value)
 * @method static \Illuminate\Database\Eloquent\Builder|\App\QuizQuestion whereQuizId($value)
 * @method static \Illuminate\Database\Eloquent\Builder|\App\QuizQuestion whereTitle($value)
 * @method static \Illuminate\Database\Eloquent\Builder|\App\QuizQuestion whereUpdatedAt($value)
 * @property int $order
 * @method static \Illuminate\Database\Eloquent\Builder|\App\QuizQuestion whereOrder($value)
 * @property string|null $image
 * @property-read mixed $image_path
 * @method static \Illuminate\Database\Eloquent\Builder|\App\QuizQuestion whereImage($value)
 */
class QuizQuestion extends Model
{
    protected $fillable = ["quiz_id", "title", "correct_answer", "points", "image", "choices","order"];
    protected $appends = ["image_path"];
    protected $table = "quiz_questions";
    protected $casts = [
        'choices' => 'array'
    ];

    /**
     * @return null|string
     */
    public function getImagePathAttribute(){
        if(!isset($this->image)) return null;

        $token = Setting::getValue('media_token');
        return route('frontend.media.image', ['token'=>$token, 'group'=>'quiz', 'file'=>$this->image]);
    }

    /**
     * @return \Illuminate\Database\Eloquent\Relations\HasMany
     */
    public function answers() {
        return $this->hasMany('App\QuizAnswer');
    }

    /**
     * @return \Illuminate\Database\Eloquent\Relations\BelongsTo
     */
    public function quiz() {
        return $this->belongsTo('App\Quiz');
    }

    /**
     * @return array Array of existing images for Quiz Questions
     */
    public static function getImageList(){
        $images = [];
        $images[''] = '';
        $path = storage_path('uploads/'. config('storage.quiz') .'/');

        if(is_dir($path)){
            $finder = new Finder();
            $dir = $finder->in($path);

            foreach($dir as $file){
                if($file->isFile()){
                    $images[$file->getBasename()] = $file->getBasename();
                }
            }
        }

        return $images;
    }
}
