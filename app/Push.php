<?php

namespace App;

use Illuminate\Database\Eloquent\Model;
use Symfony\Component\Finder\Finder;

/**
 * App\Push
 *
 * @property int $id
 * @property string $name
 * @property string|null $title
 * @property string|null $copy
 * @property string|null $image
 * @property string|null $module
 * @property \Carbon\Carbon|null $created_at
 * @property \Carbon\Carbon|null $updated_at
 * @property-read \Illuminate\Database\Eloquent\Collection|\App\Group[] $groups
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Push whereCopy($value)
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Push whereCreatedAt($value)
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Push whereId($value)
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Push whereImage($value)
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Push whereModule($value)
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Push whereName($value)
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Push whereTitle($value)
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Push whereUpdatedAt($value)
 * @mixin \Eloquent
 * @property-read mixed $image_path
 */
class Push extends Model
{
    //
    protected $table = 'pushes';

    protected $fillable = ['name', 'title', 'copy', 'image', 'module'];
    protected $appends = ['image_path'];

    public function getImagePathAttribute(){
        if(!isset($this->image)) return null;

        $token = Setting::getValue('media_token');
        return route('frontend.media.image', ['token' => $token, 'group' => 'push', 'file' =>$this->image]);
    }

    /**
     * @return \Illuminate\Database\Eloquent\Relations\BelongsToMany
     */
    public function groups(){
        return $this->belongsToMany('App\Group');
    }

    /**
     * isInGroup
     *
     * @param group $group
     * @return bool true if user is attached to given group
     */
    public function isInGroup(Group $group){
        $groups = [];
        foreach ($this->groups as $userGroup) {
            $groups[] = $userGroup->id;
        }

        return in_array($group->id, $groups);
    }

    /**
     * @return array Array of existing images for Push modules
     */
    public static function getImageList(){
        $images = [];
        $images[''] = '';
        $path = storage_path('uploads/'. config('storage.push') .'/');

        if(is_dir($path)){
            $finder = new Finder();
            $dir = $finder->in($path);

            foreach($dir as $file){
                if($file->isFile()){
                    $images[$file->getBasename()] = $file->getBasename();
                }
            }
        }

        return $images;
    }

    /**
     * @return array Module Templates
     */
    public static function getLayoutTemplates(){
        $list = [
            ''=>''
        ];

        return $list;
    }
}
