<?php

namespace App\Export;

class SettingExport extends BaseExport
{
    private $filename;
    private $headers = ['settingGroup', 'name', 'key', 'value','type', 'placeholder', 'validation', 'description'];
    private $rows;

    public function __construct($settings)
    {
        $this->filename = 'export-settings';
        foreach($settings as $setting){
            $row = [];

            foreach($this->headers as $key){
                if($key === 'settingGroup'){
                    $row[] = isset($setting[$key])? $setting[$key]->name : '';
                } else {
                    $row[] = isset($setting[$key])? $setting[$key] : '';
                }
            }

            $this->rows[] = $row;
        }

    }

    public function getFileName()
    {
        return $this->filename;
    }

    public function getHeaderRow()
    {
        return $this->headers;
    }

    public function getRow()
    {
        return $this->rows[$this->index];
    }

    public function hasNextRow()
    {
        return array_has($this->rows, ($this->index));
    }
}