<?php

namespace App\Export;

class LearningCurveExport extends BaseExport
{
    private $filename;
    private $headers = ['id', 'learning_curve', 'tags', 'groups', 'question' , 'question_id', 'order', 'correct_answer' ,'choice', 'value'];
    private $rows;

    public function __construct($learning_curves, $filename = 'all')
    {
        $this->filename = 'export-learning_curves-'.$filename;


        foreach($learning_curves as $learning_curve){
            $groups = '';
            foreach($learning_curve->groups as $group){
                $groups .= $group->name.', ';
            }
            if(strlen($groups) > 2) {
                $groups = substr($groups, 0, -2);
            }
            $this->rows[] = [
                $learning_curve->id,
                $learning_curve->name,
                $learning_curve->tags,
                $groups
            ];

            foreach($learning_curve->questions as $question){
                $this->rows[] = [
                    $learning_curve->id,
                    '', // learning_curve_name
                    '', // learning_curve_tags
                    '', // learning_curve_groups
                    $question->title,
                    $question->id,
                    $question->order,
                    $question->correct_answer
                ];

                foreach($question->choices as $c){
                    $this->rows[] = [
                        '', // learning_curve_id
                        '', // learning_curve_name
                        '', // learning_curve_tags
                        '', // learning_curve_groups
                        '', // question_title
                        $question->id,
                        '', // question_order
                        '', // question_correct_answer
                        $c['choice'],
                        $c['value']
                    ];
                }
            }
        }
    }

    public function getFileName()
    {
        return $this->filename;
    }

    public function getHeaderRow()
    {
        return $this->headers;
    }

    public function getRow()
    {
        return $this->rows[$this->index];
    }

    public function hasNextRow()
    {
        return array_has($this->rows, ($this->index));
    }
}
