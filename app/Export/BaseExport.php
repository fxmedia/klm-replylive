<?php

namespace App\Export;

use Maatwebsite\Excel\Facades\Excel;

abstract class BaseExport implements ExportInterface
{
    protected $index = 0;

    public function incrementRow()
    {
        return ++$this->index;
    }

    function export()
    {
        return Excel::create($this->getFilename(), function($excel){
            $excel->sheet('sheet1', function($sheet){
                $sheet->row(1, $this->getHeaderRow());

                $row = 3;
                $this->getStyling($sheet);

                do{
                    $sheet->row($row++, $this->getRow());
                    $this->incrementRow();
                } while ($this->hasNextRow());
            });
        })->export('xls');
    }

    public function getStyling($sheet)
    {
        return null;
    }
}