<?php

namespace App\Export;

class QuizExport extends BaseExport
{
    private $filename;
    private $headers = ['id', 'quiz', 'tags', 'groups', 'question' , 'question_id','points', 'order', 'correct_answer' ,'choice', 'value'];
    private $rows;

    public function __construct($quizzes, $filename = 'all')
    {
        $this->filename = 'export-quizzes-'.$filename;


        foreach($quizzes as $quiz){
            $groups = '';
            foreach($quiz->groups as $group){
                $groups .= $group->name.', ';
            }
            if(strlen($groups) > 2) {
                $groups = substr($groups, 0, -2);
            }
            $this->rows[] = [
                $quiz->id,
                $quiz->name,
                $quiz->tags,
                $groups
            ];

            foreach($quiz->questions as $question){
                $this->rows[] = [
                    $quiz->id,
                    '', // quiz_name
                    '', // quiz_tags
                    '', // quiz_groups
                    $question->title,
                    $question->id,
                    $question->points,
                    $question->order,
                    $question->correct_answer
                ];

                foreach($question->choices as $c){
                    $this->rows[] = [
                        '', // quiz_id
                        '', // quiz_name
                        '', // quiz_tags
                        '', // quiz_groups
                        '', // question_title
                        $question->id,
                        '', // question_points
                        '', // question_order
                        '', // question_correct_answer
                        $c['choice'],
                        $c['value']
                    ];
                }
            }
        }
    }

    public function getFileName()
    {
        return $this->filename;
    }

    public function getHeaderRow()
    {
        return $this->headers;
    }

    public function getRow()
    {
        return $this->rows[$this->index];
    }

    public function hasNextRow()
    {
        return array_has($this->rows, ($this->index));
    }
}