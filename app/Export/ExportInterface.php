<?php

namespace App\Export;

interface ExportInterface
{
    public function getFilename();

    public function getHeaderRow();

    public function getRow();

    public function hasNextRow();

    public function export();

    public function getStyling($sheet);
}