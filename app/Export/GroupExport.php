<?php

namespace App\Export;

class GroupExport extends BaseExport
{
    private $filename;
    private $headers = ['name', 'limit', 'created'];
    private $rows;

    public function __construct($groups, $fileName)
    {
        $this->filename = 'export-groups-'.$fileName;
        foreach($groups as $group){
            $row = [];

            foreach($this->headers as $key){
                $row[] = isset($group[$key])? $group[$key] : '';
            }

            $this->rows[] = $row;
        }
    }

    public function getFileName()
    {
        return $this->filename;
    }

    public function getHeaderRow()
    {
        return $this->headers;
    }

    public function getRow()
    {
        return $this->rows[$this->index];
    }

    public function hasNextRow()
    {
        return array_has($this->rows, ($this->index));
    }
}