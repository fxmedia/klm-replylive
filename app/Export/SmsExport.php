<?php

namespace App\Export;

class SmsExport extends BaseExport
{
    private $filename;
    private $headers = ['name', 'subject', 'action', 'body_text'];
    private $rows;

    public function __construct($sms, $filename = 'all')
    {
        $this->filename = 'export-sms-'.$filename;


        foreach($sms as $sm){
            $this->rows[] = [
                $sm->name,
                $sm->subject,
                $sm->action,
                $sm->body_text
            ];
        }
    }

    public function getFileName()
    {
        return $this->filename;
    }

    public function getHeaderRow()
    {
        return $this->headers;
    }

    public function getRow()
    {
        return $this->rows[$this->index];
    }

    public function hasNextRow()
    {
        return array_has($this->rows, ($this->index));
    }
}