<?php

namespace App\Export;

class EmailExport extends BaseExport
{
    private $filename;
    private $headers = ['name', 'subject', 'reply_to', 'from_name', 'from_email', 'action', 'view', 'attachment', 'body_text', 'disclaimer_text', 'header_image', 'footer_image'];
    private $rows;

    public function __construct($emails, $filename = 'all')
    {
        $this->filename = 'export-emails-'.$filename;


        foreach($emails as $email){
            $this->rows[] = [
                $email->name,
                $email->subject,
                $email->reply_to,
                $email->from_name,
                $email->from_email,
                $email->action,
                $email->view,
                $email->attachment,
                $email->body_text,
                $email->disclaimer_text,
                $email->header_image,
                $email->footer_image
            ];
        }
    }

    public function getFileName()
    {
        return $this->filename;
    }

    public function getHeaderRow()
    {
        return $this->headers;
    }

    public function getRow()
    {
        return $this->rows[$this->index];
    }

    public function hasNextRow()
    {
        return array_has($this->rows, ($this->index));
    }
}