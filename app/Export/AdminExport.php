<?php

namespace App\Export;

class AdminExport extends BaseExport
{
    private $filename;
    private $headers = ['name', 'email', 'notification_subscription'];
    private $rows;

    public function __construct($admins)
    {
        $this->filename = 'export-admins';


        foreach($admins as $admin){
            $this->rows[] = [
                $admin->name,
                $admin->email,
                $admin->notification_subscription
            ];
        }
    }

    public function getFileName()
    {
        return $this->filename;
    }

    public function getHeaderRow()
    {
        return $this->headers;
    }

    public function getRow()
    {
        return $this->rows[$this->index];
    }

    public function hasNextRow()
    {
        return array_has($this->rows, ($this->index));
    }
}