<?php

namespace App\Report;

use App\User;
use Maatwebsite\Excel\Facades\Excel;

class SurveyResultReport extends BaseReport{

    private $surveys;

    public function __construct($surveys)
    {
        $this->surveys = $surveys;
    }

    public function getFilename()
    {
        return 'survey_report_' . date('Y_m_d_h_m_s');
    }

    public function report(){
        Excel::create($this->getFilename(), function($excel){

            foreach($this->surveys as $survey){

                $name = substr(trim(preg_replace('/[\*|\:|\\|\/|\?|\[|\]]/', '', $survey->name)), 0, 31);
                $excel->sheet($name, function($sheet) use ($survey){
                    $index = 1;

                    $sheet->row($index, ['Survey Name: ' . $survey->name ]);
                    $sheet->row($index, function ($row) {
                        $row->setFontSize(18);
                        $row->setFontWeight('bold');
                    });
                    $index++;

                    foreach($survey->questions as $question) {
                        $sheet->row($index, ['Question: ' . $question->title ]);

                        $sheet->row($index, function ($row) {
                            $row->setFontSize(14);
                            $row->setFontWeight('bold');
                        });
                        $index++;

                        $sheet->row($index, ['', 'User', 'Input']);

                        $index++;

                        foreach($question->answers as $a){
                            $answer = json_decode($a);
                            $user = User::find($answer->user_id);

                            $name = $user->firstname . " " . $user->lastname;
                            $sheet->row($index,[ '', $name, $answer->input ]);
                            $index++;
                        }
                        $index++;
                    }
                });
            }

        })->export('xls');
    }
}
