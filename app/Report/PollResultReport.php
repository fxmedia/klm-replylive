<?php

namespace App\Report;

use Maatwebsite\Excel\Facades\Excel;

class PollResultReport extends BaseReport{

    private $polls;

    public function __construct($polls)
    {
        $this->polls = $polls;
    }

    public function getFilename()
    {
        return 'poll_report_' . date('Y_m_d_h_m_s');
    }

    public function report(){
        Excel::create($this->getFilename(), function($excel){

            foreach($this->polls as $poll){

                $name = substr(trim(preg_replace('/[\*|\:|\\|\/|\?|\[|\]]/', '', $poll->question)), 0, 31);

                $excel->sheet($name, function($sheet) use ($poll){
                    $index = 1;

                    $sheet->row($index, ['Question:', $poll->question, ]);

                    $sheet->row($index, function ($row) {
                        $row->setFontSize(14);
                        $row->setFontWeight('bold');
                    });
                    $index++;

                    $sheet->row($index, ['Total Answers', count($poll->answers)]);

                    $index+=2;

                    $sheet->row($index, ['', 'Choice', 'Answers', 'percentage']);

                    $index++;

                    foreach($poll->choices as $c){
                        $v = $poll->result[ $c['choice'] ] ?? 0;
                        $p = (count($poll->answers) > 0)? round($v / count($poll->answers), 2) : 0;
                        $sheet->row($index, [$c['choice'], $c['value'],$v, $p ]);
                        $index++;
                    }
                });
            }

        })->export('xls');
    }


}
