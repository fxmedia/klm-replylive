<?php

namespace App\Report;


interface ReportInterface
{
    public function getFilename();

    public function report();
}