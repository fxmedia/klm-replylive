<?php

namespace App\Http\Controllers\Backend;

use App\Export\PushExport;
use App\Helpers\ImageHelper;
use App\Import\PushImport;
use App\NodeJS\Command;
use App\Push;
use App\Group;
use App\Setting;
use Chumper\Zipper\Zipper;
use Illuminate\Http\Request;
use App\Http\Controllers\Controller;
use Exception;
use Session;

class PushesController extends Controller
{
    //
    /**
     * @return \Illuminate\Contracts\View\Factory|\Illuminate\View\View
     */
    public function index(){
        $pushes = Push::with('groups')->get();

        try{
            $status = json_decode( Command::status('push_status'), true);

            foreach($pushes as $push){
                if($status['push_id'] && $status['push_id'] == $push->id){
                    $push->isActive = true;
                }
            }
        } catch(Exception $e){}

        return view('backend.pushes.index', compact('pushes'));
    }

    /**
     * @return \Illuminate\Contracts\View\Factory|\Illuminate\View\View
     */
    public function create(){
        $groups = Group::all()->sortBy('name', SORT_NATURAL | SORT_FLAG_CASE);
        $images = Push::getImageList();
        $modules = Push::getLayoutTemplates();

        return view('backend.pushes.create', compact('groups', 'images', 'modules'));
    }

    /**
     * @param Request $request
     * @return $this|\Illuminate\Http\RedirectResponse
     */
    public function store(Request $request){
        $validators = config('validation.pushes.store');

        $data = $request->validate($validators);

        $push = new Push($data);

        $this->handleUploads($request, $push);

        if(!$push->save()){
            return redirect()->back()->withErrors(['message' => 'Error saving new push']);
        }

        $groups = [];
        if($request->input('groups')){
            $groups = (array)$request->input('groups');
        }
        $push->groups()->sync($groups);

        return redirect()->route('backend.pushes.index')->with(['message' => 'Successfully created new push']);
    }

    /**
     * @param $id
     * @return $this|\Illuminate\Contracts\View\Factory|\Illuminate\View\View
     */
    public function edit($id){
        $push = Push::find($id);

        if($push !== null){
            $groups = Group::all()->sortBy('name', SORT_NATURAL | SORT_FLAG_CASE);
            $images = Push::getImageList();
            $modules = Push::getLayoutTemplates();

            return view('backend.pushes.edit', compact('push','groups', 'images','modules'));
        }

        return redirect()->back()->withErrors(['message' => 'Unable to find selected push']);
    }

    /**
     * @param Request $request
     * @param $id
     * @return $this|\Illuminate\Http\RedirectResponse
     */
    public function update(Request $request, $id){
        $push = Push::find($id);

        if($push === null){
            redirect()->back()->withErrors(['message' => 'Unable to update non existing push']);
        }

        $validators = config('validation.pushes.store');

        $data = $request->validate($validators);

        $push->fill($data);
        $this->handleUploads($request, $push);

        if($push->save()){

            $groups = [];
            if($request->input('groups')){
                $groups = (array)$request->input('groups');
            }
            $push->groups()->sync($groups);

            return redirect()->route('backend.pushes.index')->with(['message' => 'Successfully updated push']);
        }


        return redirect()->back()->withErrors(['message' => 'Error updating push']);
    }

    /**
     * @param $id
     * @return $this|\Illuminate\Http\RedirectResponse
     */
    public function remove($id){
        $push = Push::find($id);

        if($push === null){
            return redirect()->route('backend.pushes.index')->withErrors(['message' => 'Unable to remove non existing push']);
        }

        /**
         * check first maybe if current poll is active? and deactivate it
         */

        try{
            $removed = $push->delete();
        } catch(Exception $e){
            $removed = false;
        }

        if($removed) return redirect()->back()->with('message', 'Push has been removed');

        return redirect()->back()->withErrors(['message'=>'Error removing push.']);
    }

    /**
     * @return \Illuminate\Http\RedirectResponse
     */
    public function export(){
        $pushes = Push::all();
        $count = count($pushes);

        if($count > 0){
            $export = new PushExport($pushes, 'all');
            $export->export();

            return redirect()->back()->with(['message' => 'Exporting '. $count .' Pushes']);
        }

        return redirect()->back()->with(['message' => 'No pushes to export']);
    }

    /**
     * @return $this|\Symfony\Component\HttpFoundation\BinaryFileResponse
     */
    public function exportImages()
    {
        $filename = 'pushes-image-export-' . time() . '.zip';
        $zipper = new Zipper;
        try {
            $zipper->make(storage_path('compressed/' . $filename));
        } catch (Exception $e) {
            return redirect()->back()->withErrors($e->getMessage());
        }

        $zipper->add(storage_path('uploads/' . config('storage.push')))->close();


        return response()->download(storage_path('compressed/' . $filename));
    }

    /**
     * @param Request $request
     * @return $this|\Illuminate\Http\RedirectResponse
     */
    public function import(Request $request){
        if ($request->hasFile('push_upload') && $request->file('push_upload')->isValid()) {

            $import = new PushImport($request->file('push_upload'));

            if ($import->isValid()) {

                $result = $import->import();

                Session::flash('status', $result);

                return redirect()->back();
            } else {
                return redirect()->back()->withErrors($import->getErrors());
            }
        }

        return redirect()->back()->withErrors(['no_file' => 'No or wrong file uploaded']);
    }


    /**
     * @param Request $request
     * @param Push $push
     */
    private function handleUploads(Request $request, Push $push){
        foreach(['image_upload'] as $name){
            if($request->hasFile($name) && $request->file($name)->isValid()){
                $file = $request->file($name);
                $name = str_replace('_upload', '', $name);

                $imageHelper = new ImageHelper($file, 'push');

                if( $request->input('resize_'. $name) !== null){
                    $imageHelper->convert(false)
                        ->resizeWallUpload( Setting::getValue('upload_max_size') )
                        ->moveToStorage();
                } else {
                    $imageHelper->convert(false)
                        ->moveToStorage();
                }

                $push->{$name} = $imageHelper->getFilename();
            }
        }
    }
}
