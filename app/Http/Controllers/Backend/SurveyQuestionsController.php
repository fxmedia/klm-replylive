<?php

namespace App\Http\Controllers\Backend;

use App\Survey;
use App\SurveyQuestion;
use Exception;
use Validator;
use Illuminate\Http\Request;
use App\Http\Controllers\Controller;

class SurveyQuestionsController extends Controller
{
    //
    public function create($survey_id){
        $survey = Survey::find($survey_id);
        if($survey == null){
            // cannot create question to non existing survey
            return redirect()->back()->withErrors('Cannot create Question to non existing survey');
        }

        return view('backend.surveys.question.create', compact('survey'));
    }

    public function store(Request $request, $survey_id){
        if(!Survey::whereId($survey_id)->exists()){
            return redirect()->back()->withErrors(['message' => 'Error storing question to non existing survey']);
        }

        $typeValidation = Validator::make(['type'=> $request->input('type')], ['type' => config('validation.surveys.import.question.type')]);
        if($typeValidation->fails()){
            return redirect()->back()->withErrors($typeValidation->errors());
        }

        $validator = config('validation.surveys.questions.'.$request->input('type'));

        $data = $request->validate($validator);

        if($data['type'] === 'abcd' || $data['type'] === 'abcd-multiple'){
            $choices = [];
            foreach($data['data'] as $c){
                if($c['value'] !== null) {
                    $choices[] = [
                        'choice' => $c['choice'],
                        'value' => $c['value']
                    ];
                }
            }
            $data['data'] = $choices;
        }
        if( !isset($data['data']) ) $data['data'] = [];

        $question = new SurveyQuestion($data);
        $question->survey_id = $survey_id;

        if(!$question->save()){
            return redirect()->back()->witherrors(['message'=>'Error saving new question']);
        }

        return redirect()->route('backend.surveys.edit',[$survey_id])->with(['message' => "Successfully added a new question"]);
    }

    public function edit($id){
        $question = SurveyQuestion::find($id);

        if($question == null){
            return redirect()->back()->withErrors('Unable to find Survey Question to edit');
        }

        $survey = $question->survey;

        return view('backend.surveys.question.edit', compact('question', 'survey'));

    }

    public function update(Request $request, $id){
        $question = SurveyQuestion::find($id);

        if($question == null){
            return redirect()->back()->withErrors('No Survey Question found to edit');
        }

        $typeValidation = Validator::make(['type'=> $request->input('type')], ['type' => config('validation.surveys.import.question.type')]);
        if($typeValidation->fails()){
            return redirect()->back()->withErrors($typeValidation->errors());
        }

        $validator = config('validation.surveys.questions.'.$request->input('type'));

        $data = $request->validate($validator);

        if($data['type'] === 'abcd' || $data['type'] === 'abcd-multiple'){
            $choices = [];
            foreach($data['data'] as $c){
                if($c['value'] !== null) {
                    $choices[] = [
                        'choice' => $c['choice'],
                        'value' => $c['value']
                    ];
                }
            }
            $data['data'] = $choices;
        }
        if( !isset($data['data']) ) $data['data'] = [];

        if($question->update($data)){
            return redirect()->route('backend.surveys.edit',[$question->survey_id])->with(['message' => 'Successfully updated question']);
        }

        return redirect()->back()->withErrors(['message' => 'Unable to save changes to survey question']);
    }

    public function remove($id){
        $question = SurveyQuestion::find($id);

        if($question === null){
            return redirect()->back()->withErrors(['message' => 'Unable to remove non existing question']);
        }


        try{
            $removed = $question->delete();
        } catch(Exception $e){
            $removed = false;
        }

        if($removed) return redirect()->back()->with('message', 'Question has been removed');

        return redirect()->back()->withErrors(['message'=>'Error removing question.']);
    }

    public function reset($id){
        $question = SurveyQuestion::find($id);


        if($question === null){
            return redirect()->back()->withErrors(['message'=>'Unable to reset answers to unknown question']);
        }

        $totalRemoved = 0;
        $totalFailed = 0;

        foreach ($question->answers as $answer) {
            try {
                $s = $answer->delete();
                if ($s == true) $totalRemoved++;
            } catch (Exception $e) {
                $totalFailed++;
            }
        }
        return redirect()->back()->with('message', 'Removed '.$totalRemoved. ' answers and skipped '. $totalFailed .' from question '. $question->id);
    }
}
