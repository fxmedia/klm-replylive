<?php

namespace App\Http\Controllers\Backend;

use App\Export\PostExport;
use App\Http\Controllers\Controller;
use App\NodeJS\Command;
use Chumper\Zipper\Zipper;
use Illuminate\Http\Request;
use App\Post;
use Exception;

class PostController extends Controller
{
    public function index(){
        $posts = Post::with('user')->orderBy('id','desc')->get();
        $commentAmount = 0;
        $questionAmount = 0;
        $showDisplayPosts = 0;
        foreach($posts as $post){
            if($post->type === 'comment')$commentAmount++;
            if($post->type === 'question')$questionAmount++;
        }

        try{
            $highlightStatus = json_decode( Command::status('post_highlight_status'), true);
            $showDisplayStatus = json_decode( Command::status('display_page_status'), true);

            $showDisplayPosts = ($showDisplayStatus['page'] == "post") ? 1 : 0;

            foreach($posts as $post){
                if($highlightStatus['post_id'] && $highlightStatus['post_id'] == $post->id){
                    $post->isHighlight = true;
                }
            }
        } catch(Exception $e){}

        return view('backend.posts.index', compact('posts', 'showDisplayPosts', 'commentAmount', 'questionAmount'));
    }

    public function toggleVisibility($id){
        $post = Post::find($id);
        if($post === null){
            return redirect()->back()->withErrors(['message' => 'Unable to find selected post']);
        }

        $post->visible = !$post->visible;

        if(!$post->save()){
            return redirect()->back()->withErrors(['message' => 'Unable to save change to post']);
        }

        Command::make('post_toggle_visibility', [
            'post_ids' => [$post->id],
            'visible' => $post->visible
        ]);

        return redirect()->back();
    }

    public function remove($id){
        $post = Post::find($id);

        if($post === null){
            return redirect()->back()->withErrors(['message' => 'Unable to find selected post']);
        }
        if($post->delete()){

            // Command::make([
            //     'action' => 'post_remove',
            //     'module' => 'post',
            //     'data' => [
            //         'post_id' => $post->id
            //     ]
            // ]);

            return redirect()->back()->with('message', 'Post has been removed');
        }
        return redirect()->back()->withErrors(['message' => 'Error removing post']);
    }

    public function perform(Request $request){
        $message = '';
        $count = 0;
        $postIds = $request->input('post');
        if($postIds && is_array($postIds)){
            $posts = Post::find($postIds);

            switch($request->input('perform')){
                case 'export':
                $message = 'Perform export ';

                /**
                * TODO: like below, shouldn't this be a "Report" ?
                */
                $export = new PostExport($posts, 'selection');
                $export->export();

                break;
                case 'set_visible':
                $message = 'Set visible ';
                foreach($posts as $post){
                    $post->visible = 1;

                    if($post->save()){
                        $count++;
                    }
                }
                Command::make('post_toggle_visibility', [
                    'post_ids' => $postIds,
                    'visible' => 1
                ]);
                break;
                case 'set_invisible':
                $message = 'Set invisible ';
                foreach($posts as $post){
                    $post->visible = 0;

                    if($post->save()){
                        $count++;
                    }
                }
                Command::make('post_toggle_visibility', [
                    'post_ids' => $postIds,
                    'visible' => 0
                ]);
                break;
                case 'toggle_visibility':
                $message = 'Toggle Visibility ';
                foreach($posts as $post){
                    $post->visible = !$post->visible;

                    if($post->save()){
                        $count++;
                    }
                }
                break;
                case 'delete':
                $message = 'Deleted ';
                foreach($posts as $post){
                    if($post->delete()){
                        $count++;
                    }
                }
                if($count > 0){
                    Command::make('post_remove', ['post_ids' => $postIds]);
                }

                break;
            }
        }

        return redirect()->back()->with('message', $message . $count . ' posts');
    }

    /**
    * TODO: make export, (import?), export images as zip
    * Acutally --> all this should be "Report" instead of export?
    */

    public function exportAll(){
        $posts = Post::all();
        $count = count($posts);

        if($count === 0){
            return redirect()->back()->with('message', 'No posts to export');
        }

        $export = new PostExport($posts, 'all');
        $export->export();

        return redirect()->back()->with('message', 'Exporting '.$count.' posts');
    }

    public function exportComments(){
        $posts = Post::ofType('comment')->get();
        $count = count($posts);

        if($count === 0){
            return redirect()->back()->with('message', 'No posts of type "comment" to export');
        }

        $export = new PostExport($posts, 'comments');
        $export->export();

        return redirect()->back()->with('message', 'Exporting '.$count.' posts');
    }

    public function exportQuestions(){
        $posts = Post::ofType('question')->get();
        $count = count($posts);

        if($count === 0){
            return redirect()->back()->with('message', 'No posts of type "question" to export');
        }

        $export = new PostExport($posts, 'questions');
        $export->export();

        return redirect()->back()->with('message', 'Exporting '.$count.' posts');
    }

    public function exportImages(){
        $filename = 'posts-image-export-'. time() .'.zip';
        $zipper = new Zipper;
        try{
            $zipper->make(storage_path('compressed/'.$filename));
        } catch(Exception $e){
            return redirect()->back()->withErrors($e->getMessage());
        }

        $zipper->add(storage_path('uploads/'. config('storage.post') ) )->close();


        return response()->download(storage_path('compressed/'.$filename));
    }
}
