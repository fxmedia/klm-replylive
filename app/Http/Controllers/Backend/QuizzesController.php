<?php

namespace App\Http\Controllers\Backend;

use App\Export\QuizExport;
use App\Import\QuizImport;
use App\NodeJS\Command;
use App\Report\QuizResultReport;
use App\Quiz;
use App\Group;
use App\QuizAnswer;
use App\QuizQuestion;
use Chumper\Zipper\Zipper;
use Exception;
use Session;
use Illuminate\Http\Request;
use App\Http\Controllers\Controller;

class QuizzesController extends Controller
{
    //
    public function index(){
        $quizzes = Quiz::with('questions','groups')->get();
        $totalAnswers = QuizAnswer::count();
        $amountQuestions = QuizQuestion::count();

        try {
            $status = json_decode( Command::status('quiz_status') ,true);

            foreach($quizzes as $quiz){
                if($status['quiz_id'] && $status['quiz_id'] == $quiz->id){
                    $quiz->isActive = true;
                    $quiz->activeData = $status;
                }
            }

        } catch(Exception $e){}


        return view('backend.quizzes.index', compact('quizzes', 'totalAnswers', 'amountQuestions'));
    }


    public function show($id){
        $quiz = Quiz::find($id);

        if($quiz === null){
            return redirect()->back()->withErrors(['message' => 'Unable to show non existing Quiz']);
        }

        return view('backend.quizzes.show', compact('quiz'));
    }

    public function create(){
        $groups = Group::all()->sortBy('name', SORT_NATURAL | SORT_FLAG_CASE);

        return view('backend.quizzes.create', compact('groups'));
    }

    public function store(Request $request){
        $validators = config('validation.quizzes.store');

        $data = $request->validate($validators);

        $quiz = new Quiz;
        $quiz->fill($data);


        if(!$quiz->save()){
            return redirect()->back()->withErrors(['message'=>'Error saving new quiz']);
        }

        $groups = [];
        if($request->input('groups')){
            $groups = (array)$request->input('groups');
        }
        $quiz->groups()->sync($groups);

        return redirect()->route('backend.quizzes.edit', [$quiz->id])->with(['message' => 'Successfully added new quiz. Go edit quiz to add questions']);
    }

    public function edit($id){
        $quiz = Quiz::find($id);

        if($quiz !== null){
            $groups = Group::all()->sortBy('name', SORT_NATURAL | SORT_FLAG_CASE);

            return view('backend.quizzes.edit', compact('quiz','groups'));
        }

        return redirect()->back()->withErrors(['message' => 'Unable to find selected Quiz']);
    }

    public function update(Request $request, $id){
        $quiz = Quiz::find($id);

        if($quiz === null){
            return redirect()->back()->withErrors(['message' => 'Unable to update non existing Quiz']);
        }

        $validators = config('validation.quizzes.store');

        $data = $request->validate($validators);

        if($quiz->update($data)){
            $groups = [];
            if($request->input('groups')){
                $groups = (array)$request->input('groups');
            }
            $quiz->groups()->sync($groups);

            return redirect()->route('backend.quizzes.index')->with(['message' => 'Sucessfully updated quiz']);
        }

        return redirect()->back()->withErrors(['message' => 'Error saving updates to quiz']);
    }

    public function removeAll() {
        try {
            $quizzes = Quiz::all();
            foreach($quizzes as $quiz) {
                $quiz->delete();
            }
        } catch(Exception $e) {
            return redirect()->route('backend.quizzes.index')->withErrors(['message' => 'Deleting all of the quizzes failed..']);
        }
        return redirect()->back()->with('message', 'All quizzes have been deleted');
    }

    public function remove($id){
        $quiz = Quiz::find($id);

        if($quiz === null){
            return redirect()->route('backend.quizzes.index')->withErrors(['message' => 'Unable to remove non existing quiz']);
        }

        /**
         * check first maybe if current poll is active? and deactivate it
         */

        try{
            $removed = $quiz->delete();
        } catch(Exception $e){
            $removed = false;
        }

        if($removed) return redirect()->back()->with('message', 'Quiz has been removed');

        return redirect()->back()->withErrors(['message'=>'Error removing quiz.']);
    }

    public function reset(){
        $errors = [];
        try{
            QuizAnswer::truncate();
            $success = true;
        } catch(Exception $err) {
            $success = false;
            $errors = $err->getMessage();
        }

        if($success) return redirect()->back()->with('message', 'All Quizzes have been reset');

        return redirect()->back()->withErrors($errors);
    }

    public function resetSingle($id){
        $quiz = Quiz::find($id);

        if($quiz === null){
            return redirect()->route('backend.quizzes.index')->withErrors(['message' => 'Unable to reset non existing quiz']);
        }
        $totalRemoved = 0;
        $totalFailed = 0;

        foreach($quiz->questions as $question) {
            foreach ($question->answers as $answer) {
                try {
                    $s = $answer->delete();
                    if ($s == true) $totalRemoved++;
                } catch (Exception $e) {
                    $totalFailed++;
                }
            }
        }

        return redirect()->back()->with('message', 'Removed '.$totalRemoved. ' answers and skipped '. $totalFailed .' from Quiz '. $quiz->id);
    }

    public function perform(Request $request){
        $message = '';
        $count = 0;
        $quizIds = $request->input('quiz');

        if($quizIds && is_array($quizIds)){
            $quizzes = Quiz::find($quizIds);

            /**
             * TODO: build stuff here
             */

            switch($request->input('perform')){
                case 'export':
                    $message = 'Exported ';
                    break;
                case 'clear_score':
                    $message = 'Cleared scores of ';
                    break;
                case 'report':
                    $message = 'Build Report of ';
                    break;
                case 'delete':
                    $message = 'Deleted ';
                    break;
            }
        }

        return redirect()->back()->with('message', $message . $count . ' quizzes');
    }

    public function export(){
        $quizzes = Quiz::all();
        $count = count($quizzes);

        if($count > 0){
            $export = new QuizExport($quizzes,'all');
            $export->export();

        }

        return redirect()->back()->with('message','Exporting '.$count.' quizzes');
    }

    /**
     * @return $this|\Symfony\Component\HttpFoundation\BinaryFileResponse
     */
    public function exportImages()
    {
        $filename = 'quizzes-image-export-' . time() . '.zip';
        $zipper = new Zipper;

        try {
            $zipper->make(storage_path('compressed/' . $filename));
        } catch (Exception $e) {
            return redirect()->back()->withErrors($e->getMessage());
        }

        $zipper->add(storage_path('uploads/' . config('storage.quiz')))->close();


        return response()->download(storage_path('compressed/' . $filename));
    }

    public function import(Request $request){
        if ($request->hasFile('quiz_upload') && $request->file('quiz_upload')->isValid()) {

            $import = new QuizImport($request->file('quiz_upload'));

            if ($import->isValid()) {

                $result = $import->import();

                Session::flash('status', $result);

                return redirect()->back();
            } else {
                return redirect()->back()->withErrors($import->getErrors());
            }
        }

        return redirect()->back()->withErrors(['no_file' => 'No or wrong file uploaded']);
    }

    public function report(){
        $quizzes = Quiz::all();

        if(count($quizzes) > 0){
            $report = new QuizResultReport($quizzes);

            $report->report();
        }

        return redirect()->back()->with(['message', 'No quizzes to report results of']);
    }

    public function reportSingle($id){
        return redirect()->back()->with('message','Download Result does not yet exist for Quiz');
    }
}
