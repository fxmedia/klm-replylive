<?php

namespace App\Http\Controllers\Backend;

use App\Export\AdminExport;
use App\Http\Controllers\Controller;
use App\Import\AdminImport;
use Illuminate\Http\Request;
use App\Admin;
use Illuminate\Support\Facades\Auth;
use Illuminate\Support\Facades\Hash;
use Session;

class AdminController extends Controller
{
    public function index(){
        $admins = Admin::orderBy('name')->get();

        return view('backend.admins.index', compact('admins'));
    }

    public function create(){
        return view('backend.admins.create');
    }

    public function store(Request $request){
        $validators = config('validation.admins.store');

        $validatedData = $request->validate($validators);

        Admin::create([
            'name' => $validatedData['name'],
            'email' => $validatedData['email'],
            'password' => Hash::make($validatedData['password']),
            'notification_subscription' => $validatedData['notification_subscription']
        ]);

        return redirect()->route('backend.admins.index');
    }

    public function edit($id){
        $admin = Admin::find($id);

        if($admin){
            $is_me = ($admin->id === Auth::user()->id);
            //dd($is_me);
            return view('backend.admins.edit', compact('admin', 'is_me'));
        } else {
            return redirect()->route('backend.admins.index')->withErrors( ['message' => 'Unable to find selected admin.'] );
        }
    }

    public function update($id, Request $request){
        $admin = Admin::find($id);
        $is_me = false;

        if($admin === null){
            return redirect()->route('backend.admins.index')->withErrors( ['message' => 'Error saving updates to admin.']);
        }

        if(Auth::user()->id === $admin->id){
            $validators = config('validation.admins.updateSelf');
            $is_me = true;
        } else {
            $validators = config('validation.admins.update');
        }
        if($admin->email !== $request->input('email')) $validators['email'] .= '|unique:admins';

        $validatedData = $request->validate($validators);

        if($is_me){
            if(!Hash::check($request->input('old_password'), $admin->password)){
                return redirect()->back()->withErrors( ['old_password' => 'Old password is not correct.']);
            }
            $admin->password = Hash::make($validatedData['password']);
        }

        $admin->email = $validatedData['email'];
        $admin->name = $validatedData['name'];
        $admin->notification_subscription = $validatedData['notification_subscription'];
        $admin->save();


        return redirect()->route('backend.admins.index');
    }

    public function remove($id){
        $admin = Admin::find($id);

        if($admin){

            if($admin->id == Auth::user()->id){
                Auth::logout();

                if($admin->delete()){
                    return redirect()->route('backend.index');
                }
            } else if( $admin->delete()){
                return redirect()->back()->with('message', 'Admin has been removed');
            }

        }
        return redirect()->back()->withErrors(['message' => 'Error removing admin']);
    }

    public function exportAll(){
        $admins = Admin::all();

        $file = new AdminExport($admins);
        $file->export();
    }
    public function import(Request $request){
        if($request->hasFile('admin_upload') && $request->file('admin_upload')->isValid()){

            $import = new AdminImport($request->file('admin_upload'));

            if($import->isValid()){

                $result = $import->import();

                Session::flash('status', $result);

                return redirect()->back();
            } else {
                return redirect()->back()->withErrors($import->getErrors());
            }
        }

        return redirect()->back()->withErrors([ 'no_file' => 'No or wrong file uploaded' ]);
    }
}
