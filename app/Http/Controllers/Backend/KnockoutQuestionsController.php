<?php

namespace App\Http\Controllers\Backend;

use Exception;
use App\Knockout;
use App\KnockoutQuestion;
use Illuminate\Http\Request;
use App\Http\Controllers\Controller;

class KnockoutQuestionsController extends Controller
{
    public function create($knockout_id){
        $knockout = Knockout::find($knockout_id);
        if($knockout == null){
            // cannot create question to non existing knockout
            return redirect()->back()->withErrors('Cannot create Question to non existing knockout');
        }

        return view('backend.knockouts.question.create', compact('knockout'));
    }

    public function store(Request $request, $knockout_id){
        if(!Knockout::whereId($knockout_id)->exists()){
            // cannot create question to non existing knockout
            return redirect()->back()->withErrors(['message' => 'Error storing question to non existing knockout']);
        }

        $validators = config('validation.knockouts.questions.store');
        $data = $request->validate($validators);

        $choices = [];
        foreach($data['choices'] as $c){
            if($c['value'] !== null) {
                $choices[] = [
                    'choice' => $c['choice'],
                    'value' => $c['value']
                ];
            }
        }
        $data['choices'] = $choices;

        $question = new KnockoutQuestion;
        $question->fill($data);
        $question->knockout_id = $knockout_id;

        if(!$question->save()){
            return redirect()->back()->witherrors(['message'=>'Error saving new question']);
        }

        return redirect()->route('backend.knockouts.edit',[$knockout_id])->with(['message' => "Successfully added a new question"]);
    }

    public function edit($id){
        $question = KnockoutQuestion::find($id);

        if($question == null){
            return redirect()->back()->withErrors('Unable to find Knockout Question to edit');
        }

        $knockout = $question->knockout;

        return view('backend.knockouts.question.edit', compact('question', 'knockout'));
    }

    public function update(Request $request, $id){
        $question = KnockoutQuestion::find($id);

        if($question == null){
            return redirect()->back()->withErrors('No Knockout Question found to edit');
        }

        $validators = config('validation.knockouts.questions.store');

        $data = $request->validate($validators);

        $choices = [];
        foreach($data['choices'] as $c){
            if($c['value'] !== null) {
                $choices[] = [
                    'choice' => $c['choice'],
                    'value' => $c['value']
                ];
            }
        }
        $data['choices'] = $choices;

        if($question->update($data)){
            return redirect()->route('backend.knockouts.edit',[$question->knockout_id])->with(['message' => 'Successfully updated question']);
        }

        return redirect()->back()->withErrors(['message' => 'Unable to save changes to knockout question']);
    }

    public function remove($id){
        $question = KnockoutQuestion::find($id);

        if($question === null){
            return redirect()->back()->withErrors(['message' => 'Unable to remove non existing question']);
        }


        try{
            $removed = $question->delete();
        } catch(Exception $e){
            $removed = false;
        }

        if($removed) return redirect()->back()->with('message', 'Question has been removed');

        return redirect()->back()->withErrors(['message'=>'Error removing question.']);
    }

    public function reset($id){
        $question = KnockoutQuestion::find($id);

        if($question === null){
            return redirect()->back()->withErrors(['message'=>'Unable to reset answers to unknown question']);
        }

        $totalRemoved = 0;
        $totalFailed = 0;

        foreach ($question->answers as $answer) {
            try {
                $s = $answer->delete();
                if ($s == true) $totalRemoved++;
            } catch (Exception $e) {
                $totalFailed++;
            }
        }
        return redirect()->back()->with('message', 'Removed '.$totalRemoved. ' answers and skipped '. $totalFailed .' from question '. $question->id);
    }
}
