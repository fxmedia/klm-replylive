<?php

namespace App\Http\Controllers\Backend;

use App\Export\OpenQuestionExport;
use App\Group;
use App\Import\OpenQuestionImport;
use App\NodeJS\Command;
use App\OpenQuestion;
use App\OpenQuestionAnswer;
use App\Report\OpenQuestionResultReport;
use Illuminate\Http\Request;
use App\Http\Controllers\Controller;
use Exception;
use Session;

class OpenQuestionsController extends Controller
{
    //
    public function index(){
        $open_questions = OpenQuestion::with('groups','answers')->get();
        $totalAnswers = OpenQuestionAnswer::count();

        try{
            $status = json_decode( Command::status('oq_status') ,true);

            foreach($open_questions as $open_question){
                if($status['open_question_id'] && $status['open_question_id'] == $open_question->id){
                    $open_question->isActive = true;
                }
            }

        } catch(Exception $e){}

        return view('backend.open_questions.index', compact('open_questions', 'totalAnswers'));
    }

    public function answers($id){
        $open_question_answers = OpenQuestionAnswer::orderBy('id','desc')->where(['open_question_id' => $id])->get();
        try{
            $status = json_decode( Command::status('oq_highlight_status'), true);

            foreach($open_question_answers as $open_question_answer){
                if($status['open_question_answer_id'] && $status['open_question_answer_id'] == $open_question_answer->id){
                    $open_question_answer->isHighlight = true;
                }
            }
        } catch(Exception $e){}

        return view('backend.open_questions._partials.answers', compact('open_question_answers'));
    }

    public function show($id){
        $open_question = OpenQuestion::find($id);

        if($open_question === null){
            return redirect()->back()->withErrors(['message' => 'Unable to show non existing OpenQuestion']);
        }

        return view('backend.open_questions.show', compact('open_question'));
    }

    public function create(){
        $groups = Group::all()->sortBy('name', SORT_NATURAL | SORT_FLAG_CASE);

        return view('backend.open_questions.create', compact('groups'));
    }

    public function store(Request $request){
        $validators = config('validation.open_questions.store');

        $data = $request->validate($validators);

        $open_question = new OpenQuestion;
        $open_question->fill($data);

        if(!$open_question->save()){
            return redirect()->back()->witherrors(['message'=>'Error saving new open_question']);
        }

        $groups = [];
        if($request->input('groups')){
            $groups = (array)$request->input('groups');
        }
        $open_question->groups()->sync($groups);

        return redirect()->route('backend.open_questions.index')->with(['message' => "Successfully added a new open_question"]);
    }

    public function edit($id){
       $open_question = OpenQuestion::find($id);

       if($open_question !== null){
           $groups = Group::all()->sortBy('name', SORT_NATURAL | SORT_FLAG_CASE);

           return view('backend.open_questions.edit', compact('open_question','groups'));
       }

       return redirect()->back()->withErrors(['message' => 'Unable to find selected OpenQuestion']);
    }

    public function perform(Request $request){
        $message = '';
        $count = 0;
        $open_question_answerIds = $request->input('open_question_answer');

        if($open_question_answerIds && is_array($open_question_answerIds)){
            $open_question_answers = OpenQuestionAnswer::find($open_question_answerIds);

            switch($request->input('perform')){
                case 'export':
                    $message = 'Exported ';
                    break;
                case 'clear_score':
                    $message = 'Cleared scores of ';
                    break;
                case 'report':
                    $message = 'Build Report of ';
                    break;
                case 'delete':
                    $message = 'Deleted ';
                    $count = $this->performDeleteAnswers($open_question_answerIds);
                    break;
            }
        }

        return redirect()->back()->with('message', $message . $count . ' Open Question Answers');
    }

    public function performDeleteAnswers($answer_ids) {
        $count = 0;
        foreach ($answer_ids as $answer_id) {
            if ($answer = OpenQuestionAnswer::find($answer_id)) {
                if ($answer->delete()) {
                    $count++;
                }
            }
        }
        Command::make('oq_delete_answer', ['open_question_answer_ids' => $answer_ids]);
        return $count;
    }

    public function update(Request $request, $id){
        $open_question = OpenQuestion::find($id);

        if ($open_question === null) {
            return redirect()->route('backend.open_questions.index')->withErrors(['message' => 'Error saving updates to open_question']);
        }

        $validators = config('validation.open_questions.store');

        $data = $request->validate($validators);

        if($open_question->update($data)){
            $groups = [];
            if($request->input('groups')){
                $groups = (array)$request->input('groups');
            }
            $open_question->groups()->sync($groups);

            return redirect()->route('backend.open_questions.index')->with(['message' => "Successfully edited a OpenQuestion"]);
        }

        return redirect()->back()->withErrors(['message' => 'Error saving updates to open_question']);
    }

    public function remove($id){
        $open_question = OpenQuestion::find($id);

        if($open_question === null){
            return redirect()->route('backend.open_questions.index')->withErrors(['message' => 'Unable to remove non existing open_question']);
        }

        /**
         * check first maybe if current open_question is active? and deactivate it
         */

        try{
            $removed = $open_question->delete();
        } catch(Exception $e){
            $removed = false;
        }

        if($removed) return redirect()->back()->with('message', 'OpenQuestion has been removed');

        return redirect()->back()->withErrors(['message'=>'Error removing open_question.']);
    }

    public function reset(){
        $errors = [];
        try{
            OpenQuestionAnswer::truncate();
            $success = true;
        } catch(Exception $err) {
            $success = false;
            $errors = $err->getMessage();
        }

        if($success) return redirect()->back()->with('message', 'All Open Questions have been reset');

        return redirect()->back()->withErrors($errors);
    }

    public function resetSingle($id){
        $open_question = OpenQuestion::find($id);

        if($open_question === null){
            return redirect()->route('backend.open_questions.index')->withErrors(['message' => 'Unable to reset non existing open_question']);
        }
        $totalRemoved = 0;
        $totalFailed = 0;

        foreach($open_question->answers as $answer){
            try{
                $s = $answer->delete();
                if($s == true)$totalRemoved++;
            } catch (Exception $e){
                $totalFailed++;
            }
        }

        return redirect()->back()->with('message', 'Removed '.$totalRemoved. ' answers and skipped '. $totalFailed .' from Open Question '. $open_question->id);
    }

    public function export(){
        $open_questions = OpenQuestion::all();
        $count = count($open_questions);

        if($count > 0){
            $export = new OpenQuestionExport($open_questions, 'all');
            $export->export();

            return redirect()->back()->with('message', 'Exporting ' . $count . ' open_questions');
        }

        return redirect()->back()->with('message', 'No open_questions to export');
    }

    public function import(Request $request){
        if ($request->hasFile('open_question_upload') && $request->file('open_question_upload')->isValid()) {

            $import = new OpenQuestionImport($request->file('open_question_upload'));

            if ($import->isValid()) {

                $result = $import->import();

                Session::flash('status', $result);

                return redirect()->back();
            } else {
                return redirect()->back()->withErrors($import->getErrors());
            }
        }

        return redirect()->back()->withErrors(['no_file' => 'No or wrong file uploaded']);
    }

    public function report(){
        $open_questions = OpenQuestion::all();

        if(count($open_questions) > 0){
            $report = new OpenQuestionResultReport($open_questions);

            $report->report();
        }

        return redirect()->back()->with(['message', 'No open_questions to report results of']);
    }

    public function reportSingle($id){
        $open_question = OpenQuestion::find($id);

        if($open_question !== null){
            $report = new OpenQuestionResultReport([$open_question]);

            $report->report();

        }
        return redirect()->back()->with(['message', 'No open_questions to report results of']);
    }
}
