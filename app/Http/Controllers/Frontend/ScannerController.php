<?php

namespace App\Http\Controllers\Frontend;

use App\Setting;
use Illuminate\Http\Request;
use App\Http\Controllers\Controller;

class ScannerController extends Controller
{
    //
    public function index(){
        $eventName = Setting::getValue('event_name');

        return view('frontend.scanner.index', compact('eventName'));
    }
}
