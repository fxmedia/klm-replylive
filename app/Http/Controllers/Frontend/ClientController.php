<?php

namespace App\Http\Controllers\Frontend;

use App\Events\NewUserRegistered;
use App\Setting;
use App\User;
use App\NodeJS\Command;
use Illuminate\Http\Request;
use App\Http\Controllers\Controller;
use Cookie;

class ClientController extends Controller
{
    //
    /**
     * @param Request $request
     * @return \Illuminate\Contracts\View\Factory|\Illuminate\Http\RedirectResponse|\Illuminate\View\View
     */
    public function index(Request $request){
        $settings = Setting::getSettingsAsObject();

        //dd($request->cookie('reply-live-token'));

        if($settings->online && $settings->stand_alone_register){
            $user = null;
            $eventName = $settings->event_name;

            // check if user has a token towards this url
            if($token = $request->query('token')){
                $user = User::whereToken($token)->first();
            }


            // check if user has a cookie with a token
            if($user === null && $token = $request->cookie('reply-live-token')){
                $user = User::whereToken($token)->first();
            }


            if($user !== null){
                $dataPath = route('frontend.loader.data');
                $srcPath = asset( config('loader.loader_file_path') );
                $polyfillPath = asset( config('loader.polyfill_file_path') );

                Cookie::queue(Cookie::make('reply-live-token', $user->token));
                return view('frontend.client.index', compact('user','eventName', 'dataPath', 'srcPath', 'polyfillPath'));
            }


            // otherwise register

            return view('frontend.client.register', compact('eventName', 'settings'));

        }

        return redirect()->to($settings->redirect_url);
    }

    /**
     * @param Request $request
     * @return $this|\Illuminate\Http\RedirectResponse
     */
    public function register(Request $request){
        $settings = Setting::getSettingsAsObject();

        if($settings->online && $settings->stand_alone_register){
            if($settings->register_password !== null) {
                $request->validate(['register_password' => 'required|in:' . $settings->register_password]);
            }

            $data = $request->validate(config('validation.users.register'));

            $user = new User($data);
            $user->token = User::createToken();
            if(isset($data['company']) && !empty($data['company'])) {
                $user->addExtraValue("company", $data['company']);
            }

            if($user->save()){
                Command::make('user_add', $user->toArray());


                return redirect()->route('frontend.client.index', ['token'=> $user->token])
                    ->cookie( cookie('reply-live-token', $user->token) );
            } else {
                return redirect()->back()->withErrors(['message' => 'Error creating new user']);
            }
        }
        return redirect()->to($settings->redirect_url);
    }
}
