<?php namespace App\Http\Controllers\Api;

use App\Http\Controllers\Controller;
use App\User;
use App\Display;
use Intervention\Image\ImageManagerStatic as Image;
use Illuminate\Http\Request;

class MediaServerController extends Controller {

    public function isVerified($token, $client) {
        return ($client === "user")
            ? User::where('token', $token)->first() !== null
            : Display::where('token', $token)->first() !== null;
    }

    public function serveImages(Request $request, $name) {
        if($this->isVerified($request->input('token'), $request->input('client'))) {
            $storagePath = storage_path('uploads/images/' . $name);
            if(strpos($name, "gif")) {
                return response()->download($storagePath);
            } else {
                return Image::make($storagePath)->response();
            }
        }
        return false;
    }

    public function serveVideos(Request $request, $name) {
        if($this->isVerified($request->input('token'), $request->input('client'))) {
            $storagePath = storage_path('uploads/encoded/' . $name);
            return response()->download($storagePath);
        }
        return false;
    }
}
