<?php namespace App\Http\Controllers\Api;
/**
 * Created by PhpStorm.
 * User: willem-jan
 * Date: 05/01/2018
 * Time: 10:44
 */
use App\Http\Controllers\Controller;
use App\NodeJS\Command;
use App\Setting;
use Exception;


class ApiBaseController extends Controller {

    protected $statusCode = 200;

    public function sendToNodeServer($action, $data = [], $dataToSendBack = false) {
        $send = Command::make($action, $data);

        $success = false;
        $error = null;

        if($send === false){
            return $this->respondWithError('Could not reach the NodeJS server');
        }

        try {
            $status = json_decode($send, true);
            if($status['success'] === true) $success = true;
            else $error = $status['error'] ?? ['message' => 'Unknown error from the NodeJS server.', 'statusCode' => $this->getStatusCode()];
        } catch(Exception $e){
            $error = $e->getMessage();
        }

        return $this->respond(['success'=> $success, 'error' => $error, 'data' => $dataToSendBack]);
    }

    public function getMediaPath($filename, $group='post', $type='image'){
        return route('frontend.media.'.$type, [
            'token' => Setting::getValue('media_token'),
            'group' => $group,
            'file' => $filename
        ]);
    }

    public function getStatusCode()
    {
        return $this->statusCode;
    }


    public function setStatusCode( $statusCode )
    {
        $this->statusCode = $statusCode;

        return $this;
    }


    public function respondNotFound( $message = "Not Found!" )
    {
        return $this->setStatusCode(404)->respondWithError($message);
    }


    public function respond($data, $headers = [])
    {
        return response()->json($data, $this->getStatusCode(), $headers);
    }

    public function respondWithError($message)
    {
        return $this->respond([
            'success' => false,
            'error' => [
                'message' => $message,
                'status_code' => $this->getStatusCode()
            ]
        ]);
    }

}
