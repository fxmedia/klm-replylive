<?php namespace App\Http\Controllers\Api;

use App\Poll;
use App\PollAnswer;
use Illuminate\Http\Request;

class PollController extends ApiBaseController {

    public function storeAnswer(Request $request) {
		$answer = PollAnswer::firstOrNew(['poll_id' => $request->data['poll_id'], 'user_id' => $request->input('user_id') ]);
        $answer->user_id = $request->user_id;
		$answer->choice = $request->data['choice'];

		$success = $answer->save();// $poll->answers()->save($answer);

		return $this->respond(["success" => $success, "answer" => $answer]);
    }

    public function start($id){
        $poll = Poll::with('groups:id')->find($id);

        if($poll === null){
            // respond with errors;
            return $this->respondNotFound('Poll with id '.$id. ' was not found.');
        }
        $data = $poll->toArray();
        unset($data['answers']);

        return $this->sendToNodeServer('poll_start', $data);
    }

    public function stop(){
        return $this->sendToNodeServer('poll_stop');
    }

    public function getStatus(Request $request) {
        $id = $request->input('id');
        $poll = Poll::find($id);

        if($poll === null) return $this->respondWithError('Unable to find poll to update answers from');

        return $this->respond([
            'success' => true,
            'total' => count($poll->answers),
            'result' => $poll->result
        ]);
    }
}
