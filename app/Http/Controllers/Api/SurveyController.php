<?php namespace App\Http\Controllers\Api;

use App\Survey;
use App\SurveyAnswer;
use Illuminate\Http\Request;

class SurveyController extends ApiBaseController {

    public function storeAnswer(Request $request) {
        $answer = SurveyAnswer::firstOrNew([
            'user_id' => $request->input('user_id'),
            'survey_question_id' => $request->data['survey_question_id']
        ]);
		$answer->input = $request->data['input'];

        $success = $answer->save();

        return $this->respond(["success" => $success, "answer" => $answer, "survey_id" => $request->data['survey_id']]);
    }

    // backend from here

    public function start($id){
        // check if bool takeover --> yes = command survey_start || no = command survey_start_background
        $survey = Survey::with('groups:id')->find($id);

        if($survey === null){
            return $this->respondWithError('Survey with id '. $id .' was not found');
        }

        if($survey->takeover){
            $command = 'survey_start';
        } else {
            $command = 'survey_start_background';
        }

        return $this->sendToNodeServer($command, $survey->toArray());
    }

    public function stop($id){
        $survey = Survey::find($id);

        if($survey === null){
            return $this->respondWithError('Survey with id '. $id .' was not found');
        }

        if($survey->takeover){
            $command = 'survey_stop';
        } else {
            $command = 'survey_stop_background';
        }

        return $this->sendToNodeServer($command, ['survey_id' => $id]);
    }
//    public function getStatus(Request $request){
//        $ids = $request->input('ids');
//        $surveys = Survey::find($ids);
//
//        if($surveys === null) return $this->respondWithError('Surveys with given ids not found');
//
//
//    }
}
