<?php namespace App\Http\Controllers\Api;

use App\Http\Controllers\Controller;

use App\LearningCurve;
use App\LearningCurveAnswer;
use App\LearningCurveQuestion;
use App\User;
use Illuminate\Http\Request;

class LearningCurveController extends ApiBaseController {

    public function storeAnswer(Request $request) {
        $answer = LearningCurveAnswer::firstOrNew([
            'user_id' => $request->input('user_id'),
            'learning_curve_question_id' => $request->data['learning_curve_question_id'],
            'phase' => $request->data['phase']
        ]);
        $answer->choice = $request->data['choice'];
        $answer->phase = $request->data['phase'];
        $success = $answer->save();

        return $this->respond(["success" => $success, "answer" => $answer, "curve_id" => $request->data['curve_id']]);
    }

    // backend from here

    public function getData($id){
        $learning_curve = LearningCurve::with('groups:id')->find($id);

        if($learning_curve == null){
            return $this->respondWithError('No learning_curve found');
        }

        return $this->respond(['success' => true, 'learning_curve' => $learning_curve->toArray()]);
    }

    public function start(Request $request, $id, $phase){
        // if request->question_index exists, start at that index, otherwise at 0
        $learning_curve = LearningCurve::find($id);

        if($learning_curve == null){
            return $this->respondWithError('LearningCurve with id '. $id .' was not found.');
        }

        if($phase === "post") {
            $user_answers = [];
            $users = User::all();
            foreach($learning_curve->questions as $question) {
                foreach($users as $user) {
                    $answer = LearningCurveAnswer::where([
                        'user_id' => $user->id,
                        'learning_curve_question_id' => $question->id,
                        'phase' => "pre"
                    ])->first();
                    if(!empty($answer) && count($answer)) {
                        if(!isset($user_answers[$user->id])) {
                            $user_answers[$user->id] = [];
                        }
                        array_push($user_answers[$user->id], $answer->choice);
                    }
                }
            }

            return $this->sendToNodeServer('curve_start', [
                'curve' => $learning_curve->toArray(),
                'phase' => $phase,
                'user_answers' => $user_answers
            ]);
        } else {
            return $this->sendToNodeServer('curve_start', ['curve' => $learning_curve->toArray(), 'phase' => $phase]);
        }
    }

    public function showQuestion(Request $request, $id){
        // only care about question_index, but we can check if learning_curve for it exists and there is a question at that index
        $learning_curve = LearningCurve::with('groups:id')->find($id);

        if($learning_curve == null){
            return $this->respondWithError('LearningCurve with id '. $id .' was not found.');
        }
        $index = $request->input('question_index');
        if($index === null){
            return $this->respondWithError('No question index given');
        }

        if($index >= count($learning_curve->questions) ){
            return $this->respondWithError('Index is larger then amount of questions');
        }

        return $this->sendToNodeServer('curve_show_question',['question_index' => $index]);
    }

    public function stop(){
        return $this->sendToNodeServer('curve_stop');
    }

    public function getStatus(Request $request){

    }

    public function showScore(){

    }
}
