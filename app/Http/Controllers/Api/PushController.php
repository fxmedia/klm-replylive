<?php

namespace App\Http\Controllers\Api;

use App\Push;
//use Illuminate\Http\Request;

class PushController extends ApiBaseController
{
    //
    /**
     * @param $id
     * @return \Illuminate\Http\JsonResponse
     */
    public function start($id){
        $push = Push::with('groups:id')->find($id);

        if($push === null){
            return $this->respondNotFound('Push with id '.$id. ' was not found.');
        }

        return $this->sendToNodeServer('push_start', $push->toArray());
    }

    /**
     * @return \Illuminate\Http\JsonResponse
     */
    public function stop(){
        return $this->sendToNodeServer('push_stop');
    }
}
