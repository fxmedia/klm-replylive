<?php namespace App\Http\Controllers\Api;

use App\NodeJS\Command;
use App\Post;
use App\User;
use App\Setting;
use Illuminate\Http\Request;
use App\Helpers\ImageHelper;
use App\Helpers\VideoHelper;
use Exception;
//use App\Http\Controllers\Controller;

class PostController extends ApiBaseController {

	public function storePost( Request $request) {
	    $request->validate(config('validation.posts.frontend'));

		$user = User::find($request->input('user_id'));
		$visible = Setting::getValue('posts_visible_default');

		$post = new Post;
		$post->message = $request->data['message'];
		$post->visible = $visible;

		if(isset($request->data['image'])) {
			$post->image = $request->data['image'];
		}

		$user->posts()->save($post);
		//$post->user = $user; // this should happen automatically
		$np = Post::find($post->id); // alternative cause it doesn't work automatically :)

		return $this->respond(["success" => true, "post" => $np->toArray()]);
	}


	public function storeVideo(Request $request) {
		try {
			$videoHelper = new VideoHelper(\Input::file('file'), 'post');
			$video = $videoHelper->upload()->convertToGif()->getGif();
		} catch(Exception $e) {
			return $this->respond([
				"success"=> false,
				"message" => "Video could not be converted to gif"
			]);
		}

		return $this->respond([
			"success"=> true,
			"gif" => $video,
			"image_path" => $this->getMediaPath($video)
		]);
	}

	public function storeImage(Request $request) {
		try {
			$imageHelper = new ImageHelper(\Input::file('file'), 'post');
			$image = $imageHelper->convert()->storeOriginal()->resizeWallUpload( Setting::getValue('upload_max_size') )->moveToStorage()->getFilename();
		} catch (Exception $e) {
			return $this->respond([
				"success" => false,
				"message" => "The image could not be saved, probably too big"
			]);
		}
        return $this->respond([
			"success" => true,
			"image" => $image,
			"image_path" => $this->getMediaPath($image)
		]);
	}

	public function toggleVisibility($id){
		$post = Post::find($id);
		if($post === null){
			return $this->respondWithError('Post could not be found');
		}

		$post->visible = !$post->visible;

		if(!$post->save()){
			return $this->respondWithError('Post could not be saved');
		}

		Command::make('post_toggle_visibility', [
			'post_ids' => [$post->id],
			'visible' => $post->visible
		]);

		return $this->respond(["success" => true]);
	}

	public function toggleDisplay(Request $request) {

		try {
			Command::make('display_set_page', [
				'name' => ($request->input('showDisplay') === "1") ? "post" : "idle"
			]);
		} catch(Exception $e) {
			return $this->respondWithError("Node server could not be reached");
		}

		return $this->respond(["success" => true]);
	}

	public function fetchPosts() {
		$posts = Post::with('user')->get();

		return $this->respond(["posts" => $posts->toArray()]);
	}

	public function highLightPost($id = null){
	    return $this->sendToNodeServer('post_highlight', ['post_id' => $id]);
    }
}
