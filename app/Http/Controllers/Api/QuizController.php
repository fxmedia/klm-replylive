<?php namespace App\Http\Controllers\Api;

use App\Http\Controllers\Controller;

use App\Quiz;
use App\QuizAnswer;
use App\QuizQuestion;
use App\User;
use Illuminate\Http\Request;

class QuizController extends ApiBaseController {

    public function storeAnswer(Request $request) {
        $answer = QuizAnswer::firstOrNew([
            'user_id' => $request->input('user_id'),
            'quiz_question_id' => $request->data['quiz_question_id']
        ]);
        $answer->choice = $request->data['choice'];
        $this->verifyAnswer($answer);
        $success = $answer->save();

        return $this->respond(["success" => $success, "answer" => $answer]);
    }

    public function verifyAnswer($answer) {
        $question = QuizQuestion::find($answer->quiz_question_id);
        if($answer->choice === $question->correct_answer) {
            $user = User::find($answer->user_id);
            $user->addPoints($question->points);
        }
    }

    // backend from here

    public function getData($id){
        $quiz = Quiz::find($id);

        if($quiz == null){
            return $this->respondWithError('No quiz found');
        }

        return $this->respond(['success' => true, 'quiz' => $quiz->toArray()]);
    }

    public function start(Request $request, $id){
        // if request->question_index exists, start at that index, otherwise at 0
        $quiz = Quiz::with('groups:id')->find($id);

        if($quiz == null){
            return $this->respondWithError('Quiz with id '. $id .' was not found.');
        }
        $data = ['quiz' =>$quiz->toArray()];

        $index = $request->input('question_index') ?? 0;
        if($index < 0 || $index >= count($quiz->questions)) $index = 0;

        $data['question_index'] = $index;


        return $this->sendToNodeServer('quiz_start', $data, $data);
    }

    public function checkAnswer(){
        return $this->sendToNodeServer('quiz_check_answer', []);
    }

    public function toggleResults(){
        return $this->sendToNodeServer('quiz_toggle_results', []);
    }

    public function showQuestion(Request $request, $id){
        // only care about question_index, but we can check if quiz for it exists and there is a question at that index
        $quiz = Quiz::with('groups:id')->find($id);

        if($quiz == null){
            return $this->respondWithError('Quiz with id '. $id .' was not found.');
        }
        $index = $request->input('question_index');
        if($index === null){
            return $this->respondWithError('No question index given');
        }

        if($index >= count($quiz->questions) ){
            return $this->respondWithError('Index is larger then amount of questions');
        }

        return $this->sendToNodeServer('quiz_show_question',['question_index' => $index]);
    }

    public function stop(){
        return $this->sendToNodeServer('quiz_stop');
    }

    public function getStatus(Request $request){

    }

    public function showScore(){

    }
}
