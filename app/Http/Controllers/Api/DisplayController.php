<?php namespace App\Http\Controllers\Api;

use App\Display;
use App\Setting;
//use Illuminate\Http\Request;
//use App\Http\Controllers\Controller;

class DisplayController extends ApiBaseController {

    /**
     * @param $token
     * @return \Illuminate\Http\JsonResponse
     */
    public function fetch($token) {
        $display = Display::where('token', $token)->first();
        if(empty($display)) {
            if($token === Setting::getValue('security_token')){
                return $this->respond(['success' => true, "settings" => [
                    'token' => $token,
                    'name' => 'anonymous'
                ]]);
            } else {
                return $this->respondWithError("Illegal token");
            }
            //return $this->respondWithError("Display not found..");
        }

        return $this->respond(["success" => true, "settings" => $display->toArray()]);
    }

}
