<?php namespace App\Http\Controllers\Api;

use App\Http\Controllers\Controller;

use App\OpenQuestion;
use App\OpenQuestionAnswer;
use Illuminate\Http\Request;

class OpenQuestionController extends ApiBaseController {

    public function storeAnswer(Request $request) {
		$answer = new OpenQuestionAnswer;
        $answer->user_id = $request->user_id;
        $answer->open_question_id = $request->data['open_question_id'];
		$answer->answer = $request->data['answer'];

        $success = $answer->save();

        return $this->respond(["success" => $success, "answer" => $answer]);
    }

    public function start($id){
        $open_question = OpenQuestion::with('groups:id')->find($id);

        if($open_question == null){
            // respond with errors;
            return $this->respondNotFound('OpenQuestion with id '.$id. ' was not found.');
        }
        $data = $open_question->toArray();

        // user results not needed, users can answer multiple times
        if(isset($open_question->multiple_answers) && $open_question->multiple_answers) {
            unset($data['result_users']);
        }
        unset($data['answers']);

        return $this->sendToNodeServer('oq_start', $data);
    }

    public function stop(){
        return $this->sendToNodeServer('oq_stop');
    }

    public function getStatus(Request $request) {
        $id = $request->input('id');
        $open_question = OpenQuestion::find($id);

        if($open_question === null) return $this->respondWithError('Unable to find open_question to update answers from');

        return $this->respond([
            'success' => true,
            'total' => count($open_question->answers),
            'result' => $open_question->result
        ]);
    }

    public function highLightAnswer($id = null){
        return $this->sendToNodeServer('oq_highlight_answer', ['open_question_answer_id' => $id]);
    }
}
