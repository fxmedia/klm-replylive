<?php namespace App\Http\Controllers\Api;

use App\Attending;
use App\Events\NewUserRegistered;
use App\Events\UserSetAttending;
use App\Events\UserSetCancelled;
use App\Events\UserSetWaiting;
use App\Helpers\ImageHelper;
use App\Http\Controllers\Controller;
use App\NodeJS\Command;
use App\Setting;
use App\User;
use Illuminate\Http\Request;
use Image;
use Validator;

class UserController extends ApiBaseController {

    public function eventSiteTokenCheck(Request $request){
        $token = $request->input('token');
        if($token == null) {
            return $this->respondWithError('No user token given');
        }

        $user = User::whereToken($token)->first();

        if($user === null){
            return $this->respondWithError('No user found on given token');
        }
        else {
            $user->attending;
        }

        return $this->respond(['success'=>true, 'user' => $user->toArray()]);
    }

    /**
     * Register a user for the event. Either create or update a user and set Attending
     * @param  Request $request request coming from the event website containing user data and token
     * @return response json encoded response, either succes with user or error message.
     */
    public function eventSiteRegisterUser(Request $request) {

        $userData = $request->input('userData') ?? [];
        $validator = Validator::make( $userData, config('validation.users.import'));

        if($validator->fails()) {
            $errors = $validator->errors();
            return $this->respond( ['success' => false, 'errors' => $errors ] );
        }

        $token = $request->input('token');

        // If user doesn't exist yet create a new user
        if ( $token == null || $token == 'null' ) {
            $user = new User();
            $user->token = $user->createToken();
            $attending_status = 'pending';
            $isNew = true;
        }
        else {
            $user = User::whereToken($token)->first();

            // Unset firstname and lastname from formdata. Not allowed to change this.
            unset( $userData['firstname'] );
            unset( $userData['lastname'] );
            $attending_status = Attending::determineAttendingStatus($user->id);
            $isNew = false;
        }

        $user->fill($userData);

        /**
         * Filter every userData key starting with an @ to set as extra data
         */
        $userExtra = $user->extra;
        $extraDataKeys = array_filter( array_keys( $userData ), function($key) {
            return strpos( $key, '@' ) === 0;
        } );

        if ( count($extraDataKeys) > 0 ) {
            foreach ( $extraDataKeys as $key ) {
                $userExtra[$key] = $userData[$key];
            }
        }
        $user->extra = $userExtra;

        if ( $user->save() ) {

            $attending = Attending::whereUserId( $user->id )->first();
            if ( $attending == null ) {
                $attending = new Attending();
                $attending->user_id = $user->id;
            }

            $attending->status = $attending_status;

            if ( $attending->save() ) {

                $user = $user->fresh();

                if($isNew){
                    Command::make('user_add', $user->toArray());
                } else {
                    Command::make('user_update', $user->toArray());
                }

                $this->triggerEvent($user);

                unset($user->token);
                return $this->respond( ['success' => true, 'user' => $user] );
            }
            else {
                return $this->respondWithError( 'Something wen\'t wrong while changing attending status' );
            }

        }
        else {
            return $this->respondWithError( 'Something wen\'t wrong while saving user' );
        }
    }

    public function eventSiteUpdateUser(Request $request) {

        $userData = $request->input('userData');
        $validator = Validator::make( $userData, config('validation.users.update'));

        if($validator->fails()) {
            $errors = $validator->errors();
            return $this->respond( ['success' => false, 'errors' => $errors ] );
        }

        $token = $request->input('token');

        if($token == null || $token == 'null') {
            return $this->respondWithError( 'No user token given' );
        }

        $user = User::whereToken($token)->first();
        if($user === null){
            return $this->respondWithError('No user found on given token');
        }

        /**
         * Filter every userData key starting with an @ to set as extra data
         */
        $userExtra = $user->extra;
        $extraDataKeys = array_filter( array_keys( $userData ), function($key) {
            return strpos( $key, '@' ) === 0;
        } );

        if ( count($extraDataKeys) > 0 ) {
            foreach ( $extraDataKeys as $key ) {
                $userExtra[$key] = $userData[$key];
                unset( $userData[$key]);
            }
        }
        $user->extra = $userExtra;

        foreach ( $userData as $field => $value ) {
            $user[$field] = $value;
        }

        if ( $user->save() ) {
            Command::make('user_update', $user->toArray());

            unset($user->token);
            return $this->respond( ['success' => true, 'user' => $user] );
        }
        else {
            return $this->respondWithError( 'Something wen\'t wrong while saving user' );
        }

    }

    public function eventSiteUpdateUserImage(Request $request) {

        $token = $request->input('token');
        if($token == null || $token == 'null') {
            return $this->respondWithError( 'No user token given' );
        }

        $user = User::whereToken($token)->first();
        if($user === null){
            return $this->respondWithError('No user found on given token');
        }

        $imageData = $request->input('imageData');

        if ( $imageData != null ) {
            $imageHelper = new ImageHelper($imageData, 'user', true);
            $image = $imageHelper->convert()->setEventSiteUserImage( 300 )->moveToStorage()->getFilename();
            $user->image = $image;
        }

        if ( $user->save() ) {
            Command::make('user_update', $user->toArray());

            unset($user->token);
            return $this->respond( ['success' => true, 'user' => $user] );
        }
        else {
            return $this->respondWithError( 'Something wen\'t wrong while saving user' );
        }
    }

    public function eventSiteCancelUser(Request $request) {
        $token = $request->input('token');
        if($token == null || $token == 'null') {
            return $this->respondWithError( 'No user token given' );
        }

        $user = User::whereToken($token)->first();
        if($user === null){
            return $this->respondWithError('No user found on given token');
        }

        $attending = Attending::whereUserId( $user->id )->first();
        if ( $attending === null ) {
            return $this->respondWithError('User attending status not found');
        }

        $attending->status = 'cancelled';
        if ( $attending->save() ) {
            $user = $user->fresh();
            Command::make('user_update', $user->toArray());

            $this->triggerEvent($user);
            unset($user->token);
            return $this->respond( ['success' => true, 'user' => $user] );
        }
        else {
            return $this->respondWithError( 'Something wen\'t wrong while cancelling attending status' );
        }

        return $this->respond( ['success' => true, 'attending' => $attending] );
    }

    public function eventSiteGetGuestlist() {
        $users = User::select( 'firstname', 'lastname', 'email','phonenumber', 'image' )
            ->orderBy('firstname')
            ->whereHas('attending', function($query) {
                $query->where('status', '=', 'attending');
            } )
        ->get();
        return $this->respond( ['success' => true, 'users' => $users ] );
    }

    /**
     * @return \Illuminate\Http\JsonResponse
     */
    public function fetchAll() {
        $users = User::with('groups:id')->get();
        return response()->json($users->toArray());
    }

    public function uploadPicture(Request $request) {
        $user = User::find($request->input('user_id'));

        $imageHelper = new ImageHelper(\Input::file('file'),'user');
        $image = $imageHelper->convert()->resizeWallUpload( Setting::getValue('upload_max_size') )->moveToStorage()->getFilename();

        $user->image = $image;
        $user->save();

        return response()->json(["image" => $image, "user_id" => $user->id]);
    }

    /**
     * Trigger events based on the user attending status
     *
     * @param User $user
     * @return void
     */
    private function triggerEvent(User $user){
        if($user->attending){
            switch($user->attending->status){
                case 'pending':
                    event(new NewUserRegistered($user));
                    break;
                case 'attending':
                    event(new UserSetAttending($user));
                    break;
                case 'waiting':
                    event(new UserSetWaiting($user));
                    break;
                case 'cancelled':
                    event(new UserSetCancelled($user));
                    break;
            }
        }
    }

}