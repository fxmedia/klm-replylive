<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

/**
 * App\Post
 *
 * @property-read \App\User $user
 * @mixin \Eloquent
 * @property int $id
 * @property int $user_id
 * @property string $type
 * @property string|null $message
 * @property string|null $image
 * @property int $visible
 * @property \Carbon\Carbon|null $created_at
 * @property \Carbon\Carbon|null $updated_at
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Post whereCreatedAt($value)
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Post whereId($value)
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Post whereImage($value)
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Post whereMessage($value)
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Post whereType($value)
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Post whereUpdatedAt($value)
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Post whereUserId($value)
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Post whereVisible($value)
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Post ofType($type)
 * @property-read mixed $image_path
 */
class Post extends Model
{
    protected $fillable = ["data", "user_id", "type", "message", "image", "visible"];

    protected $appends = ['image_path'];

    protected $with = ['user'];

    public function getImagePathAttribute(){
        if(!isset($this->image)) return null;

        $token = Setting::getValue('media_token');
        return route('frontend.media.image', ['token'=>$token, 'group'=>'post', 'file'=>$this->image]);
    }

    public function user() {
        return $this->belongsTo('App\User');
    }


    /**
     * Scope a query to only include posts of a given type.
     *
     * @param \Illuminate\Database\Eloquent\Builder $query
     * @param mixed $type
     * @return \Illuminate\Database\Eloquent\Builder
     */
    public function scopeOfType($query, $type){
        return $query->where('type', $type);
    }
}
