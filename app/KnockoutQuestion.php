<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

/**
 * App\KnockoutQuestion
 *
 * @property int $id
 * @property int $knockout_id
 * @property string $title
 * @property string $correct_answer
 * @property array $choices
 * @property \Carbon\Carbon|null $created_at
 * @property \Carbon\Carbon|null $updated_at
 * @property-read \Illuminate\Database\Eloquent\Collection|\App\KnockoutAnswer[] $answers
 * @property-read \App\Knockout $knockout
 * @method static \Illuminate\Database\Eloquent\Builder|\App\KnockoutQuestion whereChoices($value)
 * @method static \Illuminate\Database\Eloquent\Builder|\App\KnockoutQuestion whereCorrectAnswer($value)
 * @method static \Illuminate\Database\Eloquent\Builder|\App\KnockoutQuestion whereCreatedAt($value)
 * @method static \Illuminate\Database\Eloquent\Builder|\App\KnockoutQuestion whereId($value)
 * @method static \Illuminate\Database\Eloquent\Builder|\App\KnockoutQuestion whereKnockoutId($value)
 * @method static \Illuminate\Database\Eloquent\Builder|\App\KnockoutQuestion whereTitle($value)
 * @method static \Illuminate\Database\Eloquent\Builder|\App\KnockoutQuestion whereUpdatedAt($value)
 * @mixin \Eloquent
 * @property int $order
 * @method static \Illuminate\Database\Eloquent\Builder|\App\KnockoutQuestion whereOrder($value)
 */
class KnockoutQuestion extends Model
{
    protected $fillable = ["quiz_id", "title", "correct_answer", "points", "choices", "order"];
    protected $table = "knockout_questions";
    protected $casts = [
        'choices' => 'array'
    ];

    public function answers() {
        return $this->hasMany('App\KnockoutAnswer');
    }

    public function knockout() {
        return $this->belongsTo('App\Knockout');
    }
}
