<?php namespace App\Helpers;

use App\Setting;
use \FFMpeg;
use \FFMpeg\Coordinate\TimeCode;
use \FFMpeg\Coordinate\Dimension;
use Storage;

class VideoHelper {

    private $data;
//    private $type;
    private $filename;
    private $gif;
//    private $ffmpeg;
    private $videoPath = "videos/";
    private $gifPath = "/uploads/images/";
    private $specs = ["width" => 320, "height" => 240, "duration" => 5];

    public function __construct($data, $pathGroup = 'other') {
        $this->data = $data;
        $this->video = null;
        $this->filename = "";
        $this->gifPath = '/uploads/'. config('storage.'.$pathGroup) .'/';

        $this->specs['duration'] = Setting::getValue('upload_max_length') ?? 10;
    }

    public function upload() {
        $this->setFilename(time() . "-" . $this->data->getClientOriginalName());
        if(!Storage::disk('uploads')->has('videos')) {
            Storage::disk('uploads')->makeDirectory('videos');
        }
        $this->data->move(storage_path('/uploads/videos'), $this->filename);

        return $this;
	}

    public function convertToGif() {
        $this->setGif($this->filename . "-" . time() . ".gif");
		FFMpeg::fromDisk('uploads')
            ->open($this->videoPath . $this->filename)
		    ->gif(TimeCode::fromSeconds(0), new Dimension($this->specs["width"], $this->specs["height"]), $this->specs["duration"])
		    ->save(storage_path($this->gifPath . $this->gif));

        return $this;
	}

    public function setFilename($name) {
        $this->filename = $name;
    }

    public function getFilename() {
        return $this->filename;
    }

    public function getGif() {
        return $this->gif;
    }

    public function setGif($gif) {
        $this->gif = $gif;
    }
}

?>
