<?php namespace App\Senders;

use App\Admin;
use App\Setting;
use App\User;

class BaseSender {

    /**
     * @param User $user
     * @return array
     */
    public static function createSendDataObject(User $user)
    {
        $url = Setting::getValue('event_domain') . '/?token=' . $user->token;
        if(strpos($url, 'http') === false) $url = 'http://' . $url;

        $data = [
            'url' => $url,
            'firstname' => $user->firstname,
            'lastname' => $user->lastname,
            'name' => $user->name,
            'token' => $user->token
        ];

        if ( $user->extra !== null ) {
            foreach($user->extra as $key => $value){
                $data['extra_'.$key] = $value;
            }
        }

        return $data;
    }

    /**
     * parseTextVariables
     * This is a function with which you replace all the [variables] in a text with the variable values
     *
     * @param array $data
     * @param string $text
     *
     * @return string
     */
    public static function parseTextVariables($data, $text){
        foreach ( $data as $key => $item ) {
            $text = str_replace( '[' . $key . ']', $item, $text );
        }

        return $text;
    }

    /**
     * getSubscriberEmailAddressess
     * get email addresses of all subscribers to notifications
     *
     * @return array Addresses
     */
    public static function getNotificationSubscriberEmailAddresses(){
        $addresses = [];
        $admins = Admin::whereNotificationSubscription(true)->get();
        foreach($admins as $admin){
            $addresses[] = $admin->email;
        }

        $settingString = Setting::getValue('email_notification');
        if($settingString !== null){
            $arr = explode(',', $settingString);
            foreach($arr as $value){
                $value = trim($value);
                if($value) $addresses[] = $value;
            }
        }

        return $addresses;
    }
}