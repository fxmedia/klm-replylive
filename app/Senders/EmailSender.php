<?php namespace App\Senders;

use App\Email;
use App\EmailLog;
use App\Jobs\SendEmail;
use App\User;
use Illuminate\Mail\Message;
use Illuminate\Support\Facades\Mail;

class EmailSender extends BaseSender
{
    /**
     * Send email by action
     *
     * @param User $user
     * @param string $action
     *
     * @return bool
     */
    public static function sendAction(User $user, string $action)
    {
        if( $email = Email::getByAction($action)){
            self::send($email, $user);
            return true;
        }
        return false;
    }

    /**
     * Add The email to send to one user to the queue to be sent
     *
     * @param Email $email
     * @param User $user
     *
     * @return void
     */
    public static function send(Email $email, User $user)
    {
        /**
         * TODO: could add priorities here
         */
        $log = new EmailLog([
            'email_id' => $email->id,
            'user_id' => $user->id,
            'batch' => null
        ]);
        $log->save();
        SendEmail::dispatch($email, $user, $log);
    }

    /**
     * Add email to send to multiple users to the queue
     *
     * @param Email $email
     * @param array <User> $users
     *
     * @return integer
     */
    public static function batch(Email $email, $users)
    {
        $max = EmailLog::whereEmailId($email->id)->max('batch'); // get largest current batch number;
        $batch = ($max)? $max+1 : 1; // set new batch number to be 1 above before
//        $logs = [];

        foreach($users as $user){
            $log = new EmailLog([
                'email_id' => $email->id,
                'user_id' => $user->id,
                'batch' => $batch
            ]);
            $log->save();
//            $logs[] = $log;

            SendEmail::dispatch($email, $user, $log);
        }
//        \DB::table('email_log')->insert($logs);

        return $batch;
    }

    /**
     *  Being called by the queue-worker
     *
     * @param Email $email
     * @param User $user
     *
     * @return void
     */
    public static function _send(Email $email, User $user)
    {
        $data = self::createSendDataObject($user);

        $data['body_text'] = self::parseTextVariables($data, $email->body_text);
        $data['disclaimer_text'] = self::parseTextVariables($data, $email->disclaimer_text);
        $data['email'] = $email;

        // send mail with this data
        Mail::send($email->view, $data, function(Message $message) use ($email, $user){
            $message->subject($email->subject);
            $message->from($email->from_email, $email->from_name);

            if( $email->reply_to ){
                $message->replyTo($email->reply_to);
            }

            /**
             * TODO Add attachment correctly
             */
           if( $email->attachment ){
               $message->attach( storage_path( 'uploads' . DIRECTORY_SEPARATOR . config('storage.email_attachments') . DIRECTORY_SEPARATOR . $email->attachment ) );
           }

            $message->to( $user->email, $user->name);
        });
    }
}