<?php

/*
|--------------------------------------------------------------------------
| Web Routes
|--------------------------------------------------------------------------
|
| Here is where you can register web routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| contains the "web" middleware group. Now create something great!
|
*/

Route::get('/home', 'HomeController@index')->name('home');


// Frontend Paths
Route::group(['namespace' => 'Frontend'], function(){
    // LOADER PATHS
    Route::get('loader/script', 'LoaderController@index')->name('frontend.loader.script');

    // CLIENT FRONTEND
    Route::get('/', 'ClientController@index')->name('frontend.client.index');
    Route::post('/', 'ClientController@register')->name('frontend.client.register');

    // DISPLAY PATHS
    Route::get('display/{token}', 'DisplayController@index')->name('frontend.display');

    // SCANNER PATH
    Route::get('scanner/{token}', 'ScannerController@index')->name('frontend.scanner.index')->middleware('verifyMediaToken');
});

// Media Paths
Route::group(['prefix' => 'media','middleware' => 'verifyMediaToken', 'namespace' => 'Frontend'], function(){
    Route::get('image/{token}/{group}/{file}', 'MediaController@getImage')->name('frontend.media.image');
    Route::get('stream/{token}/{file}', 'MediaController@streamVideo')->name('frontend.media.stream-video');

});

// Backend Paths
Route::group(['prefix'=> 'backend', 'namespace' => 'Backend'], function(){

    Auth::routes();
    Route::match(['get', 'post'], 'register', function(){
        return redirect()->route('login');
    });

    // Accept/Deny Pending User Paths
    Route::group(['prefix' => 'pending-user', 'middleware' => 'verifyMediaToken'], function(){
        Route::get('{token}/accept/{id}', 'AttendingStatusController@adminAcceptedPendingUser')->name('backend.pending-user.accept');
        Route::get('{token}/declined/{id}', 'AttendingStatusController@adminDeclinedPendingUser')->name('backend.pending-user.declined');
    });


    Route::group(['middleware' => 'auth'], function(){

        Route::get('/', 'BackendController@index')->name('backend.index');

        // ADMIN SETTING PAGES
        Route::group(['prefix' => 'admins'], function(){
            Route::get('/', 'AdminController@index')->name('backend.admins.index');
            Route::get('create', 'AdminController@create')->name('backend.admins.create');
            Route::post('create', 'AdminController@store')->name('backend.admins.store');
            Route::get('edit/{id}', 'AdminController@edit')->name('backend.admins.edit');
            Route::patch('edit/{id}', 'AdminController@update')->name('backend.admins.update');
            Route::post('remove/{id}', 'AdminController@remove')->name('backend.admins.remove');

            Route::get('export', 'AdminController@exportAll')->name('backend.admins.export');
            Route::post('import', 'AdminController@import')->name('backend.admins.import');
        });

        // EMAIL PAGES
        Route::group(['prefix' => 'emails'], function(){
            Route::get('/', 'EmailsController@index')->name('backend.emails.index');
            Route::get('setup/{id}', 'EmailsController@setup')->name('backend.emails.setup');
            Route::post('setup/{id}', 'EmailsController@preview')->name('backend.emails.setup-preview');
            Route::post('send/{id}', 'EmailsController@ajaxSend')->name('backend.emails.ajax-send');
            Route::get('log/{id}', 'EmailsController@log')->name('backend.emails.log');

            Route::get('create','EmailsController@create')->name('backend.emails.create');
            Route::post('create','EmailsController@store')->name('backend.emails.store');
            Route::get('edit/{id}','EmailsController@edit')->name('backend.emails.edit');
            Route::patch('edit/{id}','EmailsController@update')->name('backend.emails.update');
            Route::delete('remove/{id}', 'EmailsController@remove')->name('backend.emails.remove');

            Route::get('export', 'EmailsController@export')->name('backend.emails.export');
            Route::post('import', 'EmailsController@import')->name('backend.emails.import');
            Route::get('report', 'EmailsController@report')->name('backend.emails.report');
            Route::get('report/{id}', 'EmailsController@reportSingle')->name('backend.emails.report.single');

            Route::post('batch-status', 'EmailsController@ajaxBatchStatus')->name('backend.emails.ajax-batch-status');
        });

        // GROUP PAGES
        Route::Group(['prefix'=> 'groups'], function(){
            Route::get('/', 'GroupsController@index')->name('backend.groups.index');
            Route::get('create', 'GroupsController@create')->name('backend.groups.create');
            Route::post('create', 'GroupsController@store')->name('backend.groups.store');
            Route::get('edit/{id}', 'GroupsController@edit')->name('backend.groups.edit');
            Route::patch('edit/{id}', 'GroupsController@update')->name('backend.groups.update');
            Route::post('remove/{id}', 'GroupsController@remove')->name('backend.groups.remove');

            Route::post('perform', 'GroupsController@perform')->name('backend.groups.perform');

            Route::get('export', 'GroupsController@exportAll')->name('backend.groups.export');
            Route::post('import', 'GroupsController@import')->name('backend.groups.import');

            Route::get('test', 'GroupsController@showTestUsers')->name('backend.groups.test');
            Route::get('/{id}', 'GroupsController@show')->name('backend.groups.show');
        });

        // KNOCKOUT PAGES
        Route::group(['prefix' => 'knockouts'], function(){
            Route::get('/', 'KnockoutsController@index')->name('backend.knockouts.index');
            Route::get('create','KnockoutsController@create')->name('backend.knockouts.create');
            Route::post('create','KnockoutsController@store')->name('backend.knockouts.store');
            Route::get('edit/{id}','KnockoutsController@edit')->name('backend.knockouts.edit');
            Route::patch('edit/{id}','KnockoutsController@update')->name('backend.knockouts.update');
            Route::post('remove/{id}', 'KnockoutsController@remove')->name('backend.knockouts.remove');

            Route::get('export', 'KnockoutsController@export')->name('backend.knockouts.export');
            Route::post('import', 'KnockoutsController@import')->name('backend.knockouts.import');
            Route::get('report', 'KnockoutsController@report')->name('backend.knockouts.report');
            Route::get('report/{id}', 'KnockoutsController@reportSingle')->name('backend.knockouts.report.single');

            Route::post('reset', 'KnockoutsController@reset')->name('backend.knockouts.reset');
            Route::post('reset/{id}', 'KnockoutsController@resetSingle')->name('backend.knockouts.reset.single');
            Route::post('perform', 'KnockoutsController@perform')->name('backend.knockouts.perform');

            Route::get('/{knockout_id}/create-question', 'KnockoutQuestionsController@create')->name('backend.knockouts.questions.create');
            Route::post('/{knockout_id}/create-question', 'KnockoutQuestionsController@store')->name('backend.knockouts.questions.store');
            Route::get('/edit-question/{id}','KnockoutQuestionsController@edit')->name('backend.knockouts.questions.edit');
            Route::patch('/edit-question/{id}','KnockoutQuestionsController@update')->name('backend.knockouts.questions.update');
            Route::post('/remove-question/{id}', 'KnockoutQuestionsController@remove')->name('backend.knockouts.questions.remove');
            Route::post('/reset-question/{id}', 'KnockoutQuestionsController@reset')->name('backend.knockouts.questions.reset');
        });

        // LEARNING CURVES PAGES
        Route::group(['prefix' => 'learning-curves'], function(){
            Route::get('/', 'LearningCurvesController@index')->name('backend.learning_curves.index');
            Route::get('create','LearningCurvesController@create')->name('backend.learning_curves.create');
            Route::post('create','LearningCurvesController@store')->name('backend.learning_curves.store');
            Route::get('edit/{id}','LearningCurvesController@edit')->name('backend.learning_curves.edit');
            Route::patch('edit/{id}','LearningCurvesController@update')->name('backend.learning_curves.update');
            Route::post('remove/{id}', 'LearningCurvesController@remove')->name('backend.learning_curves.remove');

            Route::get('export', 'LearningCurvesController@export')->name('backend.learning_curves.export');
            Route::post('import', 'LearningCurvesController@import')->name('backend.learning_curves.import');
            Route::get('report', 'LearningCurvesController@report')->name('backend.learning_curves.report');
            Route::get('report/{id}', 'LearningCurvesController@reportSingle')->name('backend.learning_curves.report.single');

            Route::post('reset', 'LearningCurvesController@reset')->name('backend.learning_curves.reset');
            Route::post('reset/{id}', 'LearningCurvesController@resetSingle')->name('backend.learning_curves.reset.single');
            Route::post('perform', 'LearningCurvesController@perform')->name('backend.learning_curves.perform');

            // Route::get('/{id}', 'SurveysController@show')->name('backend.learning_curves.show');

            Route::get('/{learning_curve_id}/create-question', 'LearningCurveQuestionsController@create')->name('backend.learning_curves.questions.create');
            Route::post('/{learning_curve_id}/create-question', 'LearningCurveQuestionsController@store')->name('backend.learning_curves.questions.store');
            Route::get('/edit-question/{id}','LearningCurveQuestionsController@edit')->name('backend.learning_curves.questions.edit');
            Route::patch('/edit-question/{id}','LearningCurveQuestionsController@update')->name('backend.learning_curves.questions.update');
            Route::post('/remove-question/{id}', 'LearningCurveQuestionsController@remove')->name('backend.learning_curves.questions.remove');
            Route::post('/reset-question/{id}', 'LearningCurveQuestionsController@reset')->name('backend.learning_curves.questions.reset');
        });

        // OPEN QUESTION PAGES
        Route::group(['prefix' => 'open-questions'], function(){
            Route::get('/', 'OpenQuestionsController@index')->name('backend.open_questions.index');
            Route::get('create','OpenQuestionsController@create')->name('backend.open_questions.create');
            Route::post('create','OpenQuestionsController@store')->name('backend.open_questions.store');
            Route::get('edit/{id}','OpenQuestionsController@edit')->name('backend.open_questions.edit');
            Route::patch('edit/{id}','OpenQuestionsController@update')->name('backend.open_questions.update');
            Route::post('remove/{id}', 'OpenQuestionsController@remove')->name('backend.open_questions.remove');
            Route::get('answers/{id}','OpenQuestionsController@answers')->name('backend.open_questions.answers');

            Route::get('export', 'OpenQuestionsController@export')->name('backend.open_questions.export');
            Route::post('import', 'OpenQuestionsController@import')->name('backend.open_questions.import');
            Route::get('report', 'OpenQuestionsController@report')->name('backend.open_questions.report');
            Route::get('report/{id}', 'OpenQuestionsController@reportSingle')->name('backend.open_questions.report.single');

            Route::post('reset', 'OpenQuestionsController@reset')->name('backend.open_questions.reset');
            Route::post('reset/{id}', 'OpenQuestionsController@resetSingle')->name('backend.open_questions.reset.single');
            Route::post('perform', 'OpenQuestionsController@perform')->name('backend.open_questions.answers.perform');
        });

        // PLEDGE PAGES
        Route::group(['prefix' => 'pledges'], function(){
            Route::get('/', 'PledgeController@index')->name('backend.pledges.index');
            Route::post('remove/{id}', 'PledgeController@remove')->name('backend.pledges.remove');
            Route::post('perform', 'PledgeController@perform')->name('backend.pledges.perform');
            Route::get('export', 'PledgeController@exportAll')->name('backend.pledges.export');
        });

        // POLL PAGES
        Route::group(['prefix' => 'polls'], function(){
            Route::get('/', 'PollsController@index')->name('backend.polls.index');
            Route::get('create','PollsController@create')->name('backend.polls.create');
            Route::post('create','PollsController@store')->name('backend.polls.store');
            Route::get('edit/{id}','PollsController@edit')->name('backend.polls.edit');
            Route::patch('edit/{id}','PollsController@update')->name('backend.polls.update');
            Route::post('remove/{id}', 'PollsController@remove')->name('backend.polls.remove');

            Route::get('export', 'PollsController@export')->name('backend.polls.export');
            Route::post('import', 'PollsController@import')->name('backend.polls.import');
            Route::get('report', 'PollsController@report')->name('backend.polls.report');
            Route::get('report/{id}', 'PollsController@reportSingle')->name('backend.polls.report.single');

            Route::post('reset', 'PollsController@reset')->name('backend.polls.reset');
            Route::post('reset/{id}', 'PollsController@resetSingle')->name('backend.polls.reset.single');
            Route::post('perform', 'PollsController@perform')->name('backend.polls.perform');

            // Route::get('/{id}', 'PollsController@show')->name('backend.polls.show');
        });

        // POST PAGES
        Route::group(['prefix' => 'posts'], function(){
            Route::get('/', 'PostController@index')->name('backend.posts.index');

            Route::get('toggle-visibility/{id}','PostController@toggleVisibility')->name('backend.posts.toggle_visibility');
            Route::post('remove/{id}', 'PostController@remove')->name('backend.posts.remove');
//
//            Route::post('search', 'PostController@search')->name('backend.posts.search');
            Route::post('perform', 'PostController@perform')->name('backend.posts.perform');

            Route::get('export', 'PostController@exportAll')->name('backend.posts.export');
            Route::get('export-comments', 'PostController@exportComments')->name('backend.posts.export.comments');
            Route::get('export-questions', 'PostController@exportQuestions')->name('backend.posts.export.questions');
            Route::get('export-images', 'PostController@exportImages')->name('backend.posts.export.images');
        });

        // PUSH PAGES
        Route::group(['prefix' => 'pushes'], function(){
            Route::get('/', 'PushesController@index')->name('backend.pushes.index');
            Route::get('create','PushesController@create')->name('backend.pushes.create');
            Route::post('create','PushesController@store')->name('backend.pushes.store');
            Route::get('edit/{id}','PushesController@edit')->name('backend.pushes.edit');
            Route::patch('edit/{id}','PushesController@update')->name('backend.pushes.update');
            Route::post('remove/{id}', 'PushesController@remove')->name('backend.pushes.remove');

            Route::get('export', 'PushesController@export')->name('backend.pushes.export');
            Route::get('export-images', 'PushesController@exportImages')->name('backend.pushes.export.images');
            Route::post('import', 'PushesController@import')->name('backend.pushes.import');
        });

        // QUIZ PAGES
        Route::group(['prefix' => 'quizzes'], function(){
            Route::get('/', 'QuizzesController@index')->name('backend.quizzes.index');
            Route::get('create','QuizzesController@create')->name('backend.quizzes.create');
            Route::post('create','QuizzesController@store')->name('backend.quizzes.store');
            Route::get('edit/{id}','QuizzesController@edit')->name('backend.quizzes.edit');
            Route::patch('edit/{id}','QuizzesController@update')->name('backend.quizzes.update');
            Route::post('remove/{id}', 'QuizzesController@remove')->name('backend.quizzes.remove');

            Route::get('export', 'QuizzesController@export')->name('backend.quizzes.export');
            Route::get('export-images', 'PushesController@exportImages')->name('backend.quizzes.export.images');
            Route::post('import', 'QuizzesController@import')->name('backend.quizzes.import');
            Route::get('report', 'QuizzesController@report')->name('backend.quizzes.report');
            Route::get('report/{id}', 'QuizzesController@reportSingle')->name('backend.quizzes.report.single');

            Route::post('reset', 'QuizzesController@reset')->name('backend.quizzes.reset');
            Route::post('remove-all', 'QuizzesController@removeAll')->name('backend.quizzes.remove.all');
            Route::post('reset/{id}', 'QuizzesController@resetSingle')->name('backend.quizzes.reset.single');
            Route::post('perform', 'QuizzesController@perform')->name('backend.quizzes.perform');

            // Route::get('/{id}', 'QuizzesController@show')->name('backend.quizzes.show');

            Route::get('/{quiz_id}/create-question', 'QuizQuestionsController@create')->name('backend.quizzes.questions.create');
            Route::post('/{quiz_id}/create-question', 'QuizQuestionsController@store')->name('backend.quizzes.questions.store');
            Route::get('/edit-question/{id}','QuizQuestionsController@edit')->name('backend.quizzes.questions.edit');
            Route::patch('/edit-question/{id}','QuizQuestionsController@update')->name('backend.quizzes.questions.update');
            Route::post('/remove-question/{id}', 'QuizQuestionsController@remove')->name('backend.quizzes.questions.remove');
            Route::post('/reset-question/{id}', 'QuizQuestionsController@reset')->name('backend.quizzes.questions.reset');
        });

        // SETTING PAGES
        Route::group(['prefix' => 'settings'], function(){
            Route::get('/', 'SettingsController@index')->name('backend.settings');
            Route::post('/', 'SettingsController@update')->name('backend.settings.update');

            Route::get('/export', 'SettingsController@export')->name('backend.settings.export');
            Route::post('/import', 'SettingsController@import')->name('backend.settings.import');
        });

        // SMS PAGES
        Route::group(['prefix' => 'sms'],function(){
            Route::get('/', 'SmsController@index')->name('backend.sms.index');
            Route::get('setup/{id}', 'SmsController@setup')->name('backend.sms.setup');
            Route::post('setup/{id}', 'SmsController@preview')->name('backend.sms.setup-preview');
            Route::post('send/{id}', 'SmsController@ajaxSend')->name('backend.sms.ajax-send');
            Route::get('log/{id}', 'SmsController@log')->name('backend.sms.log');

            Route::get('create','SmsController@create')->name('backend.sms.create');
            Route::post('create','SmsController@store')->name('backend.sms.store');
            Route::get('edit/{id}','SmsController@edit')->name('backend.sms.edit');
            Route::patch('edit/{id}','SmsController@update')->name('backend.sms.update');
            Route::delete('remove/{id}', 'SmsController@remove')->name('backend.sms.remove');

            Route::get('export', 'SmsController@export')->name('backend.sms.export');
            Route::post('import', 'SmsController@import')->name('backend.sms.import');
            Route::get('report', 'SmsController@report')->name('backend.sms.report');
            Route::get('report/{id}', 'SmsController@reportSingle')->name('backend.sms.report.single');

            Route::post('batch-status', 'SmsController@ajaxBatchStatus')->name('backend.sms.ajax-batch-status');
        });

        // SURVEY PAGES
        Route::group(['prefix' => 'surveys'], function(){
            Route::get('/', 'SurveysController@index')->name('backend.surveys.index');
            Route::get('create','SurveysController@create')->name('backend.surveys.create');
            Route::post('create','SurveysController@store')->name('backend.surveys.store');
            Route::get('edit/{id}','SurveysController@edit')->name('backend.surveys.edit');
            Route::patch('edit/{id}','SurveysController@update')->name('backend.surveys.update');
            Route::post('remove/{id}', 'SurveysController@remove')->name('backend.surveys.remove');

            Route::get('export', 'SurveysController@export')->name('backend.surveys.export');
            Route::post('import', 'SurveysController@import')->name('backend.surveys.import');
            Route::get('report', 'SurveysController@report')->name('backend.surveys.report');
            Route::get('report/{id}', 'SurveysController@reportSingle')->name('backend.surveys.report.single');

            Route::post('reset', 'SurveysController@reset')->name('backend.surveys.reset');
            Route::post('reset/{id}', 'SurveysController@resetSingle')->name('backend.surveys.reset.single');
            Route::post('perform', 'SurveysController@perform')->name('backend.surveys.perform');

            // Route::get('/{id}', 'SurveysController@show')->name('backend.surveys.show');

            Route::get('/{survey_id}/create-question', 'SurveyQuestionsController@create')->name('backend.surveys.questions.create');
            Route::post('/{survey_id}/create-question', 'SurveyQuestionsController@store')->name('backend.surveys.questions.store');
            Route::get('/edit-question/{id}','SurveyQuestionsController@edit')->name('backend.surveys.questions.edit');
            Route::patch('/edit-question/{id}','SurveyQuestionsController@update')->name('backend.surveys.questions.update');
            Route::post('/remove-question/{id}', 'SurveyQuestionsController@remove')->name('backend.surveys.questions.remove');
            Route::post('/reset-question/{id}', 'SurveyQuestionsController@reset')->name('backend.surveys.questions.reset');
        });

        // TIMER PAGES
        Route::get('timer', 'TimerController@index')->name('backend.timer.index');
        Route::post('timer/submit', 'TimerController@submit')->name('backend.timer.submit');
        Route::post('timer/stop', 'TimerController@stop')->name('backend.timer.stop');

        // USER PAGES
        Route::group(['prefix' => 'users'], function(){
            Route::get('/', 'UsersController@index')->name('backend.users.index');
            Route::get('create', 'UsersController@create')->name('backend.users.create');
            Route::post('create', 'UsersController@store')->name('backend.users.store');
            Route::get('edit/{id}', 'UsersController@edit')->name('backend.users.edit');
            Route::patch('edit/{id}', 'UsersController@update')->name('backend.users.update');
            Route::post('remove/{id}', 'UsersController@remove')->name('backend.users.remove');

            Route::post('search', 'UsersController@search')->name('backend.users.search');
            Route::post('perform', 'UsersController@perform')->name('backend.users.perform');

            Route::get('export', 'UsersController@exportAll')->name('backend.users.export');
            Route::get('export-images', 'UsersController@exportTest')->name('backend.users.export.test');
            Route::post('import', 'UsersController@import')->name('backend.users.import');
        });

    });

});
